import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';

import _ from 'lodash';
import Localize from 'components/Localize';

const styles = () => ({
    root: {
        flexGrow: 1,
        width: '100%',
        height: '100%',
        padding: 0,
        margin: 0,
    },
    paper: {
        width: '30px',
        minWidth: 0,
        height: '100%',
        padding: 8,
    },
    button: {
        margin: 0,
    },
    hide: {
        visibility: 'hidden',
    },
    selected: {
        backgroundColor: 'lightGrey',
    },
    pages: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: '1',
        maxWidth: '100%',
        padding: '8px',
        justifyContent: 'center',
    },
});

export class Pagination extends React.Component {
    handleFirst = () => {
        if (this.props.onFirst) this.props.onFirst(1);
        else this.props.onPage(1);
    };

    handleLast = () => {
        if (this.props.onLast) this.props.onLast(this.props.maxPages);
        else this.props.onPage(this.props.maxPages);
    };

    handleNext = () => {
        if (this.props.onNext) this.props.onNext(this.props.currentPage + 1);
        else this.props.onPage(this.props.currentPage + 1);
    };

    handleBack = () => {
        if (this.props.onBack) this.props.onBack(this.props.currentPage - 1);
        else this.props.onPage(this.props.currentPage - 1);
    };

    handlePagination = page => () => this.props.onPage(page);

    createPagerButton(element, index) {
        const { classes } = this.props;
        return (
            <Grid key={index} item>
                <Button
                    onClick={this.handlePagination(element)}
                    id={`Selenium-Pagination-Button-${index}`}
                    className={classNames(
                        classes.paper,
                        parseInt(element, 10) === this.props.currentPage ? classes.selected : null
                    )}
                >
                    {element}
                </Button>
            </Grid>
        );
    }

    createPagination() {
        const currentPage = this.props.currentPage;
        const maxPages = this.props.maxPages;
        const maxPagesLimit = this.props.maxPagesLimit || 9;

        // const fromPage = Math.floor((currentPage - 1) / maxPagesLimit) * maxPagesLimit + 1;
        let fromPage = Math.max(1, currentPage - Math.floor(maxPagesLimit / 2));
        const lastPage = Math.min(fromPage + maxPagesLimit, maxPages + 1);
        if (lastPage - fromPage < maxPagesLimit) {
            fromPage = Math.max(1, lastPage - maxPagesLimit);
        }
        // fromPage = fromPage > 0 ? fromPage : 1;
        const pages = _.range(fromPage, lastPage);
        return _.map(pages, (element, index) => this.createPagerButton(element, index));
    }

    render() {
        const { classes } = this.props;
        return (
            <Grid container className={classes.root} direction="row" alignItems="center" justify="space-between">
                <Grid item className={this.props.currentPage === 1 ? classes.hide : ''}>
                    <Button
                        onClick={this.handleFirst}
                        raised
                        className={classes.button}
                        id={'Selenium-Pagination-First-Button'}
                    >
                        {Localize.locale['Pagination-First'] || '<< First'}
                    </Button>
                </Grid>
                <Grid item className={this.props.currentPage === 1 ? classes.hide : ''}>
                    <Button
                        onClick={this.handleBack}
                        raised
                        className={classes.button}
                        id={'Selenium-Pagination-Back-Button'}
                    >
                        {Localize.locale['Pagination-Back']}
                    </Button>
                </Grid>
                <Grid item className={classes.pages}>
                    {this.createPagination()}
                </Grid>
                <Grid item className={this.props.currentPage >= this.props.maxPages ? classes.hide : ''}>
                    <Button
                        onClick={this.handleNext}
                        raised
                        className={classes.button}
                        id={'Selenium-Pagination-Next-Button'}
                    >
                        {Localize.locale['Pagination-Next']}
                    </Button>
                </Grid>
                <Grid item className={this.props.currentPage >= this.props.maxPages ? classes.hide : ''}>
                    <Button
                        onClick={this.handleLast}
                        raised
                        className={classes.button}
                        id={'Selenium-Pagination-Last-Button'}
                    >
                        {Localize.locale['Pagination-Last'] || 'Last >>'}
                    </Button>
                </Grid>
            </Grid>
        );
    }
}

Pagination.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    onPage: PropTypes.func.isRequired,
    onNext: PropTypes.func,
    onBack: PropTypes.func,
    onFirst: PropTypes.func,
    onLast: PropTypes.func,
    currentPage: PropTypes.number.isRequired,
    maxPages: PropTypes.number.isRequired,
    maxPagesLimit: PropTypes.number,
};

Pagination.defaultProps = {};

export default withStyles(styles, 'Pagination')(Pagination);
