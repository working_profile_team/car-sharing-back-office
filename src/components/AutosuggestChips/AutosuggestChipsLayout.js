import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import ChipsArray from 'containers/ChipsArray/ChipsArray';
import Typography from 'material-ui/Typography';

import AutosuggestLayout from 'components/Autosuggests/AutosuggestLayout';

const styles = theme => ({
    root: {
        margin: 0,
    },
    item: {
        maxWidth: '350px',
        minWidth: '150px',
        // width: "350px",
        flex: 'unset',
    },
    hide: {
        display: 'none',
    },
    messageContainer: {
        marginBottom: '8px',
        textAlign: 'center',
    },
    emptyMessage: {
        fontStyle: 'italic',
    },
    container: {
        alignItems: 'baseline',
    },
});

class AutosuggestChipsLayout extends React.Component {
    constructor(props) {
        super(props);
        this.renderAutosuggest = this._renderAutosuggest.bind(this);
    }

    _renderAutosuggest = autosuggestProps => {
        const {
            label,
            placeholder,
            allSuggestions,
            currentSuggestions,
            onChangeAutosuggest,
            onSuggestionSelected,
            inputValue,
            defaultValue,
            currentChips,
            inputRef,
            ...others
        } = autosuggestProps;

        return (
            <AutosuggestLayout
                label={label}
                placeholder={placeholder}
                allSuggestions={allSuggestions}
                currentSuggestions={currentSuggestions}
                onChangeAutosuggest={onChangeAutosuggest}
                onSuggestionSelected={onSuggestionSelected}
                inputValue={inputValue}
                defaultValue={defaultValue}
                currentChips={currentChips}
                inputRef={inputRef}
                {...others}
            />
        );
    };

    render() {
        const classes = this.props.classes;

        return (
            <Grid container direction="row" justify="flex-start" className={classes.container}>
                {this.props.data.length === 0 ? (
                    <Grid item className={[classes.item, classes.messageContainer].join(' ')}>
                        <Typography type="body2" color="secondary" gutterBottom>
                            <span className={classes.emptyMessage}>{this.props.emptyMessage}</span>
                        </Typography>
                    </Grid>
                ) : (
                    <Grid item className={classes.item}>
                        <ChipsArray
                            onRequestDelete={this.props.onRequestDelete}
                            canDelete={this.props.canDelete}
                            data={this.props.data}
                        />
                    </Grid>
                )}
                <Grid item className={!this.props.canAdd ? classes.hide : ''}>
                    {this.renderAutosuggest(this.props.autosuggestProps)}
                </Grid>
            </Grid>
        );
    }
}

AutosuggestChipsLayout.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles, 'AutosuggestChipsLayout')(AutosuggestChipsLayout);
