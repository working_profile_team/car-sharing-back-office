import LocalizedStrings from 'react-localization';
import {loadState, saveState} from "helpers/localStorage";
import Localization from "constants/Localization";

/*
    Simple singleton to support localize content
*/
class Localize {

    constructor() {
        const language = loadState('language');
        this.language = language ? language : "en";
        this.locale = new LocalizedStrings(Localization);
        this.setLanguage(this.language);
    }

    setLanguage(lg) {
        this.language = lg;
        saveState("language", this.language);
        this.locale.setLanguage(this.language);
    }

    getCurrent() {
        return this.locale.getLanguage();
    }

    getLanguages() {
        return this.locale.getAvailableLanguages();
    }

}

const instance = new Localize();

export default instance;
