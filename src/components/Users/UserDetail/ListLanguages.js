import React from 'react';
import PropTypes from 'prop-types';
import { onlyUpdateForPropTypes } from 'recompose';

import FunctionHelpers from 'helpers/FunctionHelpers';
import Localize from 'components/Localize';
import { Input, Select } from 'material-ui';
import Languages from 'constants/Languages';

import _ from 'lodash';

const ListLanguages = ({ locale, onChangeUser, flowState, foLangages }) => (
    <Select
        native
        classes={{
            select: 'Selenium-Select',
            selectMenu: 'Selenium-Select-Menu',
        }}
        value={FunctionHelpers.null2String(locale)}
        onChange={event => {
            onChangeUser({ locale: event.target.value });
        }}
        input={<Input id="Selenium-FormUserIdentity-Input-Language" style={{ width: '200px' }} />}
        disabled={flowState === 'READ_ONLY'}
    >
        {FunctionHelpers.formatSelectOptions(
            _.map(Languages(Localize.locale.global.languages, foLangages), l => (
                <option id={`Selenium-FormUserIdentity-MenuItem${l.code}`} value={l.code} key={l.code}>
                    {l.name}
                </option>
            )),
            'html'
        )}
    </Select>
);

ListLanguages.propTypes = {
    locale: PropTypes.string,
    flowState: PropTypes.string.isRequired,
    foLangages: PropTypes.arrayOf(PropTypes.string).isRequired,
    onChangeUser: PropTypes.func.isRequired,
};

export default onlyUpdateForPropTypes(ListLanguages);
