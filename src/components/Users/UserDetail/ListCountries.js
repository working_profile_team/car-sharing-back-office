import React from 'react';
import PropTypes from 'prop-types';
import { onlyUpdateForPropTypes } from 'recompose';

import FunctionHelpers from 'helpers/FunctionHelpers';
import { Input, Select } from 'material-ui';
import Countries from 'constants/Countries';

import _ from 'lodash';

const ListCountries = ({ nationality, onChangeUser, flowState }) => (
    <Select
        native
        classes={{
            select: 'Selenium-Select',
            selectMenu: 'Selenium-Select-Menu',
        }}
        id="Selenium-FormUserIdentity-Select-Nationality"
        value={FunctionHelpers.null2String(nationality)}
        onChange={event => {
            onChangeUser({ nationality: event.target.value });
        }}
        input={<Input id="nationality" style={{ width: '200px' }} />}
        disabled={flowState === 'READ_ONLY'}
    >
        {FunctionHelpers.formatSelectOptions(
            _.map(Countries, c => (
                <option id={`Selenium-FormUserIdentity-MenuItem${c.code}`} value={c.name} key={c.code ? c.code : null}>
                    {c.name}
                </option>
            )),
            'html'
        )}
    </Select>
);

ListCountries.propTypes = {
    nationality: PropTypes.string,
    flowState: PropTypes.string.isRequired,
    onChangeUser: PropTypes.func.isRequired,
};

export default onlyUpdateForPropTypes(ListCountries);
