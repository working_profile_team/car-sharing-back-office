import React from 'react';
import PropTypes from 'prop-types';
import { onlyUpdateForPropTypes } from 'recompose';
import FunctionHelpers from 'helpers/FunctionHelpers';
import { Input, Select } from 'material-ui';
import { USER_ROLES } from 'constants/Users';
import _ from 'lodash';

const ListRoles = ({ lastRole, onChangeUser, flowState }) => (
    <Select
        native
        classes={{
            select: 'Selenium-Select',
            selectMenu: 'Selenium-Select-Menu',
        }}
        value={FunctionHelpers.null2String(lastRole)}
        onChange={event => {
            onChangeUser({ role: event.target.value, oldRole: lastRole });
        }}
        input={<Input id="Selenium-FormUserIdentity-Input-role" style={{ width: '200px' }} />}
        disabled={flowState === 'READ_ONLY'}
    >
        {FunctionHelpers.formatSelectOptions(
            _.map(USER_ROLES, l => (
                <option id={`Selenium-FormUserIdentity-Role${l.id}`} value={l.name} key={l.id}>
                    {l.name}
                </option>
            )),
            'html'
        )}
    </Select>
);

ListRoles.propTypes = {
    lastRole: PropTypes.string,
    flowState: PropTypes.string.isRequired,
    onChangeUser: PropTypes.func.isRequired,
};

export default onlyUpdateForPropTypes(ListRoles);
