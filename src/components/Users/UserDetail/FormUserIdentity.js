import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles } from 'material-ui/styles';
import moment from 'moment';

import {
    Grid,
    Paper,
    Typography,
    TextField,
    Radio,
    RadioGroup,
    FormControl,
    FormControlLabel,
    FormGroup,
    FormLabel,
    InputLabel,
} from 'material-ui';

import FunctionHelpers from 'helpers/FunctionHelpers';
import ValidationHelpers from 'helpers/ValidationHelpers';
import Localize from 'components/Localize';
import ListLanguages from 'components/Users/UserDetail/ListLanguages';
import ListCountries from 'components/Users/UserDetail/ListCountries';
import ListStatuses from 'components/Users/UserDetail/ListStatuses';
import PasswordField from 'components/FormFields/PasswordField';
import ListRoles from 'components/Users/UserDetail/ListRoles';
import { getLastRoleUser } from 'helpers/DataHelpers/users';

const styles = theme => ({
    root: {
        flex: 1,
        float: 'left',
        width: '100%',
        height: '100%',
        display: 'inline-block',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
    }),
    input: {
        width: '100%',
    },
    menu: {
        postion: 'bottom',
    },
    menuList: {
        postion: 'bottom',
    },
    selection: {
        display: 'inline-block',
        marginRight: '15px',
    },
    sorting: {
        display: 'inline-block',
    },
    container: {
        padding: 0,
        width: '100%',
        margin: '10px',
        display: 'inline-block',
    },
    title: {
        marginBottom: '15px',
    },
    item: {
        marginRight: '5em',
        display: 'inline-block',
        width: '13.5em',
        position: 'relative',
        padding: '8px',
    },
    form: {
        display: 'inline',
    },
    gender: {
        display: 'inline-block',
        width: '275px',
    },
    formGroup: {
        padding: '8px',
        paddingBottom: '0',
        paddingTop: '0',
    },
    formLabel: {
        transform: 'translate(0, 1.5px) scale(0.75)',
        transformOrigin: 'top left',
    },
    coloredRadio: {
        color: theme.palette.primary[500],
    },
    transparentRadio: {
        color: 'rgba(0, 0, 0, 0.54)',
    },
});

const FormUserIdentity = ({ classes, selectedUser, flowState, foLangages, onChangeUser }) => {
    const maxDate = moment()
        .utc()
        .format('YYYY-MM-DD');

    return (
        <Grid item className={classnames(classes.container, 'Selenium-User-Form-Identity')}>
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={4}>
                    <Typography type="headline" component="h3">
                        Edit {selectedUser.userName}&apos;s info
                    </Typography>
                    <Grid container direction="column" justify="flex-start" className={classes.form}>
                        <FormGroup
                            row
                            className={classes.formGroup}
                            style={flowState !== 'CREATE' ? { display: 'none' } : {}}
                        >
                            <Grid item className={classes.item}>
                                <TextField
                                    autoComplete="off"
                                    id="Selenium-FormUserIdentity-TextField-UserName"
                                    label={Localize.locale['Forms-FormUsersIdentity-UserName']}
                                    value={FunctionHelpers.null2String(selectedUser.userName)}
                                    className={classnames(classes.input, 'Selenium-TextField')}
                                    required
                                    error={ValidationHelpers.testUsername(selectedUser.userName) ? undefined : true}
                                    disabled={flowState !== 'CREATE'}
                                    onChange={event => {
                                        onChangeUser({ userName: event.target.value });
                                    }}
                                    helperText={
                                        ValidationHelpers.testUsername(selectedUser.userName) || !selectedUser.userName
                                            ? ' '
                                            : Localize.locale['FormUserIdentity-StringField-helperText']
                                    }
                                />
                            </Grid>
                        </FormGroup>
                        <FormGroup
                            row
                            className={classes.formGroup}
                            style={flowState !== 'CREATE' ? { display: 'none' } : {}}
                        >
                            <Grid item className={classes.item}>
                                <PasswordField
                                    autoComplete="off"
                                    id="Selenium-FormUserIdentity-TextField-Password"
                                    hintText="At least 4 alphanumeric characters"
                                    label="Password"
                                    value={FunctionHelpers.null2String(selectedUser.password)}
                                    className={classnames(classes.input, 'Selenium-TextField')}
                                    disabled={flowState === 'READ_ONLY'}
                                    onChange={event => {
                                        onChangeUser({ password: event.target.value });
                                    }}
                                    isError={!ValidationHelpers.testStringField(selectedUser.password)}
                                    showHelperText={
                                        !ValidationHelpers.testStringField(selectedUser.password) &&
                                        selectedUser.password
                                    }
                                />
                            </Grid>
                        </FormGroup>
                        <FormGroup
                            row
                            className={classes.formGroup}
                            style={flowState !== 'CREATE' ? { display: 'none' } : {}}
                        >
                            <Grid item className={classes.item}>
                                <TextField
                                    id="Selenium-FormUserIdentity-TextField-Email"
                                    label={Localize.locale['Forms-FormUsersIdentity-Email']}
                                    value={FunctionHelpers.null2String(selectedUser.email)}
                                    className={classnames(classes.input, 'Selenium-TextField')}
                                    required
                                    error={ValidationHelpers.testEmailField(selectedUser.email) ? undefined : true}
                                    helperText={
                                        ValidationHelpers.testEmailField(selectedUser.email) || !selectedUser.email
                                            ? ' '
                                            : Localize.locale['Forms-FormUsersIdentity-helperText-email']
                                    }
                                    disabled={flowState !== 'CREATE'}
                                    onChange={event => {
                                        onChangeUser({ email: event.target.value });
                                    }}
                                />
                            </Grid>
                        </FormGroup>
                        <FormGroup row className={classes.formGroup}>
                            <Grid item className={classes.item}>
                                <TextField
                                    id="Selenium-FormUserIdentity-TextField-LastName"
                                    label={Localize.locale['Forms-FormUsersIdentity-LastName']}
                                    value={FunctionHelpers.null2String(selectedUser.lastName)}
                                    className={classnames(classes.input, 'Selenium-TextField')}
                                    disabled={flowState === 'READ_ONLY'}
                                    required
                                    error={
                                        ValidationHelpers.testUserNameField(selectedUser.lastName) ? undefined : true
                                    }
                                    helperText={
                                        ValidationHelpers.testUserNameField(selectedUser.lastName) ||
                                        !selectedUser.lastName
                                            ? ' '
                                            : Localize.locale['FormUserIdentity-PersonsNameField-helperText']
                                    }
                                    onChange={event => {
                                        onChangeUser({ lastName: event.target.value });
                                    }}
                                />
                            </Grid>
                            <Grid item className={classes.item}>
                                <TextField
                                    id="Selenium-FormUserIdentity-TextField-FirstName"
                                    label={Localize.locale['Forms-FormUsersIdentity-FirstName']}
                                    value={FunctionHelpers.null2String(selectedUser.firstName)}
                                    className={classnames(classes.input, 'Selenium-TextField')}
                                    required
                                    error={
                                        ValidationHelpers.testUserNameField(selectedUser.firstName) ? undefined : true
                                    }
                                    helperText={
                                        ValidationHelpers.testUserNameField(selectedUser.firstName) ||
                                        !selectedUser.firstName
                                            ? ' '
                                            : Localize.locale['FormUserIdentity-PersonsNameField-helperText']
                                    }
                                    disabled={flowState === 'READ_ONLY'}
                                    onChange={event => {
                                        onChangeUser({ firstName: event.target.value });
                                    }}
                                />
                            </Grid>
                            <Grid item className={classes.item}>
                                <FormControl className={classes.formControl}>
                                    <FormLabel component="label" className={classes.formLabel}>
                                        {Localize.locale.users.infos.status}
                                    </FormLabel>
                                    <ListStatuses
                                        accountStatus={selectedUser.accountStatus}
                                        onChangeUser={onChangeUser}
                                        flowState={flowState}
                                    />
                                </FormControl>
                            </Grid>
                        </FormGroup>
                        <FormGroup row className={classes.formGroup}>
                            <Grid item className={classes.item}>
                                <TextField
                                    id="Selenium-FormUserIdentity-TextField-MiddleName"
                                    label={Localize.locale.users.infos.middleName}
                                    value={FunctionHelpers.null2String(selectedUser.middleName)}
                                    className={classnames(classes.input, 'Selenium-TextField')}
                                    disabled={flowState === 'READ_ONLY'}
                                    onChange={event => {
                                        onChangeUser({ middleName: event.target.value });
                                    }}
                                />
                            </Grid>
                            <Grid item className={classes.item}>
                                <TextField
                                    id="Selenium-FormUserIdentity-TextField-PreferredName"
                                    label={Localize.locale.users.infos.preferredName}
                                    value={FunctionHelpers.null2String(selectedUser.preferredName)}
                                    className={classnames(classes.input, 'Selenium-TextField')}
                                    disabled={flowState === 'READ_ONLY'}
                                    onChange={event => {
                                        onChangeUser({ preferredName: event.target.value });
                                    }}
                                />
                            </Grid>
                            <Grid item className={classes.item}>
                                <FormControl className={classes.formControl}>
                                    <FormLabel component="label" className={classes.formLabel}>
                                        Roles
                                    </FormLabel>
                                    <ListRoles
                                        flowState={flowState}
                                        onChangeUser={onChangeUser}
                                        lastRole={selectedUser.role || getLastRoleUser(selectedUser.roles)}
                                    />
                                </FormControl>
                            </Grid>
                        </FormGroup>
                        <FormGroup row className={classes.formGroup}>
                            <Grid item className={classes.item}>
                                <TextField
                                    id="Selenium-FormUserIdentity-TextField-BirthDate"
                                    type="date"
                                    label={Localize.locale.users.infos.birthday}
                                    disabled={flowState === 'READ_ONLY'}
                                    defaultValue={FunctionHelpers.null2Date(
                                        selectedUser.birthDate,
                                        undefined,
                                        'YYYY-MM-DD',
                                        'YYYY-MM-DD'
                                    )}
                                    className={classnames(classes.input, 'Selenium-TextField')}
                                    fullWidth
                                    onChange={event => {
                                        onChangeUser({
                                            birthDate: FunctionHelpers.null2Date(
                                                event.target.value,
                                                undefined,
                                                'YYYY-MM-DD',
                                                'YYYY-MM-DDTHH:mm:ss.sss[Z]'
                                            ),
                                        });
                                    }}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    inputProps={{
                                        max: maxDate,
                                    }}
                                />
                            </Grid>
                            <FormControl className={classes.formControl}>
                                <Grid item className={classes.item}>
                                    <div style={{ position: 'relative' }}>
                                        <InputLabel htmlFor="language">
                                            {Localize.locale.users.infos.language}
                                        </InputLabel>
                                        <ListLanguages
                                            locale={selectedUser.locale}
                                            onChangeUser={onChangeUser}
                                            flowState={flowState}
                                            foLangages={foLangages}
                                        />
                                    </div>
                                </Grid>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <Grid item className={classes.item}>
                                    <div style={{ position: 'relative' }}>
                                        <InputLabel htmlFor="nationality">
                                            {Localize.locale.users.infos.nationality}
                                        </InputLabel>
                                        <ListCountries
                                            nationality={selectedUser.nationality}
                                            onChangeUser={onChangeUser}
                                            flowState={flowState}
                                        />
                                    </div>
                                </Grid>
                            </FormControl>
                        </FormGroup>
                        <FormGroup row className={classes.formGroup}>
                            <Grid item className={classes.item}>
                                <TextField
                                    id="Selenium-FormUserIdentity-TextField-MembershipNumber"
                                    label={Localize.locale.users.infos.membershipNumber}
                                    value={FunctionHelpers.null2String(selectedUser.membershipNumber)}
                                    className={classnames(classes.input, 'Selenium-TextField-Username')}
                                    disabled={flowState === 'READ_ONLY'}
                                    onChange={event => {
                                        onChangeUser({ membershipNumber: event.target.value });
                                    }}
                                />
                            </Grid>
                            <Grid item>
                                <FormControl component="fieldset">
                                    <FormLabel component="label" className={classes.formLabel}>
                                        {Localize.locale.users.infos.gender}
                                    </FormLabel>
                                    <RadioGroup
                                        aria-label="gender"
                                        name="gender"
                                        className={classes.gender}
                                        value={selectedUser.gender}
                                        onChange={event => {
                                            onChangeUser({ gender: event.target.value });
                                        }}
                                        id="Selenium-FormUserIdentity-RadioGroup-Gender"
                                    >
                                        <FormControlLabel
                                            value="MALE"
                                            control={
                                                <Radio
                                                    id="Selenium-FormUserIdentity-Radio-Gender-Male"
                                                    className={
                                                        selectedUser.gender === 'MALE'
                                                            ? classes.coloredRadio
                                                            : classes.transparentRadio
                                                    }
                                                />
                                            }
                                            label={Localize.locale.users.infos.genderType.male}
                                            disabled={flowState === 'READ_ONLY'}
                                            style={{ height: '32px' }}
                                        />
                                        <FormControlLabel
                                            value="FEMALE"
                                            control={
                                                <Radio
                                                    id="Selenium-FormUserIdentity-Radio-Gender-Female"
                                                    className={
                                                        selectedUser.gender === 'FEMALE'
                                                            ? classes.coloredRadio
                                                            : classes.transparentRadio
                                                    }
                                                />
                                            }
                                            label={Localize.locale.users.infos.genderType.female}
                                            disabled={flowState === 'READ_ONLY'}
                                            style={{ height: '32px', color: 'yellow' }}
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                        </FormGroup>
                    </Grid>
                </Paper>
            </div>
        </Grid>
    );
};

FormUserIdentity.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    selectedUser: PropTypes.shape({}).isRequired,
    flowState: PropTypes.string.isRequired,
    foLangages: PropTypes.arrayOf(PropTypes.string).isRequired,
    onChangeUser: PropTypes.func.isRequired,
};

export default withStyles(styles, 'FormUserIdentity')(FormUserIdentity);
