import React from 'react';
import PropTypes from 'prop-types';
import { onlyUpdateForPropTypes } from 'recompose';

import FunctionHelpers from 'helpers/FunctionHelpers';
import { USER_STATUS } from 'constants/Users';
import { Input, Select } from 'material-ui';

import _ from 'lodash';

const ListStatuses = ({ accountStatus, onChangeUser, flowState }) => (
    <Select
        native
        classes={{ select: 'Selenium-Select', selectMenu: 'Selenium-Select-Menu' }}
        value={FunctionHelpers.null2String(accountStatus)}
        onChange={event => {
            onChangeUser({ accountStatus: event.target.value });
        }}
        input={<Input id="Selenium-FormUserIdentity-Dropdown-Status-Trigger" style={{ width: '200px' }} />}
        disabled={flowState === 'READ_ONLY'}
    >
        {FunctionHelpers.formatSelectOptions(
            _.map(USER_STATUS, s => (
                <option id={`Selenium-FormUserIdentity-MenuItem-Status${s.id}`} value={s.name} key={s.id}>
                    {s.name}
                </option>
            )),
            'html'
        )}
    </Select>
);

ListStatuses.propTypes = {
    accountStatus: PropTypes.string,
    flowState: PropTypes.string.isRequired,
    onChangeUser: PropTypes.func.isRequired,
};

export default onlyUpdateForPropTypes(ListStatuses);
