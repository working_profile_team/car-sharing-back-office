import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { Drawer, Typography, Divider, Button } from 'material-ui';
import Localize from 'components/Localize';
import { pure } from 'recompose';

const styles = {
    root: {
        width: '385px',
        height: 'calc(100% - 64px)',
        zIndex: 1000,
        top: '64px',
        right: '0',
        borderTopLeftRadius: '10px',
        borderBottomLeftRadius: '10px',
        padding: 15,
    },
    backdropModal: {
        opacity: '0.3 !important',
    },
    input: {
        marginTop: 16,
    },
};

const AdvancedFilters = ({ classes, isOpen, onCloseFilters, onClearFilters, children }) => (
    <Drawer
        anchor="right"
        open={isOpen}
        type="temporary"
        classes={{ paper: classes.root }}
        onClose={onCloseFilters}
        ModalProps={{
            BackdropProps: { classes: { root: classes.backdropModal } },
        }}
    >
        <Typography type="headline" component="h4">
            {Localize.locale.trips.advancedFilters}
        </Typography>
        <Divider />
        {children}
        <Button raised color={'primary'} className={classes.input} onClick={onClearFilters}>
            {Localize.locale.trips.clearFilters}
        </Button>
    </Drawer>
);

AdvancedFilters.propTypes = {
    classes: PropTypes.shape().isRequired,
    isOpen: PropTypes.bool.isRequired,
    onCloseFilters: PropTypes.func.isRequired,
    onClearFilters: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
};

export default withStyles(styles, 'AdvancedFilters')(pure(AdvancedFilters));
