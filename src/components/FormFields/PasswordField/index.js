import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {withStyles} from 'material-ui/styles';

import TextField from 'material-ui/TextField'
import IconButton from 'material-ui/IconButton'
import Visibility from 'material-ui-icons/Visibility'
import VisibilityOff from 'material-ui-icons/VisibilityOff'
import ToggleIcon from 'components/ToggleIcon'

const styles = (theme) => ({
    root: {
        position: 'relative',
        display: 'inline-block'
    },
    input: {
        paddingRight: 59,
        boxSizing: 'border-box'
    },
    hint: {
        position: 'relative',
        bottom: 2,
        fontSize: 12,
        lineHeight: '12px',
        transition: theme.transitions.easing.easeOut
    },
    error: {
        color: theme.palette.error.A400,
    },
    visibilityButton: {
        marginLeft: 8,
        width: 48,
        height: 48,
        padding: 12,
        position: 'absolute',
        top: 0,
        right: 0
    },
    button: {
        right: 0,
        position: "absolute",
    }
});

class PasswordField extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            focused: false,
            visible: props.visible
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.visible !== this.props.visible) {
            this.setState({
                visible: nextProps.visible
            })
        }
    }

    /**
     * Toogles the visibility the password.
     * @public
     */
    toggleVisibility() {
        this.setState({
            visible: !this.state.visible
        })
    }

    handleInputFocus(event) {
        this.setState({focused: true})
        if (this.props.onFocus) {
            this.props.onFocus(event)
        }
    }

    handleInputBlur(event) {
        this.setState({focused: false})
        if (this.props.onBlur) {
            this.props.onBlur(event)
        }
    }

    render() {
        const {
            classes,
            disableButton,
            errorText,
            hintText,
            visible: visibleProp, // eslint-disable-line
            showHelperText,
            ...other
        } = this.props;

        //Remove unknown props for TextField
        if (other.hasOwnProperty('isError')) {
            delete other.isError;
        }

        const {
            visible
        } = this.state;

        const hintOrErrorText = this.props.error ? errorText : hintText;

        return (
            <div className={classes.root}>
                <TextField
                    {...other}
                    required
                    error={this.props.isError}
                    helperText={this.props.showHelperText ? hintOrErrorText : '\n\t'}
                    helperTextClassName={classes.hint}
                    className={classnames(classes.input, 'Selenium-PasswordField')}
                    type={visible ? 'text' : 'password'}
                    onFocus={(event) => this.handleInputFocus(event)}
                    onBlur={(event) => this.handleInputBlur(event)}
                    style={{whiteSpace: 'pre-wrap'}}
                />
                <IconButton
                    onTouchTap={() => this.toggleVisibility()}
                    onMouseDown={(e) => e.preventDefault()}
                    disabled={disableButton}
                    className={classes.button}
                >
                    <ToggleIcon
                        on={visible}
                        onIcon={<Visibility/>}
                        offIcon={<VisibilityOff/>}
                    />
                </IconButton>
            </div>
        )
    }
}

PasswordField.propTypes = {
    classes: PropTypes.object.isRequired,

};

PasswordField.defaultProps = {
    disableButton: false,
    visible: false
}

export default withStyles(styles, 'PasswordField')(PasswordField);
