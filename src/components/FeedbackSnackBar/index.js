import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import Button from 'material-ui/Button';
import {withRouter} from 'react-router-dom'
import {compose} from "recompose";

const styles = (theme) => ({
    popup: {
        width: "450px",
        height: "250px",
        overflow: 'hidden',
        margin: "0"
    },
    close: {
        width: theme.spacing.unit * 4,
        height: theme.spacing.unit * 4,
    }
});

const enhance = compose(
    withRouter,
    withStyles(styles, 'FeedbackSnackBar')
);
class FeedbackSnackBar extends Component {
    constructor(props) {
        super(props);

        this.handleRequestClose = this.handleRequestClose.bind(this);
    }

    handleRequestClose() {
        this.props.onClose();
    }

    render() {
        const isError = this.props.error;

        return (
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                autoHideDuration={isError ? null : 4000}
                open={this.props.open}
                onExited={this.handleRequestClose}
                onClose={this.handleRequestClose}
                SnackbarContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{this.props.message}</span>}
                action={[
                    this.props.actionData ? <Button key="undo" color="secondary" dense onClick={() => this.props.history.push(this.props.actionData.href, { referer: this.props.history.location })}>
                        {this.props.actionData.text}
                    </Button> : null,
                    isError ? <Button key="undo" color="secondary" dense onClick={() => {
                            window.parent.location = window.parent.location.href;
                        }}>
                            RELOAD PAGE
                        </Button> : null
                    ,
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={styles.close}
                        onClick={this.handleRequestClose}
                    >
                        <CloseIcon />
                    </IconButton>,
                ]}
            />
        );
    }

}

FeedbackSnackBar.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    actionData: PropTypes.object,
};


FeedbackSnackBar.defaultProps = {
    open: false,
    message: "",
    actionData: null
};

export default enhance(FeedbackSnackBar);

