import React from 'react';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import { FormControl } from 'material-ui/Form';
import Input  from 'material-ui/Input';
import Paper from 'material-ui/Paper';
import { ListItem, ListItemText } from 'material-ui/List';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import {withStyles} from 'material-ui/styles';
import _ from "lodash";
import Localize from "../Localize";

const styles = theme => ({
    container: {
        flexGrow: 1,
        position: 'relative',
    },
    suggestionSelected: {
        backgroundColor: 'rgba(0, 0, 0, 0.12)'
    },
    suggestionsContainerPadding: {
        padding: 0
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit * 3,
        left: 0,
        right: 0,
        zIndex: 100,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    textField: {
        width: '100%',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
    inputInkbar: {
        '&:before': {
            backgroundColor: '#fff!important',
        },
        '&:after': {
            backgroundColor: '#fff!important',
        },
    },
    inputRoot: {
        color: '#fff',
        width: '220px'
    }
});


class SearchSuggestLayout extends React.Component {

    constructor(props) {
        super(props);
        this.renderInput = this._renderInput.bind(this);
        this.renderSuggestion = this._renderSuggestion.bind(this);
        this.renderSuggestionsContainer = this._renderSuggestionsContainer.bind(this);
        this.getSuggestionValue = this._getSuggestionValue.bind(this);
        this.getSuggestions = this._getSuggestions.bind(this);
        this.onSuggestionsFetchRequested = this._handleOnSuggestionsFetchRequested.bind(this);
        this.onSuggestionsClearRequested = this._handleOnSuggestionsClearRequested.bind(this);
        this.onSuggestionSelected = this._handleOnSuggestionSelected.bind(this);
        this.onChange = this._handleOnChange.bind(this);
        this.onBlur = this._handleOnBlur.bind(this);
    }


    _renderInput = (inputProps) => {
        const {classes, home, value, ref, label, disabled, ...other} = inputProps;

        return (
            <FormControl className={classes.formControl}>
                <Input
                    classes={{
                        root: classes.inputRoot,
                        inkbar: classes.inputInkbar,
                    }}
                    value={value}
                    inputRef={ref}
                    {...other}
                />
            </FormControl>
        );
    };

    _renderSuggestion = (suggestion, {query, isHighlighted}) => {
        const matches = match(suggestion.name, query);
        const parts = parse(suggestion.name, matches);
        const classes = this.props.classes;
        const friendlySearchResultType = (suggestion.type === 'vehicules') ?
            Localize.locale['autosuggestChips-AutosuggestChipsRoles-Vehicle'] :
            Localize.locale['autosuggestChips-AutosuggestChipsRoles-User']
        return (
            <ListItem button dense classes={{root: isHighlighted ? classes.suggestionSelected : ''}} component={'div'}>
                <ListItemText
                    primary={parts.map((part, index) => {
                        return part.highlight
                            ?
                            <span key={index} style={{fontWeight: 300}}>
                                        {part.text}
                                    </span>
                            :
                            <strong key={index} style={{fontWeight: 500}}>
                                {part.text}
                            </strong>;
                    })}
                    secondary={friendlySearchResultType}
                />
            </ListItem>
        );
    };

    _renderSuggestionsContainer = (options) => {
        const {containerProps, children} = options;

        return (
            <Paper {...containerProps} square>
                {children}
            </Paper>
        );
    };


    _getSuggestionValue(suggestion) {
        return suggestion.name;
    }

    _getSuggestions = (suggestions, value) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        let count = 0;

        suggestions = _.map(suggestions, (suggestion) => {
            const suggestionId = this.props.suggestionId ? this.props.suggestionId : 'id';
            const suggestionName = this.props.suggestionName ? this.props.suggestionName : 'name';
            return {id: suggestion[suggestionId], name: suggestion[suggestionName]};
        });
        let newSuggestions = suggestions.filter(suggestion => {
            let maxSuggestions = this.props.maxSuggestions ? this.props.maxSuggestions : 5;
            const keepCondition = (count < maxSuggestions) && (suggestion.name && suggestion.name.toLowerCase().slice(0, inputLength) === inputValue);
            if (keepCondition) {
                count += 1;
            }
            return keepCondition;
        });

        this.props.onChangeAutosuggest({suggestions: newSuggestions});
    };

    _handleOnSuggestionsFetchRequested = ({value}) => {
        let allSuggestions = this.props.allSuggestions;
        this.getSuggestions(allSuggestions, value);
    };

    _handleOnSuggestionsClearRequested = () => {
        this.props.onChangeAutosuggest({suggestions: []});
    };

    _handleOnChange = (event, {newValue, method}) => {
        this.props.onChangeAutosuggest({value: newValue, method});
    };

    _handleOnBlur = (event, {highlightedSuggestion}) => {
        if (this.props.onBlur){
            this.props.onBlur();
        }
    };

    _handleOnSuggestionSelected = (event, {suggestion}) => {
        this.props.onSuggestionSelected(event, {suggestion});

    };

    render() {
        const {classes, id} = this.props;

        return (

            <Autosuggest
                theme={{
                    container: classes.container,
                    suggestionsContainerOpen: classes.suggestionsContainerOpen,
                    suggestionsList: classes.suggestionsList,
                    suggestion: classes.suggestion,
                }}
                renderInputComponent={this.renderInput}
                suggestions={this.props.currentSuggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                renderSuggestionsContainer={this.renderSuggestionsContainer}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                onSuggestionSelected={this.onSuggestionSelected}
                alwaysRenderSuggestions={this.props.alwaysRenderSuggestions ? this.props.alwaysRenderSuggestions : true}
                inputProps={{
                    id,
                    label: this.props.label,
                    disabled: this.props.disabled,
                    autoFocus: true,
                    classes,
                    placeholder: this.props.placeholder,
                    // value: this.props.autosuggestVubox.value !== undefined ? this.props.autosuggestVubox.value : this.props.value,
                    value: this.props.inputValue ? this.props.inputValue : '',
                    onChange: this.onChange,
                    onBlur: this.onBlur
                }}
            />
        )
    }


}


SearchSuggestLayout.propTypes = {
    classes: PropTypes.object.isRequired,
};


SearchSuggestLayout.defaultProps = {};

export default withStyles(styles, 'SearchSuggestLayout')(SearchSuggestLayout);
