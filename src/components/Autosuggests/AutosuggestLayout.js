import React from 'react';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import {MenuItem} from 'material-ui/Menu';
import {withStyles} from 'material-ui/styles';
import _ from "lodash";
import FunctionHelpers from "helpers/FunctionHelpers";
import SearchHelpers from 'helpers/SearchHelpers';


const styles = theme => ({
    container: {
        flexGrow: 1,
        position: 'relative',
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit * 3,
        left: 0,
        right: 0,
        zIndex: 100,
        width: 'max-content'
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    textField: {
        width: '100%',
    },
});


class AutosuggestLayout extends React.Component {

    constructor(props) {
        super(props);
        this.renderInput = this._renderInput.bind(this);
        this.renderSuggestion = this._renderSuggestion.bind(this);
        this.renderSuggestionsContainer = this._renderSuggestionsContainer.bind(this);
        this.getSuggestionValue = this._getSuggestionValue.bind(this);
        this.getSuggestions = this._getSuggestions.bind(this);
        this.onSuggestionsFetchRequested = this._handleOnSuggestionsFetchRequested.bind(this);
        this.onSuggestionsClearRequested = this._handleOnSuggestionsClearRequested.bind(this);
        this.onSuggestionSelected = this._handleOnSuggestionSelected.bind(this);
        this.onChange = this._handleOnChange.bind(this);
        this.onBlur = this._handleOnBlur.bind(this);
    }

    componentWillUpdate(nextProps) {
        if (nextProps.defaultValue && this.props.defaultValue !== nextProps.defaultValue) {
            this.props.onChangeAutosuggest({value: nextProps.defaultValue});
        }
    }

    _renderInput = (inputProps) => {
        const {classes, home, value, ref, label, disabled, required, error, helperText, ...other} = inputProps;

        return (
            <TextField
                label={label}
                autoFocus={home}
                className={this.props.textFieldClassName ? this.props.textFieldClassName : classes.textField}
                value={value}
                inputRef={ref}
                InputProps={{
                    classes: {
                        input: classes.input,
                    },
                    ...other,
                }}
                disabled={disabled}
                required={required}
                error={error}
                helperText={helperText}
            />
        );
    };

    _renderSuggestion = (suggestion, {query, isHighlighted}) => {
        const suggestionName = suggestion.name;
        const matches = SearchHelpers.fuzzySearch(query, suggestionName);

        return (
            <MenuItem selected={isHighlighted} component="div">
                <div>
                    {
                        suggestionName.split('').map((suggestionChar, suggestionCharIndex) => {
                            return matches.includes(suggestionCharIndex) ?
                                    <span key={suggestionCharIndex} style={{fontWeight: 300}}>{suggestionChar}</span>
                                :
                                    <strong key={suggestionCharIndex} style={{fontWeight: 500}}>{suggestionChar}</strong>;
                        })
                    }
                </div>
            </MenuItem>
        );
    };

    _renderSuggestionsContainer = (options) => {
        const {containerProps, children} = options;

        return (
            <Paper {...containerProps} square>
                {children}
            </Paper>
        );
    };


    _getSuggestionValue(suggestion) {
        return suggestion.name;
    }

    _getSuggestions = (suggestions, inputValue, currentChips) => {
        const suggestionId = this.props.suggestionId ? this.props.suggestionId : 'id';
        const suggestionName = this.props.suggestionName ? this.props.suggestionName : 'name';
        suggestions = suggestions.filter(s => s[suggestionId] && s[suggestionName]);
        let count = 0;

        suggestions = _.map(suggestions, (suggestion) => {
            return {id: suggestion[suggestionId], name: suggestion[suggestionName]};
        });
        let newSuggestions = suggestions.filter(suggestion => {
            let maxSuggestions = this.props.maxSuggestions ? this.props.maxSuggestions : 5;
            const matches = SearchHelpers.fuzzySearch(inputValue, suggestion.name);
            const keepCondition = (count < maxSuggestions) && matches.length !== 0;
            if (keepCondition) {
                count += 1;
            }
            return keepCondition;
        });

        //Not show suggestions of already selected suggestions (for autosuggestchips)
        if (currentChips.length > 0) {
            newSuggestions = newSuggestions.filter(suggestion => {
                if (_.find(currentChips, c => c.id.toString() === suggestion.id.toString())) {
                    return null;
                } else {
                    return suggestion;
                }
            });
        }

        this.props.onChangeAutosuggest({suggestions: newSuggestions});
    };

    _handleOnSuggestionsFetchRequested = ({value}) => {
        let allSuggestions = this.props.allSuggestions;
        let currentChips = this.props.currentChips || [];
        this.getSuggestions(allSuggestions, value, currentChips);
    };

    _handleOnSuggestionsClearRequested = () => {
        this.props.onChangeAutosuggest({suggestions: []});
    };

    _handleOnChange = (event, {newValue}) => {
        this.props.onChangeAutosuggest({value: newValue});
    };

    _handleOnBlur = (event, {highlightedSuggestion}) => {
        if (this.props.defaultValue !== undefined)
            this.props.onChangeAutosuggest({value: this.props.defaultValue});
    };

    _handleOnSuggestionSelected = (event, {suggestion}) => {
        this.props.onSuggestionSelected(event, {suggestion});
    };

    render() {
        const {classes, id} = this.props;

        return (

            <Autosuggest
                id={id}
                theme={{
                    container: classes.container,
                    suggestionsContainerOpen: classes.suggestionsContainerOpen,
                    suggestionsList: classes.suggestionsList,
                    suggestion: classes.suggestion,
                }}
                renderInputComponent={this.renderInput}
                suggestions={FunctionHelpers.null2Array(this.props.currentSuggestions)}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                renderSuggestionsContainer={this.renderSuggestionsContainer}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                onSuggestionSelected={this.onSuggestionSelected}
                alwaysRenderSuggestions={this.props.alwaysRenderSuggestions ? this.props.alwaysRenderSuggestions : true}
                ref={this.props.inputRef ? this.props.inputRef : null}
                inputProps={{
                    id: `${id}-Input`,
                    label: this.props.label,
                    disabled: this.props.disabled,
                    autoFocus: false,
                    classes,
                    placeholder: this.props.placeholder,
                    value: FunctionHelpers.null2String(this.props.inputValue),
                    onChange: this.onChange,
                    onBlur: this.onBlur,
                    required: this.props.required,
                    error: this.props.error,
                    helperText: this.props.helperText
                }}
            />
        )
    }


}


AutosuggestLayout.propTypes = {
    classes: PropTypes.object.isRequired,
};


AutosuggestLayout.defaultProps = {};

export default withStyles(styles, 'AutosuggestLayout')(AutosuggestLayout);
