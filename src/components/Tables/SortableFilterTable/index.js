import React from 'react';
import PropTypes from 'prop-types';

import { withRouter } from 'react-router-dom';

import { withStyles } from 'material-ui/styles';
import { Button, Checkbox, Paper, Table, TableBody, TableCell, TableRow } from 'material-ui';
import keycode from 'keycode';

import classnames from 'classnames';

import HeaderTable from 'components/Tables/SortableFilterTable/components/HeaderTable';
import ToolbarTable from 'components/Tables/SortableFilterTable/components/ToolbarTable';
import Pagination from 'components/Pagination';

const styles = theme => ({
    paper: {
        width: '100%',
        // marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    button: {
        margin: theme.spacing.unit,
        justifyContent: 'left',
    },
    actionButton: {
        border: `solid 1px ${theme.palette.primary[500]}`,
        padding: '5px 10px',
        color: theme.palette.primary[500],
        borderRadius: '10px',
        minHeight: '30px',
    },
});

export class SortableFilterTable extends React.Component {
    state = {
        selected: [],
    };

    //  TODO on reducer if you want

    onClickColumnAction = payload => {
        const { columnsDataActions, fieldName, event, rowId, row } = payload;
        event.stopPropagation();

        columnsDataActions[fieldName](event, rowId, row);
    };

    getColumnName = column => {
        if (typeof column.id !== 'undefined') {
            return column.id;
        } else if (typeof column.formattedFieldName !== 'undefined') {
            return column.formattedFieldName;
        }
        return column.fieldName;
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.props.orderBy === property && this.props.order === 'desc') {
            order = 'asc';
        }

        if (this.props.onRequestSort) {
            this.props.onRequestSort(orderBy, order);
        }
    };

    handleSelectAllClickOnHeader = (event, checked, rowIdName) => {
        if (checked) {
            this.setState({ selected: this.props.data.map(row => row[rowIdName]) });
            return;
        }

        if (this.props.onSelected) {
            this.props.onSelected([]);
        }
        this.setState({ selected: [] });
    };

    handleKeyDown = (event, rowId) => {
        if (keycode(event) === 'space') {
            this.handleClickOnRow(event, rowId);
        }
    };

    handleClickOnRow = (event, rowId) => {
        const { selected } = this.state;
        const selectedIndex = selected.indexOf(rowId);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, rowId);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
        }

        if (this.props.onSelected) {
            this.props.onSelected(newSelected);
        }
        this.setState({ selected: newSelected });
    };

    isSelected(rowId) {
        return this.state.selected.indexOf(rowId) !== -1;
    }

    handleOpenItem(event, rowId) {
        if (this.props.onOpenItem) {
            event.preventDefault();
            event.stopPropagation();
            this.props.onOpenItem(rowId);
        } else {
            console.error('[SortableFilterTable] - handleOpenItem - no callback');
        }
    }

    renderCellsOfOneRow = (row, props) => {
        const { classes, columnsData, columnsDataActions, rowIdName, actionsDisplayCondition } = props;

        const rowId = row[rowIdName];

        const cellsOfRow = columnsData
            .filter(columnData => columnData.fieldType !== 'Id')
            .map((columnData, columnDataIndex) => {
                const itemVal = row[this.getColumnName(columnData)];
                const cellOnClick =
                    !columnData.button && columnData.fieldType !== 'Button'
                        ? event => this.handleClickOnRow(event, rowId)
                        : undefined;
                let content;

                if (columnData.button) {
                    content = (
                        <Button
                            onClick={event => this.handleOpenItem(event, rowId)}
                            className={classnames(classes.button, 'Selenium-Selectable-Table-Row-Button')}
                        >
                            {itemVal}
                        </Button>
                    );
                } else if (columnData.buttonLabel) {
                    if (
                        (actionsDisplayCondition[columnData.fieldName] &&
                            actionsDisplayCondition[columnData.fieldName](row)) ||
                        !actionsDisplayCondition[columnData.fieldName]
                    ) {
                        content = (
                            <Button
                                className={classnames(
                                    classes.actionButton,
                                    'Selenium-Selectable-Table-Row-ActionButton'
                                )}
                                onClick={event =>
                                    this.onClickColumnAction({
                                        columnsDataActions,
                                        fieldName: columnData.fieldName,
                                        event,
                                        rowId,
                                        row,
                                    })
                                }
                            >
                                {columnData.buttonLabel}
                            </Button>
                        );
                    } else {
                        return undefined;
                    }
                } else {
                    content = itemVal;
                }

                return (
                    <TableCell
                        key={columnDataIndex}
                        onClick={cellOnClick}
                        padding={
                            columnData.disablePadding === undefined || columnData.disablePadding ? 'none' : 'default'
                        }
                    >
                        {content}
                    </TableCell>
                );
            })
            .filter(e => e);
        return cellsOfRow;
    };

    renderRows = (data, props) => {
        const { rowIdName, selectionable } = props;

        if (data === null) return null;

        return data.map((row, rowIndex) => {
            const isSelected = this.isSelected(row[rowIdName]);
            let selectAllCell = <TableCell padding="checkbox" />;
            if (selectionable) {
                selectAllCell = (
                    <TableCell
                        padding="checkbox"
                        onClick={event => {
                            this.handleClickOnRow(event, row[rowIdName]);
                        }}
                    >
                        <Checkbox checked={isSelected} />
                    </TableCell>
                );
            }

            return (
                <TableRow
                    hover
                    onKeyDown={event => this.handleKeyDown(event, row[rowIdName])}
                    onClick={this.props.onOpenItem ? event => this.handleOpenItem(event, row[rowIdName]) : null}
                    role="checkbox"
                    aria-checked={isSelected}
                    tabIndex="-1"
                    key={rowIndex}
                    selected={selectionable && isSelected}
                >
                    {selectAllCell}
                    {this.renderCellsOfOneRow(row, props)}
                </TableRow>
            );
        });
    };

    render() {
        const classes = this.props.classes;
        const classNames = classnames(classes.paper, this.props.wrapperClass);

        const { order, orderBy, onRemoveToolbarChip, columnsDataActions, actionsDisplayCondition } = this.props;
        const columnsData = this.props.columns;
        const selectionable = this.props.selectionable ? true : this.props.selectionable;
        const onClickAdvancedFilters = this.props.onClickAdvancedFilters ? this.props.onClickAdvancedFilters : null;
        const advancedFilters = this.props.advancedFilters ? this.props.advancedFilters : [];

        const fieldId = columnsData.find(o => o.fieldType === 'Id');
        const rowIdName = fieldId ? fieldId.fieldName : 'id';
        return (
            <Paper className={classNames} id={this.props.wrapperId}>
                <ToolbarTable
                    dataSize={this.props.dataSize}
                    numSelected={this.state.selected.length}
                    title={this.props.title}
                    booleanFilters={this.props.booleanFilters} // replace options
                    onSelectedOptions={this.props.onSelectedOptions}
                    onClickAddItem={this.props.onClickAddItem}
                    handleRemoveItem={this.props.handleRemoveItem}
                    onClickAdvancedFilters={onClickAdvancedFilters}
                    advancedFilters={advancedFilters}
                    selectionable={selectionable}
                    onRemoveToolbarChip={onRemoveToolbarChip}
                />
                <Table>
                    <HeaderTable
                        selectionable={selectionable}
                        columns={columnsData}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={(event, checked) =>
                            this.handleSelectAllClickOnHeader(event, checked, rowIdName)
                        }
                        onRequestSort={this.handleRequestSort}
                    />
                    <TableBody>
                        {this.renderRows(this.props.data, {
                            rowIdName,
                            selectionable,
                            classes,
                            columnsData,
                            columnsDataActions,
                            actionsDisplayCondition,
                        })}
                    </TableBody>
                </Table>
                <Pagination
                    classes={{ root: 'pagination-for-table' }}
                    onNext={this.props.onNext}
                    onBack={this.props.onBack}
                    onPage={this.props.onPage}
                    currentPage={this.props.currentPage}
                    maxPagesLimit={this.props.maxPagesLimit}
                    maxPages={this.props.maxPages}
                />
            </Paper>
        );
    }
}

SortableFilterTable.propTypes = {
    booleanFilters: PropTypes.arrayOf(PropTypes.shape({})),
    columns: PropTypes.arrayOf(PropTypes.shape({})),
    data: PropTypes.arrayOf(PropTypes.shape({})),
    dataSize: PropTypes.number,
    onSelectedOptions: PropTypes.func,
    handleRemoveItem: PropTypes.func,
    onRemoveToolbarChip: PropTypes.func,
    columnsDataActions: PropTypes.shape({}),
    onClickAdvancedFilters: PropTypes.func,
    onNext: PropTypes.func,
    onBack: PropTypes.func,
    currentPage: PropTypes.number,
    maxPagesLimit: PropTypes.number,
    maxPages: PropTypes.number,
    onClickAddItem: PropTypes.func,
    onOpenItem: PropTypes.func,
    onPage: PropTypes.func,
    onRequestSort: PropTypes.func,
    order: PropTypes.string,
    orderBy: PropTypes.string,
    selectionable: PropTypes.bool,
    title: PropTypes.string,
    wrapperId: PropTypes.string,
    wrapperClass: PropTypes.string,
    classes: PropTypes.shape({}).isRequired,
    onSelected: PropTypes.func,
    actionsDisplayCondition: PropTypes.objectOf(PropTypes.func),
    advancedFilters: PropTypes.arrayOf(PropTypes.shape({})),
};

SortableFilterTable.defaultProps = {
    actionsDisplayCondition: {},
};

export default withStyles(styles, 'SortableFilterTable')(withRouter(SortableFilterTable));
