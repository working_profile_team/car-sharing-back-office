import React from 'react';
import { JSDOM } from 'jsdom';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';
import { Table, TableBody, TableCell, TableRow } from 'material-ui';
import { SortableFilterTable } from '../../../Tables/SortableFilterTable/index';
import ToolbarTable from '../../../Tables/SortableFilterTable/components/ToolbarTable';
import HeaderTable from '../../../Tables/SortableFilterTable/components/HeaderTable';
import { Pagination } from '../../../Pagination/index';

Enzyme.configure({ adapter: new Adapter() });

describe('<SortableFilterTable/>', () => {
    let props;

    beforeEach(() => {
        const dom = new JSDOM(`<!doctype html><html><body></body></html>`);
        global.window = dom.window;
        global.document = dom.window.document;

        props = {
            wrapperId: 'table-wrapper-id',
            classes: {
                title: 'table-title',
            },
            title: 'Table title',
            selectionable: false,
            columns: [
                { fieldName: 'firstName', label: 'First name', button: false },
                { fieldName: 'lastName', label: 'Last name', button: false },
                { formattedFieldName: 'formattedDob', fieldName: 'dob', label: 'Age', button: false },
            ],
            data: [
                {
                    id: 'first-row',
                    firstName: 'John',
                    lastName: 'Briggs',
                    username: 'jbriggs',
                    dob: '1998-05-14',
                    formattedDob: '14/05/1998',
                },
                {
                    id: 'second-row',
                    firstName: 'Luke',
                    lastName: 'Barnes',
                    username: 'lbarnes',
                    dob: '1997-03-02',
                    formattedDob: '02/03/1997',
                },
            ],

            onPage: () => {},
            order: 'asc',
            orderBy: 'firstName',
        };
    });

    afterEach(() => {
        global.window = undefined;
        global.document = undefined;
    });

    it('renders as expected', () => {
        const wrapper = mount(<SortableFilterTable {...props} />);

        expect(
            wrapper
                .find(ToolbarTable)
                .first()
                .text()
                .trim()
        ).toEqual('Table title');

        expect(wrapper.find(Table)).toHaveLength(1);
        expect(wrapper.find(HeaderTable)).toHaveLength(1);
        expect(wrapper.find(TableBody)).toHaveLength(1);
        expect(wrapper.find(TableRow)).toHaveLength(3);
        expect(wrapper.find(Pagination)).toHaveLength(1);

        expect(
            wrapper
                .find('table tbody tr')
                .first()
                .find(TableCell)
        ).toHaveLength(4);
        expect(
            wrapper
                .find('table tbody tr')
                .first()
                .find(TableCell)
                .at(1)
                .text()
        ).toEqual('John');
        expect(
            wrapper
                .find('table tbody tr')
                .first()
                .find(TableCell)
                .at(2)
                .text()
        ).toEqual('Briggs');
        expect(
            wrapper
                .find('table tbody tr')
                .first()
                .find(TableCell)
                .at(3)
                .text()
        ).toEqual('14/05/1998');
    });

    it('implements an onClick handler on column headers which can be used to implement our own sorting logic', done => {
        props.onRequestSort = (orderBy, order) => {
            expect(orderBy).toEqual('firstName');
            expect(order).toEqual('desc');
            done();
        };

        const wrapper = mount(<SortableFilterTable {...props} />);

        wrapper
            .find('table thead tr th')
            .at(1)
            .find('span')
            .first()
            .simulate('click');
    });

    it('implements an onClick handler on table body row cells which can be used to implement our own logic', done => {
        props.columns.push({ id: 'username', label: 'Username', button: true });

        props.onOpenItem = id => {
            expect(id).toEqual('first-row');
            done();
        };

        const wrapper = mount(<SortableFilterTable {...props} />);

        wrapper
            .find('table tbody tr td button')
            .at(0)
            .simulate('click');
    });

    it('implements basic pagination functionality', done => {
        props.currentPage = 1;
        props.maxPages = 2;
        props.onPage = page => {
            expect(page).toEqual(2);
            done();
        };

        const wrapper = mount(<SortableFilterTable {...props} />);
        const gridWrapper = wrapper.find('.pagination-for-table').filterWhere(n => typeof n.type() === 'string');

        expect(gridWrapper).toHaveLength(1);
        expect(gridWrapper.find('button')).toHaveLength(6);

        gridWrapper
            .find('button')
            .at(3)
            .simulate('click');
    });
});
