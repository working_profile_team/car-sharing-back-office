import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Checkbox, TableCell, TableHead, TableRow, TableSortLabel } from 'material-ui';

export default class HeaderTable extends Component {
    static propTypes = {
        onRequestSort: PropTypes.func.isRequired,
        onSelectAllClick: PropTypes.func.isRequired,
        order: PropTypes.string.isRequired,
        orderBy: PropTypes.string.isRequired,
        columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
        selectionable: PropTypes.bool,
    };

    createSortHandler = property => event => {
        if (this.props.onRequestSort) {
            this.props.onRequestSort(event, property);
        } else {
            console.error('[HeaderTable] - createSortHandler - no onRequestSort props');
        }
    };

    renderHeaderRow = () => {
        const { onSelectAllClick, order, orderBy } = this.props;
        const selectionable = this.props.selectionable === undefined ? true : this.props.selectionable;
        const columnsData = this.props.columns;

        let selectAllRowsCheckbox = <TableCell padding="checkbox" />;
        if (selectionable) {
            selectAllRowsCheckbox = (
                <TableCell padding="checkbox">
                    <Checkbox onChange={onSelectAllClick} />
                </TableCell>
            );
        }

        const cellsOfHeaderRow = columnsData.filter(column => column.fieldType !== 'Id').map((column, columnIndex) => {
            const columnName = column.id ? column.id : column.fieldName;
            return (
                <TableCell
                    key={columnIndex}
                    numeric={column.numeric}
                    padding={column.disablePadding === undefined || column.disablePadding ? 'none' : 'default'}
                >
                    {column.disabledSort ? (
                        column.label
                    ) : (
                        <TableSortLabel
                            classes={{ root: 'Selenium-Table-Header-Column' }}
                            active={orderBy.split('Parsed')[0] === columnName.split('Parsed')[0]}
                            direction={order}
                            onClick={this.createSortHandler(columnName)}
                        >
                            {column.label}
                        </TableSortLabel>
                    )}
                </TableCell>
            );
        });

        return (
            <TableRow>
                {selectAllRowsCheckbox}
                {cellsOfHeaderRow}
            </TableRow>
        );
    };

    render() {
        return <TableHead>{this.renderHeaderRow()}</TableHead>;
    }
}
