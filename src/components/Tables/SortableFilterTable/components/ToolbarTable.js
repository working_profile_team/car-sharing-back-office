import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import DeleteIcon from 'material-ui-icons/Delete';
import AddCircleIcon from 'material-ui-icons/AddCircle';
import FilterListIcon from 'material-ui-icons/FilterList';

import { FormControlLabel } from 'material-ui/Form';

import _ from 'lodash';

import ChipsArray from 'containers/ChipsArray/ChipsArray';
import Localize from 'components/Localize';

import Switch from 'material-ui/Switch';
import Chip from 'material-ui/Chip';

const styles = theme => ({
    root: {
        paddingRight: 2,
    },
    spacer: {
        flex: '1 1 100%',
    },
    chipResults: {
        marginLeft: '15px',
        marginRight: '8px',
        marginBottom: '8px',
    },
    actions: {
        color: theme.palette.text.secondary,
        minWidth: 'max-content',
        textAlign: 'right',
    },
    title: {
        flex: '0 0 auto',
    },
    sorting: {
        display: 'inline-block',
        marginRight: '20px',
    },
    formLabel: {
        margin: '0',
    },
    buttons: {
        verticalAlign: 'middle',
    },
    filters: {
        display: 'inline-flex',
    },
    advancedFilters: {
        minHeight: 'unset !important',
    },
});

class ToolbarTable extends React.Component {
    constructor(props) {
        super(props);
        this.onClickAddItem = this.handleClickAddItem.bind(this);
        this.onClickDeleteItem = this.handleClickDeleteItem.bind(this);
    }

    state = {
        anchorElTimeFilter: undefined,
        openTimeFilter: false,
        selectionTimeFilter: 0,
    };

    handleClickAddItem() {
        this.props.onClickAddItem();
    }

    handleClickDeleteItem() {
        this.props.onClickDeleteItem();
    }

    render() {
        const { selectionable, numSelected, classes, onRemoveToolbarChip } = this.props;
        const toolbarButtonprops = {
            raised: true,
            color: 'primary',
        };

        return (
            <div>
                <Toolbar>
                    <div className={classes.title}>
                        {selectionable && numSelected > 0 ? (
                            <Typography type="subheading">
                                {numSelected} {Localize.locale['Components-ToolBarTable-Selected']}
                            </Typography>
                        ) : (
                            <Typography type="title">{this.props.title} </Typography>
                        )}
                    </div>
                    {this.props.dataSize !== undefined && (
                        <Chip
                            key={'Selenium-Chip-Results'}
                            label={`${this.props.dataSize} ${Localize.locale.tables.results}`}
                            id={'Selenium-Chip-Results'}
                            className={classes.chipResults}
                        />
                    )}
                    {!!this.props.booleanFilters &&
                        _.map(this.props.booleanFilters, (booleanFilter, booleanFilterIndex) => (
                            <div key={booleanFilterIndex} className={classes.filters}>
                                <FormControlLabel
                                    control={
                                        <Switch
                                            classes={{ root: 'Selenium-Table-Header-Switch' }}
                                            checked={booleanFilter.value}
                                            onChange={(event, checked) => {
                                                booleanFilter.onChange({ id: booleanFilter.id, value: checked });
                                            }}
                                        />
                                    }
                                    label={booleanFilter.label}
                                    className={classes.formLabel}
                                />
                            </div>
                        ))}
                    <div className={classes.spacer} />
                    <div className={classes.actions}>
                        {numSelected > 0 &&
                            !!this.props.onClickDeleteItem && (
                                <Button
                                    {...toolbarButtonprops}
                                    aria-label="Delete"
                                    color={'primary'}
                                    className={classes.buttons}
                                    id={'Selenium-Table-Header-Delete'}
                                >
                                    {Localize.locale.actions.common.delete}
                                    <DeleteIcon />
                                </Button>
                            )}
                        {!!this.props.onClickAdvancedFilters && (
                            <div className={classes.sorting}>
                                <Button
                                    {...toolbarButtonprops}
                                    color={'primary'}
                                    className={classes.buttons}
                                    id={'Selenium-Table-Header-Filters-Button'}
                                    onClick={this.props.onClickAdvancedFilters}
                                >
                                    {Localize.locale.actions.common.filters}
                                    <FilterListIcon />
                                </Button>
                            </div>
                        )}
                        {!!this.props.onClickAddItem && (
                            <Button
                                {...toolbarButtonprops}
                                aria-label="Add Item"
                                color={'primary'}
                                className={classes.buttons}
                                id={'Selenium-Table-Header-Add-Button'}
                                onClick={this.onClickAddItem}
                            >
                                {Localize.locale['action-add']}
                                {Localize.locale.actions.common.add}
                                <AddCircleIcon />
                            </Button>
                        )}
                    </div>
                </Toolbar>
                {this.props.advancedFilters.length > 0 ? (
                    <Toolbar className={classes.advancedFilters}>
                        <ChipsArray data={this.props.advancedFilters} onRequestDelete={onRemoveToolbarChip} canDelete />
                    </Toolbar>
                ) : (
                    ''
                )}
            </div>
        );
    }
}

ToolbarTable.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    booleanFilters: PropTypes.arrayOf(PropTypes.shape({})),
    onClickAdvancedFilters: PropTypes.func,
    selectionable: PropTypes.bool,
    numSelected: PropTypes.number,
    onRemoveToolbarChip: PropTypes.func,
    advancedFilters: PropTypes.arrayOf(PropTypes.shape({})),
    title: PropTypes.string,
    onClickAddItem: PropTypes.func,
    onClickDeleteItem: PropTypes.func,
    dataSize: PropTypes.number,
};

ToolbarTable.defaultProps = {};

export default withStyles(styles, 'ToolbarTable')(ToolbarTable);
