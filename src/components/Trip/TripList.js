import React from 'react';
import PropTypes from 'prop-types';

import { Grid, AppBar, Tabs, Tab, TextField } from 'material-ui';
import { withStyles } from 'material-ui/styles';

import Localize from 'components/Localize';
import SortableFilterTable from 'components/Tables/SortableFilterTable';
import AdvancedFilters from 'components/Filters/AdvancedFilters';
import Section from 'components/Section';

const styles = {
    root: {
        height: '100%',
        paddingTop: 48,
        boxSizing: 'border-box',
    },
    table: {
        padding: 20,
        height: '100%',
        boxSizing: 'border-box',
    },
    input: {
        marginTop: 16,
    },
};

const TripList = ({
    classes,
    onOpenTripDetail,
    onChangeTab,
    selectedTabIndex,
    columns,
    tripsTableAllData,
    onPage,
    onRequestSort,
    onClickAdvancedFilters,
    advancedFiltersIsVisible,
    onCloseFilters,
    onClearFilters,
    onChangeAdvancedFilters,
    advancedFilters,
    advancedFiltersChips,
    onRemoveToolbarChip,
    rows,
    order,
    orderBy,
    currentPage,
    maxPage,
    dataSize
}) => (
    <Grid className={classes.root}>
        <AppBar
            position={'fixed'}
            color="default"
            style={{
                marginTop: 64,
                zIndex: 198,
                paddingLeft: 180,
            }}
        >
            <Tabs
                value={selectedTabIndex}
                scrollable
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                onChange={onChangeTab}
            >
                <Tab label={Localize.locale.trips.types.inProgress} id="Selenium-Tab-inProgress" />
                <Tab label={Localize.locale.trips.types.history} id="Selenium-Tab-history" />
            </Tabs>
        </AppBar>
        <div className={classes.table}>
            <SortableFilterTable
                title=""
                columns={columns}
                wrapperId="Selenium-Trip-Table"
                data={rows}
                dataSize={dataSize}
                totalData={tripsTableAllData}
                order={order}
                orderBy={orderBy}
                onOpenItem={onOpenTripDetail}
                onPage={onPage}
                onRequestSort={onRequestSort}
                currentPage={currentPage}
                maxPages={maxPage}
                onClickAdvancedFilters={selectedTabIndex === 1 ? onClickAdvancedFilters : null}
                selectionable={false}
                advancedFilters={advancedFiltersChips}
                onRemoveToolbarChip={onRemoveToolbarChip}
            />
        </div>
        <AdvancedFilters
            isOpen={advancedFiltersIsVisible}
            onCloseFilters={onCloseFilters}
            onClearFilters={onClearFilters}
        >
            <TextField
                id="Selenium-Filter-Started"
                type="date"
                label="Started after"
                className={classes.input}
                value={advancedFilters.started || ''}
                fullWidth
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={onChangeAdvancedFilters('started')}
            />
            <TextField
                id="Selenium-Filter-Ended"
                type="date"
                label="Ended before"
                className={classes.input}
                value={advancedFilters.ended || ''}
                fullWidth
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={onChangeAdvancedFilters('ended')}
            />
        </AdvancedFilters>
    </Grid>
);

TripList.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    selectedTabIndex: PropTypes.number.isRequired,
    columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    tripsTableAllData: PropTypes.arrayOf(PropTypes.shape({})),
    advancedFilters: PropTypes.shape({}).isRequired,
    advancedFiltersIsVisible: PropTypes.bool.isRequired,
    advancedFiltersChips: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    onOpenTripDetail: PropTypes.func.isRequired,
    onChangeTab: PropTypes.func.isRequired,
    onPage: PropTypes.func.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onClickAdvancedFilters: PropTypes.func.isRequired,
    onCloseFilters: PropTypes.func.isRequired,
    onClearFilters: PropTypes.func.isRequired,
    onChangeAdvancedFilters: PropTypes.func.isRequired,
    onRemoveToolbarChip: PropTypes.func.isRequired,
    rows: PropTypes.arrayOf(PropTypes.shape({})),
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    currentPage: PropTypes.number.isRequired,
    maxPage: PropTypes.number.isRequired,
};

export default Section('Selenium-Trips-Scene')(withStyles(styles, 'TripList')(TripList));
