import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Localize from 'components/Localize';
import { Cluster, GeoJSONLayer, Marker } from 'react-mapbox-gl';

import _ from 'lodash';

import MapBox from 'components/Maps/Mapbox';

import VehiculeMarker from 'components/Maps/Markers/VehiculeMarker';
import FilterListIcon from 'material-ui-icons/FilterList';

import FunctionHelpers from 'helpers/FunctionHelpers';

import * as SupervisorActions from 'redux/supervisor/actions';
import Button from 'material-ui/Button';

class SupervisorMap extends Component {
    constructor(props) {
        super(props);
        this.clusterMarker = this._clusterMarker.bind(this);
    }

    _clusterMarker(coordinates, pointCount) {
        return (
            <Marker
                key={coordinates[0]}
                coordinates={coordinates}
                style={{
                    borderRadius: 20,
                    width: '40px',
                    height: '40px',
                    backgroundColor: 'blue',
                    color: 'white',
                    fontSize: '32px',
                    justifyContent: 'center',
                    cursor: 'zoom-in',
                }}
            >
                <div
                    style={{
                        margin: 'auto',
                        textAlign: 'center',
                        height: 40,
                        width: 40,
                        lineHeight: '40px',
                        fontSize: pointCount > 99 ? 14 : null,
                    }}
                >
                    {FunctionHelpers.formatMarkerPointCount(pointCount)}
                </div>
            </Marker>
        );
    }

    closeDrawer = () => {
        this.props.supervisorActions.showFilters(true);
    };

    render() {
        const allFeatures = this.props.allFeatures.data.slice();
        const allFeaturesSorted = allFeatures.sort((a, b) => (a.properties.type === 'allowed' ? -1 : 1));
        return (
            <MapBox {...this.props}>
                _.map(this.props.fleets.slice(0, 1), (fleet, indexFleet) => {
                    const fleetVehicles = _.filter(
                        fleet.vehicules,
                        vehicule => vehicule.location && vehicule.location.longitude && vehicule.location.latitude
                    );
                    return (
                        <Cluster
                            key={fleet.fleetid + indexFleet}
                            ClusterMarkerFactory={this.clusterMarker}
                            zoomOnClick="true"
                            style={{
                                cursor: 'pointer',
                            }}
                        >
                            {_.map(fleetVehicles, (vehicule, indexVehicule) => (
                                <VehiculeMarker
                                    key={vehicule.id + indexVehicule}
                                    vehicule={vehicule}
                                    onClick={() => this.props.onClickMarker({ type: 'vehicule', payload: vehicule })}
                                    coordinates={[vehicule.location.longitude, vehicule.location.latitude]}
                                />
                            ))}
                        </Cluster>
                    );
                })}

                {_.map(allFeaturesSorted, (zone, indexZone) => {
                    const fillOpacity = 0.5;
                    const outlineColor = '#038EDE';
                    const fillColor = 'rgba(0,255,0, 0.25)';
                    return (
                        <GeoJSONLayer
                            key={indexZone}
                            data={zone}
                            fillPaint={
                                zone.properties.type === 'allowed'
                                    ? {
                                          'fill-color': fillColor,
                                          'fill-outline-color': outlineColor,
                                          'fill-opacity': fillOpacity,
                                      }
                                    : {
                                          'fill-color': 'rgba(255,0,0,0.35)',
                                          'fill-outline-color': '#038EDE',
                                          'fill-opacity': fillOpacity,
                                      }
                            }
                        />
                    );
                })}

                <div
                    style={{
                        top: 0,
                        right: 0,
                        textAlign: 'center',
                        position: 'absolute',
                        zIndex: '100',
                        margin: '20px 30px',
                    }}
                >
                    <Button color="primary" onClick={this.closeDrawer} raised>
                        {Localize.locale.supervisor.buttonLabels.filters}
                        <FilterListIcon />
                    </Button>
                </div>

                {this.props.children}
            </MapBox>
        );
    }
}

SupervisorMap.propTypes = {
    fleets: PropTypes.array.isRequired,
};

SupervisorMap.defaultProps = {
    fleets: [],
};

const mapActionsToProps = dispatch => ({
    supervisorActions: bindActionCreators(SupervisorActions, dispatch),
});

export default connect(
    state => ({
        mapbox: state.application.mapbox,
    }),
    mapActionsToProps
)(SupervisorMap);
