import React from 'react';
import SortableFilterTable from 'components/Tables/SortableFilterTable';
import { TripsByVehicleTable } from 'redux/trips/models/TripsByVehicleTable.model';
import PropTypes from 'prop-types';
import DateFilterHOC from 'containers/DateFilter';
import DateFilter from 'components/DateFilter'
import Localize from 'components/Localize';

const DateFilterComponent = DateFilterHOC(DateFilter)

const TabTrips = ({
    tripsData,
    tripsByVehicleTableCurrentPage,
    onOpenTripDetail,
    tripsByVehicleTableDataMaxPages,
    onRequestSortTrip,
    onPageTrip,
}) => (
    <div>
        <DateFilterComponent/>
        <SortableFilterTable
            id="tabTrips"
            wrapperId="Selenium-TripsTableByVehicleId-Wrapper"
            wrapperClass="tabTrips"
            title={Localize.locale.vehicles.vehicleTrips.title}
            columns={TripsByVehicleTable.columnsData}
            data={tripsData}
            totalData={tripsData}
            order={tripsByVehicleTableCurrentPage.order}
            orderBy={tripsByVehicleTableCurrentPage.orderBy}
            onOpenItem={onOpenTripDetail}
            currentPage={tripsByVehicleTableCurrentPage.page}
            maxPages={tripsByVehicleTableDataMaxPages}
            onPage={onPageTrip}
            onRequestSort={onRequestSortTrip}
            selectionable={false}
        />
    </div>
);

TabTrips.propTypes = {
    tripsData: PropTypes.arrayOf(PropTypes.shape({})),
    tripsByVehicleTableCurrentPage: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.func,
    ]),
    onOpenTripDetail: PropTypes.func,
    tripsByVehicleTableDataMaxPages: PropTypes.number,
    onRequestSortTrip: PropTypes.func,
    onPageTrip: PropTypes.func,
};

export default TabTrips;
