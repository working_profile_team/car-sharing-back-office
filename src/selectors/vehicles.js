import { createSelector } from 'reselect';
import moment from 'moment';
import * as _ from 'lodash';
import { sortAndSliceData, applyBooleanFilters } from 'helpers/DataHelpers/common';
import { formatApiVehiclesToTableData, getVehicleStatus } from 'helpers/DataHelpers/vehicles';
import { VehicleTable } from 'redux/vehicles/models/VehicleTable.model';
import Localize from 'components/Localize';
import { getVehiclesAssociatedToCurrentServiceSelector } from './services';

const allVehiclesSelector = state => state.vehicles.allVehicles;
const allFleetsSelector = state => state.fleet.allFleets;
const vehiclesTableMetadataSelector = state => state.vehicles.vehiclesTableCurrentPage;
const notAssignedVehiclesTableMetadataSelector = state => state.vehicles.notAssignedVehiclesTableCurrentPage;
const booleanFiltersSelector = state => state.vehicles.vehiclesTableCurrentPage.booleanFilters;
const localeSelector = state => state.localization.current;
const selectedFleetVehicleSelector = state => state.fleet.selectedFleetVehicle;
const expertCommandResponseSelector = state => state.fleet.expertCommandResponse;
const selectedVehicleIdSelector = state => state.vehicles.selectedVehicle.vehicleId;

export const flowStateSelector = state => state.vehicles.flowState;
export const selectedVehicleSelector = state => state.vehicles.selectedVehicle;
export const listWakeUpProviderSelector = state => state.vehicles.listWakeUpProvider;
export const advancedFiltersSelector = state => state.vehicles.advancedFilters;
export const getTabActionsSelected = state => state.vehicles.tabActionsSelected;
export const allModelsSelector = state => state.vehicles.allModels;
export const advancedFiltersIsVisibleSelector = state => state.vehicles.advancedFiltersIsVisible;
export const selectedTabIndexSelector = state => state.vehicles.selectedTabIndex;

export const canClickExpertCommandSelector = createSelector(
    [selectedVehicleIdSelector],
    selectedVehicleId => selectedVehicleId !== '' && selectedVehicleId !== null && selectedVehicleId !== undefined
);

export const advancedFiltersChipSelector = createSelector(
    [advancedFiltersSelector, localeSelector],
    (advancedFilters, locale) => {
        moment.locale(locale);
        const labelKeys = {
            status: 'statusLabel',
            archived: 'archived',
            'model.name': 'model',
            'model.energyType': 'energyType',
        };
        return Object.keys(advancedFilters)
            .map(key => ({
                id: key,
                name: `${Localize.locale.vehicles.fieldLabels[labelKeys[key]]}: ${advancedFilters[key]}`,
                category: key,
            }))
            .filter(v => advancedFilters[v.id]);
    }
);

export const filteredVehiclesSelector = createSelector(
    [allVehiclesSelector, advancedFiltersSelector, allFleetsSelector],
    (allVehicles, filters, fleets) => {
        const filtersKeys = Object.keys(filters);
        // archived is a boolean and activate by default so we can remove this key from filters
        filtersKeys.splice(filtersKeys.indexOf('archived'), 1);
        const matchKeys = filtersKeys.filter(f => !!filters[f]);
        const vehicles = allVehicles.filter(v => v.archived === filters.archived);

        if (matchKeys.length === 0) {
            return vehicles;
        }

        return vehicles.filter(
            vehicle =>
                filtersKeys.filter(k => {
                    let value = _.get(vehicle, k);
                    if (k === 'status') {
                        value = getVehicleStatus(vehicle, fleets);
                    }
                    return (
                        filters[k] !== '' &&
                        (typeof value === 'string' && value.toLowerCase() === filters[k].toLowerCase())
                    );
                    // If length is equal to matchKeys we apply a AND filter, remove this condition and we apply a OR filter
                }).length === matchKeys.length
        );
    }
);

// Get available options from vehicle list
export const filtersOptionsSelector = createSelector([allVehiclesSelector, allFleetsSelector], (vehicles, fleets) => {
    const vehiclesFilterValue = vehicles.map(({ model: { energyType, status, name } }) => ({
        energyType,
        status,
        name,
    }));

    const values = {
        status: new Set(),
    };

    vehicles.forEach(v => values.status.add(getVehicleStatus(v, fleets)));

    ['energyType', 'name'].forEach(k => {
        values[k] = new Set();
        // Remove duplicate values with Set
        vehiclesFilterValue.forEach(v => v[k] && values[k].add(v[k]));
    });

    // Convert Set to Array
    return { energyType: [...values.energyType], status: [...values.status], model: [...values.name] };
});

export const vehiclesTableDataSelector = createSelector(
    [
        filteredVehiclesSelector,
        allFleetsSelector,
        vehiclesTableMetadataSelector,
        booleanFiltersSelector,
        localeSelector,
    ],
    (allVehicles, allFleets, vehiclesTableCurrentPage, booleanFilters, locale) => {
        const columnsData = VehicleTable.columnsData;
        const { page, order, orderBy, pageSize } = vehiclesTableCurrentPage;
        moment.locale(locale);

        // const allVehiclesWithBooleanFileters = applyBooleanFilters({ data: [...allVehicles], booleanFilters });
        const formattedData = formatApiVehiclesToTableData(allVehicles, allFleets, columnsData);
        const slicedAndSortedData = sortAndSliceData({
            data: [...formattedData],
            page,
            pageSize,
            order,
            orderBy,
            columnsData,
        });
        return slicedAndSortedData;
    }
);

export const vehiclesTableDataMaxPagesSelector = createSelector(
    [filteredVehiclesSelector, vehiclesTableMetadataSelector, booleanFiltersSelector],
    (allVehicles, vehiclesTableCurrentPage, booleanFilters) => {
        const { pageSize } = vehiclesTableCurrentPage;
        const filteredData = applyBooleanFilters({ data: [...allVehicles], booleanFilters });
        return Math.ceil(filteredData.length / pageSize);
    }
);

export const vehiclesTableDataSizeSelector = createSelector(
    [filteredVehiclesSelector, booleanFiltersSelector],
    (allVehicles, booleanFilters) => {
        const filteredData = applyBooleanFilters({ data: [...allVehicles], booleanFilters });
        return filteredData.length;
    }
);

export const notAssignedVehiclesTableDataSelector = createSelector(
    [allVehiclesSelector, allFleetsSelector, notAssignedVehiclesTableMetadataSelector, localeSelector],
    (allVehicles, allFleets, notAssignedVehiclesTableCurrentPage, locale) => {
        const columnsData = VehicleTable.columnsData;
        const { page, order, orderBy, pageSize } = notAssignedVehiclesTableCurrentPage;
        moment.locale(locale);
        const formattedData = formatApiVehiclesToTableData(allVehicles, allFleets, columnsData);
        const notAssignedVehiclesFormattedData = _.filter(formattedData, vehicle => !vehicle.vuboxId);
        const slicedAndSortedData = sortAndSliceData({
            data: [...notAssignedVehiclesFormattedData],
            page,
            pageSize,
            order,
            orderBy,
            columnsData,
        });
        return slicedAndSortedData;
    }
);

export const notAssignedVehiclesTableDataMaxPagesSelector = createSelector(
    [allVehiclesSelector, notAssignedVehiclesTableMetadataSelector],
    (allVehicles, notAssignedVehiclesTableCurrentPage) => {
        const { pageSize } = notAssignedVehiclesTableCurrentPage;
        const filteredData = _.filter(allVehicles, vehicle => !vehicle.vuboxId);
        return Math.ceil(filteredData.length / pageSize);
    }
);

/**
 * It is not entirely clear whether returning the name of the last zone makes sense here
 */
export const getInZones = createSelector(
    [selectedFleetVehicleSelector],
    selectedFleetVehicle => selectedFleetVehicle.inZones
);

export const isSelectedVehicleAssignedToCurrentService = createSelector(
    [selectedVehicleSelector, getVehiclesAssociatedToCurrentServiceSelector],
    (selectedVehicle, vehiclesAssociatedToCurrentService) =>
        vehiclesAssociatedToCurrentService.indexOf(selectedVehicle.id) > -1
);

export const formatExpertCommandResponse = createSelector([expertCommandResponseSelector], expertCommandResponse => {
    let formattedReponse = expertCommandResponse;
    if (formattedReponse && formattedReponse.length > 0 && formattedReponse.indexOf(';') !== -1) {
        formattedReponse = formattedReponse.replace(new RegExp(';', 'g'), ';<br/>');
    }

    return { __html: formattedReponse };
});
