import { createSelector } from 'reselect';
import * as _ from 'lodash';
import moment from 'moment';

import {
    formatApiUserTripsToTableData,
    formatApiBillingOtherChargesToTableData,
    formatApiBillingTripsToTableData,
    formatApiUserSystemCreditsToTableData,
} from 'helpers/DataHelpers/users';
import { sortAndSliceData, sortData } from '../helpers/DataHelpers/common';
import ValidationHelpers from 'helpers/ValidationHelpers';
import { USER_ROLE_MANAGER } from '../constants/Users';
import { UserTable } from 'redux/users/models/UserTable.model';
import { UserTripTable } from 'redux/users/models/UserTripTable.model';
import { UserSystemCreditsTableModel } from '../redux/users/models/UserSystemCredits.model';
import { formatApiUsersToTableData } from '../helpers/DataHelpers/users';
import { TRIP_STATUSES } from '../constants/Trips';
import { currency as currencySelector } from 'selectors/application';
import { selectedFleetVehicleSelector } from 'selectors/fleets';

export const allUsersSelector = state => state.users.allUsers;
const usersTableMetadataSelector = state => state.users.usersTableCurrentPage;
const billingAllTripsSelector = state => state.users.selectedUser.billing.allBillingTrips;
const billingAllOtherChargesSelector = state => state.users.selectedUser.billing.allBillingOtherCharges;
const allVehiclesSelector = state => state.vehicles.allVehicles;
const billingYearSelector = state => state.users.selectedUser.billing.year;
const billingMonthSelector = state => state.users.selectedUser.billing.month;
const billingTripTableMetadataSelector = state => state.users.selectedUser.billing.billingTripsTableCurrentPage;
const billingOtherChargesTableMetadataSelector = state =>
    state.users.selectedUser.billing.billingOtherChargesTableCurrentPage;
const chargeAmountTypeSelector = state => state.users.selectedUser.billing.productChargeFormData.amountType;
const chargeAmountSelector = state => state.users.selectedUser.billing.productChargeFormData.amount;
const chargeTaxSelector = state => state.users.selectedUser.billing.productChargeFormData.tax;
const localeSelector = state => state.localization.current;
const selectedUserSelector = state => state.users.selectedUser;
const allUserTripsSelector = state => state.users.selectedUser.allUserTrips;
const usersTripTableMetadataSelector = state => state.users.selectedUser.userTripTableCurrentPage;
const usersFlowStateSelector = state => state.users.flowState;
const pendingUsersTableCurrentPageSelector = state => state.users.pendingUsersTableCurrentPage;
const getSelectedUsersTabIndex = state => state.users.selectedUsersTabIndex;
const allUserSystemCreditsSelector = state => state.users.selectedUser.allUserCredits;
const userSystemCreditsTableMetadataSelector = state => state.users.selectedUser.userSystemCreditsTableCurrentPage;

const getUsersTableDataFilterSelector = createSelector(
    [allUsersSelector, getSelectedUsersTabIndex],
    (allUsers, tabIndex) => {
        if (tabIndex === 4) {
            return allUsers;
        } // All
        let status = '';
        switch (tabIndex) {
            case 1:
                status = 'APPROVED';
                break;
            case 2:
                status = 'REJECTED';
                break;
            case 3:
                status = 'SUSPENDED';
                break;
            default:
                status = 'PENDING';
                break;
        }
        return allUsers.filter(user => user.accountStatus === status);
    }
);

export const usersTableDataSelector = createSelector(
    [usersTableMetadataSelector, localeSelector, getUsersTableDataFilterSelector],
    (usersTableCurrentPage, locale, filteredData) => {
        const columnsData = UserTable.columnsData;
        const { page, order, orderBy, pageSize } = usersTableCurrentPage;
        moment.locale(locale);
        const formattedData = formatApiUsersToTableData(filteredData, columnsData);
        const slicedAndSortedData = sortAndSliceData({
            data: [...formattedData],
            page,
            pageSize,
            order,
            orderBy,
            columnsData,
        });
        return slicedAndSortedData;
    }
);

export const usersTableDataMaxPagesSelector = createSelector(
    [getUsersTableDataFilterSelector, usersTableMetadataSelector],
    (allUsers, usersTableCurrentPage) => {
        const { pageSize } = usersTableCurrentPage;
        return Math.ceil(allUsers.length / pageSize);
    }
);

export const usersTableDataSizeSelector = state => getUsersTableDataFilterSelector(state).length;

export const billingTripsTableDataSelector = createSelector(
    [billingAllTripsSelector, billingTripTableMetadataSelector, allVehiclesSelector, localeSelector, currencySelector],
    (allBillingTrips, billingTripTableMetadata, allVehicles, locale, currency) => {
        const { order, orderBy, columnsData } = billingTripTableMetadata;
        moment.locale(locale);
        const formattedData = formatApiBillingTripsToTableData(allBillingTrips, allVehicles, columnsData, currency);
        // We don't want to paginate here
        return sortData({ data: [...formattedData], order, orderBy, columnsData });
    }
);

export const billingTripsTableDataMaxPagesSelector = createSelector(
    [billingAllOtherChargesSelector, billingTripTableMetadataSelector, billingYearSelector, billingMonthSelector],
    (allBillingTrips, billingTripTableMetadata, year, month) => {
        const { pageSize } = billingTripTableMetadata;
        const allBillingTripsFilteredByDate = _.filter(
            allBillingTrips,
            o => moment(o.startDate).month() + 1 === month && moment(o.startDate).year() === year
        );
        return Math.ceil(allBillingTripsFilteredByDate.length / pageSize);
    }
);

export const billingOtherChargesTableDataSelector = createSelector(
    [billingAllOtherChargesSelector, billingOtherChargesTableMetadataSelector, localeSelector, currencySelector],
    (allOtherCharges, billingOtherChargesTableMetadata, locale, currency) => {
        const { page, order, orderBy, pageSize, columnsData } = billingOtherChargesTableMetadata;
        moment.locale(locale);
        // We don't want to paginate here
        const formattedData = formatApiBillingOtherChargesToTableData(allOtherCharges, columnsData, currency);
        return sortAndSliceData({ data: [...formattedData], page, pageSize, order, orderBy, columnsData });
    }
);

export const billingOtherChargesTableDataMaxPagesSelector = createSelector(
    [
        billingAllOtherChargesSelector,
        billingOtherChargesTableMetadataSelector,
        billingYearSelector,
        billingMonthSelector,
    ],
    (allOtherCharges, billingOtherChargesTableMetadata, year, month) => {
        const { pageSize } = billingOtherChargesTableMetadata;
        const allOtherChargesFilteredByDate = _.filter(
            allOtherCharges,
            o => moment(o.startDate).month() + 1 === month && moment(o.startDate).year() === year
        );
        return Math.ceil(allOtherChargesFilteredByDate.length / pageSize);
    }
);

export const netAmountDisplaySelector = createSelector(
    [chargeAmountTypeSelector, chargeAmountSelector, chargeTaxSelector],
    (chargeAmountType, chargeAmount, chargeTax) => {
        let netAmountDisplay = 0;

        if (chargeAmountType === 'total') {
            netAmountDisplay = chargeAmount / (1 + chargeTax / 100);
        } else {
            netAmountDisplay = parseFloat(chargeAmount);
        }

        return netAmountDisplay.toFixed(2);
    }
);

export const taxAmountDisplaySelector = createSelector(
    [chargeAmountTypeSelector, chargeAmountSelector, chargeTaxSelector],
    (chargeAmountType, chargeAmount, chargeTax) => {
        let taxAmountDisplay = 0;

        if (chargeAmountType === 'total') {
            const withoutTaxPrice = chargeAmount / (1 + chargeTax / 100);
            taxAmountDisplay = withoutTaxPrice * (chargeTax / 100);
        } else {
            taxAmountDisplay = chargeAmount * (chargeTax / 100);
        }

        return taxAmountDisplay.toFixed(2);
    }
);

/**
 * Condition copied straight from component, inverted.
 * Will discuss this with Walter
 */
export const isUserFormValidSelector = createSelector(
    [selectedUserSelector, usersFlowStateSelector],
    (selectedUser, flowState) =>
        [
            flowState !== 'READ_ONLY',
            flowState !== 'CREATE' ||
                (ValidationHelpers.testStringField(selectedUser.password) &&
                    ValidationHelpers.testEmailField(selectedUser.email) &&
                    ValidationHelpers.testUsername(selectedUser.userName)),
            ValidationHelpers.testUserNameField(selectedUser.firstName),
            ValidationHelpers.testUserNameField(selectedUser.lastName),
            ValidationHelpers.testStringField(selectedUser.locale),
            ValidationHelpers.testStringField(selectedUser.accountStatus),
        ].every(b => b)
);

export const selectPendingUsersData = createSelector(
    [allUsersSelector, pendingUsersTableCurrentPageSelector, localeSelector],
    (allUsers, pendingUsersTableCurrentPage, locale) => {
        const columnsData = UserTable.columnsData;
        const { page, order, orderBy, pageSize } = pendingUsersTableCurrentPage;
        moment.locale(locale);
        const pendingUsersAllData = _.filter(allUsers, u => u.accountStatus === 'PENDING');
        const formattedData = formatApiUsersToTableData(pendingUsersAllData, columnsData);
        const slicedAndSortedData = sortAndSliceData({
            data: [...formattedData],
            page,
            pageSize,
            order,
            orderBy,
            columnsData,
        });
        return slicedAndSortedData;
    }
);

export const selectPendingUsersTableDataMaxPagesSelector = createSelector(
    [allUsersSelector, pendingUsersTableCurrentPageSelector],
    (allUsers, pendingUsersTableCurrentPage) => {
        const pendingUsersAllData = _.filter(allUsers, u => u.accountStatus === 'PENDING');
        const { pageSize } = pendingUsersTableCurrentPage;
        return Math.ceil(pendingUsersAllData.length / pageSize);
    }
);

export const isSelectedUserAdminSelector = createSelector(
    [selectedUserSelector],
    selectedUser => selectedUser.roles.indexOf(USER_ROLE_MANAGER) > -1
);

export const userTripsTableDataSelector = createSelector(
    [allUserTripsSelector, usersTripTableMetadataSelector, allVehiclesSelector, localeSelector],
    (allUserTrips, userTripTableMetadata, allVehicles, locale) => {
        const columnsData = UserTripTable.columnsData;
        const { page, order, orderBy, pageSize } = userTripTableMetadata;
        moment.locale(locale);
        const formattedData = formatApiUserTripsToTableData(allUserTrips, allVehicles, columnsData);
        const slicedAndSortedData = sortAndSliceData({
            data: [...formattedData],
            page,
            pageSize,
            order,
            orderBy,
            columnsData,
        });
        return slicedAndSortedData;
    }
);

export const userTripsTableDataMaxPagesSelector = createSelector(
    [allUserTripsSelector, usersTripTableMetadataSelector],
    (allUserTrips, userTripTableMetadata) => {
        const { pageSize } = userTripTableMetadata;
        return Math.ceil(allUserTrips.length / pageSize);
    }
);

export const lastTripStatusSelector = createSelector(
    [selectedUserSelector, selectedFleetVehicleSelector],
    (selectedUser, selectedFleetVehicle) => {
        if (!selectedUser.userLastTrip || !selectedUser.userLastTrip.id) return TRIP_STATUSES[0];

        const { booking_status, box_status } = selectedFleetVehicle;

        if (booking_status === 2) {
            if (box_status < 4) {
                return TRIP_STATUSES[1];
            }

            return TRIP_STATUSES[2];
        }

        return TRIP_STATUSES[3];
    }
);

export const userSystemCreditsTableDataSelector = createSelector(
    [allUserSystemCreditsSelector, userSystemCreditsTableMetadataSelector, localeSelector],
    (allUserCredits, userSystemCreditsTableMetadata, locale) => {
        const columnsData = UserSystemCreditsTableModel.columnsData;
        const { page, order, orderBy, pageSize } = userSystemCreditsTableMetadata;
        moment.locale(locale);
        const formattedData = formatApiUserSystemCreditsToTableData(
            allUserCredits,
            UserSystemCreditsTableModel.columnsData
        );
        const slicedAndSortedData = sortAndSliceData({
            data: [...formattedData],
            page,
            pageSize,
            order,
            orderBy,
            columnsData,
        });
        return slicedAndSortedData;
    }
);

export const userTripsSystemCreditsDataMaxPagesSelector = createSelector(
    [allUserSystemCreditsSelector, userSystemCreditsTableMetadataSelector],
    (allUserCredits, userSystemCreditsTableMetadata) => {
        const { pageSize } = userSystemCreditsTableMetadata;
        return Math.ceil(allUserCredits.length / pageSize);
    }
);
