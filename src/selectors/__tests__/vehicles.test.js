import { getVehicleStatus } from 'helpers/DataHelpers/vehicles';
import { filteredVehiclesSelector, filtersOptionsSelector } from '../vehicles';
import vehicles from '../../__tests__/fixtures/vehicles.json';
import fleets from '../../__tests__/fixtures/fleets.json';

describe('filters options selector', () => {
    it('return one of same occurences of status, model.name, model.energyType in vehicle list', () => {
        const state = {
            vehicles: {
                allVehicles: vehicles,
            },
            fleet: {
                allFleets: fleets,
            },
        };

        const availableOptions = filtersOptionsSelector(state);

        expect(availableOptions).toEqual({
            energyType: ['ELECTRIC', 'FUEL', 'HYBRID'],
            status: ['NOT CONNECTED', 'IN USE', 'AVAILABLE'],
            model: ['STK', 'Citroen C0', 'Citroen DS', 'Renault Zoe'],
        });
    });

    it('return empty arrays', () => {
        const state = {
            vehicles: {
                allVehicles: [],
            },
            fleet: {
                allFleets: fleets,
            },
        };

        const availableOptions = filtersOptionsSelector(state);

        expect(availableOptions).toEqual({ energyType: [], model: [], status: [] });
    });

    it('return empty status list', () => {
        const state = {
            vehicles: {
                allVehicles: vehicles,
            },
            fleet: {
                allFleets: [],
            },
        };

        const availableOptions = filtersOptionsSelector(state);

        expect(availableOptions).toEqual({
            energyType: ['ELECTRIC', 'FUEL', 'HYBRID'],
            model: ['STK', 'Citroen C0', 'Citroen DS', 'Renault Zoe'],
            status: [''],
        });
    });
});

describe('filtered vehicles selector', () => {
    it('return the right number of filtered vehicles', () => {
        const state = {
            vehicles: {
                allVehicles: vehicles,
                advancedFilters: {
                    status: '',
                    archived: false,
                    'model.name': '',
                    'model.energyType': '',
                },
            },
            fleet: {
                allFleets: fleets,
            },
        };
        const filteredVehicles = filteredVehiclesSelector(state);
        expect(filteredVehicles.length).toEqual(vehicles.length - vehicles.filter(v => v.archived).length);
    });

    it('return vehicles filtered by status', () => {
        const state = {
            vehicles: {
                allVehicles: vehicles,
                advancedFilters: {
                    status: 'NOT CONNECTED',
                    archived: false,
                    'model.name': '',
                    'model.energyType': '',
                },
            },
            fleet: {
                allFleets: fleets,
            },
        };
        const rightNumber = vehicles.filter(
            v =>
                getVehicleStatus(v, fleets) === state.vehicles.advancedFilters.status &&
                v.archived === state.vehicles.advancedFilters.archived
        ).length;

        const filteredVehicles = filteredVehiclesSelector(state);
        expect(filteredVehicles.length).toEqual(19);
        expect(rightNumber).toEqual(19);
    });

    it('return vehicles filtered by status and name', () => {
        const state = {
            vehicles: {
                allVehicles: vehicles,
                advancedFilters: {
                    status: 'NOT CONNECTED',
                    archived: false,
                    'model.name': 'Citroen DS',
                    'model.energyType': '',
                },
            },
            fleet: {
                allFleets: fleets,
            },
        };

        const rightNumber = vehicles.filter(
            v =>
                getVehicleStatus(v, fleets) === state.vehicles.advancedFilters.status &&
                v.archived === state.vehicles.advancedFilters.archived &&
                v.model.name === state.vehicles.advancedFilters['model.name']
        ).length;
        const filteredVehicles = filteredVehiclesSelector(state);
        expect(filteredVehicles.length).toEqual(10);
        expect(rightNumber).toEqual(10);
    });

    it('return vehicles filtered by status and name', () => {
        const state = {
            vehicles: {
                allVehicles: vehicles,
                advancedFilters: {
                    status: 'NOT CONNECTED',
                    archived: false,
                    'model.name': 'Citroen DS',
                    'model.energyType': 'HYBRID',
                },
            },
            fleet: {
                allFleets: fleets,
            },
        };

        const rightNumber = vehicles.filter(
            v =>
                getVehicleStatus(v, fleets) === state.vehicles.advancedFilters.status &&
                v.archived === state.vehicles.advancedFilters.archived &&
                v.model.name === state.vehicles.advancedFilters['model.name'] &&
                v.model.energyType === state.vehicles.advancedFilters['model.energyType']
        ).length;
        const filteredVehicles = filteredVehiclesSelector(state);
        expect(filteredVehicles.length).toEqual(0);
        expect(rightNumber).toEqual(0);
    });

    it('return archied vehicles', () => {
        const state = {
            vehicles: {
                allVehicles: vehicles,
                advancedFilters: {
                    status: '',
                    archived: true,
                    'model.name': '',
                    'model.energyType': '',
                },
            },
            fleet: {
                allFleets: fleets,
            },
        };

        const rightNumber = vehicles.filter(v => v.archived === state.vehicles.advancedFilters.archived).length;
        const filteredVehicles = filteredVehiclesSelector(state);
        expect(filteredVehicles.length).toEqual(6);
        expect(rightNumber).toEqual(6);
    });

    it('return archied vehicles filtered by name', () => {
        const state = {
            vehicles: {
                allVehicles: vehicles,
                advancedFilters: {
                    status: '',
                    archived: true,
                    'model.name': 'Citroen DS',
                    'model.energyType': '',
                },
            },
            fleet: {
                allFleets: fleets,
            },
        };

        const rightNumber = vehicles.filter(
            v =>
                v.archived === state.vehicles.advancedFilters.archived &&
                v.model.name === state.vehicles.advancedFilters['model.name']
        ).length;
        const filteredVehicles = filteredVehiclesSelector(state);
        expect(filteredVehicles.length).toEqual(2);
        expect(rightNumber).toEqual(2);
    });
});
