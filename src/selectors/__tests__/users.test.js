import {
    netAmountDisplaySelector,
    taxAmountDisplaySelector,
    isUserFormValidSelector
} from "../users";

describe('Users selectors', () => {
    describe('netAmountDisplaySelector', () => {

        it('returns net amount', () => {
            const state = {
                users: {
                    selectedUser: {
                        billing: {
                            productChargeFormData: {
                                amountType: 'total',
                                amount: 22,
                                tax: 10
                            }
                        }
                    }
                },
            };

            const result = netAmountDisplaySelector(state);
            expect(result).toEqual("20.00");
        });
    })

    describe('taxAmountDisplaySelector', () => {

        it('returns tax amount', () => {
            const state = {
                users: {
                    selectedUser: {
                        billing: {
                            productChargeFormData: {
                                amountType: 'total',
                                amount: 22,
                                tax: 10
                            }
                        }
                    }
                },
            };

            const result = taxAmountDisplaySelector(state);
            expect(result).toEqual("2.00");
        });
    });

    describe('isUserFormValidSelector', () => {
        let state;

        beforeEach(() => {
            state = {
                users: {
                    selectedUser: {
                        userName: 'test@test.com',
                        password: 'Vulog1',
                        email: 'test@test.com',
                        firstName: 'Andy',
                        lastName: 'Andy',
                        locale: 'en_US',
                        accountStatus: 'APPROVED',
                    },
                    flowState: ''
                },
            };
        });

        describe('when flowState is READ_ONLY', () => {
            beforeEach(() => {
                state.users.flowState = 'READ_ONLY';
            });

            it('returns false regardless of data validity', () => {
                expect(isUserFormValidSelector(state)).toBe(false);
            });
        });

        describe('when flowState is CREATE', () => {
            beforeEach(() => {
                state.users.flowState = 'CREATE';
            });

            it('returns false if all data except password is invalid', () => {
                state.users.selectedUser.password = '';
                expect(isUserFormValidSelector(state)).toBe(false);
            });

            it('returns false if all data except email is invalid', () => {
                state.users.selectedUser.email = '';
                expect(isUserFormValidSelector(state)).toBe(false);
            });
        });

        describe('when data is valid and flowState is UPDATE', () => {
            it('returns true', () => {
                state.users.flowState = 'UPDATE';
                expect(isUserFormValidSelector(state)).toBe(true);
            });
        });
    });
});
