import fleets from '../../__tests__/fixtures/fleets';
import vehicles from '../../__tests__/fixtures/vehicles';
import {filterFleet} from "../supervisor";

describe('Supervisor selectors', () => {
    describe('filterFleet', () => {
        let state;

        beforeEach(() => {
            state = {
                supervisor: {
                    sideFilters: {
                        vehiclestatusAvailable: true,
                        vehiclestatusInUse: true,
                        vehiclestatusOutOfService: true,
                        vehiclestatusArchived: true,
                        vehiclestatusToBeInfleet: true,
                        vehiclestatusUnsync: true,
                        optionBabySeat: true,
                        optionAirCo: true,
                        optionManual: true,
                        optionAutomatic: true,
                        energyElectric: true,
                        energyHybrid: true,
                        energyFuel: true,
                        model214: true,
                        model217: true,
                        model218: true,
                        model219: true,
                        model220: true
                    },
                    sideOptionsConversions: [
                        {
                            columnText: 'model',
                            columnField: 'model.id',
                            type: 'selection',
                            conversion: {
                                '214': 214,
                                '217': 217,
                                '218': 218,
                                '219': 219,
                                '220': 220
                            }
                        }
                    ]
                },
                fleet: {
                    allFleets: fleets
                },
                vehicles: {
                    allVehicles: vehicles
                }
            };
        });

        it('filters vehicles based on status', () => {
            const result = filterFleet(state);

            expect(result).toHaveLength(1);
            expect(result[0].fleetid).toEqual('f0001');
            expect(result[0].vehicules).toHaveLength(6);

            expect(result[0].vehicules[0].id).toEqual('53748d31-595c-475c-ab53-da7282d171df');
            expect(result[0].vehicules[1].id).toEqual('296acf01-a63e-4f95-b500-a983795787b1');
            expect(result[0].vehicules[2].id).toEqual('ee0a3c9c-cba8-4eb9-b7dc-05b912b57acb');
            expect(result[0].vehicules[3].id).toEqual('5c37c9c9-2b94-4a5a-97a3-d05e8a04188e');
            expect(result[0].vehicules[4].id).toEqual('77241380-6de3-4589-b88f-9eb50910f57d');
            expect(result[0].vehicules[5].id).toEqual('de26dd50-4653-46de-9a32-b2adf92a5919');
        });

        it('filters vehicles based on Available status', () => {
            state.supervisor.sideFilters.vehiclestatusInUse = false;
            state.supervisor.sideFilters.vehiclestatusOutOfService = false;
            state.supervisor.sideFilters.vehiclestatusArchived = false;
            state.supervisor.sideFilters.vehiclestatusToBeInfleet = false;
            state.supervisor.sideFilters.vehiclestatusUnsync = false;
            const result = filterFleet(state);

            expect(result).toHaveLength(1);
            expect(result[0].fleetid).toEqual('f0001');
            expect(result[0].vehicules).toHaveLength(4);
            expect(result[0].vehicules[0].id).toEqual('53748d31-595c-475c-ab53-da7282d171df');
            expect(result[0].vehicules[1].id).toEqual('5c37c9c9-2b94-4a5a-97a3-d05e8a04188e');
            expect(result[0].vehicules[2].id).toEqual('77241380-6de3-4589-b88f-9eb50910f57d');
            expect(result[0].vehicules[3].id).toEqual('de26dd50-4653-46de-9a32-b2adf92a5919');
        });

        it('filters vehicles based on electric energy type', () => {
            state.supervisor.sideFilters.energyHybrid = false;
            state.supervisor.sideFilters.energyFuel = false;

            const result = filterFleet(state);

            expect(result).toHaveLength(1);
            expect(result[0].fleetid).toEqual('f0001');
            expect(result[0].vehicules).toHaveLength(4);
            expect(result[0].vehicules[0].id).toEqual('53748d31-595c-475c-ab53-da7282d171df');
            expect(result[0].vehicules[1].id).toEqual('296acf01-a63e-4f95-b500-a983795787b1');
            expect(result[0].vehicules[2].id).toEqual('ee0a3c9c-cba8-4eb9-b7dc-05b912b57acb');
            expect(result[0].vehicules[3].id).toEqual('5c37c9c9-2b94-4a5a-97a3-d05e8a04188e');
        });

        it('filters vehicles based on car model Citroen C0', () => {
            state.supervisor.sideFilters.model214 = false;
            state.supervisor.sideFilters.model218 = false;
            state.supervisor.sideFilters.model219 = false;
            state.supervisor.sideFilters.model220 = false;

            const result = filterFleet(state);

            expect(result).toHaveLength(1);
            expect(result[0].fleetid).toEqual('f0001');
            expect(result[0].vehicules).toHaveLength(1);
            expect(result[0].vehicules[0].id).toEqual('296acf01-a63e-4f95-b500-a983795787b1');
        });
    });

});
