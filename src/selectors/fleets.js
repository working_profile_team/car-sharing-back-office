import { createSelector } from 'reselect';
import { VEHICLE_STATUSES_CONVERSION } from 'constants/Vehicles';

import Localize from 'components/Localize';

export const selectedFleetVehicleSelector = state => state.fleet.selectedFleetVehicle;
export const sendExportCommandSelector = state => state.fleet.sendExpertCommand;

export const selectedFleetVehicle = state => state.fleet.selectedFleetVehicle;
export const selectedFleetUser = state => state.fleet.selectedFleetUser;
export const showExpertCommandResponseSelector = state => state.fleet.showExpertCommandResponse;
export const expertCommandResponseSelector = state => state.fleet.expertCommandResponse;
export const vuboxesToIntegrateTableAllDataSelector = state => state.fleet.vuboxesToIntegrateTableAllData;
export const vuboxesToIntegrateTableCurrentPageSelector = state => state.fleet.vuboxesToIntegrateTableCurrentPage;

export const selectedFleetVehicleStatusNameSelector = createSelector(
    [selectedFleetVehicleSelector],
    selectedFleetVehicle => {
        const vehicleStatus = {
            key: '-1',
            state: '',
        };
        if (selectedFleetVehicle.id && selectedFleetVehicle.vehicle_status !== undefined) {
            vehicleStatus.key = selectedFleetVehicle.vehicle_status.toString();
        }

        for (const key in VEHICLE_STATUSES_CONVERSION) {
            if (vehicleStatus.key === VEHICLE_STATUSES_CONVERSION[key]) {
                vehicleStatus.state = Localize.locale.vehicles.fieldLabels.status[key]
                    ? Localize.locale.vehicles.fieldLabels.status[key]
                    : 'undefined';
                return vehicleStatus;
            }
        }
    }
);
