import { createSelector } from 'reselect';

const stateSelector = state => state.application;

const applicationLoadingSelector = state => state.application.loading.active;
const initialLoadCompletedSelector = state => state.application.initialLoadCompleted;
const userOffSelector = state => state.user.userOff;
const lastTokenValidSelector = state => state.user.lastTokenValid;

export const confirmDialogSelector = state => state.application.confirmDialog.active;
export const currency = state => state.application.currency;

export const shouldRenderAppSelector = createSelector(
    [userOffSelector, applicationLoadingSelector, initialLoadCompletedSelector],
    (userOff, applicationLoading) => userOff || !applicationLoading
);

export const showLoadingPopupSelector = createSelector(
    [userOffSelector, applicationLoadingSelector, initialLoadCompletedSelector, lastTokenValidSelector],
    (userOff, applicationLoading, initialLoadCompleted, lastTokenValid) =>
        applicationLoading || (!userOff && !initialLoadCompleted && lastTokenValid)
);

export const foLanguagesSelector = createSelector([stateSelector], state => state.foLanguages);
export const currentHostSelector = state => state.application.currentHost;

