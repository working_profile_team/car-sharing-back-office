import {all, fork} from "redux-saga/effects"


import {showSnackBarMessage} from './helpers/application.saga'

export default function* applicationSaga(apiClients) {
    yield all([
            fork(showSnackBarMessage, apiClients)
        ]);
}
