import { sendExpertCmdSaga, } from 'redux/sagas/fleet.saga'
import { ACTION_SNACKBAR_UPDATE, ACTION_SNACKBAR_OPEN } from 'redux/application/constants'
import {ACTION_SET_EXPERT_COMMAND_RESPONSE, ACTION_SHOW_EXPERT_COMMAND_RESPONSE} from "../../fleet/constants";

describe('fleetSaga', () => {

    describe('sendExpertCmdSaga', () => {

        it('Should send command and display a message', (done) => {

            const APIClients = {
                Fleet: {
                    sendExpertCmd: command => 'Response OK'
                },
            };

            const gen = sendExpertCmdSaga(APIClients);

            expect(gen.next().value.PUT.action.type).toEqual(ACTION_SET_EXPERT_COMMAND_RESPONSE);
            expect(gen.next().value.PUT.action.type).toEqual(ACTION_SHOW_EXPERT_COMMAND_RESPONSE);
            expect(gen.next().value.SELECT.args).toEqual([]);
            expect(gen.next('123').value.CALL.args[0]).toEqual('123');
            expect(gen.next().value.PUT.action.type).toEqual(ACTION_SET_EXPERT_COMMAND_RESPONSE);

            done();
        })

    });

});
