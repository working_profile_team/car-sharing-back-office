import {fetchAllUsersSaga} from '../users.saga'
import {cloneableGenerator} from 'redux-saga/utils';
import {call, put, take} from "redux-saga/effects"
import Localize from "components/Localize";

import {
    ACTION_FETCH_ALL_USERS_FAILURE,
    ACTION_FETCH_ALL_USERS_REQUEST,
    ACTION_FETCH_ALL_USERS_SUCCESS,
    ACTION_USERS_PAGE_CHANGE_INITIAL_STATE,
} from '../../users/constants'
import {ACTION_APPLICATION_ERROR} from '../../application/constants';

import {
    changeFlowState,
    getAllUsers,
    getSelectedUserData,
    getUserLastTrip,
    getUserRoles,
    setSelectedUser
} from '../helpers/users.saga'
import {applicationErrorSaga} from 'redux/application/actions';

describe('Sagas: fetchAllUsersSaga', () => {
    describe('Scenario: users not loaded yet', () => {
        it('dispatches all thunks and actions as expected', (done) => {
            const apiClients = {
                Users: {
                    listAllUsers: () => ([]),
                }
            };
            const gen = cloneableGenerator(fetchAllUsersSaga)(apiClients);
            expect(gen.next().value).toEqual(call(getAllUsers, apiClients));
            expect(gen.throw('Unexpected Error').value).toEqual(put(applicationErrorSaga('Unexpected Error')));
            expect(gen.next().value).toEqual(put({type: ACTION_FETCH_ALL_USERS_FAILURE}));
            done();
        });
    });
});


describe('Sagas: getAllUsers', () => {
    describe('Scenario: users not loaded yet', () => {
        it('dispatches all thunks and actions as expected', (done) => {
            const apiClients = {
                Users: {
                    listAllUsers: () => ([]),
                }
            };
            const gen = cloneableGenerator(getAllUsers)(apiClients);
            expect(gen.next().value).toEqual(take(ACTION_FETCH_ALL_USERS_REQUEST));
            expect(gen.next().value).toEqual(put({type: ACTION_USERS_PAGE_CHANGE_INITIAL_STATE}));
            expect(gen.next().value).toEqual(call(apiClients.Users.listAllUsers));
            const cloneAllUsersSuccess = gen.clone();
            const allUsers = apiClients.Users.listAllUsers();
            expect(cloneAllUsersSuccess.next(allUsers).value).toEqual(put({
                type: ACTION_FETCH_ALL_USERS_SUCCESS,
                payload: {allUsers: allUsers}
            }));
            done();
        });
    });
});
