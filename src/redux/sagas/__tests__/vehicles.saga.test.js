import { listWakeUpProviderSaga, } from 'redux/sagas/vehicles.saga';
import { ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS, } from 'redux/vehicles/constants';

describe('Vehicles Saga', () => {

    describe('listWakeUpProviderSaga()', () => {

        it('Should return a list of Wake Up Provider', () => {

            const APIClients = {
                Vehicles: {
                    listWakeUpProvider: () => [
                        { providerName: 'Provider 1', id: 'id-test-1', fleetId: 'fleetTest' },
                        { providerName: 'Provider 2', id: 'id-test-2', fleetId: 'fleetTest' },
                        { providerName: 'Provider 3', id: 'id-test-3', fleetId: 'fleetTest' },
                    ],
                },
            };

            const gen = listWakeUpProviderSaga(APIClients);

            expect(gen.next().value.CALL.args).toEqual([]);
            expect(gen.next().value.PUT.action.type).toEqual(ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS);
        });

    });

});
