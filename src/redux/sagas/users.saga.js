import {all, call, fork, put, select, take} from "redux-saga/effects"
import {
    ACTION_FETCH_ALL_USERS_FAILURE,
    ACTION_GET_SELECTED_USER_DOCUMENT_REQUEST,
    ACTION_GET_SELECTED_USER_DOCUMENT_SUCCESS,
    ACTION_GET_SELECTED_USER_DOCUMENT_FAILURE,
    ACTION_OPEN_EDIT_DOCUMENT_POPUP,
    ACTION_SELECT_USER_FAILURE,
    ACTION_SELECT_USER_REQUEST,
    ACTION_UPDATE_SELECTED_USER_DOCUMENT_FAILURE,
    ACTION_UPDATE_SELECTED_USER_DOCUMENT_REQUEST
} from '../users/constants'
import {applicationErrorSaga} from '../application/actions';
import {ACTION_SHOW_SNACK_BAR_MESSAGE_REQUEST} from '../application/constants'

import {
    changeFlowState,
    fetchAvailableProductCharges,
    fetchSelectedUserBilling,
    fetchUserEntityBalance,
    getAllUserSystemCredits,
    getAllUsers,
    getAllUserTrips,
    getSelectedUserData,
    getSelectedUserDocuments,
    getUserLastTrip,
    getUserRoles,
    setSelectedUser,
    updateSelectedUserDocument,
    usersSelector,
    extractDocuments,
    extractAvailableDocumentTypes
} from './helpers/users.saga'

import Localize from "components/Localize";
import * as _ from "lodash";

export const selectUserSaga = function* (apiClients) {
    while (true) {
        const {payload} = yield take(ACTION_SELECT_USER_REQUEST);
        try {
            const userData = yield call(getSelectedUserData, apiClients, payload);
            const userDocuments = yield call(getSelectedUserDocuments, apiClients, payload);
            const documents = extractDocuments(userDocuments);
            const availableDocumentTypes = extractAvailableDocumentTypes(userDocuments);
            const userRoles = yield call(getUserRoles, apiClients, payload);
            const userLastTrip = yield call(getUserLastTrip, apiClients, payload);
            const allUserTrips = yield call(getAllUserTrips, apiClients, payload);
            const allUserCredits = yield call(getAllUserSystemCredits, apiClients, payload);
            yield call(setSelectedUser, {
                ...userData,
                roles: userRoles,
                userLastTrip,
                allUserTrips,
                documents,
                availableDocumentTypes,
                allUserCredits
            });
            yield call(fetchUserEntityBalance, apiClients);
            yield call(fetchSelectedUserBilling, apiClients);
            yield call(fetchAvailableProductCharges, apiClients);
            yield call(changeFlowState, 'EDIT');
        } catch(e) {
            yield put(applicationErrorSaga(typeof e === 'string' ? e : Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError));
            yield put({type: ACTION_SELECT_USER_FAILURE})
        }
    }
};

export const fetchAllUsersSaga = function* (apiClients) {
    while (true) {
        try{
            yield call(getAllUsers, apiClients);
        } catch(e) {
            yield put(applicationErrorSaga(e));
            yield put({type: ACTION_FETCH_ALL_USERS_FAILURE})
        }
    }
};

export const updateSelectedUserDocumentSaga = function* (apiClients) {
    while (true){
        const { payload } = yield take(ACTION_UPDATE_SELECTED_USER_DOCUMENT_REQUEST);
        try {
            const documentType = payload.documentType;
            yield call(updateSelectedUserDocument, apiClients, {documentType});
            yield put({type: ACTION_SHOW_SNACK_BAR_MESSAGE_REQUEST, payload: {message: Localize.locale.users.messages.saved.document}});
            yield put({type: ACTION_GET_SELECTED_USER_DOCUMENT_REQUEST, documentType: documentType});
            yield take(ACTION_GET_SELECTED_USER_DOCUMENT_SUCCESS)
            yield put({type: ACTION_OPEN_EDIT_DOCUMENT_POPUP, payload: {documentType, open: false}})
        } catch(e) {
            yield put(applicationErrorSaga(e));
            yield put({type: ACTION_UPDATE_SELECTED_USER_DOCUMENT_FAILURE})
        }
    }
};


export const getSelectedUserDocument = function* (apiClients) {
    while(true){
    const {documentType} = yield take(ACTION_GET_SELECTED_USER_DOCUMENT_REQUEST);
        try {
            const retrievedUsersState = yield select(usersSelector);
            const selectedUser = retrievedUsersState.selectedUser;

            if (!selectedUser || !selectedUser.documents[documentType]) {
                throw new Error()
            }

            const selectedUserAllDocuments = yield call(apiClients.Users.getUserDocuments, selectedUser.id);

            if (!selectedUserAllDocuments) {
                throw new Error()
            }

            let selectedUserDocument = _.findLast(selectedUserAllDocuments.documents, (userDocument) => {
                return userDocument.documentType.type === documentType
            });

            yield call(setSelectedUser, {
                ...selectedUser,
                documents:{
                    ...selectedUser.documents,
                    [documentType]: selectedUserDocument,
                }
            });
            yield put({type: ACTION_GET_SELECTED_USER_DOCUMENT_SUCCESS, payload: selectedUserDocument});
        } catch(e) {
            yield put(applicationErrorSaga(e));
            yield put({type: ACTION_GET_SELECTED_USER_DOCUMENT_FAILURE})
        }
    }
};

export default function* userSaga(apiClients) {
    yield all([
        fork(selectUserSaga, apiClients),
        fork(fetchAllUsersSaga, apiClients),
        fork(getSelectedUserDocument, apiClients),
        fork(updateSelectedUserDocumentSaga, apiClients)
    ]);
}
