import { spawn, all } from 'redux-saga/effects';
import applicationSaga from 'redux/sagas/application.saga';
import usersSaga from 'redux/sagas/users.saga';
import fleetSaga from 'redux/sagas/fleet.saga';
import vehiclesSaga from 'redux/sagas/vehicles.saga';
import systemCreditsSaga from 'redux/sagas/systemcredits.saga';
import bookingRequestSaga from 'redux/sagas/bookingRequests.saga';
import tripsRequestSaga from 'redux/sagas/trips.saga';
import ticketsRequestSaga from 'redux/sagas/tickets.saga';

export default function* indexSaga(apiClients) {
    yield all([
        spawn(applicationSaga, apiClients),
        spawn(usersSaga, apiClients),
        spawn(fleetSaga, apiClients),
        spawn(vehiclesSaga, apiClients),
        spawn(bookingRequestSaga, apiClients),
        spawn(systemCreditsSaga, apiClients),
        spawn(tripsRequestSaga, apiClients),
        spawn(ticketsRequestSaga, apiClients),
    ]);
}
