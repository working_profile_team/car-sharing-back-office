import { all, race, call, fork, put, take, takeEvery } from "redux-saga/effects"
import {
    ACTION_SELECT_FLEET_VEHICLE_FAILURE,
    ACTION_SELECT_FLEET_VEHICLE_REQUEST,
    ACTION_SELECT_FLEET_VEHICLE_SUCCESS,
    ACTION_FETCH_ALL_FLEET_VEHICLES_REQUEST,
    ACTION_FETCH_ALL_FLEET_VEHICLES_SUCCESS,
    ACTION_FETCH_ALL_FLEET_VEHICLES_FAILURE,
    ACTION_CHANGE_REQUESTING_STATE,
} from '../fleet/constants';
import {
    ACTION_LOCK_VEHICLE_REQUEST,
    ACTION_LOCK_VEHICLE_SUCCESS,
    ACTION_LOCK_VEHICLE_FAILURE,
    ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST,
} from '../vehicles/constants';
import {
    ACTION_APPLICATION_ERROR
} from '../application/constants';
import Localize from "components/Localize";

import {
    listWakeUpProviderSuccess,
    listWakeUpProviderFailure,
} from 'redux/vehicles/actions';

export const lockVehicleSaga = function* (apiClients) {
    while (true) {
        const {payload} = yield take(ACTION_LOCK_VEHICLE_REQUEST);
        try {
            yield call(apiClients.Vehicles.lockUnlockVehicle, payload);
            yield put({type: ACTION_CHANGE_REQUESTING_STATE, payload: true});
            yield put({type: ACTION_FETCH_ALL_FLEET_VEHICLES_REQUEST});
            const {fetchAllFleetsError} = yield race({
                fetchAllFleetsSuccess: take(ACTION_FETCH_ALL_FLEET_VEHICLES_SUCCESS),
                fetchAllFleetsError: take(ACTION_FETCH_ALL_FLEET_VEHICLES_FAILURE)
            });
            if (fetchAllFleetsError) {
                yield put({type: ACTION_APPLICATION_ERROR,
                    payload: {
                        message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                        error: true
                    }
                });
                yield put({type: ACTION_LOCK_VEHICLE_FAILURE});
                continue;
            }
            yield put({type: ACTION_SELECT_FLEET_VEHICLE_REQUEST, payload: payload});
            const {selectFleetVehicleError} = yield race({
                selectFleetVehicleSuccess: take(ACTION_SELECT_FLEET_VEHICLE_SUCCESS),
                selectFleetVehicleError: take(ACTION_SELECT_FLEET_VEHICLE_FAILURE)
            });
            if (selectFleetVehicleError) {
                yield put({type: ACTION_APPLICATION_ERROR,
                    payload: {
                        message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                        error: true
                    }
                });
                yield put({type: ACTION_LOCK_VEHICLE_FAILURE});
                continue;
            }
            yield put({type: ACTION_CHANGE_REQUESTING_STATE, payload: false});
            yield put({type: ACTION_LOCK_VEHICLE_SUCCESS});
        } catch (e) {
            yield put({type: ACTION_APPLICATION_ERROR, payload: {message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError, error: true}});
            yield put({type: ACTION_LOCK_VEHICLE_FAILURE});
        }
    }
};

export const listWakeUpProviderSaga = function* (apiClients) {
    try {
        const response = yield call(apiClients.Vehicles.listWakeUpProvider);
        yield put(listWakeUpProviderSuccess(response));
    } catch (err) {
        console.error('[listWakeUpProviderSage] - FAIL : ', err.response);
        yield put(listWakeUpProviderFailure(err.response.statusText));
    }
};

export default function* vehiclesSaga(apiClients) {
    yield all([
        takeEvery(ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST, listWakeUpProviderSaga, apiClients),
        fork(lockVehicleSaga, apiClients)
    ]);
}
