import {put, take} from "redux-saga/effects"
import Localize from "../../../components/Localize";
import {applicationErrorSaga} from '../../application/actions';

import {
    ACTION_SHOW_SNACK_BAR_MESSAGE_FAILURE,
    ACTION_SHOW_SNACK_BAR_MESSAGE_REQUEST,
    ACTION_SHOW_SNACK_BAR_MESSAGE_SUCCESS
} from '../../application/constants';

export const showSnackBarMessage = function* (apiClients) {
    while (true){
        const { payload } = yield take(ACTION_SHOW_SNACK_BAR_MESSAGE_REQUEST);
        let snackBarMessage = Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError;
        if (payload.message) {
            snackBarMessage = payload.message
        }
        try {
            yield put({type: ACTION_SHOW_SNACK_BAR_MESSAGE_SUCCESS, payload: {message: snackBarMessage}});
        } catch (e) {
            yield put(applicationErrorSaga(e));
            yield put({type: ACTION_SHOW_SNACK_BAR_MESSAGE_FAILURE})
        }
    }
};
