import { call, put, race, select, take } from 'redux-saga/effects';
import {
    ACTION_CHANGE_FLOW_STATE,
    ACTION_FETCH_ALL_USERS_FAILURE,
    ACTION_FETCH_ALL_USERS_REQUEST,
    ACTION_FETCH_ALL_USERS_SUCCESS,
    ACTION_SELECT_USER_SUCCESS,
    ACTION_SET_AVAILABLE_CHARGE_PRODUCTS,
    ACTION_SET_SELECTED_USER_BILLING,
    ACTION_USERS_PAGE_CHANGE_INITIAL_STATE,
} from 'redux/users/constants';
import {
    ACTION_SELECT_FLEET_VEHICLE_FAILURE,
    ACTION_SELECT_FLEET_VEHICLE_REQUEST,
    ACTION_SELECT_FLEET_VEHICLE_SUCCESS,
    ACTION_UNSELECT_FLEET_VEHICLE,
} from 'redux/fleet/constants';
import * as _ from 'lodash';
import moment from 'moment';

export const usersSelector = state => state.users;

export const getSelectedUserData = function*(apiClients, payload) {
    const retrievedUsersState = yield select(usersSelector);
    const userId = retrievedUsersState.selectedUser.id;
    const userFetched = retrievedUsersState.allUsers.length > 0;
    if (!userId || !userFetched || retrievedUsersState.allUsers.find(u => u.id === userId) === undefined) {
        yield put({ type: ACTION_FETCH_ALL_USERS_REQUEST });
        const { fetchAllUsersError } = yield race({
            fetchAllUsersSuccess: take(ACTION_FETCH_ALL_USERS_SUCCESS),
            fetchAllUsersError: take(ACTION_FETCH_ALL_USERS_FAILURE),
        });
        if (fetchAllUsersError) {
            throw new Error();
        }
    }
    const userData = yield call(apiClients.Users.getUser, payload);
    if (userData.profiles.length > 0 && userData.profiles[0].email !== null) {
        userData.email = userData.profiles[0].email;
    }
    return userData;
};

export const getSelectedUserDocuments = function*(apiClients, payload) {
    return yield call(apiClients.Users.getUserDocuments, payload);
};

export const getUserRoles = function*(apiClients, payload) {
    return yield call(apiClients.Users.getUserRoles, payload);
};

export const getUserLastTrip = function*(apiClients, payload) {
    yield put({ type: ACTION_UNSELECT_FLEET_VEHICLE });
    const trips = yield call(apiClients.Users.getTripsOfUser, { userId: payload });
    const userTrips = trips.sort((a, b) => (moment(b.date).isSameOrBefore(moment(a.date)) ? -1 : 1));
    const lastTrip = userTrips.length > 0 ? userTrips[0] : undefined;

    if (lastTrip && lastTrip.vehicleId) {
        yield put({ type: ACTION_SELECT_FLEET_VEHICLE_REQUEST, payload: { vehicleId: lastTrip.vehicleId } });
        const { selectFleetVehicleError } = yield race({
            selectFleetVehicleSuccess: take(ACTION_SELECT_FLEET_VEHICLE_SUCCESS),
            selectFleetVehicleError: take(ACTION_SELECT_FLEET_VEHICLE_FAILURE),
        });
        if (selectFleetVehicleError) {
            return undefined;
        }
    }
    return lastTrip;
};

export const getAllUserTrips = function*(apiClients, payload) {
    return yield call(apiClients.Users.getTripsOfUser, { userId: payload });
};

export const getAllUserPromoCodes = function*(apiClients, payload) {
    return yield call(apiClients.Users.getUserCodes, { userId: payload });
};

export const getAllUserSystemCredits = function*(apiClients, payload) {
    return yield call(apiClients.Users.getUserCodes, { userId: payload });
};

export const fetchUserEntityBalance = function*(apiClients) {
    const retrievedUsersState = yield select(usersSelector);
    const selectedUser = retrievedUsersState.selectedUser;

    const isvalidBillingData =
        selectedUser &&
        selectedUser.profiles &&
        selectedUser.profiles.length > 0 &&
        selectedUser.profiles[0] &&
        selectedUser.profiles[0].entity &&
        selectedUser.profiles[0].entity.id;

    if (!isvalidBillingData) {
        throw new Error();
    }

    const entityId = selectedUser.profiles[0].entity.id;
    const entityBalance = yield call(apiClients.Users.getEntityBalance, { entityId });
    const customerBalance = parseFloat(entityBalance.amount || '0');
    yield put({ type: ACTION_SET_SELECTED_USER_BILLING, payload: { billing: { customerBalance } } });
};

export const fetchSelectedUserBilling = function*(apiClients) {
    const retrievedUsersState = yield select(usersSelector);
    const selectedUser = retrievedUsersState.selectedUser;

    const isvalidBillingData =
        selectedUser &&
        selectedUser.profiles &&
        selectedUser.profiles.length > 0 &&
        selectedUser.profiles[0] &&
        selectedUser.profiles[0].entity &&
        selectedUser.profiles[0].entity.id;

    if (!isvalidBillingData) {
        throw new Error();
    }

    const entityId = selectedUser.profiles[0].entity.id;
    const billing = selectedUser.billing;
    const now = moment().utc();
    const momentDate = now
        .clone()
        .set('month', billing.month - 1)
        .set('year', billing.year);
    let startDateMoment = momentDate.clone().startOf('month');
    if (startDateMoment.isAfter(now)) {
        startDateMoment = now.clone();
    }

    let endDateMoment = momentDate.clone().endOf('month');

    const searchFields = {
        startDate: startDateMoment.format('YYYY-MM-DD'),
        endDate: endDateMoment.format('YYYY-MM-DD'),
    };
    const selectedUserBilling = yield call(apiClients.Users.getSelectedUserBilling, {
        entityId,
        searchFields,
    });
    const allBillingOtherCharges = yield call(apiClients.Users.getSelectedUserOtherCharges, {
        entityId,
        searchFields,
    });
    const allBillingTrips = selectedUserBilling.Billing;
    yield put({
        type: ACTION_SET_SELECTED_USER_BILLING,
        payload: { billing: { allBillingTrips, allBillingOtherCharges } },
    });
};

export const fetchAvailableProductCharges = function*(apiClients) {
    const availableProductCharges = yield call(apiClients.Users.getAvailableProductCharges);
    yield put({ type: ACTION_SET_AVAILABLE_CHARGE_PRODUCTS, payload: availableProductCharges });
};

export const changeFlowState = function*(payload) {
    yield put({ type: ACTION_CHANGE_FLOW_STATE, payload });
};

export const setSelectedUser = function*(payload) {
    yield put({ type: ACTION_SELECT_USER_SUCCESS, payload });
};

export const getAllUsers = function*(apiClients) {
    yield take(ACTION_FETCH_ALL_USERS_REQUEST);
    yield put({ type: ACTION_USERS_PAGE_CHANGE_INITIAL_STATE });

    const allUsers = yield call(apiClients.Users.listAllUsers);

    if (!allUsers) {
        throw new Error();
    }
    yield put({ type: ACTION_FETCH_ALL_USERS_SUCCESS, payload: { allUsers } });
};

export const updateSelectedUserDocument = function*(apiClients, { documentType }) {
    const retrievedUsersState = yield select(usersSelector);
    const selectedUser = retrievedUsersState.selectedUser;
    const userId = selectedUser.id;
    const document = selectedUser.documents[documentType];

    if (selectedUser && document) {
        if (!document.id) {
            const documentToSave = {
                document: {
                    documentNumber: document.documentNumber,
                    documentType,
                    expirationDate: document.expirationDate,
                },
                userId,
            };

            yield call(apiClients.Users.createDocument, documentToSave);
        } else {
            const documentToSave = {
                document: {
                    number: document.documentNumber,
                    documentType,
                    expirationDate: document.expirationDate,
                    id: document.id,
                },
                userId,
            };

            yield call(apiClients.Users.updateDocument, documentToSave);
        }
    }
};

export const extractDocuments = userDocuments => {
    const documents = {};
    _.forEach(
        userDocuments.documents.filter(document => !!document.documentType.type),
        userDocument =>
            (documents[userDocument.documentType.type] = {
                ...Document,
                ...userDocument,
            })
    );
    return documents;
};

export const extractAvailableDocumentTypes = userDocuments =>
    userDocuments.documentTypes.filter(documentType => !!documentType.type);
