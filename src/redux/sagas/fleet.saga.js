import { all, race, call, fork, put, take, takeEvery, select } from 'redux-saga/effects';
import { sendExportCommandSelector } from 'selectors/fleets';
import { actionApplicationError } from 'redux/fleet/actions';
import { showExpertCommandResponse, setExpertCommandResponse } from 'redux/fleet/actions';
import FunctionHelpers from 'helpers/FunctionHelpers';
import {
    ACTION_SELECT_FLEET_VEHICLE_FAILURE,
    ACTION_SELECT_FLEET_VEHICLE_REQUEST,
    ACTION_SELECT_FLEET_VEHICLE_SUCCESS,
    ACTION_START_TRIP_REQUEST,
    ACTION_START_TRIP_SUCCESS,
    ACTION_START_TRIP_FAILURE,
    ACTION_END_TRIP_REQUEST,
    ACTION_END_TRIP_SUCCESS,
    ACTION_END_TRIP_FAILURE,
    ACTION_FETCH_ALL_FLEET_VEHICLES_REQUEST,
    ACTION_FETCH_ALL_FLEET_VEHICLES_SUCCESS,
    ACTION_FETCH_ALL_FLEET_VEHICLES_FAILURE,
    ACTION_CHANGE_REQUESTING_STATE,
    ACTION_SEND_EXPERT_COMMAND,
} from '../fleet/constants';
import { ACTION_APPLICATION_ERROR } from '../application/constants';
import Localize from 'components/Localize';

export const selectFleetVehicleSaga = function*(apiClients) {
    while (true) {
        const { payload } = yield take(ACTION_SELECT_FLEET_VEHICLE_REQUEST);
        try {
            const fleetVehicle = yield call(apiClients.Fleet.fetchVehicleById, payload.vehicleId);

            if (fleetVehicle) {
                const allZoneIds = fleetVehicle.zones.filter(zone => zone.type === 'allowed').map(zone => zone.zoneId);
                if (allZoneIds.length > 0) {
                    try {
                        const zoneIds = FunctionHelpers.removeDuplicatesInArray(allZoneIds);
                        let zones = yield all(zoneIds.map(zoneId => call(apiClients.Zones.getZone, zoneId)));
                        zones = zoneIds.map((zoneId, index) => ({
                            ...zones[index],
                            id: zoneId,
                        }));

                        fleetVehicle.zones = fleetVehicle.zones.map(zone => {
                            // Find the zone
                            const matchedZone = zones.find(z => zone.zoneId === z.id);
                            // Find the version (within the matched zone)
                            const matchedVersion = matchedZone.features.find(
                                f => f.properties.version === zone.version
                            );
                            // Extract name property only, for now
                            const { name } = matchedVersion.properties;
                            // Return new zone object, with name
                            return {
                                ...zone,
                                name,
                            };
                        });
                    } catch (error) {
                        // Non-critical, but should probably report this somewhere
                    }
                }

                yield call(apiClients.Mapbox.tryGeocodingOnVehicle, fleetVehicle);
                yield put({ type: ACTION_SELECT_FLEET_VEHICLE_SUCCESS, payload: fleetVehicle });
            } else {
                yield put({
                    type: ACTION_APPLICATION_ERROR,
                    payload: {
                        message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                        error: true,
                    },
                });
                yield put({ type: ACTION_SELECT_FLEET_VEHICLE_FAILURE });
            }
        } catch (e) {
            yield put({
                type: ACTION_APPLICATION_ERROR,
                payload: {
                    message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                    error: true,
                },
            });
            yield put({ type: ACTION_SELECT_FLEET_VEHICLE_FAILURE });
        }
    }
};

export const fetchAllFleetsSaga = function*(apiClients) {
    while (true) {
        yield take(ACTION_FETCH_ALL_FLEET_VEHICLES_REQUEST);
        try {
            const allFleets = yield call(apiClients.Fleet.fetchAllFleetVehicles);

            if (allFleets) {
                yield put({ type: ACTION_FETCH_ALL_FLEET_VEHICLES_SUCCESS, payload: { allFleets } });
            } else {
                yield put({
                    type: ACTION_APPLICATION_ERROR,
                    payload: {
                        message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                        error: true,
                    },
                });
                yield put({ type: ACTION_FETCH_ALL_FLEET_VEHICLES_FAILURE });
            }
        } catch (e) {
            yield put({
                type: ACTION_APPLICATION_ERROR,
                payload: {
                    message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                    error: true,
                },
            });
            yield put({ type: ACTION_FETCH_ALL_FLEET_VEHICLES_FAILURE });
        }
    }
};

export const startTripSaga = function*(apiClients) {
    while (true) {
        const { payload } = yield take(ACTION_START_TRIP_REQUEST);
        try {
            yield call(apiClients.Fleet.startTrip, payload);
            yield put({ type: ACTION_CHANGE_REQUESTING_STATE, payload: true });
            yield put({ type: ACTION_FETCH_ALL_FLEET_VEHICLES_REQUEST });
            const { fetchAllFleetsError } = yield race({
                fetchAllFleetsSuccess: take(ACTION_FETCH_ALL_FLEET_VEHICLES_SUCCESS),
                fetchAllFleetsError: take(ACTION_FETCH_ALL_FLEET_VEHICLES_FAILURE),
            });
            if (fetchAllFleetsError) {
                yield put({
                    type: ACTION_APPLICATION_ERROR,
                    payload: {
                        message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                        error: true,
                    },
                });
                yield put({ type: ACTION_START_TRIP_FAILURE });
                continue;
            }
            yield put({ type: ACTION_SELECT_FLEET_VEHICLE_REQUEST, payload });
            const { selectFleetVehicleError } = yield race({
                selectFleetVehicleSuccess: take(ACTION_SELECT_FLEET_VEHICLE_SUCCESS),
                selectFleetVehicleError: take(ACTION_SELECT_FLEET_VEHICLE_FAILURE),
            });
            if (selectFleetVehicleError) {
                yield put({
                    type: ACTION_APPLICATION_ERROR,
                    payload: {
                        message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                        error: true,
                    },
                });
                yield put({ type: ACTION_START_TRIP_FAILURE });
                continue;
            }
            yield put({ type: ACTION_CHANGE_REQUESTING_STATE, payload: false });
            yield put({ type: ACTION_START_TRIP_SUCCESS });
        } catch (e) {
            yield put({
                type: ACTION_APPLICATION_ERROR,
                payload: {
                    message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                    error: true,
                },
            });
            yield put({ type: ACTION_START_TRIP_FAILURE });
        }
    }
};

export const endTripSaga = function*(apiClients) {
    while (true) {
        const { payload } = yield take(ACTION_END_TRIP_REQUEST);
        try {
            yield call(apiClients.Fleet.endTrip, payload);
            yield put({ type: ACTION_CHANGE_REQUESTING_STATE, payload: true });
            yield put({ type: ACTION_FETCH_ALL_FLEET_VEHICLES_REQUEST });
            const { fetchAllFleetsError } = yield race({
                fetchAllFleetsSuccess: take(ACTION_FETCH_ALL_FLEET_VEHICLES_SUCCESS),
                fetchAllFleetsError: take(ACTION_FETCH_ALL_FLEET_VEHICLES_FAILURE),
            });
            if (fetchAllFleetsError) {
                yield put({
                    type: ACTION_APPLICATION_ERROR,
                    payload: {
                        message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                        error: true,
                    },
                });
                yield put({ type: ACTION_END_TRIP_FAILURE });
                continue;
            }
            yield put({ type: ACTION_SELECT_FLEET_VEHICLE_REQUEST, payload });
            const { selectFleetVehicleError } = yield race({
                selectFleetVehicleSuccess: take(ACTION_SELECT_FLEET_VEHICLE_SUCCESS),
                selectFleetVehicleError: take(ACTION_SELECT_FLEET_VEHICLE_FAILURE),
            });
            if (selectFleetVehicleError) {
                yield put({
                    type: ACTION_APPLICATION_ERROR,
                    payload: {
                        message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                        error: true,
                    },
                });
                yield put({ type: ACTION_END_TRIP_FAILURE });
                continue;
            }
            yield put({ type: ACTION_CHANGE_REQUESTING_STATE, payload: false });
            yield put({ type: ACTION_END_TRIP_SUCCESS });
        } catch (e) {
            yield put({
                type: ACTION_APPLICATION_ERROR,
                payload: {
                    message: Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
                    error: true,
                },
            });
            yield put({ type: ACTION_END_TRIP_FAILURE });
        }
    }
};

export const sendExpertCmdSaga = function*(apiClients) {
    const { thereWasAnUnexpectedError } = Localize.locale.generalValidations.errorMessages;
    try {
        yield put(setExpertCommandResponse(Localize.locale.vehicles.summary.commands.requesting));
        yield put(showExpertCommandResponse());
        const command = yield select(sendExportCommandSelector);
        const response = yield call(apiClients.Fleet.sendExpertCmd, command);
        yield put(setExpertCommandResponse(response || thereWasAnUnexpectedError));
    } catch (err) {
        console.error('[sendExpertCmdSaga] - FAIL : ', err.response);
        let errorMessage = null;
        if (err && err.response && err.response.data && err.response.data.message) {
            errorMessage = err.response.data.message;
        }
        yield put(actionApplicationError(errorMessage));
    }
};

export default function* userSaga(apiClients) {
    yield all([
        takeEvery(ACTION_SEND_EXPERT_COMMAND, sendExpertCmdSaga, apiClients),
        fork(selectFleetVehicleSaga, apiClients),
        fork(fetchAllFleetsSaga, apiClients),
        fork(startTripSaga, apiClients),
        fork(endTripSaga, apiClients),
    ]);
}
