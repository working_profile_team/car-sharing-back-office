import {
    ACTION_CHANGE_FILTER,
    ACTION_CHANGE_QUERY,
    ACTION_CHANGE_SIDE_FILTERS,
    ACTION_REMOVE_SELECTIONS,
    ACTION_SEARCH_SELECTED,
    ACTION_UPDATE_SIDE_QUERY_CONVERSION,
    ACTION_INIT_EXTRA_FILTERS,
    ACTION_SHOW_FILTERS,
} from './constants';

const initialState = {
    /*
        last filter computed from the last query
    */
    filter: '',
    /*
        Last valid query sended by user
    */
    query: {},
    /*
        Array of selections
    */
    selection: undefined,

    sideFilters: {
        vehiclestatusAvailable: true,
        vehiclestatusInUse: true,
        vehiclestatusOutOfService: true,
        vehiclestatusArchived: false,
        vehiclestatusToBeInfleet: true,
        vehiclestatusUnsync: true,
        optionBabySeat: true,
        optionAirCo: true,
        optionManual: true,
        optionAutomatic: true,
        energyElectric: true,
        energyHybrid: true,
        energyFuel: true,
    },

    sideOptionsConversions: [],

    selectAllSideFilters: false,
};

export default function state(state = initialState, action) {
    let newState = Object.assign({}, state);
    switch (action.type) {
        case ACTION_REMOVE_SELECTIONS:
            newState.selection = undefined;
            break;

        case ACTION_SEARCH_SELECTED:
            newState.selection = action.payload;
            break;

        case ACTION_CHANGE_QUERY:
            newState.query = action.payload;
            break;

        case ACTION_CHANGE_FILTER:
            newState.filter = action.payload;
            break;

        case ACTION_CHANGE_SIDE_FILTERS:
            newState.sideFilters = { ...newState.sideFilters, ...action.payload.sideFilters };
            newState.selectAllSideFilters = action.payload.selectAllSideFilters;
            break;

        case ACTION_UPDATE_SIDE_QUERY_CONVERSION:
            newState.sideOptionsConversions = [...newState.sideOptionsConversions, ...action.payload];
            break;

        case ACTION_INIT_EXTRA_FILTERS:
            newState.sideFilters = { ...newState.sideFilters, ...action.payload };
            break;

        case ACTION_SHOW_FILTERS:
            newState.filtersVisibility = action.payload;
            break;

        default:
            newState = state;
            break;
    }
    return Object.assign({}, state, newState);
}
