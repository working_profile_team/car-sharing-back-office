import _ from 'lodash';

import {Options} from "constants/Query";
import {
    ACTION_CHANGE_FILTER,
    ACTION_CHANGE_SIDE_FILTERS,
    ACTION_CHANGE_QUERY,
    ACTION_REMOVE_SELECTIONS,
    ACTION_SEARCH_SELECTED,
    ACTION_UPDATE_SIDE_QUERY_CONVERSION,
    ACTION_INIT_EXTRA_FILTERS,
    ACTION_SHOW_FILTERS
} from './constants';

export function trySearchSelected(payload) {
    return (dispatch, state, APIClients) => {
        return new Promise((resolve, reject) => {
            const fleets = state().fleet.allFleets;
            let vehicle = undefined;

            _.each(fleets, (fleet) => {
                vehicle = _.find(fleet.vehicules, (o) => o.id === payload.vehicle_id);
                if (vehicle !== undefined) {
                    return false;
                }
            });

            if (vehicle !== undefined) {
                APIClients.Mapbox.tryGeocodingOnVehicle(vehicle)
                    .then((vehicle) => {
                        dispatch({
                            type: ACTION_SEARCH_SELECTED,
                            payload: vehicle
                        });
                        resolve();
                    });
            } else {
                reject(new Error("Vehicule not found"));
            }

        });
    }
}


export function tryRemoveSelection() {
    return (dispatch, state, APIClients) => {
        return new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_REMOVE_SELECTIONS,
            })
            resolve();
        })
    }
}

export function changeSideFilters(payload) {
    return (dispatch, state, APIClients) => {
        return new Promise((resolve, reject) => {
            let sideFilters = state().supervisor.sideFilters;

            let areAllSelected = true;

            if (payload.hasOwnProperty('changeFilterKey')) {
                const filterKey = payload.changeFilterKey;
                sideFilters[filterKey] = !sideFilters[filterKey];

                for (let key in sideFilters) {
                    if (sideFilters[key] === false) {
                        areAllSelected = false;
                        break;
                    }
                }
            }

            if (payload.hasOwnProperty('changeSelectAll')) {
                areAllSelected = payload.changeSelectAll;
                sideFilters = _.mapValues(sideFilters, () => {
                    return (areAllSelected)
                });
            }

            dispatch({
                type: ACTION_CHANGE_SIDE_FILTERS,
                payload: {
                    selectAllSideFilters: areAllSelected,
                    sideFilters
                }
            });
            resolve();
        })
    }
}

export function updateSideQueryConversions() {
    return (dispatch, state, APIClients) => {
        return new Promise((resolve, reject) => {
            const options = [];

            if (state().vehicles.allModels.length > 0) {
                let modelsConversion = {
                    columnText: "model",
                    columnField: "model.id",
                    type: "selection",
                    conversion: {}
                };

                for (let m of state().vehicles.allModels) {
                    if (m && m.name && m.id) {
                        modelsConversion.conversion[m.id] = m.id;
                    }
                }

                options.push(modelsConversion);
            }

            //TODO do the same for vehicle options

            dispatch({
                type: ACTION_UPDATE_SIDE_QUERY_CONVERSION,
                payload: options
            });
            resolve();
        })
    }
}

export function initExtraFilters() {
    return (dispatch, state, APIClients) => {
        return new Promise((resolve, reject) => {
            const selectAll = state().supervisor.selectAllSideFilters;

            let extraFilters = {};

            if (state().vehicles.allModels.length > 0) {
                for (let model of state().vehicles.allModels) {
                    if (model && model.id && model.name){
                        extraFilters[`model${model.id}`] = selectAll;
                    }
                }
            }

            if (state().vehicles.allOptions.length > 0) {
                for (let m of state().vehicles.allOptions) {
                    extraFilters[`option${m.id}`] = selectAll;
                }
            }

            dispatch({
                type: ACTION_INIT_EXTRA_FILTERS,
                payload: extraFilters
            });
            resolve();
        })
    }
}

//  TODO manage ()
export function tryComputeQuery(payload) {
    return (dispatch, state, APIClients) => {
        return new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_QUERY,
                payload,
            });

            /*
                One crazy weird way to make eval
            */
            let stringConditionEval = "";

            const getOptionFromName = (name) => {
                return _.filter(Options, (o) => {
                    return o.columnText === name;
                })[0];
            };

            const filterCondition = (condition, expression = false) => {
                let options = getOptionFromName(condition.category);
                let value = condition.value;

                if (options.type === "selection") {
                    value = options.conversion[condition.value];
                }

                let localConditionString = "";

                switch (condition.operator) { //category value
                    case "==":
                        //  to string for better condition generic
                        localConditionString += "( (o." + options.columField + ".toString()) == '" + value.toString() + "' ) ";
                        break;
                    case "!=":
                        localConditionString += "( (o." + options.columField + ".toString()) != '" + value.toString() + "' ) ";
                        break;
                    case "contains":
                        localConditionString += "( (o." + options.columField + ".toString()).indexOf('" + value + "') > -1 ) ";
                        // localConditionString += "( _.filter(fleet.vehicules, (e) => { return e."+ options.columField +".toString().indexOf('"+ value +"');  }).length > -1 ) ";
                        break;
                    case "!contains":
                        localConditionString += "( (o." + options.columField + ".toString()).indexOf('" + value + "') == -1 ) ";
                        // localConditionString += "( _.filter(fleet.vehicules, (e) => { return e."+ options.columField +".toString().indexOf('"+ value +"');  }).length == -1 ) ";
                        break;
                    default:
                        break;
                }

                if (condition.hasOwnProperty("conditionType")) {
                    switch (condition.conditionType) {
                        case "AND":
                            localConditionString = " && " + localConditionString;
                            break;
                        case "OR":
                            localConditionString = " || " + localConditionString;
                            break;
                        default:
                            break;
                    }
                }

                stringConditionEval += localConditionString;
            }

            //  TODO recursive
            // const recursiveResolver = (array) => {
            //     filterCondition(array.expressions, true);
            // };

            payload.map((condition) => {
                if (condition.hasOwnProperty("expression")) {
                    // doArray(condition.expressions);
                    // recursiveResolver(condition)
                } else {
                    filterCondition(condition)
                }
                return true
            });

            dispatch({
                type: ACTION_CHANGE_FILTER,
                payload: stringConditionEval
            })

            resolve();
        });
    };
}

export function showFilters(payload) {
    return {
        type: ACTION_SHOW_FILTERS,
        payload
    }
}
