import {
    ACTION_CHANGE_FLOW_STATE,
    ACTION_CHANGE_REQUESTING_STATE,
    ACTION_FETCH_ALL_FLEETS,
    ACTION_FETCH_ALL_VUBOXES_TO_INTEGRATE,
    ACTION_GET_LIST_EXPERT_COMMANDS,
    ACTION_SET_EXPERT_COMMAND_RESPONSE,
    ACTION_SET_PASSTHROUGH_COMMAND_RESPONSE,
    ACTION_SET_SELECTED_FLEET_VEHICLE,
    ACTION_SET_SELECTED_VUBOX_TO_INTEGRATE,
    ACTION_SHOW_EXPERT_COMMAND_RESPONSE,
    ACTION_SHOW_FLEET_VEHICLE_DETAIL,
    ACTION_SHOW_VUBOX_TO_INTEGRATE_DETAIL,
    ACTION_UNSELECT_FLEET_VEHICLE,
    ACTION_UNSELECT_VUBOX_TO_INTEGRATE,
    ACTION_VUBOXES_TO_INTEGRATE_PAGE_CHANGE,
    ACTION_VUBOXES_TO_INTEGRATE_PAGE_CHANGE_INITIAL_STATE,
    ACTION_SELECT_FLEET_VEHICLE_SUCCESS,
    ACTION_SELECT_FLEET_VEHICLE_FAILURE,
    ACTION_SELECT_FLEET_USER_INFO,
    ACTION_SEND_EXPERT_COMMAND,
    ACTION_PING_VEHICLE,
} from './constants';

import { FleetStore } from './models/FleetStore.model';
import { VuboxesToIntegrate } from './models/VuboxesToIntegrate.model';
import { VuboxesToIntegrateTable } from './models/VuboxesToIntegrateTable.model';
import { FleetVehicle } from './models/FleetVehicle.model';

const fleetStore = FleetStore;

export default function state(state = fleetStore, action) {
    let newState = Object.assign({}, state);
    switch (action.type) {
        case ACTION_PING_VEHICLE:
            fleetStore.pong = action.payload;
            break;
        case ACTION_VUBOXES_TO_INTEGRATE_PAGE_CHANGE_INITIAL_STATE:
            fleetStore.vuboxesToIntegrateTableCurrentPage = Object.assign({}, VuboxesToIntegrateTable);
            break;

        case ACTION_FETCH_ALL_FLEETS:
            fleetStore.allFleets = action.payload;
            fleetStore.lastFetch = new Date();
            break;

        case ACTION_FETCH_ALL_VUBOXES_TO_INTEGRATE:
            fleetStore.allVuboxesToIntegrate = action.payload.allVuboxesToIntegrate;
            fleetStore.vuboxesToIntegrateTableAllData = action.payload.vuboxesToIntegrateTableAllData;
            fleetStore.vuboxesToIntegrateTableCurrentPage = {
                ...newState.vuboxesToIntegrateTableCurrentPage,
                ...action.payload.vuboxesToIntegrateTableCurrentPage,
            };
            break;

        case ACTION_CHANGE_REQUESTING_STATE:
            fleetStore.isRequestingFleet = action.payload;
            break;

        case ACTION_VUBOXES_TO_INTEGRATE_PAGE_CHANGE:
            fleetStore.vuboxesToIntegrateTableCurrentPage = {
                ...newState.vuboxesToIntegrateTableCurrentPage,
                ...action.payload,
            };
            break;

        case ACTION_UNSELECT_FLEET_VEHICLE:
            fleetStore.selectedFleetVehicle = { ...FleetVehicle };
            break;

        case ACTION_SHOW_FLEET_VEHICLE_DETAIL:
            fleetStore.showFleetVehicleDetail = action.payload;
            break;

        case ACTION_SHOW_VUBOX_TO_INTEGRATE_DETAIL:
            fleetStore.showVuboxToIntegrateDetail = action.payload;
            break;

        case ACTION_SET_SELECTED_VUBOX_TO_INTEGRATE:
            fleetStore.selectedVuboxToIntegrate = { ...action.payload };
            break;

        case ACTION_UNSELECT_VUBOX_TO_INTEGRATE:
            fleetStore.selectedVehicle = Object.assign({}, VuboxesToIntegrate);
            break;

        case ACTION_CHANGE_FLOW_STATE:
            fleetStore.flowState = action.payload;
            break;

        case ACTION_GET_LIST_EXPERT_COMMANDS:
            fleetStore.listExpertCommands = action.payload;
            break;

        case ACTION_SET_EXPERT_COMMAND_RESPONSE:
            if (typeof action.payload === 'object') {
                fleetStore.expertCommandResponse = action.payload.wakeUpNeeded ?
                    'Error: wake up is needed' :
                    JSON.stringify(action.payload);
            } else {
                fleetStore.expertCommandResponse = action.payload;
            }
            break;

        case ACTION_SHOW_EXPERT_COMMAND_RESPONSE:
            fleetStore.showExpertCommandResponse = action.payload;
            break;

        case ACTION_SET_PASSTHROUGH_COMMAND_RESPONSE:
            if (typeof action.payload === 'object' && action.payload) {
                fleetStore.passthroughCommandResponse = JSON.stringify(action.payload);
            } else {
                fleetStore.passthroughCommandResponse = action.payload;
            }
            break;

        case ACTION_SELECT_FLEET_VEHICLE_SUCCESS:
            fleetStore.selectedFleetVehicle = { ...action.payload };
            break;

        case ACTION_SELECT_FLEET_VEHICLE_FAILURE:
            fleetStore.selectedFleetVehicle = { ...FleetVehicle };
            break;

        case ACTION_SET_SELECTED_FLEET_VEHICLE:
            fleetStore.selectedFleetVehicle = { ...action.payload };
            break;

        case ACTION_SELECT_FLEET_USER_INFO:
            fleetStore.selectedFleetUser = action.payload;
            break;

        case ACTION_SEND_EXPERT_COMMAND:
            fleetStore.sendExpertCommand = action.payload;
            break;

        default:
            newState = state;
            break;
    }
    newState = fleetStore;
    return Object.assign({}, state, newState);
}
