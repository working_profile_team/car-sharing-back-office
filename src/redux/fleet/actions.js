import React from 'react';
import _ from 'lodash';
import FunctionHelpers from 'helpers/FunctionHelpers';
import * as ErrorHandler from 'apis/VuLog/ErrorHandler';
import DateDisplay from 'containers/DateDisplay';

import Localize from 'components/Localize';
import { ACTION_APPLICATION_ERROR } from 'redux/application/constants';

import { allZoneFeaturesSelectors } from 'selectors/zones';

import {
    ACTION_FETCH_ALL_FLEETS,
    ACTION_MERGE_VEHICLES,
    ACTION_FETCH_ALL_VUBOXES_TO_INTEGRATE,
    ACTION_VUBOXES_TO_INTEGRATE_PAGE_CHANGE,
    ACTION_VUBOXES_TO_INTEGRATE_PAGE_CHANGE_INITIAL_STATE,
    ACTION_CHANGE_REQUESTING_STATE,
    ACTION_TOGGLE_VEHICLE,
    ACTION_SET_SELECTED_FLEET_VEHICLE,
    ACTION_SHOW_FLEET_VEHICLE_DETAIL,
    ACTION_UNSELECT_FLEET_VEHICLE,
    ACTION_WAKE_VEHICLE,
    ACTION_CANCEL_TRIP,
    ACTION_RELEASE_TRIP,
    ACTION_SHOW_VUBOX_TO_INTEGRATE_DETAIL,
    ACTION_SET_SELECTED_VUBOX_TO_INTEGRATE,
    ACTION_UNSELECT_VUBOX_TO_INTEGRATE,
    ACTION_CHANGE_FLOW_STATE,
    ACTION_GET_LIST_EXPERT_COMMANDS,
    ACTION_SET_EXPERT_COMMAND_RESPONSE,
    ACTION_SHOW_EXPERT_COMMAND_RESPONSE,
    ACTION_SET_PASSTHROUGH_COMMAND_RESPONSE,
    ACTION_START_TRIP_REQUEST,
    ACTION_END_TRIP_REQUEST,
    ACTION_SELECT_FLEET_USER_INFO,
    ACTION_SEND_EXPERT_COMMAND,
    ACTION_PING_VEHICLE,
} from './constants';

function formatApiVuboxesToIntegrateToTableData(payload) {
    return _.map(payload.allVuboxesToIntegrate, vubox => {
        const isConnected = vubox.gatewayUrl
            ? Localize.locale['fleet-actions-Connected']
            : Localize.locale['fleet-actions-NotConnected'];
        return {
            fleetId: FunctionHelpers.null2Num(vubox.fleetid),
            id: FunctionHelpers.null2Num(vubox.id),
            vuboxId: FunctionHelpers.null2String(vubox.boxid),
            connected: FunctionHelpers.null2String(isConnected),
            last_active_date: <DateDisplay value={vubox.last_active_date} />,
            box_status: FunctionHelpers.null2String(vubox.box_status),
            autonomy: FunctionHelpers.null2String(vubox.autonomy),
            speed: FunctionHelpers.null2String(vubox.speed),
            battery: FunctionHelpers.null2String(vubox.battery),
            km: FunctionHelpers.null2String(vubox.km),
            location: vubox.location,
            zones: vubox.zones,
            isDoorClosed: FunctionHelpers.null2String(vubox.isDoorClosed),
            isDoorLocked: FunctionHelpers.null2String(vubox.isDoorLocked),
        };
    });
}

export function tryFetchAllFleets() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const payload = [];

            APIClients.Fleet.fetchAllFleetVehicles()
                .then(res => {
                    if (res.length > 0) {
                        payload.push({
                            // todo: fleetId should be config-based (?)
                            fleetid: res[0].fleetid.toLowerCase(),
                            vehicules: res,
                        });
                        dispatch({
                            type: ACTION_FETCH_ALL_FLEETS,
                            payload,
                        });
                    }
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function tryFetchAndMergeVehicles() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Vehicles.listAllVehicles()
                .then(res => {
                    dispatch({
                        type: ACTION_MERGE_VEHICLES,
                        payload: res,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function tryToggleVehicle(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            if (payload.enable) {
                APIClients.Fleet.enableVehicle(payload)
                    .then(res => {
                        dispatch({
                            type: ACTION_TOGGLE_VEHICLE,
                        });
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    });
            } else {
                APIClients.Fleet.disableVehicle(payload)
                    .then(res => {
                        dispatch({
                            type: ACTION_TOGGLE_VEHICLE,
                        });
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    });
            }
        });
}

export function tryWakeUpVehicle(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Fleet.wakeUpVehicle(payload)
                .then(res => {
                    dispatch({
                        type: ACTION_WAKE_VEHICLE,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function tryPingVehicle(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Fleet.pingVehicle(payload)
                .then(res => {
                    dispatch({
                        type: ACTION_PING_VEHICLE,
                        payload: res,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function tryCancelBook(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Fleet.cancelBook(payload)
                .then(res => {
                    dispatch({
                        type: ACTION_CANCEL_TRIP,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function tryReleaseTrip(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Fleet.releaseTrip(payload)
                .then(res => {
                    dispatch({
                        type: ACTION_RELEASE_TRIP,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function getWakeUpProviders() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Fleet.wakeUpProviders()
                .then(res => {
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function tryFetchAllVuboxesToIntegrate() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_VUBOXES_TO_INTEGRATE_PAGE_CHANGE_INITIAL_STATE,
            });
            APIClients.Fleet.fetchToIntegrateVehicles()
                .then(res => {
                    const vuboxesAssignedToVehicles = _.map(state().vehicles.allVehicles, elem => elem.vuboxId);
                    const vuboxesNotAssignedToVehicles = [...res].filter(
                        elem => !new Set(vuboxesAssignedToVehicles).has(elem.id)
                    );
                    const vuboxesToIntegrateTableAllData = formatApiVuboxesToIntegrateToTableData({
                        allVuboxesToIntegrate: vuboxesNotAssignedToVehicles,
                    });

                    const vuboxesToIntegrateTablePageData = fetchPageVuboxToIntegrateTable({
                        data: vuboxesToIntegrateTableAllData,
                        page: 1,
                        order: state().fleet.vuboxesToIntegrateTableCurrentPage.order,
                        orderBy: state().fleet.vuboxesToIntegrateTableCurrentPage.orderBy,
                    });

                    dispatch({
                        type: ACTION_FETCH_ALL_VUBOXES_TO_INTEGRATE,
                        payload: {
                            allVuboxesToIntegrate: formatApiVuboxesToIntegrateToTableData({
                                allVuboxesToIntegrate: vuboxesNotAssignedToVehicles,
                            }),
                            vuboxesToIntegrateTableAllData,
                            vuboxesToIntegrateTableCurrentPage: {
                                data: vuboxesToIntegrateTablePageData,
                                total: vuboxesToIntegrateTableAllData.length,
                                maxPages: Math.ceil(vuboxesToIntegrateTableAllData.length / 10),
                            },
                        },
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function mergeVehicleFromState() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const vehiclesData = state().vehicles.allVehicles;

            if (vehiclesData.length > 0) {
                dispatch({
                    type: ACTION_MERGE_VEHICLES,
                    payload: vehiclesData,
                });
                resolve();
            }
        });
}

export function changeRequestingState(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_REQUESTING_STATE,
                payload,
            });
            resolve();
        });
}

export function tryChangeVuboxesToIntegrateTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const vuboxesToIntegrateTableAllData = state().fleet.vuboxesToIntegrateTableAllData;

            const newPage = payload.newPage ? payload.newPage : state().fleet.vuboxesToIntegrateTableCurrentPage.page;
            const orderBy = payload.orderBy
                ? payload.orderBy
                : state().fleet.vuboxesToIntegrateTableCurrentPage.orderBy;
            const order = payload.order ? payload.order : state().fleet.vuboxesToIntegrateTableCurrentPage.order;

            const vuboxesToIntegratePageData = fetchPageVuboxToIntegrateTable({
                data: vuboxesToIntegrateTableAllData,
                page: newPage,
                order,
                orderBy,
            });

            dispatch({
                type: ACTION_VUBOXES_TO_INTEGRATE_PAGE_CHANGE,
                payload: { page: newPage, data: vuboxesToIntegratePageData, orderBy, order },
            });
            resolve();
        });
}

function fetchPageVuboxToIntegrateTable({ data, page, order, orderBy }) {
    data.sort((a, b) => {
        let orderedData;
        orderBy === 'last_active_date'
            ? (orderedData =
                  order === 'asc'
                      ? new Date(a[orderBy]) - new Date(b[orderBy])
                      : new Date(b[orderBy]) - new Date(a[orderBy]))
            : (orderedData =
                  order === 'asc' ? a[orderBy].localeCompare(b[orderBy]) : b[orderBy].localeCompare(a[orderBy]));
        return orderedData;
    });
    const startPos = (page - 1) * 10;
    const endPos = page * 10;
    data = _.slice(data, startPos, endPos);
    return data;
}

export function selectFleetVehicle(payload) {
    return (dispatch, getState, APIClients) =>
        new Promise(async (resolve, reject) => {
            let fleetVehicle;

            try {
                fleetVehicle = await APIClients.Fleet.fetchVehicleById(payload.vehicleId);
            } catch (e) {
                // 404 is legitimate, other status codes not quite
            }

            if (fleetVehicle) {
                const allZoneIds = fleetVehicle.zones
                    .filter(zone => zone.type === 'allowed' || zone.type === 'forbidden')
                    .map(zone => zone.zoneId);

                fleetVehicle.inZones = [];

                if (allZoneIds.length > 0) {
                    try {
                        const zoneIds = FunctionHelpers.removeDuplicatesInArray(allZoneIds);
                        let zones = allZoneFeaturesSelectors(getState());
                        if (!zones || zones.length === 0) {
                            zones = await APIClients.Zones.listAllZonesGeoJson();
                            zones = zones.features;
                        }
                        zones = zones
                            .map(zone => ({
                                id: zone.properties.zoneId,
                                version: zone.properties.version,
                                name: zone.properties.name,
                            }))
                            .filter(zone => !!zoneIds.find(z => z === zone.id));

                        // For each of the zon  es associated to the vehicle
                        fleetVehicle.inZones = fleetVehicle.zones.reduce((accumulator, zone) => {
                            // Find the zone
                            const matchedZone = zones.find(z => zone.zoneId === z.id && zone.version === z.version);
                            if (!matchedZone) return accumulator;

                            // Extract name property only, for now
                            const { name } = matchedZone;

                            // const data

                            return [
                                ...accumulator,
                                {
                                    ...zone,
                                    name,
                                },
                            ];
                        }, []);
                    } catch (error) {
                        // Non-critical, but should probably report this somewhere
                    }
                }
                APIClients.Mapbox.tryGeocodingOnVehicle(fleetVehicle).then(res => {
                    dispatch({
                        type: ACTION_SET_SELECTED_FLEET_VEHICLE,
                        payload: res,
                    });
                    resolve();
                });
            } else {
                dispatch({
                    type: ACTION_UNSELECT_FLEET_VEHICLE,
                });
                resolve();
            }
        });
}

export function showFleetVehicleDetail(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_SHOW_FLEET_VEHICLE_DETAIL,
                payload,
            });
            resolve();
        });
}

export function unselectFleetVehicle(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_UNSELECT_FLEET_VEHICLE,
            });
            resolve();
        });
}

export function showVuboxToIntegrateDetail(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_SHOW_VUBOX_TO_INTEGRATE_DETAIL,
                payload,
            });
            resolve();
        });
}

export function selectVuboxToIntegrate(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const vuboxesToIntegrate = state().fleet.allVuboxesToIntegrate;
            const vuboxToIntegrate = _.find(vuboxesToIntegrate, v => v.id === payload.id);

            if (vuboxToIntegrate) {
                APIClients.Mapbox.tryGeocodingOnVehicle(vuboxToIntegrate).then(vuboxToIntegrate => {
                    dispatch({
                        type: ACTION_SET_SELECTED_VUBOX_TO_INTEGRATE,
                        payload: vuboxToIntegrate,
                    });
                    resolve();
                });
            } else {
                dispatch({
                    type: ACTION_UNSELECT_VUBOX_TO_INTEGRATE,
                });
            }
            resolve();
        });
}

export function changeFlowState(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_FLOW_STATE,
                payload,
            });
            resolve();
        });
}

export function getListExpertCommands() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Fleet.listExpertCmds()
                .then(res => {
                    dispatch({
                        type: ACTION_GET_LIST_EXPERT_COMMANDS,
                        payload: res,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export const setExpertCommandResponse = payload => ({
    type: ACTION_SET_EXPERT_COMMAND_RESPONSE,
    payload,
});

export const showExpertCommandResponse = () => ({
    type: ACTION_SHOW_EXPERT_COMMAND_RESPONSE,
    payload: true,
});

export const hideExpertCommandResponse = () => ({
    type: ACTION_SHOW_EXPERT_COMMAND_RESPONSE,
    payload: false,
});

export const actionApplicationError = errorMessage => ({
    type: ACTION_APPLICATION_ERROR,
    payload: {
        message: errorMessage || Localize.locale.generalValidations.errorMessages.thereWasAnUnexpectedError,
        error: true,
    },
});

export const sendExpertCmd = payload => ({
    type: ACTION_SEND_EXPERT_COMMAND,
    payload,
});

export function sendPassthroughCmd(vehicleId, cmd) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_SET_PASSTHROUGH_COMMAND_RESPONSE,
                payload: Localize.locale.vehicles.summary.commands.requesting,
            });
            APIClients.Fleet.sendPassthroughCmd(vehicleId, cmd)
                .then(res => {
                    dispatch({
                        type: ACTION_SET_PASSTHROUGH_COMMAND_RESPONSE,
                        payload: res,
                    });
                    resolve();
                })
                .catch(err => {
                    dispatch({
                        type: ACTION_APPLICATION_ERROR,
                        payload: {
                            message: 'There was an unexpected error',
                            error: true,
                        },
                    });
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function clearPassthroughCommandResponse() {
    return {
        type: ACTION_SET_PASSTHROUGH_COMMAND_RESPONSE,
        payload: '',
    };
}

export function startTripRequest(payload) {
    return {
        type: ACTION_START_TRIP_REQUEST,
        payload,
    };
}

export function endTripRequest(payload) {
    return {
        type: ACTION_END_TRIP_REQUEST,
        payload,
    };
}

export const selectFleetUserInfo = payload => ({
    type: ACTION_SELECT_FLEET_USER_INFO,
    payload,
});

export const fetchUserInformationByFleetThunk = () => async (dispatch, getState, APIClients) => {
    const { userId } = await getState().fleet.selectedFleetVehicle;

    if (userId) {
        try {
            const user = await APIClients.Users.getUser(userId);
            dispatch(selectFleetUserInfo(user));
        } catch (err) {
            console.error('[fetchUserInformationByFleetThunk] - ERROR : ', err);
        }
    }
};
