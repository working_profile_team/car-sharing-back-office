import sinon from 'sinon';
import {selectFleetVehicle, fetchUserInformationByFleetThunk} from "../actions";
import {ACTION_SET_SELECTED_FLEET_VEHICLE} from "../constants";
import {
    ACTION_FETCH_USER_PAYMENT_DETAILS_REQUEST,
    ACTION_FETCH_USER_PAYMENT_DETAILS_SUCCESS
} from 'redux/users/constants';
import {
    ACTION_SELECT_FLEET_USER_INFO
} from 'redux/fleet/constants'

describe('Fleet actions and thunks', () => {

    describe('selectFleetVehicle', () => {
        describe('Positive scenario', () => {

            it('dispatches 2 actions as expected', (done) => {
                const dispatchSpy = sinon.spy();
                const getStateStub = sinon.stub();
                const listAllZonesGeoJsonStub = sinon.stub();
                const fetchVehicleByIdStub = sinon.stub();
                const tryGeocodingOnVehicleStub = sinon.stub();

                getStateStub.returns({
                    zone: {
                        allZones: {
                            features: []
                        }
                    }
                });

                fetchVehicleByIdStub.returns(Promise.resolve({
                    id: 'vehicle-id',
                    zones: [{type: 'allowed', zoneId: 'zone-id-1', version: 1}]
                }));

                listAllZonesGeoJsonStub.returns(Promise.resolve({
                    features: [{
                        properties: {
                            zoneId: 'zone-id-1',
                            version: 1,
                            name: 'vulog HQ'
                        }}
                    ]
                }));

                tryGeocodingOnVehicleStub.returns(Promise.resolve({
                    id: 'vehicle-id',
                    zones: [{type: 'allowed', zoneId: 'zone-id-1', version: 1}]
                }));

                const thunkFn = selectFleetVehicle({fleetId: 'F0001', vehicleId: 'vehicle-id'});

                thunkFn(dispatchSpy, getStateStub, {
                    Fleet: {fetchVehicleById: fetchVehicleByIdStub},
                    Zones: {listAllZonesGeoJson: listAllZonesGeoJsonStub},
                    Mapbox: {tryGeocodingOnVehicle: tryGeocodingOnVehicleStub}
                })
                    .then(() => {
                        expect(dispatchSpy.callCount).toEqual(1);
                        expect(getStateStub.callCount).toEqual(1);
                        expect(listAllZonesGeoJsonStub.callCount).toEqual(1);
                        expect(tryGeocodingOnVehicleStub.callCount).toEqual(1);

                        // expect(listAllZonesGeoJsonStub.firstCall.calledWith('zone-id-1')).toBe(true);

                        expect(tryGeocodingOnVehicleStub.firstCall.args[0]).toEqual({
                            id: 'vehicle-id',
                            zones: [{type: 'allowed', zoneId: 'zone-id-1', version: 1}],
                            inZones: [{type: 'allowed', zoneId: 'zone-id-1', version: 1, name: 'vulog HQ'}]
                        });

                        expect(dispatchSpy.firstCall.args[0].type).toEqual(ACTION_SET_SELECTED_FLEET_VEHICLE);
                        expect(dispatchSpy.firstCall.args[0].payload).toEqual({
                            id: 'vehicle-id',
                            zones: [{type: 'allowed', zoneId: 'zone-id-1', version: 1}]
                        });

                        done();
                    });
            });

        });
    });

    describe('fetchUserInformationByFleetThunk', () => {

        let dispatchSpy;
        let getStateSpy;
        let getStateStub;
        let APIClients;

        describe('Positive scenario', () => {

            const thunkFn = fetchUserInformationByFleetThunk();
            let getUserStub;

            beforeEach(() => {
                dispatchSpy = sinon.spy();
                getStateSpy = sinon.spy();
                getStateStub = sinon.stub();
                getUserStub = sinon.stub();

                getStateStub.returns({
                    fleet: {
                        selectedFleetVehicle: {
                            userId: 'id-test'
                        },
                        selectedFleetUser: null
                    }
                });

                APIClients = {Users: {getUser: getUserStub}};

                getUserStub.returns(Promise.resolve([]));
            });

            it('dispatches 1 actions as expected', (done) => {

                thunkFn(dispatchSpy, getStateStub, APIClients)
                    .then(() => {
                        expect(dispatchSpy.callCount).toBe(1);
                        expect(getStateSpy.callCount).toEqual(0);
                        expect(getUserStub.callCount).toEqual(1);

                        expect(dispatchSpy.firstCall.args[0].type).toEqual(ACTION_SELECT_FLEET_USER_INFO);

                        done();
                    });
            });
        });

    });

});
