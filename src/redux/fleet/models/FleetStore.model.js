import { VuboxesToIntegrate } from './VuboxesToIntegrate.model';
import { VuboxesToIntegrateTable } from './VuboxesToIntegrateTable.model';
import { FleetVehicle } from './FleetVehicle.model';

export const FleetStore = {
    allFleets: [],
    allVuboxesToIntegrate: [],
    vuboxesToIntegrateTableAllData: [],
    vuboxesToIntegrateTableCurrentPage: Object.assign({}, VuboxesToIntegrateTable),
    selectedFleetVehicle: Object.assign({}, FleetVehicle),
    selectedFleetUser: null,
    isRequestingFleet: false,
    mergedVehicleData: false,
    showFleetVehicleDetail: false,
    lastFetch: new Date(),
    flowState: 'READ_ONLY',
    showVuboxToIntegrateDetail: false,
    selectedVuboxToIntegrate: Object.assign({}, VuboxesToIntegrate),
    listExpertCommands: [],
    showExpertCommandResponse: false,
    expertCommandResponse: '',
    passthroughCommandResponse: '',
    sendExpertCommand: null,
    pong: '',
};
