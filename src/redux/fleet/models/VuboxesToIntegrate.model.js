import {Location} from "./Location.model"

export const VuboxesToIntegrate = {
    id: undefined,
    fleetId: undefined,
    vuboxId: '',
    connected: '',
    last_active_date: '',
    box_status: '',
    autonomy: 0,
    battery: 0,
    km: 0,
    speed: 0,
    location: Object.assign({}, Location),
    isDoorClosed: false,
    isDoorLocked: false,
    zones: [],
    formattedAddr: '',
    formattedPostcode: '',
};
