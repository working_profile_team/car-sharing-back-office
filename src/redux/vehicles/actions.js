import _ from 'lodash';
import * as ErrorHandler from 'apis/VuLog/ErrorHandler';
import * as DataHelpers from 'helpers/DataHelpers/common';

import {
    ACTION_ADD_OPTION,
    ACTION_ARCHIVE_SELECTED_VEHICLE,
    ACTION_CHANGE_AUTOCOMPLETE_MODEL,
    ACTION_CHANGE_AUTOSUGGEST_VUBOX,
    ACTION_CHANGE_FLOW_STATE,
    ACTION_CHANGE_OPTIONS_AUTOCOMPLETE,
    ACTION_CLEAR_OPTIONS_AUTOCOMPLETE,
    ACTION_CHANGE_SELECTED_VEHICLE,
    ACTION_CHANGE_SELECTED_VEHICLE_MODEL,
    ACTION_FETCH_ALL_MODELS,
    ACTION_FETCH_ALL_OPTIONS,
    ACTION_FETCH_ALL_VEHICLES,
    ACTION_MERGE_FLEET,
    ACTION_REMOVE_OPTION,
    ACTION_SAVE_SELECTED_VEHICLE,
    ACTION_SAVE_SELECTED_VEHICLE_OPTIONS,
    ACTION_SET_AUTOCOMPLETE_MODEL,
    ACTION_SET_SELECTED_VEHICLE,
    ACTION_UNSELECT_VEHICLE,
    ACTION_VEHICLES_PAGE_CHANGE,
    ACTION_VEHICLES_PAGE_CHANGE_INITIAL_STATE,
    ACTION_NOT_ASSIGNED_VEHICLES_PAGE_CHANGE,
    ACTION_CHANGE_FILTERS_TABLE,
    ACTION_WAS_SAVED,
    ACTION_SHOW_EXPERT_COMMANDS,
    ACTION_UNSELECT_VUBOX,
    ACTION_LOCK_VEHICLE_REQUEST,
    ACTION_SET_SELECTED_ACTIONS_TAB_INDEX,
    ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS,
    ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST, ACTION_FETCH_LIST_WAKEUP_PROVIDER_FAILURE,
    ACTION_SET_ADVANCED_FILTERS_VISIBLE,
    ACTION_CLEAR_ADVANCED_FILTERS,
    ACTION_CLEAR_ADVANCED_FILTER,
    ACTION_CHANGE_ADVANCED_FILTERS,
    ACTION_SET_SELECTED_TAB_INDEX,
} from './constants';

export const setSelectedTabIndex = index => ({
    type: ACTION_SET_SELECTED_TAB_INDEX,
    payload: index,
});

export const setAdvancedFiltersVisible = visible => ({
    type: ACTION_SET_ADVANCED_FILTERS_VISIBLE,
    payload: visible,
});

export const clearAdvancedFilters = () => ({
    type: ACTION_CLEAR_ADVANCED_FILTERS,
});

export const clearAdvancedFilter = payload => ({ type: ACTION_CLEAR_ADVANCED_FILTER, payload });

export const changeAdvancedFilters = payload => ({
    type: ACTION_CHANGE_ADVANCED_FILTERS,
    payload,
});

export function setSelectedUsersTabIndex(index) {
    return {
        type: ACTION_SET_SELECTED_ACTIONS_TAB_INDEX,
        payload: index,
    };
}

export function tryFetchAllVehicles() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_VEHICLES_PAGE_CHANGE_INITIAL_STATE,
            });
            APIClients.Vehicles.listAllVehicles()
                .then(res => {
                    _.map(res, vehicle => {
                        vehicle.vehicleId = vehicle.id;
                        return vehicle;
                    });
                    dispatch({
                        type: ACTION_FETCH_ALL_VEHICLES,
                        payload: {
                            allVehicles: res,
                        },
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function tryChangeVehiclesTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const newPage = payload.newPage ? payload.newPage : state().vehicles.vehiclesTableCurrentPage.page;
            const orderBy = payload.orderBy ? payload.orderBy : state().vehicles.vehiclesTableCurrentPage.orderBy;
            const order = payload.order ? payload.order : state().vehicles.vehiclesTableCurrentPage.order;

            dispatch({
                type: ACTION_VEHICLES_PAGE_CHANGE,
                payload: { page: newPage, orderBy, order },
            });
            resolve();
        });
}

export function selectVehicle(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const vehicles = state().vehicles.allVehicles;
            const vehicle = _.find(vehicles, v => v.id === payload.id);

            if (vehicle) {
                dispatch({
                    type: ACTION_SET_SELECTED_VEHICLE,
                    payload: vehicle,
                });
            } else {
                dispatch({
                    type: ACTION_UNSELECT_VEHICLE,
                });
            }
            resolve();
        });
}

export function selectVehicleFromVuboxId(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const vehicles = state().vehicles.allVehicles;
            const vehicle = _.find(vehicles, v => v.vuboxId === payload.vuboxId);

            if (vehicle) {
                dispatch({
                    type: ACTION_SET_SELECTED_VEHICLE,
                    payload: vehicle,
                });
            } else {
                dispatch({
                    type: ACTION_UNSELECT_VEHICLE,
                });
            }
            resolve();
        });
}

export function unselectVehicle(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_UNSELECT_VEHICLE,
            });
            resolve();
        });
}

export function tryFetchAllModels() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Vehicles.listAllModels()
                .then(allModels => {
                    dispatch({
                        type: ACTION_FETCH_ALL_MODELS,
                        payload: allModels,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function changeAutocompleteModel(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_AUTOCOMPLETE_MODEL,
                payload,
            });
            resolve();
        });
}

export function setAutocompleteModel(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_SET_AUTOCOMPLETE_MODEL,
                payload,
            });
            resolve();
        });
}

export function changeSelectedVehicle(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_SELECTED_VEHICLE,
                payload,
            });
            resolve();
        });
}

export function changeSelectedVehicleModel(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_SELECTED_VEHICLE_MODEL,
                payload,
            });
            resolve();
        });
}

export function saveSelectedVehicle(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            let vehicleToSave = state().vehicles.selectedVehicle;
            vehicleToSave.model = { id: vehicleToSave.model.id };
            vehicleToSave = {
                id: vehicleToSave.id,
                name: vehicleToSave.name,
                vin: vehicleToSave.vin,
                vuboxId: vehicleToSave.vuboxId,
                plate: vehicleToSave.plate,
                model: vehicleToSave.model,
                externalId: vehicleToSave.externalId,
                archived: vehicleToSave.archived,
                wakeupProvider: vehicleToSave.wakeupProvider,
            };

            vehicleToSave = DataHelpers.filterOutEmptyProperties(vehicleToSave);

            if (vehicleToSave.id === undefined) {
                // delete payload.id;
                APIClients.Vehicles.createVehicle(vehicleToSave)
                    .then(vehicle => {
                        dispatch({
                            type: ACTION_SET_SELECTED_VEHICLE,
                            payload: vehicle,
                        });
                        resolve();
                    })
                    .catch(err => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            } else {
                const vehicleId = vehicleToSave.id;
                delete vehicleToSave.id;
                APIClients.Vehicles.updateVehicleById(vehicleId, vehicleToSave)
                    .then(res => {
                        dispatch({
                            type: ACTION_SAVE_SELECTED_VEHICLE,
                        });
                        resolve();
                    })
                    .catch((err, msg, others) => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            }
        });
}

export function removeVuboxFromSelectedVehicle() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const vehicleToSave = state().vehicles.selectedVehicle;

            if (vehicleToSave.id === undefined) {
                const err = new Error('No selected vehicle');

                ErrorHandler.logError(err);
                reject(err);
            } else {
                const vehicle_id = vehicleToSave.id;
                APIClients.Vehicles.removeVuboxFromVehicle({ vehicle_id })
                    .then(res => {
                        dispatch({
                            type: ACTION_SAVE_SELECTED_VEHICLE,
                        });
                        resolve();
                    })
                    .catch((err, msg, others) => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            }
        });
}

export function archiveSelectedFleetVehicle() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const vehicleToArchive = state().fleet.selectedFleetVehicle;

            if (vehicleToArchive && vehicleToArchive.id === undefined) {
                // DO NOT THROW ERROR BECAUSE IT CAN BE THAT WE REMOVED A VUBOX OF A VEHICLE WHICH IS NOT IN THE FLEET
                resolve();
            } else {
                const vehicle_id = vehicleToArchive.id;
                APIClients.Fleet.archiveFleetVehicle({ vehicle_id })
                    .then(res => {
                        dispatch({
                            type: ACTION_SAVE_SELECTED_VEHICLE,
                        });
                        resolve();
                    })
                    .catch((err, msg, others) => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            }
        });
}

export function saveSelectedVehicleOptions(payload) {
    return (dispatch, state, APIClients) => {
        const vehicles = state().vehicles;
        const vehicleId = vehicles.selectedVehicle.id;
        const optionsToSave = _.map(vehicles.selectedVehicle.options, elem => elem.id);
        const vehicle = _.find(vehicles.allVehicles, elem => elem.id === vehicleId);
        const originalOptions = _.map(vehicle.options, elem => elem.id);
        const optionsToAdd = [...optionsToSave].filter(elem => !new Set(originalOptions).has(elem));
        const optionsToRemove = [...originalOptions].filter(elem => !new Set(optionsToSave).has(elem));
        const numberOfOptionsToModify = optionsToAdd.length + optionsToRemove.length;
        let numberOfCompletedOptions = 0;

        return new Promise((resolve, reject) => {
            function addOptions(optionId) {
                APIClients.Vehicles.addOptionsToVehicle({ vehicle_id: vehicleId, option_id: optionId })
                    .then(res => {
                        numberOfCompletedOptions++;
                        if (numberOfCompletedOptions === numberOfOptionsToModify) {
                            dispatch({
                                type: ACTION_SAVE_SELECTED_VEHICLE_OPTIONS,
                            });
                            resolve();
                        }
                    })
                    .catch(err => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            }

            function removeOptions(optionId) {
                APIClients.Vehicles.delOptionsToVehicle({ vehicle_id: vehicleId, option_id: optionId })
                    .then(res => {
                        numberOfCompletedOptions++;
                        if (numberOfCompletedOptions === numberOfOptionsToModify) {
                            dispatch({
                                type: ACTION_SAVE_SELECTED_VEHICLE_OPTIONS,
                            });
                            resolve();
                        }
                    })
                    .catch(err => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            }

            for (const optionId of optionsToAdd) addOptions(optionId);

            for (const optionId of optionsToRemove) removeOptions(optionId);

            if (numberOfOptionsToModify === 0) {
                dispatch({
                    type: ACTION_SAVE_SELECTED_VEHICLE_OPTIONS,
                });
                resolve();
            }
        });
    };
}

export function archiveSelectedVehicle() {
    return (dispatch, state, APIClients) => {
        const selectedVehicle = state().vehicles.selectedVehicle;
        const vehicleId = selectedVehicle.id;
        return new Promise((resolve, reject) => {
            if (!selectedVehicle.archived) {
                APIClients.Vehicles.archiveVehicle(vehicleId)
                    .then(res => {
                        dispatch({
                            type: ACTION_ARCHIVE_SELECTED_VEHICLE,
                        });
                        resolve();
                    })
                    .catch(err => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            } else {
                resolve();
            }
        });
    };
}

export function deleteVuboxToIntegrateIfNecessary() {
    return (dispatch, state, APIClients) => {
        const selectedVehicle = state().vehicles.selectedVehicle;
        const allVuboxesToIntegrate = state().fleet.allVuboxesToIntegrate;
        return new Promise((resolve, reject) => {
            if (selectedVehicle.vuboxId) {
                if (
                    allVuboxesToIntegrate.length > 0 &&
                    _.find(allVuboxesToIntegrate, v => v.id === selectedVehicle.vuboxId)
                ) {
                    APIClients.Fleet.deleteVuboxToIntegrate({ vubox_id: selectedVehicle.vuboxId })
                        .then(res => {
                            dispatch({
                                type: ACTION_ARCHIVE_SELECTED_VEHICLE,
                            });
                            resolve();
                        })
                        .catch(err => {
                            ErrorHandler.logError(err);
                            reject(err);
                        });
                } else {
                    // DO NOT THROW ERROR FOR WORKFLOW
                    resolve();
                }
            } else {
                resolve();
            }
        });
    };
}

export function changeOptionsAutocomplete(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_OPTIONS_AUTOCOMPLETE,
                payload,
            });
            resolve();
        });
}

export function clearOptionsAutocomplete() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CLEAR_OPTIONS_AUTOCOMPLETE,
            });
            resolve();
        });
}

export function addOption(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_ADD_OPTION,
                payload,
            });
            resolve();
        });
}

export function removeOption(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_REMOVE_OPTION,
                payload,
            });
            resolve();
        });
}

export function tryFetchAllOptions() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Vehicles.listAllOptions()
                .then(allOptions => {
                    dispatch({
                        type: ACTION_FETCH_ALL_OPTIONS,
                        payload: allOptions,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function changeAutosuggestVubox(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_AUTOSUGGEST_VUBOX,
                payload,
            });
            resolve();
        });
}

export function changeFlowState(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_FLOW_STATE,
                payload,
            });
            resolve();
        });
}

export function mergeFleetFromState() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const fleetData = state().fleet.allFleets;

            if (fleetData.length > 0) {
                dispatch({
                    type: ACTION_MERGE_FLEET,
                    payload: fleetData,
                });
                resolve();
            }
        });
}

export function redirectToVehicleList() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            state().application.history.push('/app/vehicles', {
                referer: this.props.location,
            });
        });
}

export function tryChangeNotAssignedVehiclesTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const newPage = payload.newPage
                ? payload.newPage
                : state().vehicles.notAssignedVehiclesTableCurrentPage.page;
            const orderBy = payload.orderBy
                ? payload.orderBy
                : state().vehicles.notAssignedVehiclesTableCurrentPage.orderBy;
            const order = payload.order ? payload.order : state().vehicles.notAssignedVehiclesTableCurrentPage.order;

            dispatch({
                type: ACTION_NOT_ASSIGNED_VEHICLES_PAGE_CHANGE,
                payload: { page: newPage, orderBy, order },
            });
            resolve();
        });
}

export function changeFiltersTable(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_FILTERS_TABLE,
                payload,
            });
            resolve();
        });
}

export function showExpertCommands(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_SHOW_EXPERT_COMMANDS,
                payload,
            });
            resolve();
        });
}

export function wasSaved(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_WAS_SAVED,
                payload,
            });
            resolve();
        });
}

export function unselectVubox(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_UNSELECT_VUBOX,
            });
            resolve();
        });
}

export function lockVehicleRequest(payload) {
    return {
        type: ACTION_LOCK_VEHICLE_REQUEST,
        payload,
    };
}

export const listWakeUpProviderRequest = () => ({ type: ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST });

export const listWakeUpProviderSuccess = payload => ({ type: ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS, payload });

export const listWakeUpProviderFailure = payload => ({ type: ACTION_FETCH_LIST_WAKEUP_PROVIDER_FAILURE, payload });
