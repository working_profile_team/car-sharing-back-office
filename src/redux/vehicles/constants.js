export const ACTION_FETCH_ALL_VEHICLES = '@vehicles/ACTION_FETCH_ALL_VEHICLES';
export const ACTION_VEHICLES_PAGE_CHANGE = '@vehicles/ACTION_VEHICLES_PAGE_CHANGE';
export const ACTION_SET_SELECTED_VEHICLE = '@vehicles/ACTION_SET_SELECTED_VEHICLE';
export const ACTION_VEHICLES_PAGE_CHANGE_INITIAL_STATE = '@vehicles/ACTION_VEHICLES_PAGE_CHANGE_INITIAL_STATE';
export const ACTION_CHANGE_SELECTED_VEHICLE = '@vehicles/ACTION_CHANGE_SELECTED_VEHICLE';
export const ACTION_SAVE_SELECTED_VEHICLE = '@vehicles/ACTION_SAVE_SELECTED_VEHICLE';
export const ACTION_FETCH_ALL_MODELS = '@vehicles/ACTION_FETCH_ALL_MODELS';
export const ACTION_SET_AUTOCOMPLETE_MODEL = '@vehicles/ACTION_SET_AUTOCOMPLETE_MODEL';
export const ACTION_CHANGE_AUTOCOMPLETE_MODEL = '@vehicles/ACTION_CHANGE_AUTOCOMPLETE_MODEL';
export const ACTION_UNSELECT_VEHICLE = '@vehicles/ACTION_UNSELECT_VEHICLE';
export const ACTION_CHANGE_SELECTED_VEHICLE_MODEL = '@vehicles/ACTION_CHANGE_SELECTED_VEHICLE_MODEL';
export const ACTION_FETCH_ALL_OPTIONS = '@vehicles/ACTION_FETCH_ALL_OPTIONS';
export const ACTION_CHANGE_OPTIONS_AUTOCOMPLETE = '@vehicles/ACTION_CHANGE_OPTIONS_AUTOCOMPLETE';
export const ACTION_CLEAR_OPTIONS_AUTOCOMPLETE = '@vehicles/ACTION_CLEAR_OPTIONS_AUTOCOMPLETE';
export const ACTION_ADD_OPTION = '@vehicles/ACTION_ADD_OPTION';
export const ACTION_REMOVE_OPTION = '@vehicles/ACTION_REMOVE_OPTION';
export const ACTION_CHANGE_AUTOSUGGEST_VUBOX = '@vehicles/ACTION_CHANGE_AUTOSUGGEST_VUBOX';
export const ACTION_CHANGE_FLOW_STATE = '@vehicles/ACTION_CHANGE_FLOW_STATE';
export const ACTION_SAVE_SELECTED_VEHICLE_OPTIONS = '@vehicles/ACTION_SAVE_SELECTED_VEHICLE_OPTIONS';
export const ACTION_MERGE_FLEET = '@vehicles/ACTION_MERGE_FLEET';
export const ACTION_ARCHIVE_SELECTED_VEHICLE = '@vehicles/ACTION_ARCHIVE_SELECTED_VEHICLE';
export const ACTION_NOT_ASSIGNED_VEHICLES_PAGE_CHANGE = '@vehicles/ACTION_NOT_ASSIGNED_VEHICLES_PAGE_CHANGE';
export const ACTION_CHANGE_FILTERS_TABLE = '@vehicles/ACTION_CHANGE_FILTERS_TABLE';
export const ACTION_UNSELECT_VUBOX = '@vehicles/ACTION_UNSELECT_VUBOX';
export const ACTION_SHOW_EXPERT_COMMANDS = '@vehicles/ACTION_SHOW_EXPERT_COMMANDS';
export const ACTION_WAS_SAVED = '@vehicles/ACTION_WAS_SAVED';
export const ACTION_SET_SELECTED_ACTIONS_TAB_INDEX = '@vehicles/ACTION_SET_SELECTED_ACTIONS_TAB_INDEX';
export const ACTION_LOCK_VEHICLE_REQUEST = '@vehicles/ACTION_LOCK_VEHICLE_REQUEST';
export const ACTION_LOCK_VEHICLE_SUCCESS = '@vehicles/ACTION_LOCK_VEHICLE_SUCCESS';
export const ACTION_LOCK_VEHICLE_FAILURE = '@vehicles/ACTION_LOCK_VEHICLE_FAILURE';
export const ACTION_SET_ADVANCED_FILTERS_VISIBLE = '@vehicles/ACTION_SET_ADVANCED_FILTERS_VISIBLE';
export const ACTION_CLEAR_ADVANCED_FILTERS = '@vehicles/ACTION_CLEAR_ADVANCED_FILTERS';
export const ACTION_CLEAR_ADVANCED_FILTER = '@vehicles/ACTION_CLEAR_ADVANCED_FILTER';
export const ACTION_CHANGE_ADVANCED_FILTERS = '@vehicles/ACTION_CHANGE_ADVANCED_FILTERS';
export const ACTION_SET_SELECTED_TAB_INDEX = '@vehicles/ACTION_SET_SELECTED_TAB_INDEX';
export const ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST = '@vehicles/ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST';
export const ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS = '@vehicles/ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS';
export const ACTION_FETCH_LIST_WAKEUP_PROVIDER_FAILURE = '@vehicles/ACTION_FETCH_LIST_WAKEUP_PROVIDER_FAILURE';
