import Localize from "components/Localize";

export const VehicleTable = {
    page: 1,
    maxPages: 1,
    pageSize: 10,
    order: 'asc',
    orderBy: 'name',
    booleanFilters: [{id: 'archived', value: false}],
    columnsData: [
        {fieldName: 'name', fieldType: "String", button: true, label: Localize.locale["Components-VehicleTable-Name"]},
        {fieldName: 'plate', fieldType: "String", button: false, label: Localize.locale["Components-VehicleTable-Plate"]},
        {fieldName: 'vin', fieldType: "String", button: false, label: Localize.locale["Components-VehicleTable-VIN"]},
        {fieldName: 'vuboxId', fieldType: "String", button: false, label: Localize.locale["Components-VehicleTable-VuboxID"]},
        {fieldName: 'model', fieldType: "String", button: false, label: Localize.locale["Components-VehicleTable-Model"]},
        {fieldName: 'status', fieldType: "String", button: false, label: Localize.locale["Components-VehicleTable-FleetVehicleStatus"]},
    ],
};
