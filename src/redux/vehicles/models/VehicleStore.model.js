import { Vehicle } from './Vehicle.model';
import { VehicleTable } from './VehicleTable.model';
import { NotAssignedVehicleTable } from './NotAssignedVehicleTable.model';
import { Autosuggest } from '../../users/models/Autosuggest.model';
import { AutosuggestChips } from '../../users/models/AutosuggestChips.model';

export const VehicleStore = {
    allVehicles: [],
    allModels: [],
    allOptions: [],
    vehiclesTableCurrentPage: Object.assign({}, VehicleTable),
    selectedVehicle: Object.assign({}, Vehicle),
    autosuggestChipsOptions: Object.assign({}, AutosuggestChips),
    autosuggestModel: Object.assign({}, Autosuggest),
    autosuggestVubox: Object.assign({}, Autosuggest),
    flowState: '',
    mergedFleetData: false,
    fetchedVehicles: false,
    notAssignedVehiclesTableCurrentPage: Object.assign({}, NotAssignedVehicleTable),
    showExpertCommands: false,
    wasSaved: false,
    tabActionsSelected: 0,
    listWakeUpProvider: {
        isFetching: false,
        data: [],
        error: '',
    },
    selectedTabIndex: 0,
    advancedFiltersIsVisible: false,
    advancedFilters: {
        status: '',
        archived: false,
        'model.name': '',
        'model.energyType': '',
    },
};
