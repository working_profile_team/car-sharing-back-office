import {Model} from "./Model.model";

export const Vehicle = {
    id: undefined,
    vehicleId: undefined,
    name: '',
    plate: '',
    vin: '',
    vuboxId: '',
    model: Object.assign({}, Model),
    options: [],
    fleetId: '',
    externalId: '',
    archived: false,
};
