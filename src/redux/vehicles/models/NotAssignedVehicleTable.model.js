export const NotAssignedVehicleTable = {
    page: 1,
    maxPages: 1,
    pageSize: 5,
    order: 'asc',
    orderBy: 'name',
    columnsData: [
        {fieldName: 'name', fieldType: 'String', button: true, disablePadding: true, label: 'Name'},
        {fieldName: 'plate', fieldType: 'String', button: false, disablePadding: true, label: 'Plate'},
        {fieldName: 'vin', fieldType: 'String', button: false, disablePadding: true, label: 'VIN'},
        {fieldName: 'model', fieldType: 'String', button: false, disablePadding: false, label: 'Model'},
    ],
};
