import _ from 'lodash';
import deepmerge from 'deepmerge';

import { Autosuggest } from 'redux/users/models/Autosuggest.model';
import { AutosuggestChips } from 'redux/users/models/AutosuggestChips.model';
import {
    ACTION_ADD_OPTION,
    ACTION_CHANGE_AUTOCOMPLETE_MODEL,
    ACTION_CHANGE_AUTOSUGGEST_VUBOX,
    ACTION_CHANGE_FLOW_STATE,
    ACTION_CHANGE_OPTIONS_AUTOCOMPLETE,
    ACTION_CHANGE_SELECTED_VEHICLE,
    ACTION_CHANGE_SELECTED_VEHICLE_MODEL,
    ACTION_FETCH_ALL_MODELS,
    ACTION_FETCH_ALL_OPTIONS,
    ACTION_FETCH_ALL_VEHICLES,
    ACTION_REMOVE_OPTION,
    ACTION_SAVE_SELECTED_VEHICLE,
    ACTION_SET_AUTOCOMPLETE_MODEL,
    ACTION_SET_SELECTED_VEHICLE,
    ACTION_UNSELECT_VEHICLE,
    ACTION_VEHICLES_PAGE_CHANGE,
    ACTION_VEHICLES_PAGE_CHANGE_INITIAL_STATE,
    ACTION_SAVE_SELECTED_VEHICLE_OPTIONS,
    ACTION_MERGE_FLEET,
    ACTION_ARCHIVE_SELECTED_VEHICLE,
    ACTION_NOT_ASSIGNED_VEHICLES_PAGE_CHANGE,
    ACTION_CHANGE_FILTERS_TABLE,
    ACTION_SHOW_EXPERT_COMMANDS,
    ACTION_WAS_SAVED,
    ACTION_UNSELECT_VUBOX,
    ACTION_SET_SELECTED_ACTIONS_TAB_INDEX,
    ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST,
    ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS,
    ACTION_FETCH_LIST_WAKEUP_PROVIDER_FAILURE,
    ACTION_SET_ADVANCED_FILTERS_VISIBLE,
    ACTION_CHANGE_ADVANCED_FILTERS,
    ACTION_CLEAR_ADVANCED_FILTERS,
    ACTION_CLEAR_ADVANCED_FILTER,
    ACTION_SET_SELECTED_TAB_INDEX,
} from './constants';
import { VehicleStore } from './models/VehicleStore.model';
import { VehicleTable } from './models/VehicleTable.model';
import { Vehicle } from './models/Vehicle.model';

export default function reducer(state = VehicleStore, action) {
    const { type, payload } = action;
    switch (type) {
        case ACTION_SET_SELECTED_TAB_INDEX:
            return {
                ...state,
                selectedTabIndex: action.payload,
            };

        case ACTION_SET_ADVANCED_FILTERS_VISIBLE:
            return {
                ...state,
                advancedFiltersIsVisible: action.payload,
            };

        case ACTION_CHANGE_ADVANCED_FILTERS:
            return {
                ...state,
                advancedFilters: {
                    ...state.advancedFilters,
                    [payload.type]: payload.value,
                },
            };

        case ACTION_CLEAR_ADVANCED_FILTERS:
            return {
                ...state,
                advancedFilters: {
                    status: '',
                    archived: false,
                    'model.name': '',
                    'model.energyType': '',
                },
            };

        case ACTION_CLEAR_ADVANCED_FILTER:
            return {
                ...state,
                advancedFilters: {
                    ...state.advancedFilters,
                    [action.payload]: typeof state.advancedFilters[action.payload] === 'boolean' ? false : '',
                },
            };

        case ACTION_SET_SELECTED_ACTIONS_TAB_INDEX:
            return {
                ...state,
                tabActionsSelected: payload,
            };

        case ACTION_FETCH_ALL_VEHICLES:
            return {
                ...state,
                allVehicles: payload.allVehicles,
                fetchedVehicles: true,
            };

        case ACTION_VEHICLES_PAGE_CHANGE_INITIAL_STATE:
            return {
                ...state,
                vehiclesTableCurrentPage: VehicleTable,
            };

        case ACTION_VEHICLES_PAGE_CHANGE:
            return {
                ...state,
                vehiclesTableCurrentPage: { ...state.vehiclesTableCurrentPage, ...payload },
            };

        case ACTION_SET_SELECTED_VEHICLE:
            return {
                ...state,
                selectedVehicle: { ...payload },
                autosuggestModel: {
                    ...state.autosuggestModel,
                    value: payload.model.name,
                },
                autosuggestVubox: {
                    ...state.autosuggestModel,
                    value: payload.vuboxId,
                },
                tabActionsSelected: 0,
            };

        case ACTION_FETCH_ALL_MODELS:
            return {
                ...state,
                allModels: payload,
            };

        case ACTION_CHANGE_AUTOCOMPLETE_MODEL:
            return {
                ...state,
                autosuggestModel: { ...state.autosuggestModel, ...payload },
            };

        case ACTION_SET_AUTOCOMPLETE_MODEL:
            return {
                ...state,
                autosuggestModel: { ...payload },
            };

        case ACTION_CHANGE_SELECTED_VEHICLE:
            return {
                ...state,
                selectedVehicle: { ...state.selectedVehicle, ...payload },
            };

        case ACTION_CHANGE_SELECTED_VEHICLE_MODEL:
            return {
                ...state,
                selectedVehicle: {
                    ...state.selectedVehicle,
                    model: { ...state.selectedVehicle.model, ...payload },
                },
            };

        case ACTION_UNSELECT_VEHICLE:
            return {
                ...state,
                selectedVehicle: { ...Vehicle },
                autosuggestChipsOptions: { ...AutosuggestChips },
                autosuggestModel: { ...Autosuggest },
                autosuggestVubox: { ...Autosuggest },
            };

        case ACTION_SAVE_SELECTED_VEHICLE:
            break;

        case ACTION_FETCH_ALL_OPTIONS:
            return {
                ...state,
                allOptions: payload,
            };

        case ACTION_CHANGE_OPTIONS_AUTOCOMPLETE:
            return {
                ...state,
                autosuggestChipsOptions: {
                    ...state.autosuggestChipsOptions,
                    autosuggest: { ...state.autosuggestChipsOptions.autosuggest, ...payload },
                },
            };

        case ACTION_CHANGE_AUTOSUGGEST_VUBOX:
            return {
                ...state,
                autosuggestVubox: { ...state.autosuggestVubox, ...payload },
            };

        case ACTION_ADD_OPTION: {
            const newValues = [...state.selectedVehicle.options];
            if (newValues.findIndex(elem => elem.id === payload.id) === -1) {
                const newOption = _.find(state.allOptions, elem => elem.id.toString() === payload.id.toString());
                newValues.push(newOption);
            }

            return {
                ...state,
                selectedVehicle: {
                    ...state.selectedVehicle,
                    options: newValues,
                },
                autosuggestChipsOptions: {
                    ...state.autosuggestChipsOptions,
                    autosuggest: {
                        ...state.autosuggestChipsOptions.autosuggest,
                        value: '',
                        suggestions: '',
                    },
                },
            };
        }

        case ACTION_SAVE_SELECTED_VEHICLE_OPTIONS:
            break;

        case ACTION_ARCHIVE_SELECTED_VEHICLE:
            break;

        case ACTION_REMOVE_OPTION: {
            let newValues = [...state.selectedVehicle.options];
            newValues = newValues.filter(elemX => elemX.id !== payload.id);

            return {
                ...state,
                selectedVehicle: {
                    ...state.selectedVehicle,
                    options: newValues,
                },
            };
        }

        case ACTION_CHANGE_FLOW_STATE:
            return {
                ...state,
                flowState: payload,
            };

        case ACTION_MERGE_FLEET:
            _.each(state.allVehicles, vehicle => {
                const fleet = _.find(payload, o => vehicle.fleetId === o.idFleet);
                if (fleet) {
                    let foundVehicle = _.find(fleet.vehicules, o => o.id === vehicle.id);
                    if (foundVehicle) {
                        foundVehicle = deepmerge(foundVehicle, vehicle);
                        const vIndex = _.findIndex(state.allVehicles, { id: foundVehicle.id });
                        state.allVehicles.splice(vIndex, 1, foundVehicle);
                    }
                }
            });

            return {
                ...state,
                mergedFleetData: true,
            };

        case ACTION_NOT_ASSIGNED_VEHICLES_PAGE_CHANGE:
            return {
                ...state,
                notAssignedVehiclesTableCurrentPage: { ...state.notAssignedVehiclesTableCurrentPage, ...payload },
            };

        case ACTION_CHANGE_FILTERS_TABLE: {
            const filterIndex = _.findIndex(
                state.vehiclesTableCurrentPage.booleanFilters,
                elem => elem.id === payload.id
            );
            const newValues = [...state.vehiclesTableCurrentPage.booleanFilters];
            newValues[filterIndex].value = payload.value;

            return {
                ...state,
                vehiclesTableCurrentPage: {
                    ...state.vehiclesTableCurrentPage,
                    booleanFilters: newValues,
                },
            };
        }

        case ACTION_SHOW_EXPERT_COMMANDS:
            return {
                ...state,
                showExpertCommands: payload,
            };

        case ACTION_WAS_SAVED:
            return {
                ...state,
                wasSaved: payload,
            };

        case ACTION_UNSELECT_VUBOX:
            return {
                ...state,
                selectedVehicle: {
                    ...state.selectedVehicle,
                    vuboxId: '',
                },
                autosuggestVubox: { ...Autosuggest },
            };

        case ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST:
            return {
                ...state,
                listWakeUpProvider: {
                    ...state.listWakeUpProvider,
                    isFetching: true,
                    data: [],
                    error: '',
                }
            }

        case ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS:
            return {
                ...state,
                listWakeUpProvider: {
                    ...state.listWakeUpProvider,
                    isFetching: false,
                    data: payload,
                }
            }

        case ACTION_FETCH_LIST_WAKEUP_PROVIDER_FAILURE:
            return {
                ...state,
                listWakeUpProvider: {
                    ...state.listWakeUpProvider,
                    isFetching: false,
                    error: payload,
                }
            }

        default:
            break;
    }
    return state;
}
