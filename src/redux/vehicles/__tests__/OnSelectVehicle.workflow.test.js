import sinon from 'sinon';
import OnSelectVehicle from "../OnSelectVehicle.workflow";


describe('Workflow: OnSelectVehicle', () => {
    describe('Positive scenario', () => {
        it('dispatches all thunks and actions as expected', (done) => {
            const stubbedVehicleThunkKeys = [
                'showExpertCommands',
                'tryFetchAllVehicles',
                'selectVehicle',
            ];
            const stubbedFleetThunkKeys = [
                'tryFetchAllFleets',
                'getListExpertCommands',
                'selectFleetVehicle',
                'tryFetchAllVuboxesToIntegrate',
            ];

            const dispatchSpy = sinon.spy();
            const getStateStub = sinon.stub();

            const VehicleThunks = {};
            const FleetThunks = {};

            stubbedVehicleThunkKeys.forEach(thunkKey => {
                VehicleThunks[thunkKey] = sinon.stub();
                VehicleThunks[thunkKey].returns(Promise.resolve());
            });

            stubbedFleetThunkKeys.forEach(thunkKey => {
                FleetThunks[thunkKey] = sinon.stub();
                FleetThunks[thunkKey].returns(Promise.resolve());
            });

            getStateStub.returns({
                vehicles: {
                    allVehicles: [
                        {id: '1', fleetId: 'F00001'}
                    ]
                }
            });

            const payload = {id: '1'};

            const thunkFn = OnSelectVehicle(payload, VehicleThunks, FleetThunks);
            thunkFn(dispatchSpy, getStateStub).then(() => {
                const expectedCallCount = stubbedVehicleThunkKeys.length + stubbedFleetThunkKeys.length;
                expect(dispatchSpy.callCount).toEqual(expectedCallCount);
                expect(getStateStub.callCount).toEqual(1);

                expect(VehicleThunks.showExpertCommands.callCount).toEqual(1);
                expect(VehicleThunks.tryFetchAllVehicles.callCount).toEqual(1);
                expect(VehicleThunks.selectVehicle.callCount).toEqual(1);
                expect(FleetThunks.tryFetchAllFleets.callCount).toEqual(1);
                expect(FleetThunks.getListExpertCommands.callCount).toEqual(1);
                expect(FleetThunks.selectFleetVehicle.callCount).toEqual(1);
                expect(FleetThunks.tryFetchAllVuboxesToIntegrate.callCount).toEqual(1);

                expect(FleetThunks.selectFleetVehicle.firstCall.calledWith({
                    vehicleId: '1', fleetId: 'F00001'
                })).toBe(true);

                done();
            });
        });
    })
});
