import sinon from 'sinon';
import * as actions from "redux/vehicles/actions";
import reducer from "redux/vehicles/reducer";
import { VehicleStore } from "redux/vehicles/models/VehicleStore.model"
import { Vehicle } from "redux/vehicles/models/Vehicle.model"
import {
    ACTION_UPDATE_SELECTED_PROMO,
    ACTION_CREATE_PROMO,
    ACTION_UPDATE_PROMO
} from "redux/vehicles/constants";
import { ACTION_ARCHIVE_SELECTED_VEHICLE, ACTION_SET_SELECTED_VEHICLE } from "../constants";

describe('Vehicles Actions', () => {
    describe("Action Tabs", () => {
        it('Set tabs', () => {
            const stateBefore = {
                ...VehicleStore
            }
            const action = actions.setSelectedUsersTabIndex(2)
            const stateAfter = {
                ...VehicleStore,
                tabActionsSelected: 2
            }

            expect(reducer(stateBefore, action)).toEqual(stateAfter)
        })

        it('Set Selected Vehicle', () => {
            const stateBefore = {
                ...VehicleStore,
                tabActionsSelected: 2
            }
            const action = {
                type: ACTION_SET_SELECTED_VEHICLE,
                payload: {
                    ...Vehicle
                }
            }

            expect(reducer(stateBefore, action).tabActionsSelected).toEqual(0)
        })
    })
})

describe('archiveSelectedVehicle', () => {

    let dispatchSpy;
    let getStateStub;
    let thunkFn;
    let apiClientStub;
    let archiveVehicleStub;

    describe("creates or edit depending on selectedPromo attributes", () => {

        beforeEach(() => {
            dispatchSpy = sinon.spy();
            getStateStub = sinon.stub();
            archiveVehicleStub = sinon.stub();
            archiveVehicleStub.returns(Promise.resolve());

            apiClientStub = {
                Vehicles: {
                    archiveVehicle: archiveVehicleStub,
                }
            };
        });

        it("makes a call to archive the selected vehicle as expected", (done) => {
            getStateStub.returns({
                vehicles: {
                    selectedVehicle: {
                        id: "vehicle-id",
                        archived: false
                    }
                }
            });

            thunkFn = actions.archiveSelectedVehicle();

            thunkFn(dispatchSpy, getStateStub, apiClientStub)
                .then(() => {
                    expect(dispatchSpy.callCount).toEqual(1);
                    expect(getStateStub.callCount).toEqual(1);
                    expect(apiClientStub.Vehicles.archiveVehicle.callCount).toEqual(1);

                    expect(dispatchSpy.firstCall.args[0].type).toEqual(ACTION_ARCHIVE_SELECTED_VEHICLE);

                    done();
                });
        });

        it("skips the call to archive the selected vehicle as expected", (done) => {
            getStateStub.returns({
                vehicles: {
                    selectedVehicle: {
                        id: "vehicle-id",
                        archived: true
                    }
                }
            });

            thunkFn = actions.archiveSelectedVehicle();

            thunkFn(dispatchSpy, getStateStub, apiClientStub)
                .then(() => {
                    expect(dispatchSpy.callCount).toEqual(0);
                    expect(getStateStub.callCount).toEqual(1);

                    done();
                });
        });
    });
});
