import sinon from 'sinon';
import OnSaveVehicle from "../OnSaveVehicle.workflow";


describe('Workflow: OnSaveVehicle', () => {
    describe('Positive scenario', () => {
        it('dispatches all thunks and actions as expected', (done) => {
            const stubbedVehicleThunkKeys = [
                'saveSelectedVehicle',
                'deleteVuboxToIntegrateIfNecessary',
                'tryFetchAllVehicles',
                'saveSelectedVehicleOptions',
                'wasSaved',
            ];
            const stubbedFleetThunkKeys = [
                'tryFetchAllVuboxesToIntegrate',
            ];

            const dispatchSpy = sinon.spy();
            const getStateStub = sinon.stub();

            const VehicleThunks = {};
            const FleetThunks = {};
            const AppThunks = {};

            stubbedVehicleThunkKeys.forEach(thunkKey => {
                VehicleThunks[thunkKey] = sinon.stub();
                VehicleThunks[thunkKey].returns(Promise.resolve());
            });

            stubbedFleetThunkKeys.forEach(thunkKey => {
                FleetThunks[thunkKey] = sinon.stub();
                FleetThunks[thunkKey].returns(Promise.resolve());
            });

            AppThunks.updateSnackBar = sinon.stub();
            AppThunks.openSnackBar = sinon.stub();

            AppThunks.updateSnackBar.returns({message:'', actionData:{test:'', href:''}});
            AppThunks.openSnackBar.returns({});

            getStateStub.returns({
                vehicles: {
                    selectedVehicle: {
                        id: '1'
                    }
                }
            });

            const payload = {id: '1'};

            const thunkFn = OnSaveVehicle(payload, VehicleThunks, FleetThunks, AppThunks);
            thunkFn(dispatchSpy, getStateStub).then(() => {
                expect(dispatchSpy.callCount).toEqual(10);
                expect(getStateStub.callCount).toEqual(1);

                expect(VehicleThunks.saveSelectedVehicle.callCount).toEqual(1);
                expect(VehicleThunks.deleteVuboxToIntegrateIfNecessary.callCount).toEqual(1);
                expect(VehicleThunks.tryFetchAllVehicles.callCount).toEqual(2);
                expect(VehicleThunks.saveSelectedVehicleOptions.callCount).toEqual(1);
                expect(VehicleThunks.wasSaved.callCount).toEqual(1);

                expect(FleetThunks.tryFetchAllVuboxesToIntegrate.callCount).toEqual(2);

                expect(AppThunks.updateSnackBar.callCount).toEqual(1);
                expect(AppThunks.openSnackBar.callCount).toEqual(1);

                done();
            });
        });
    })
});
