import * as actions from 'redux/vehicles/constants';
import reducer from 'redux/vehicles/reducer';
import { VehicleStore as state } from 'redux/vehicles/models/VehicleStore.model'

describe('Vehicles Reducer', () => {

    it('Test for ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST', () => {

        const action = {
            type: actions.ACTION_FETCH_LIST_WAKEUP_PROVIDER_REQUEST,
        };

        const expectedResult = {
            ...state,
            listWakeUpProvider: {
                ...state.listWakeUpProvider,
                isFetching: true,
                data: [],
                error: '',
            },
        };

        expect(reducer(undefined, action)).toEqual(expectedResult);
    });

    it('Test for ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS', () => {

        const action = {
            type: actions.ACTION_FETCH_LIST_WAKEUP_PROVIDER_SUCCESS,
            payload: [{ id: 'id-test', label: 'label test' }],
        };

        const expectedResult = {
            ...state,
            listWakeUpProvider: {
                ...state.listWakeUpProvider,
                isFetching: false,
                data: [{ id: 'id-test', label: 'label test' }],
                error: '',
            },
        };

        expect(reducer(undefined, action)).toEqual(expectedResult);
    });

    it('Test for ACTION_FETCH_LIST_WAKEUP_PROVIDER_FAILURE', () => {

        const action = {
            type: actions.ACTION_FETCH_LIST_WAKEUP_PROVIDER_FAILURE,
            payload: 'Error test',
        };

        const expectedResult = {
            ...state,
            listWakeUpProvider: {
                ...state.listWakeUpProvider,
                isFetching: false,
                data: [],
                error: 'Error test',
            },
        };

        expect(reducer(undefined, action)).toEqual(expectedResult);
    });

});
