import sinon from 'sinon';
import { getSortedPageTable, tryFetchAllUsers, fetchPaymentDetailsByUser } from '../actions';
import {
    ACTION_FETCH_ALL_USERS,
    ACTION_USERS_PAGE_CHANGE_INITIAL_STATE,
    ACTION_FETCH_USER_PAYMENT_DETAILS_REQUEST,
    ACTION_FETCH_USER_PAYMENT_DETAILS_SUCCESS,
} from '../constants';
import { stateStubUsersInitial, usersTableAllData2Elements } from '../../../helpers/users';

describe('tryFetchAllUsers', () => {
    describe('Positive scenario with empty result set', () => {
        it('dispatches 2 actions as expected', done => {
            const dispatchSpy = sinon.spy();
            const getStateSpy = sinon.spy();
            const listAllUsersStub = sinon.stub();

            listAllUsersStub.returns(Promise.resolve([]));

            const thunkFn = tryFetchAllUsers();
            const expectedPayload = {
                allUsers: [],
            };

            thunkFn(dispatchSpy, getStateSpy, { Users: { listAllUsers: listAllUsersStub } }).then(() => {
                expect(dispatchSpy.callCount).toBe(2);
                expect(listAllUsersStub.callCount).toBe(1);
                expect(getStateSpy.callCount).toBe(0);

                expect(dispatchSpy.firstCall.args[0].type).toBe(ACTION_USERS_PAGE_CHANGE_INITIAL_STATE);

                expect(dispatchSpy.secondCall.args[0].type).toBe(ACTION_FETCH_ALL_USERS);
                expect(dispatchSpy.secondCall.args[0].payload).toEqual(expectedPayload);

                done();
            });
        });
    });

    describe('Positive scenario with 2 elements result set', () => {
        it('dispatches 2 actions as expected', done => {
            const dispatchSpy = sinon.spy();
            const getStateStub = sinon.stub();
            const listAllUsersStub = sinon.stub();

            const usersTableAllData = usersTableAllData2Elements;
            const stateStubUsers = stateStubUsersInitial;

            getStateStub.returns({ users: stateStubUsers });

            listAllUsersStub.returns(Promise.resolve(usersTableAllData));

            const thunkFn = tryFetchAllUsers();

            const usersTablePageData = getSortedPageTable({
                data: usersTableAllData,
                page: 1,
                order: stateStubUsers.usersTableCurrentPage.order,
                orderBy: stateStubUsers.usersTableCurrentPage.orderBy,
            });

            const expectedPayload = {
                allUsers: usersTableAllData,
            };

            thunkFn(dispatchSpy, getStateStub, { Users: { listAllUsers: listAllUsersStub } }).then(() => {
                expect(dispatchSpy.callCount).toBe(2);
                expect(listAllUsersStub.callCount).toBe(1);

                expect(dispatchSpy.firstCall.args[0].type).toBe(ACTION_USERS_PAGE_CHANGE_INITIAL_STATE);

                expect(dispatchSpy.secondCall.args[0].type).toBe(ACTION_FETCH_ALL_USERS);
                expect(dispatchSpy.secondCall.args[0].payload).toEqual(expectedPayload);

                done();
            });
        });
    });
});

describe('getSortedPageTable', () => {
    describe('Positive scenario with 2 elements result set, sort by registrationDate', () => {
        it('dispatches 2 actions as expected, and testing sorting, registrationDate asc', done => {
            const usersTableAllData = new Array(...usersTableAllData2Elements);

            const usersTablePageData = getSortedPageTable({
                data: usersTableAllData,
                page: 1,
                order: 'asc',
                orderBy: 'registrationDate',
            });

            let expectedPayload = new Array(...usersTableAllData2Elements);
            expectedPayload = [expectedPayload[0], expectedPayload[1]];
            expect(usersTablePageData).toEqual(expectedPayload);

            done();
        });
        it('dispatches 2 actions as expected, and testing sorting, registrationDate desc', done => {
            const usersTableAllData = new Array(...usersTableAllData2Elements);
            const usersTablePageData = getSortedPageTable({
                data: usersTableAllData,
                page: 1,
                order: 'desc',
                orderBy: 'registrationDate',
            });

            let expectedPayload = new Array(...usersTableAllData2Elements);
            expectedPayload = [expectedPayload[1], expectedPayload[0]];
            expect(usersTablePageData).toEqual(expectedPayload);

            done();
        });
    });

    describe('Positive scenario with 2 elements result set, sort by firstName', () => {
        it('dispatches 2 actions as expected, and testing sorting, firstName asc', done => {
            const usersTableAllData = new Array(...usersTableAllData2Elements);

            const usersTablePageData = getSortedPageTable({
                data: usersTableAllData,
                page: 1,
                order: 'asc',
                orderBy: 'firstName',
            });

            let expectedPayload = new Array(...usersTableAllData2Elements);
            expectedPayload = [expectedPayload[0], expectedPayload[1]];
            expect(usersTablePageData).toEqual(expectedPayload);

            done();
        });
        it('dispatches 2 actions as expected, and testing sorting, firstName desc', done => {
            const usersTableAllData = new Array(...usersTableAllData2Elements);

            const usersTablePageData = getSortedPageTable({
                data: usersTableAllData,
                page: 1,
                order: 'desc',
                orderBy: 'firstName',
            });

            let expectedPayload = new Array(...usersTableAllData2Elements);
            expectedPayload = [expectedPayload[1], expectedPayload[0]];
            expect(usersTablePageData).toEqual(expectedPayload);

            done();
        });
    });

    describe('Positive scenario with 2 elements result set, sort by lastName', () => {
        it('dispatches 2 actions as expected, and testing sorting, lastName asc', done => {
            const usersTableAllData = new Array(...usersTableAllData2Elements);

            const usersTablePageData = getSortedPageTable({
                data: usersTableAllData,
                page: 1,
                order: 'asc',
                orderBy: 'lastName',
            });

            let expectedPayload = new Array(...usersTableAllData2Elements);
            expectedPayload = [expectedPayload[0], expectedPayload[1]];
            expect(usersTablePageData).toEqual(expectedPayload);

            done();
        });
        it('dispatches 2 actions as expected, and testing sorting, lastName desc', done => {
            const usersTableAllData = new Array(...usersTableAllData2Elements);

            const usersTablePageData = getSortedPageTable({
                data: usersTableAllData,
                page: 1,
                order: 'desc',
                orderBy: 'lastName',
            });

            let expectedPayload = new Array(...usersTableAllData2Elements);
            expectedPayload = [expectedPayload[1], expectedPayload[0]];
            expect(usersTablePageData).toEqual(expectedPayload);

            done();
        });
    });
});

describe('fetchPaymentDetailsByUser', () => {
    let dispatchSpy;
    let getStateSpy;
    let getStateStub;
    let APIClients;

    describe('Positive scenario', () => {
        const thunkFn = fetchPaymentDetailsByUser();
        let getPaymentDetailsStub;

        beforeEach(() => {
            dispatchSpy = sinon.spy();
            getStateSpy = sinon.spy();
            getStateStub = sinon.stub();
            getPaymentDetailsStub = sinon.stub();

            getStateStub.returns({
                users: {
                    selectedUser: {
                        id: 'userId-test',
                        profiles: [{ id: 'profileId-test' }],
                        paymentDetails: {
                            isFetching: false,
                            data: [],
                            error: '',
                        },
                    },
                },
            });

            APIClients = { Users: { getPaymentDetails: getPaymentDetailsStub } };

            getPaymentDetailsStub.returns(Promise.resolve([]));
        });

        it('dispatches 2 actions as expected', done => {
            thunkFn(dispatchSpy, getStateStub, APIClients).then(() => {
                expect(dispatchSpy.callCount).toBe(2);
                expect(getStateSpy.callCount).toEqual(0);
                expect(getPaymentDetailsStub.callCount).toEqual(1);

                expect(dispatchSpy.firstCall.args[0].type).toEqual(ACTION_FETCH_USER_PAYMENT_DETAILS_REQUEST);
                expect(dispatchSpy.secondCall.args[0].type).toEqual(ACTION_FETCH_USER_PAYMENT_DETAILS_SUCCESS);
                expect(dispatchSpy.secondCall.args[0].payload).toHaveLength(0);

                done();
            });
        });
    });
});
