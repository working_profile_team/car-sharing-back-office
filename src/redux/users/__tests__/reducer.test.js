import deepmerge from 'deepmerge';
import userReducer from '../reducer'
import {UserStore} from "../models/UserStore.model"
import {UserTable} from '../models/UserTable.model'
import allUsers from '__tests__/fixtures/users.json'

import {
    ACTION_FETCH_ALL_USERS,
    ACTION_USERS_PAGE_CHANGE_INITIAL_STATE,
    ACTION_USERS_PAGE_CHANGE,
    ACTION_SET_SELECTED_USER,
    ACTION_CHANGE_SELECTED_USER,
    ACTION_UNSELECT_USER,
    ACTION_CHANGE_SELECTED_USER_ADDRESS
} from '../constants'

describe('usersReducer reducer function', () => {
    it('returns UserStore when no action is given', () => {
        expect(userReducer(undefined, {})).toEqual(UserStore)
    })
    it('appends data from ACTION_FETCH_ALL_USERS', () => {
        const mockActionData = {
            type: ACTION_FETCH_ALL_USERS,
            payload: {
                allUsers: allUsers
            }
        };
        expect(userReducer(undefined, mockActionData).allUsers).toEqual(allUsers)
    })
    it('appends data from ACTION_USERS_PAGE_CHANGE_INITIAL_STATE', () => {
        const mockActionData = {
            type: ACTION_USERS_PAGE_CHANGE_INITIAL_STATE
        }
        expect(userReducer(undefined, mockActionData).usersTableCurrentPage).toEqual(UserTable)
    })
    it('appends data from ACTION_USERS_PAGE_CHANGE', () => {
        const mockActionData = {
            type: ACTION_USERS_PAGE_CHANGE,
            payload: {
                maxPages: 2,
            }
        }
        expect(userReducer(undefined, mockActionData).usersTableCurrentPage).toEqual({...UserStore.usersTableCurrentPage, ...mockActionData.payload})
    })
    it('appends data from ACTION_SET_SELECTED_USER', () => {
        const mockUser = {
            "id": "0017d7d9-734f-449a-9181-18ce172dea0f",
            "fleetId": "F0001",
            "userName": "byvVkQiGGo",
            "lastName": "Not Bloggs",
        };
        const mockActionData = {
            type: ACTION_SET_SELECTED_USER,
            payload: {
                ...mockUser,
            }
        };
        const userState = deepmerge({}, UserStore, {clone: true});
        userState.selectedUser.email = "vulogemail@gmail.com";
        let expectedReturnValue = {
            ...UserStore.selectedUser,
            email: 'vulogemail@gmail.com',
            ...mockActionData.payload,
        };
        expectedReturnValue.email = userState.selectedUser.email;
        expect(userReducer(userState, mockActionData).selectedUser).toEqual(expectedReturnValue)
    })
    it('appends data from ACTION_CHANGE_SELECTED_USER', () => {
        const mockUser = {
            "id": "0017d7d9-734f-449a-9181-18ce172dea0f",
            "fleetId": "F0001",
            "userName": "byvVkQiGGo",
            "lastName": "Not Bloggs",
        };
        const mockActionData = {
            type: ACTION_CHANGE_SELECTED_USER,
            payload: {
                ...mockUser,
            }
        };
        const expectedReturnValue = {
            ...UserStore.selectedUser,
            ...mockActionData.payload,
        };
        expect(userReducer(undefined, mockActionData).selectedUser).toEqual(expectedReturnValue)
    })
    it('appends data from ACTION_UNSELECT_USER', () => {
        const mockUser = {
            "id": "0017d7d9-734f-449a-9181-18ce172dea0f",
            "fleetId": "F0001",
            "userName": "byvVkQiGGo",
            "lastName": "Not Bloggs",
        };
        const mockActionData = {
            type: ACTION_UNSELECT_USER,
        };
        let userState = {
            selectedUser: {...mockUser}
        };
        expect(userReducer(userState, mockActionData).selectedUser).toEqual(UserStore.selectedUser)
    })
    it('appends data from ACTION_CHANGE_SELECTED_USER_ADDRESS', () => {
        const mockActionData = {
            type: ACTION_CHANGE_SELECTED_USER_ADDRESS,
            payload: {
                streetName: "Rue de la République 14"
            }
        };
        const expectedReturnValue = {
            ...UserStore.selectedUser.address,
            streetName: mockActionData.payload.streetName
        };
        expect(userReducer(undefined, mockActionData).selectedUser.address).toEqual(expectedReturnValue)
    })

})
