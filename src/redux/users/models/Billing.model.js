import moment from 'moment'
import {BillingTripTable} from "./BillingTripTable.model"
import {BillingOtherChargeTable} from "./BillingOtherChargeTable.model"
import {BillingProductCharge} from "./BillingProductCharge.model"

export const Billing = {
    year: new moment().year(),
    month: new moment().month()+1,
    allBillingTrips: [],
    billingTripsTableCurrentPage: Object.assign({}, BillingTripTable),
    allBillingOtherCharges: [],
    billingOtherChargesTableCurrentPage: Object.assign({}, BillingOtherChargeTable),
    openChargePopup: false,
    productChargeFormData: Object.assign({}, BillingProductCharge),
    availableChargeProducts: [],
    customerBalance: 0
};
