import Localize from 'components/Localize';

export const BillingOtherChargeTable = {
    page: 1,
    maxPages: 1,
    pageSize: 10,
    order: 'asc',
    orderBy: 'billingDate',
    columnsData: [
        {fieldName: 'billingDateParsed', fieldType: "Date", label: Localize.locale.users.summary.billing.otherChargeTable.billingDate},
        {fieldName: 'productName', fieldType: "String", label: Localize.locale.users.summary.billing.otherChargeTable.productName},
        {fieldName: 'billingStatus', fieldType: "String", label: Localize.locale.users.summary.billing.otherChargeTable.billingStatus},
        {fieldName: 'totalWithoutTax', fieldType: "Currency", label: Localize.locale.users.summary.billing.otherChargeTable.totalWithoutTax},
        {fieldName: 'totalTaxAmount', fieldType: "Currency", label: Localize.locale.users.summary.billing.otherChargeTable.totalTaxAmount},
        {fieldName: 'totalWithTax', fieldType: "Currency", label: Localize.locale.users.summary.billing.otherChargeTable.totalWithTax},
        {fieldName: 'invoiceButton', fieldType: "Button", label: Localize.locale.users.summary.billing.actions, buttonLabel: Localize.locale.users.summary.billing.otherChargeTable.invoiceButton},
    ],
};
