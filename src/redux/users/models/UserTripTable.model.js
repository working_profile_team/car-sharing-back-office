import Localize from "components/Localize";

const { name, fleetId, bookingDate, duration, mileage, status } = Localize.locale.users.summary.trips; 

export const UserTripTable = {
    page: 1,
    maxPages: 1,
    pageSize: 10,
    order: 'desc',
    orderBy: 'date',
    columnsData: [
        {fieldName: 'tripId', fieldType: "Id", button: false},
        {fieldName: 'name', fieldType: "String", button: true, label: name},
        {fieldName: 'fleetId', fieldType: "String", button: false, label: fleetId},
        {fieldName: 'dateParsed', fieldType: "DateTime", button: false, label: bookingDate},
        {fieldName: 'durationParsed', fieldType: "Duration", button: false, label: duration},
        {fieldName: 'lengthParsed', fieldType: "Distance", button: false, label: mileage},
        {fieldName: 'tripStatus', fieldType: "String", button: false, label: status},
    ],
};
