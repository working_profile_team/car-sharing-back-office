import {Address} from "./Address.model"
import {Billing} from "./Billing.model"
import {UserTripTable} from "./UserTripTable.model"
import { UserSystemCreditsTableModel } from "./UserSystemCredits.model";

export const User = {
    id: undefined,
    userId: undefined,
    fleetId: undefined,
    userName: '',
    password: '',
    accountStatus: 'PENDING',
    firstName: '',
    lastName: '',
    preferredName: '',
    middleName: '',
    address: Object.assign({}, Address),
    profiles: [],
    registrationDate: '',
    birthDate: '',
    documents: [],
    invoices: [],
    gender: 'UNDEFINED',
    membershipNumber: '',
    nationality: '',
    email: '',
    roles: [],
    lastRole:'',
    oldRoles: [],
    locale: 'en_US',
    billing: Object.assign({}, Billing),
    editDocumentPopup: {
        documentType: undefined,
        open: false
    },
    userLastTrip: undefined,
    allUserTrips: [],
    allUserCredits : [],
    userTripTableCurrentPage: Object.assign({}, UserTripTable),
    userSystemCreditsTableCurrentPage: { ...UserSystemCreditsTableModel },
    paymentDetails: {
        isFetching: false,
        data: [],
        error: ''
    },
};
