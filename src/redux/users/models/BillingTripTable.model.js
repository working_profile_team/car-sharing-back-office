import Localize from '../../../components/Localize';

export const BillingTripTable = {
    page: 1,
    maxPages: 1,
    pageSize: 10,
    order: 'asc',
    orderBy: 'startDate',
    paymentStatus: {
        REFUSED: 'REFUSED',
        PENDING: 'PENDING',
    },
    columnsData: [
        {
            fieldName: 'startDateParsed',
            fieldType: 'Date',
            label: Localize.locale.users.summary.billing.billingTripTable.startDate,
        },
        {
            fieldName: 'durationParsed',
            fieldType: 'Duration',
            label: Localize.locale.users.summary.billing.billingTripTable.duration,
        },
        {
            fieldName: 'vehiculeModelName',
            fieldType: 'String',
            label: Localize.locale.users.summary.billing.billingTripTable.vehiculeModelName,
        },
        {
            fieldName: 'lengthParsed',
            fieldType: 'Distance',
            label: Localize.locale.users.summary.billing.billingTripTable.lengthParsed,
        },
        {
            fieldName: 'totalWithoutTax',
            fieldType: 'Currency',
            label: Localize.locale.users.summary.billing.billingTripTable.totalWithoutTax,
        },
        {
            fieldName: 'paymentStatus',
            fieldType: 'String',
            label: Localize.locale.users.summary.billing.billingTripTable.paymentStatus,
        },
        {
            fieldName: 'totalTaxAmount',
            fieldType: 'Currency',
            label: Localize.locale.users.summary.billing.billingTripTable.totalTaxAmount,
        },
        {
            fieldName: 'totalWithTax',
            fieldType: 'Currency',
            label: Localize.locale.users.summary.billing.billingTripTable.totalWithTax,
        },
        {
            fieldName: 'tripDetailsButton',
            fieldType: 'Button',
            label: Localize.locale.users.summary.billing.actions,
            buttonLabel: Localize.locale.users.summary.billing.billingTripTable.tripDetailsButton,
        },
        {
            fieldName: 'invoiceButton',
            fieldType: 'Button',
            label: '',
            buttonLabel: Localize.locale.users.summary.billing.billingTripTable.invoiceButton,
        },
        {
            fieldName: 'payButton',
            fieldType: 'Button',
            label: '',
            buttonLabel: Localize.locale.users.summary.billing.billingTripTable.payButton,
        },
    ],
};
