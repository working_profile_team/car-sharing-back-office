export const UserDocumentTable = {
    page: 1,
    maxPages: 1,
    data: [],
    order: 'asc',
    orderBy: 'name'
};
