import {User} from "./User.model"
import {UserDocumentTable} from "./UserDocumentTable.model"
import {UserTable} from "./UserTable.model"
import {PendingUserTable} from "./PendingUserTable.model"
import {AutosuggestChips} from "./AutosuggestChips.model"


export const UserStore = {
    allUsers: [],
    usersTableCurrentPage: Object.assign({}, UserTable),
    pendingUsersTableCurrentPage: Object.assign({}, PendingUserTable),
    userDocumentTableAllData: [],
    userDocumentTableCurrentPage: Object.assign({}, UserDocumentTable),
    userInvoiceTableAllData: [],
    userInvoiceTableCurrentPage: Object.assign({}, UserDocumentTable),
    autosuggestChipsRoles: Object.assign({}, AutosuggestChips),
    selectedUser: Object.assign({}, User),
    flowState: 'READ_ONLY',
    wasSaved: false,
    userSummaryCurrentTab: 0,
    selectedUsersTabIndex: 0
};
