import Localize from 'components/Localize';

const {
    initialAmount,
    usedAmount,
    validityEndDate,
    validityStartDate,
} = Localize.locale.users.summary.systemCredits.credits;

export const UserSystemCreditsTableModel = {
    page: 1,
    maxPages: 1,
    pageSize: 10,
    order: 'desc',
    orderBy: 'date',
    columnsData: [
        { fieldName: 'id', fieldType: 'Id', button: false, label: 'ID' }, // @TODO regarder pourquoi le champs "ID" n'apparait pas
        { fieldName: 'initialAmount', fieldType: 'Number', button: true, label: initialAmount },
        { fieldName: 'usedAmount', fieldType: 'Number', button: false, label: usedAmount },
        { fieldName: 'validityEndDate', fieldType: 'DateTime', button: false, label: validityEndDate },
        { fieldName: 'validityStartDate', fieldType: 'DateTime', button: false, label: validityStartDate },
    ],
};
