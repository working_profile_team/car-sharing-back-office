export const Profile = {
    id: undefined,
    name: '',
    email: '',
    rfid: '',
    phoneNumber: '',
    entity: '',
    identificationNumber: '',
    emailConsent: false
};
