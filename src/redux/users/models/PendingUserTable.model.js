import Localize from "components/Localize";

export const PendingUserTable = {
    page: 1,
    maxPages: 1,
    pageSize: 10,
    order: 'asc',
    orderBy: 'userName',
    columnsData: [
        {fieldName: 'userName', fieldType: "String", button: true, label: Localize.locale["Components-UserTable-UserName"]},
        {fieldName: 'lastName', fieldType: "String", button: false, label: Localize.locale["Components-UserTable-LastName"]},
        {fieldName: 'firstName', fieldType: "String", button: false, label: Localize.locale["Components-UserTable-FirstName"]}
    ]
};
