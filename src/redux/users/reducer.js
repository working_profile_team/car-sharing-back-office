import {
    ACTION_CHANGE_AUTOSUGGEST_USER_STATUS,
    ACTION_CHANGE_FLOW_STATE,
    ACTION_CHANGE_SELECTED_USER,
    ACTION_CHANGE_SELECTED_USER_ADDRESS,
    ACTION_CHANGE_SELECTED_USER_BILLING_ADDRESS,
    ACTION_CHANGE_SELECTED_USER_BILLING_OTHER_CHARGES_PAGE_CHANGE,
    ACTION_CHANGE_SELECTED_USER_BILLING_TRIPS_PAGE_CHANGE,
    ACTION_CHANGE_SELECTED_USER_DOCUMENT,
    ACTION_CHANGE_SELECTED_USER_PROFILE,
    ACTION_CHANGE_SELECTED_USER_USER_TRIPS,
    ACTION_FETCH_ALL_USERS,
    ACTION_FETCH_ALL_USERS_SUCCESS,
    ACTION_FETCH_SELECT_USER_DOCUMENTS,
    ACTION_FETCH_USER_PAYMENT_DETAILS_REQUEST,
    ACTION_FETCH_USER_PAYMENT_DETAILS_SUCCESS,
    ACTION_OPEN_CHARGE_PRODUCT_POPUP,
    ACTION_OPEN_EDIT_DOCUMENT_POPUP,
    ACTION_PENDING_USERS_PAGE_CHANGE,
    ACTION_RESET_PASSWORD,
    ACTION_SAVE_SELECTED_USER,
    ACTION_SELECT_USER_SUCCESS,
    ACTION_SET_AVAILABLE_CHARGE_PRODUCTS,
    ACTION_SET_SELECTED_USER,
    ACTION_SET_SELECTED_USER_BILLING,
    ACTION_SET_USER_CHARGE_FORM_DATA,
    ACTION_SET_USER_SUMMARY_CURRENT_TAB,
    ACTION_UNSELECT_USER,
    ACTION_USER_DOCUMENTS_PAGE_CHANGE,
    ACTION_USER_DOCUMENTS_PAGE_CHANGE_INITIAL_STATE,
    ACTION_USER_INVOICES_PAGE_CHANGE,
    ACTION_USER_INVOICES_PAGE_CHANGE_INITIAL_STATE,
    ACTION_USERS_PAGE_CHANGE,
    ACTION_USERS_PAGE_CHANGE_INITIAL_STATE,
    ACTION_WAS_SAVED,
    ACTION_SET_SELECTED_USERS_TAB_INDEX,
    ACTION_CHANGE_SELECTED_USER_USER_SYSTEM_CREDITS,
    ACTION_UPDATE_SELECTED_USER_SYSTEM_CREDITS,
} from './constants';

import { UserStore } from './models/UserStore.model';
import { UserTable } from './models/UserTable.model';
import { UserDocumentTable } from './models/UserDocumentTable.model';
import { UserInvoiceTable } from './models/UserInvoiceTable.model';
import { User } from './models/User.model';

const userStore = UserStore;

export default function state(state = userStore, action) {
    switch (action.type) {
        case ACTION_SET_SELECTED_USERS_TAB_INDEX:
            return {
                ...state,
                selectedUsersTabIndex: action.payload,
            };

        case ACTION_FETCH_ALL_USERS:
            return {
                ...state,
                allUsers: action.payload.allUsers,
            };

        case ACTION_USERS_PAGE_CHANGE_INITIAL_STATE:
            return {
                ...state,
                usersTableCurrentPage: { ...UserTable },
            };

        case ACTION_USERS_PAGE_CHANGE:
            return {
                ...state,
                usersTableCurrentPage: {
                    ...state.usersTableCurrentPage,
                    ...action.payload,
                },
            };

        case ACTION_PENDING_USERS_PAGE_CHANGE:
            return {
                ...state,
                pendingUsersTableCurrentPage: {
                    ...state.pendingUsersTableCurrentPage,
                    ...action.payload,
                },
            };

        // DEPRECATED please use selectUserSaga, or ACTION_SELECT_USER_SUCCESS if you need to dispatch directly this action
        case ACTION_SET_SELECTED_USER:
            return {
                ...state,
                selectedUser: {
                    ...User,
                    email: state.selectedUser.email,
                    ...action.payload,
                },
            };

        case ACTION_SELECT_USER_SUCCESS:
            return {
                ...state,
                selectedUser: {
                    ...User,
                    email: state.selectedUser.email,
                    ...action.payload,
                },
            };

        case ACTION_CHANGE_SELECTED_USER:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    ...action.payload,
                },
            };

        case ACTION_UNSELECT_USER:
            return {
                ...state,
                selectedUser: { ...User },
            };

        case ACTION_CHANGE_SELECTED_USER_ADDRESS:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    address: {
                        ...state.selectedUser.address,
                        ...action.payload,
                    },
                },
            };

        case ACTION_CHANGE_SELECTED_USER_DOCUMENT:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    documents: {
                        ...state.selectedUser.documents,
                        [action.payload.documentType]: {
                            ...state.selectedUser.documents[action.payload.documentType],
                            ...action.payload.documentData,
                        },
                    },
                },
            };

        case ACTION_CHANGE_SELECTED_USER_USER_TRIPS:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    userTripTableCurrentPage: {
                        ...state.selectedUser.userTripTableCurrentPage,
                        ...action.payload,
                    },
                },
            };

        case ACTION_CHANGE_SELECTED_USER_USER_SYSTEM_CREDITS:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    userSystemCreditsTableCurrentPage: {
                        ...state.selectedUser.userSystemCreditsTableCurrentPage,
                        ...action.payload,
                    },
                },
            };
        case ACTION_UPDATE_SELECTED_USER_SYSTEM_CREDITS:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    allUserCredits: action.payload,
                },
            };
        case ACTION_SAVE_SELECTED_USER:
            if (action.payload && action.payload.id) {
                return {
                    ...state,
                    selectedUser: {
                        ...state.selectedUser,
                        id: action.payload.id,
                    },
                };
            }
            break;

        case ACTION_CHANGE_FLOW_STATE:
            return {
                ...state,
                flowState: action.payload,
            };

        case ACTION_CHANGE_AUTOSUGGEST_USER_STATUS:
            return {
                ...state,
                autosuggestAccountStatus: {
                    ...state.autosuggestAccountStatus,
                    ...action.payload,
                },
            };

        case ACTION_FETCH_SELECT_USER_DOCUMENTS:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    documents: action.payload.documents,
                },
                userDocumentTableAllData: action.payload.userDocumentTableAllData,
                userDocumentTableCurrentPage: {
                    ...state.userDocumentTableCurrentPage,
                    ...action.payload.userDocumentTableCurrentPage,
                },
            };

        case ACTION_USER_DOCUMENTS_PAGE_CHANGE_INITIAL_STATE:
            return {
                ...state,
                userDocumentTableCurrentPage: { ...UserDocumentTable },
            };

        case ACTION_USER_INVOICES_PAGE_CHANGE_INITIAL_STATE:
            return {
                ...state,
                userInvoiceTableCurrentPage: { ...UserInvoiceTable },
            };

        case ACTION_USER_DOCUMENTS_PAGE_CHANGE:
            return {
                ...state,
                userDocumentTableCurrentPage: {
                    ...state.userDocumentTableCurrentPage,
                    ...action.payload,
                },
            };

        case ACTION_USER_INVOICES_PAGE_CHANGE:
            return {
                ...state,
                userInvoiceTableCurrentPage: {
                    ...state.userInvoiceTableCurrentPage,
                    ...action.payload,
                },
            };

        case ACTION_CHANGE_SELECTED_USER_PROFILE:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    profiles: [
                        {
                            ...state.selectedUser.profiles[0],
                            ...action.payload,
                        },
                    ],
                },
            };

        case ACTION_CHANGE_SELECTED_USER_BILLING_ADDRESS:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    profiles: [
                        {
                            ...state.selectedUser.profiles[0],
                            entity: {
                                ...state.selectedUser.profiles[0].entity,
                                billingAddress: {
                                    ...state.selectedUser.profiles[0].entity.billingAddress,
                                    ...action.payload,
                                },
                            },
                        },
                    ],
                },
            };

        case ACTION_RESET_PASSWORD:
            break;

        case ACTION_WAS_SAVED:
            return {
                ...state,
                wasSaved: action.payload,
            };

        case ACTION_SET_SELECTED_USER_BILLING:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    billing: {
                        ...state.selectedUser.billing,
                        ...action.payload.billing,
                    },
                },
            };

        case ACTION_SET_USER_CHARGE_FORM_DATA:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    billing: {
                        ...state.selectedUser.billing,
                        productChargeFormData: {
                            ...state.selectedUser.billing.productChargeFormData,
                            ...action.payload,
                        },
                    },
                },
            };

        case ACTION_CHANGE_SELECTED_USER_BILLING_TRIPS_PAGE_CHANGE:
            return {
                ...state,
                billingTripsTableCurrentPage: {
                    ...state.billingTripsTableCurrentPage,
                    ...action.payload,
                },
            };

        case ACTION_CHANGE_SELECTED_USER_BILLING_OTHER_CHARGES_PAGE_CHANGE:
            return {
                ...state,
                billingOtherChargesTableCurrentPage: {
                    ...state.billingOtherChargesTableCurrentPage,
                    ...action.payload,
                },
            };

        case ACTION_OPEN_CHARGE_PRODUCT_POPUP:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    billing: {
                        ...state.selectedUser.billing,
                        ...action.payload,
                    },
                },
            };

        case ACTION_OPEN_EDIT_DOCUMENT_POPUP:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    editDocumentPopup: {
                        ...state.selectedUser.editDocumentPopup,
                        ...action.payload,
                    },
                },
            };

        case ACTION_SET_AVAILABLE_CHARGE_PRODUCTS:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    billing: {
                        ...state.selectedUser.billing,
                        availableChargeProducts: action.payload,
                    },
                },
            };

        case ACTION_SET_USER_SUMMARY_CURRENT_TAB:
            return {
                ...state,
                userSummaryCurrentTab: action.payload,
            };

        case ACTION_FETCH_ALL_USERS_SUCCESS:
            return {
                ...state,
                allUsers: action.payload.allUsers,
            };

        case ACTION_FETCH_USER_PAYMENT_DETAILS_REQUEST:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    paymentDetails: {
                        ...state.selectedUser.paymentDetails,
                        isFetching: true,
                        error: '',
                        data: [],
                    },
                },
            };

        case ACTION_FETCH_USER_PAYMENT_DETAILS_SUCCESS:
            return {
                ...state,
                selectedUser: {
                    ...state.selectedUser,
                    paymentDetails: {
                        ...state.selectedUser.paymentDetails,
                        isFetching: false,
                        data: action.payload,
                    },
                },
            };

        default:
            break;
    }

    return state;
}
