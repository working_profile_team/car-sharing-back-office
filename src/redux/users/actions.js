import _ from 'lodash';
import moment from 'moment';
import * as ErrorHandler from 'apis/VuLog/ErrorHandler';
import {
    ACTION_APPLICATION_ERROR,
    ACTION_SHOW_SNACK_BAR_MESSAGE_SUCCESS,
    ACTION_SHOW_SNACK_BAR_MESSAGE_FAILURE,
} from 'redux/application/constants';
import Localize from 'components/Localize';
import {
    ACTION_CHANGE_AUTOSUGGEST_USER_STATUS,
    ACTION_CHANGE_FLOW_STATE,
    ACTION_CHANGE_SELECTED_USER,
    ACTION_CHANGE_SELECTED_USER_ADDRESS,
    ACTION_CHANGE_SELECTED_USER_BILLING_ADDRESS,
    ACTION_CHANGE_SELECTED_USER_BILLING_OTHER_CHARGES_PAGE_CHANGE,
    ACTION_CHANGE_SELECTED_USER_BILLING_TRIPS_PAGE_CHANGE,
    ACTION_CHANGE_SELECTED_USER_DOCUMENT,
    ACTION_CHANGE_SELECTED_USER_PROFILE,
    ACTION_CHANGE_SELECTED_USER_USER_TRIPS,
    ACTION_CHANGE_SELECTED_USER_USER_SYSTEM_CREDITS,
    ACTION_FETCH_ALL_USERS,
    ACTION_FETCH_ALL_USERS_REQUEST,
    ACTION_FETCH_SELECT_USER_DOCUMENTS,
    ACTION_FETCH_USER_PAYMENT_DETAILS_REQUEST,
    ACTION_FETCH_USER_PAYMENT_DETAILS_SUCCESS,
    ACTION_OPEN_CHARGE_PRODUCT_POPUP,
    ACTION_OPEN_EDIT_DOCUMENT_POPUP,
    ACTION_PENDING_USERS_PAGE_CHANGE,
    ACTION_RESET_PASSWORD,
    ACTION_SAVE_SELECTED_USER,
    ACTION_SELECT_USER_REQUEST,
    ACTION_SELECT_USER_SUCCESS,
    ACTION_SET_AVAILABLE_CHARGE_PRODUCTS,
    ACTION_SET_SELECTED_USER,
    ACTION_SET_SELECTED_USER_BILLING,
    ACTION_SET_USER_CHARGE_FORM_DATA,
    ACTION_SET_USER_SUMMARY_CURRENT_TAB,
    ACTION_UNSELECT_USER,
    ACTION_UPDATE_SELECTED_USER_DOCUMENT_REQUEST,
    ACTION_USER_DOCUMENTS_PAGE_CHANGE,
    ACTION_USER_DOCUMENTS_PAGE_CHANGE_INITIAL_STATE,
    ACTION_USER_INVOICES_PAGE_CHANGE,
    ACTION_USERS_PAGE_CHANGE,
    ACTION_USERS_PAGE_CHANGE_INITIAL_STATE,
    ACTION_WAS_SAVED,
    ACTION_SET_SELECTED_USERS_TAB_INDEX,
    ACTION_SAVE_USER_ROLE,
    ACTION_DELETE_USER_ROLE,
    ACTION_CHANGE_SELECTED_USER_LAST_ROLE,
} from './constants';
import { BillingProductCharge } from './models/BillingProductCharge.model';
import { Document } from './models/Document.model';

export function setSelectedUsersTabIndex(index) {
    return {
        type: ACTION_SET_SELECTED_USERS_TAB_INDEX,
        payload: index,
    };
}

export function setPageChangeInitialState() {
    return {
        type: ACTION_USERS_PAGE_CHANGE_INITIAL_STATE,
    };
}

export const fetchPaymentDetailsByUserRequest = () => ({
    type: ACTION_FETCH_USER_PAYMENT_DETAILS_REQUEST,
});

export const fetchPaymentDetailsByUserSuccess = payload => ({
    type: ACTION_FETCH_USER_PAYMENT_DETAILS_SUCCESS,
    payload,
});

export const fetchPaymentDetailsByUser = () => async (dispatch, getState, APIClients) => {
    const selectedUser = await getState().users.selectedUser;
    const userId = selectedUser.id;
    const profileId = selectedUser.profiles.map(profile => profile.id);

    dispatch(fetchPaymentDetailsByUserRequest());
    if (userId && profileId) {
        const data = await APIClients.Users.getPaymentDetails(userId, profileId);
        dispatch(fetchPaymentDetailsByUserSuccess(data));
    }
};

export function getSortedPageTable({ data, page, order, orderBy }) {
    data.sort(
        (a, b) => (order === 'asc' ? a[orderBy].localeCompare(b[orderBy]) : b[orderBy].localeCompare(a[orderBy]))
    );
    const startPos = (page - 1) * 10;
    const endPos = page * 10;
    data = _.slice(data, startPos, endPos);
    return data;
}

export function tryFetchAllUsers() {
    return (dispatch, getState, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_USERS_PAGE_CHANGE_INITIAL_STATE,
            });
            APIClients.Users.listAllUsers()
                .then(allUsers => {
                    dispatch({
                        type: ACTION_FETCH_ALL_USERS,
                        payload: {
                            allUsers,
                        },
                    });
                    resolve();
                })
                .catch(err => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function tryChangeUsersTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const newPage = payload.newPage ? payload.newPage : state().users.usersTableCurrentPage.page;
            const orderBy = payload.orderBy ? payload.orderBy : state().users.usersTableCurrentPage.orderBy;
            const order = payload.order ? payload.order : state().users.usersTableCurrentPage.order;

            dispatch({
                type: ACTION_USERS_PAGE_CHANGE,
                payload: { page: newPage, orderBy, order },
            });
            resolve();
        });
}

export function tryChangeUserTripsTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const newPage = payload.newPage
                ? payload.newPage
                : state().users.selectedUser.userTripTableCurrentPage.page;
            const orderBy = payload.orderBy
                ? payload.orderBy
                : state().users.selectedUser.userTripTableCurrentPage.orderBy;
            const order = payload.order ? payload.order : state().users.selectedUser.userTripTableCurrentPage.order;

            dispatch({
                type: ACTION_CHANGE_SELECTED_USER_USER_TRIPS,
                payload: { page: newPage, orderBy, order },
            });
            resolve();
        });
}

export function tryChangeUserSystemCreditsTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const newPage = payload.newPage
                ? payload.newPage
                : state().users.selectedUser.userSystemCreditsTableCurrentPage.page;
            const orderBy = payload.orderBy
                ? payload.orderBy
                : state().users.selectedUser.userSystemCreditsTableCurrentPage.orderBy;
            const order = payload.order
                ? payload.order
                : state().users.selectedUser.userSystemCreditsTableCurrentPage.order;

            dispatch({
                type: ACTION_CHANGE_SELECTED_USER_USER_SYSTEM_CREDITS,
                payload: { page: newPage, orderBy, order },
            });
            resolve();
        });
}

export function tryChangePendingUsersTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const newPage = payload.newPage ? payload.newPage : state().users.usersTableCurrentPage.page;
            const orderBy = payload.orderBy ? payload.orderBy : state().users.usersTableCurrentPage.orderBy;
            const order = payload.order ? payload.order : state().users.usersTableCurrentPage.order;

            dispatch({
                type: ACTION_PENDING_USERS_PAGE_CHANGE,
                payload: { page: newPage, orderBy, order },
            });
            resolve();
        });
}

export function saveSelectedUser(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const userToSave = _.omit(state().users.selectedUser, ['billing']);

            if (userToSave.id === undefined) {
                APIClients.Users.createUser(userToSave)
                    .then(user => {
                        dispatch({
                            type: ACTION_CHANGE_SELECTED_USER,
                            payload: { id: user.id },
                        });
                        resolve();
                    })
                    .catch(err => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            } else {
                const userId = userToSave.id;
                delete userToSave.id;
                APIClients.Users.updateUser(userId, userToSave)
                    .then(res => {
                        dispatch({
                            type: ACTION_SAVE_SELECTED_USER,
                        });
                        resolve();
                    })
                    .catch((err, msg, others) => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            }
        });
}

export function selectUserRequest(userId) {
    return {
        type: ACTION_SELECT_USER_REQUEST,
        payload: userId,
    };
}

export function selectUserSuccess(userData) {
    return {
        type: ACTION_SELECT_USER_SUCCESS,
        payload: userData,
    };
}

// DEPRECATED please use selectUserSaga to get all User data
export function selectUser(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            Promise.all([APIClients.Users.getUser(payload.id), APIClients.Users.getUserRoles(payload.id)])
                .then(([data, roles]) => {
                    if (data.profiles.length > 0 && data.profiles[0].email !== null) {
                        data.email = data.profiles[0].email;
                    }

                    data.roles = roles;

                    dispatch({
                        type: ACTION_SET_SELECTED_USER,
                        payload: data,
                    });
                    resolve();
                })
                .catch((err, msg, others) => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function changeSelectedUser(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_SELECTED_USER,
                payload,
            });
            resolve();
        });
}

export function unselectUser(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_UNSELECT_USER,
            });
            resolve();
        });
}

export function changeSelectedUserAddress(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_SELECTED_USER_ADDRESS,
                payload,
            });
            resolve();
        });
}

export function changeSelectedUserDocument(payload) {
    return (dispatch, state, APIClients) => {
        dispatch({
            type: ACTION_CHANGE_SELECTED_USER_DOCUMENT,
            payload,
        });
    };
}

export function changeFlowState(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_FLOW_STATE,
                payload,
            });
            resolve();
        });
}

export function changeAutosuggestAccountStatus(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_AUTOSUGGEST_USER_STATUS,
                payload,
            });
            resolve();
        });
}

export function saveSelectedUserChangeAccountStatus(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const users = state().users;
            const selectedUser = users.selectedUser;
            const payloadToSave = {
                user_id: selectedUser.id,
                accountStatus: selectedUser.accountStatus,
            };

            APIClients.Users.updateUserStatus(payloadToSave)
                .then(res => {
                    dispatch({
                        type: ACTION_SAVE_SELECTED_USER,
                    });
                    resolve();
                })
                .catch((err, msg, others) => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function saveSelectedChangeRole(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const users = state().users;
            const selectedUser = users.selectedUser;

            APIClients.Users.addRoleToUser(selectedUser.id, selectedUser.role)
                .then(res => {
                    dispatch({
                        type: ACTION_SAVE_USER_ROLE,
                    });
                    resolve();
                })
                .catch((err, msg, others) => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function deleteOldRole(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Users.deleteRoleOfUser(payload.id, payload.oldRole)
                .then(res => {
                    dispatch({
                        type: ACTION_DELETE_USER_ROLE,
                    });
                    resolve();
                })
                .catch((err, msg, others) => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function saveSelectedUserChangeUserAddress(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const users = state().users;
            const selectedUser = users.selectedUser;

            const userAddress = {
                city: '',
                country: '',
                postalCode: '',
                region: '',
                streetName: '',
                ...selectedUser.address,
            };
            delete userAddress.id;

            const payloadToSave = {
                user_id: selectedUser.id,
                address: userAddress,
            };

            let isEmpty = true;

            for (const key in userAddress) {
                if (userAddress[key] !== '') {
                    isEmpty = false;
                    break;
                }
            }

            if (isEmpty) {
                resolve();
                return;
            }

            APIClients.Users.updateUserAddress(payloadToSave)
                .then(res => {
                    dispatch({
                        type: ACTION_SAVE_SELECTED_USER,
                    });
                    resolve();
                })
                .catch((err, msg, others) => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function saveSelectedUserChangeUserProfile(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const users = state().users;
            const selectedUser = users.selectedUser;
            const selectedUserProfile = selectedUser.profiles[0];

            const payloadToSave = {
                profile_id: selectedUserProfile.id,
                profile: selectedUserProfile,
            };

            delete selectedUserProfile.id;

            APIClients.Users.updateUserProfile(payloadToSave)
                .then(res => {
                    dispatch({
                        type: ACTION_SAVE_SELECTED_USER,
                    });
                    resolve();
                })
                .catch((err, msg, others) => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function saveSelectedUserChangeUserEmail(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const users = state().users;
            const selectedUser = users.selectedUser;

            const payloadToSave = {
                profile_id: selectedUser.profiles[0].id,
                profile: {
                    email: selectedUser.email,
                },
            };

            APIClients.Users.updateUserProfile(payloadToSave)
                .then(res => {
                    dispatch({
                        type: ACTION_SAVE_SELECTED_USER,
                    });
                    resolve();
                })
                .catch((err, msg, others) => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function getUserDocuments(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_USER_DOCUMENTS_PAGE_CHANGE_INITIAL_STATE,
            });
            APIClients.Users.getUserDocuments({ userId: payload.id })
                .then(userDocumentTableAllData => {
                    dispatch({
                        type: ACTION_FETCH_SELECT_USER_DOCUMENTS,
                        payload: {
                            documents: userDocumentTableAllData,
                            userDocumentTableAllData,
                            userDocumentTableCurrentPage: {
                                data: userDocumentTableAllData,
                                total: userDocumentTableAllData.length,
                                maxPages: Math.ceil(userDocumentTableAllData.length / 10),
                            },
                        },
                    });
                    resolve();
                })
                .catch(err => {
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function tryChangeUserDocumentTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const userDocumentTableAllData = state().users.userDocumentTableAllData;
            const newPage =
                payload && payload.newPage ? payload.newPage : state().users.userDocumentTableCurrentPage.page;
            const orderBy =
                payload && payload.orderBy ? payload.orderBy : state().users.userDocumentTableCurrentPage.orderBy;
            const order = payload && payload.order ? payload.order : state().users.userDocumentTableCurrentPage.order;

            const userDocumentTablePageData = getSortedPageTable({
                data: userDocumentTableAllData,
                page: newPage,
                order,
                orderBy,
            });

            dispatch({
                type: ACTION_USER_DOCUMENTS_PAGE_CHANGE,
                payload: { page: newPage, data: userDocumentTablePageData, orderBy, order },
            });
            resolve();
        });
}

export function tryChangeUserInvoiceTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const userInvoiceTableAllData = state().users.userInvoiceTableAllData;

            const newPage =
                payload && payload.newPage ? payload.newPage : state().users.userInvoiceTableCurrentPage.page;
            const orderBy =
                payload && payload.orderBy ? payload.orderBy : state().users.userInvoiceTableCurrentPage.orderBy;
            const order = payload && payload.order ? payload.order : state().users.userInvoiceTableCurrentPage.order;

            const userInvoiceTablePageData = getSortedPageTable({
                data: userInvoiceTableAllData,
                page: newPage,
                order,
                orderBy,
            });

            dispatch({
                type: ACTION_USER_INVOICES_PAGE_CHANGE,
                payload: { page: newPage, data: userInvoiceTablePageData, orderBy, order },
            });
            resolve();
        });
}

export function changeSelectedUserProfile(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_SELECTED_USER_PROFILE,
                payload,
            });
            resolve();
        });
}

export function changeSelectedUserBillingAddress(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_SELECTED_USER_BILLING_ADDRESS,
                payload,
            });
            resolve();
        });
}

export function saveLastRoleOnStore(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_CHANGE_SELECTED_USER_LAST_ROLE,
                payload,
            });
            resolve();
        });
}

export function saveSelectedUserChangeUserBillingAddress(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const users = state().users;
            const selectedUser = users.selectedUser;
            const selectedUserProfile = selectedUser.profiles[0];
            if (selectedUserProfile.entity && selectedUserProfile.entity.billingAddress) {
                const userBillingAddress = selectedUserProfile.entity.billingAddress;
                delete userBillingAddress.id;

                const payloadToSave = {
                    entity_id: selectedUserProfile.entity.id,
                    address: userBillingAddress,
                };

                APIClients.Users.updateUserBillingAddress(payloadToSave)
                    .then(res => {
                        dispatch({
                            type: ACTION_SAVE_SELECTED_USER,
                        });
                        resolve();
                    })
                    .catch((err, msg, others) => {
                        ErrorHandler.logError(err);
                        reject(err);
                    });
            } else {
                // We need to continue the save workflow even if there is no entity
                resolve();
            }
        });
}

export function resetPassword(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Users.resetPassword({ email: payload.email })
                .then(res => {
                    dispatch({
                        type: ACTION_RESET_PASSWORD,
                        payload: res,
                    });
                    resolve();
                })
                .catch((err, msg, others) => {
                    dispatch({
                        type: ACTION_APPLICATION_ERROR,
                        payload: {
                            message: 'There was an unexpected error',
                            error: true,
                        },
                    });
                    ErrorHandler.logError(err);
                    reject(err);
                });
        });
}

export function wasSaved(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_WAS_SAVED,
                payload,
            });
            resolve();
        });
}

export function getSelectedUserBilling() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const { users, user } = state();
            const selectedUser = users.selectedUser;
            const timeZone = user.timeZone;

            if (
                selectedUser &&
                selectedUser.profiles &&
                selectedUser.profiles.length > 0 &&
                selectedUser.profiles[0] &&
                selectedUser.profiles[0].entity &&
                selectedUser.profiles[0].entity.id
            ) {
                const entityId = selectedUser.profiles[0].entity.id;
                const billing = selectedUser.billing;
                const now = moment().tz(timeZone);
                const momentDate = now
                    .clone()
                    .set('month', billing.month - 1)
                    .set('year', billing.year);
                let startDateMoment = momentDate.clone().startOf('month');
                if (startDateMoment.isAfter(now)) {
                    startDateMoment = now.clone().subtract(1, 'day');
                }

                let endDateMoment = momentDate.clone().endOf('month');
                if (endDateMoment.isAfter(now)) {
                    endDateMoment = now.clone().subtract(1, 'day');
                }

                APIClients.Users.getSelectedUserBilling({
                    entityId,
                    searchFields: {
                        startDate: startDateMoment.format('YYYY-MM-DD'),
                        endDate: endDateMoment.format('YYYY-MM-DD'),
                    },
                })
                    .then(res => {
                        dispatch({
                            type: ACTION_SET_SELECTED_USER_BILLING,
                            payload: {
                                billing: {
                                    allBillingTrips: res.Billing,
                                    // allBillingOtherCharges: res.otherCharges
                                },
                            },
                        });
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    });
            } else {
                // We need to continue the save workflow even if there is no Entity
                resolve();
            }
        });
}

export function getSelectedUserOtherCharges() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const { users, user } = state();
            const selectedUser = users.selectedUser;
            const timeZone = user.timeZone;

            if (
                selectedUser &&
                selectedUser.profiles &&
                selectedUser.profiles.length > 0 &&
                selectedUser.profiles[0] &&
                selectedUser.profiles[0].entity &&
                selectedUser.profiles[0].entity.id
            ) {
                const entityId = selectedUser.profiles[0].entity.id;
                const billing = selectedUser.billing;
                const now = moment().tz(timeZone);
                const momentDate = now
                    .clone()
                    .set('month', billing.month - 1)
                    .set('year', billing.year);
                let startDateMoment = momentDate.clone().startOf('month');
                if (startDateMoment.isAfter(now)) {
                    startDateMoment = now.clone().subtract(1, 'day');
                }

                let endDateMoment = momentDate.clone().endOf('month');
                if (endDateMoment.isAfter(now)) {
                    endDateMoment = now.clone();
                }

                APIClients.Users.getSelectedUserOtherCharges({
                    entityId,
                    searchFields: {
                        startDate: startDateMoment.format('YYYY-MM-DD'),
                        endDate: endDateMoment.format('YYYY-MM-DD'),
                    },
                })
                    .then(res => {
                        dispatch({
                            type: ACTION_SET_SELECTED_USER_BILLING,
                            payload: {
                                billing: {
                                    allBillingOtherCharges: res,
                                },
                            },
                        });
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    });
            } else {
                // We need to continue the save workflow even if there is no Entity
                resolve();
            }
        });
}

export function getSelectedUserEntityBalance() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const { users } = state();
            const selectedUser = users.selectedUser;

            if (
                selectedUser &&
                selectedUser.profiles &&
                selectedUser.profiles.length > 0 &&
                selectedUser.profiles[0] &&
                selectedUser.profiles[0].entity &&
                selectedUser.profiles[0].entity.id
            ) {
                const entityId = selectedUser.profiles[0].entity.id;

                APIClients.Users.getEntityBalance({ entityId })
                    .then(res => {
                        const amount = res.amount || '0';

                        dispatch({
                            type: ACTION_SET_SELECTED_USER_BILLING,
                            payload: {
                                billing: {
                                    customerBalance: parseFloat(amount),
                                },
                            },
                        });
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    });
            } else {
                reject(new Error('Missing selectedUser and entity.'));
            }
        });
}

export function getAvailableProductCharges() {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Users.getAvailableProductCharges()
                .then(res => {
                    dispatch({
                        type: ACTION_SET_AVAILABLE_CHARGE_PRODUCTS,
                        payload: res,
                    });
                    resolve();
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function updateUserBilling(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const userBilling = state().users.selectedUser.billing;

            dispatch({
                type: ACTION_SET_SELECTED_USER_BILLING,
                payload: {
                    billing: { ...userBilling, ...payload },
                },
            });
            resolve();
        });
}

export function updateUserChargeProduct(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_SET_USER_CHARGE_FORM_DATA,
                payload,
            });
            resolve();
        });
}

export function tryChangeBillingTripsTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const newPage = payload.newPage
                ? payload.newPage
                : state().users.selectedUser.billing.billingTripsTableCurrentPage.page;
            const orderBy = payload.orderBy
                ? payload.orderBy
                : state().users.selectedUser.billing.billingTripsTableCurrentPage.orderBy;
            const order = payload.order
                ? payload.order
                : state().users.selectedUser.billing.billingTripsTableCurrentPage.order;

            dispatch({
                type: ACTION_CHANGE_SELECTED_USER_BILLING_TRIPS_PAGE_CHANGE,
                payload: { page: newPage, orderBy, order },
            });
            resolve();
        });
}

export function tryChangeBillingOtherChargesTablePage(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const newPage = payload.newPage
                ? payload.newPage
                : state().users.selectedUser.billing.billingOtherChargesTableCurrentPage.page;
            const orderBy = payload.orderBy
                ? payload.orderBy
                : state().users.selectedUser.billing.billingOtherChargesTableCurrentPage.orderBy;
            const order = payload.order
                ? payload.order
                : state().users.selectedUser.billing.billingOtherChargesTableCurrentPage.order;

            dispatch({
                type: ACTION_CHANGE_SELECTED_USER_BILLING_OTHER_CHARGES_PAGE_CHANGE,
                payload: { page: newPage, orderBy, order },
            });
            resolve();
        });
}

export function openChargeProductPopup(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_OPEN_CHARGE_PRODUCT_POPUP,
                payload: { openChargePopup: payload, productChargeFormData: { ...BillingProductCharge } },
            });
            resolve();
        });
}

export function openEditDocumentPopup(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            dispatch({
                type: ACTION_OPEN_EDIT_DOCUMENT_POPUP,
                payload,
            });
            resolve();
        });
}

export function sendProductCharge(payload) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const { users } = state();
            const selectedUser = users.selectedUser;

            if (
                selectedUser &&
                selectedUser.profiles &&
                selectedUser.profiles.length > 0 &&
                selectedUser.profiles[0] &&
                selectedUser.profiles[0].entity &&
                selectedUser.profiles[0].entity.id
            ) {
                const entityId = selectedUser.profiles[0].entity.id;

                APIClients.Users.sendProductCharge({
                    userId: payload.userId,
                    entityId,
                    productId: payload.productCharge.product,
                    amount: payload.productCharge.amount,
                })
                    .then(res => {
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    });
            } else {
                reject(new Error('Missing selectedUser and entity.'));
            }
        });
}

export function getInvoiceThunk(invoiceId) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Users.getInvoice(invoiceId)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                });
        });
}

export function updateInvoiceThunk(invoiceId, status) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            APIClients.Users.updateInvoice(invoiceId, status)
                .then(res => {
                    resolve(res);
                    dispatch({
                        type: ACTION_SHOW_SNACK_BAR_MESSAGE_SUCCESS,
                        payload: { message: Localize.locale.users.summary.paymentDetails.paymentProcess },
                    });
                    dispatch(getSelectedUserBilling());
                })
                .catch(err => {
                    dispatch({
                        type: ACTION_SHOW_SNACK_BAR_MESSAGE_FAILURE,
                        payload: { message: err.message },
                    });
                    reject(err);
                });
        });
}

export function setUserSummaryCurrentTab(payload) {
    return (dispatch, state, APIClients) => {
        dispatch({
            type: ACTION_SET_USER_SUMMARY_CURRENT_TAB,
            payload,
        });
    };
}

export function fetchAllUsersRequest() {
    return {
        type: ACTION_FETCH_ALL_USERS_REQUEST,
    };
}

export function updateSelectedUserDocumentRequest(payload) {
    return {
        type: ACTION_UPDATE_SELECTED_USER_DOCUMENT_REQUEST,
        payload,
    };
}

export function getSelectedUserDocumentByDocumentType({ documentType }) {
    return (dispatch, state, APIClients) =>
        new Promise((resolve, reject) => {
            const { users } = state();
            const selectedUser = users.selectedUser;
            const selectedUserDocument = selectedUser.documents[documentType];

            if (selectedUser && selectedUserDocument) {
                APIClients.Users.getUserDocuments(users.selectedUser.id)
                    .then(userDocuments => {
                        const documentData = _.find(
                            userDocuments.documents,
                            userDocument => userDocument.documentType.type === documentType
                        );

                        dispatch({
                            type: ACTION_CHANGE_SELECTED_USER_DOCUMENT,
                            payload: {
                                documentType,
                                documentData: {
                                    ...Document,
                                    ...documentData,
                                },
                            },
                        });
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    });
            } else {
                reject(new Error('Missing selectedUser . '));
            }
        });
}
