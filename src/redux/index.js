import {applyMiddleware, compose, createStore} from "redux";

import thunk from "redux-thunk";
import createSagaMiddleware from 'redux-saga'
import indexSaga from './sagas/index.saga'
import reducers from './reducers';

import FleetClient from "clients/FleetClient";
import OpenIDClient from "clients/OpenIDClient";
import POIClient from "clients/POIClient";
import SearchClient from "clients/SearchClient";
import ServiceClient from "clients/ServiceClient";
import TripClient from "clients/TripClient";
import UserClient from "clients/UserClient";
import VehicleClient from "clients/VehicleClient";
import ZoneClient from "clients/ZoneClient";
import TicketClient from "clients/TicketClient";
import PromoClient from "clients/PromoClient";
import BookingRequestClient from "../clients/BookingRequestClient";

import MapboxClient from 'apis/Mapbox';

export default function (apiConf, initialState) {

    const Fleet = new FleetClient(apiConf);
    const OpenID = new OpenIDClient(apiConf);
    const POIs = new POIClient(apiConf);
    const Search = new SearchClient(apiConf);
    const Services = new ServiceClient(apiConf);
    const Trip = new TripClient(apiConf);
    const Users = new UserClient(apiConf);
    const Vehicles = new VehicleClient(apiConf);
    const Zones = new ZoneClient(apiConf);
    const Tickets = new TicketClient(apiConf);
    const Promos = new PromoClient(apiConf);
    const BookingRequests = new BookingRequestClient(apiConf);
    const Mapbox = new MapboxClient();
    const APIConf = apiConf;

    let enhancer = {};

    const apiClientsClasses = {Fleet, OpenID, POIs, Search, Services, Trip,
        Users, Vehicles, Zones, Tickets, Promos,
        BookingRequests, Mapbox, APIConf};
    const thunkmiddlewares = [
        thunk.withExtraArgument(apiClientsClasses),
    ];
    const sagaMiddleware = createSagaMiddleware()


    if (process.env.NODE_ENV === "development") {
        //  Dev remote devtools
        const composeEnhancers = typeof window === 'object' &&
            window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
                window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
                    // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
                    name: "FerryBotRedux",
                }) : compose;

        enhancer = composeEnhancers(
            applyMiddleware(
                ...thunkmiddlewares,
                sagaMiddleware
            )
        );
    } else {
        enhancer = applyMiddleware(
            ...thunkmiddlewares,
            sagaMiddleware
        );
    }


    const store = createStore(reducers, initialState,
        compose(
            enhancer,
        )
    );
    sagaMiddleware.run(indexSaga, apiClientsClasses)
    return store;
}
