import {
    combineReducers
} from "redux";


import application from "./application/reducer";
import navigation from './navigation/reducer';
import user from "./user/reducer";
import users from "./users/reducer";
import vehicles from "./vehicles/reducer";
import localization from './localization/reducer';
import fleet from './fleet/reducer';
import trips from './trips/reducer';
import supervisor from './supervisor/reducer';
import settings from './settings/reducer';
import zone from './zones/reducer';
import pois from './pois/reducer';
import promos from './promos/reducer';
import tickets from './tickets/reducer';
import bookingRequests from './bookingRequests/reducer';
import systemCredits from './systemCredits/reducer';


export default
    combineReducers({
        application,
        navigation,
        user,
        users,
        vehicles,
        fleet,
        trips,
        supervisor,
        localization,
        settings,
        zone,
        pois,
        promos,
        tickets,
        bookingRequests,
        systemCredits,
    });
