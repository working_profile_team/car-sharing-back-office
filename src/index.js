import "babel-polyfill";
import 'whatwg-fetch'

import React from 'react';
import ReactDOM from 'react-dom';
import 'moment/locale/fr.js'

import 'typeface-roboto';
import "react-filter-box/lib/react-filter-box.css"
import './index.css';

import Application from "./Application";

ReactDOM.render(<Application />, document.getElementById('root'));
