import State from 'helpers/state';

import ApiClient from 'clients/ApiClient';
import * as ErrorHandler from 'apis/VuLog/ErrorHandler';
import queryString from 'query-string';

export default class UserClient extends ApiClient {
    listAllUsers = () => {
        const apiUrl = this.endPoints.users();
        return this.get(apiUrl);
    };

    listAllRoles = role => {
        const apiUrl = `${this.endPoints.roles()}/${role}`;
        return this.get(apiUrl);
    };

    getUser = userId => this.get(`${this.endPoints.users()}/${userId}`);

    getPaymentDetails = (userId, profileId) => {
        const apiUrl = `${this.endPoints.users()}/${userId}/profiles/${profileId}/paymentMethodDetails`;
        return this.get(apiUrl);
    };

    getUserRoles = userId => {
        const apiUrl = `${this.endPoints.users()}/${userId}/roles`;
        return this.get(apiUrl);
    };

    addRoleToUser(userId, role) {
        const apiUrl = `${this.endPoints.users()}/${userId}/roles/${role}`;
        return this.put(apiUrl);
    }

    deleteRoleOfUser(userId, role) {
        const apiUrl = `${this.endPoints.users()}/${userId}/roles/${role}`;
        return this.delete(apiUrl);
    }

    getSelectedUserOtherCharges = payload => {
        const searchStringified = payload && payload.searchFields ? queryString.stringify(payload.searchFields) : '';
        const apiUrl = `${this.endPoints.billingEntity()}/${
            payload.entityId
        }/invoices/productDetails?${searchStringified}`;
        return this.get(apiUrl);
    };

    getSelectedUserBilling = payload => {
        const searchStringified = payload && payload.searchFields ? queryString.stringify(payload.searchFields) : '';
        const apiUrl = `${this.endPoints.billingEntity()}/${
            payload.entityId
        }/invoices/tripDetails?${searchStringified}`;
        return this.get(apiUrl);
    };

    getEntityBalance = payload => {
        const apiUrl = `${this.endPoints.billingEntity()}/${payload.entityId}/balance`;
        return this.get(apiUrl);
    };

    getAvailableProductCharges = () => {
        const apiUrl = `${this.endPoints.products()}`;
        return this.get(apiUrl);
    };

    sendProductCharge(payload) {
        const apiUrl = `${this.endPoints.invoiceProduct()}`;
        return this.post(apiUrl, payload);
    }

    getTripsOfUser = payload => {
        const apiUrl = `${this.endPoints.trip()}/users/${payload.userId}`;
        return this.get(apiUrl);
    };

    getUserDocuments = userId => this.get(`${this.endPoints.users()}/${userId}/documents`);

    createDocument = ({ userId, document }) => {
        const apiUrl = `${this.endPoints.users()}/${userId}/documents`;
        return this.post(apiUrl, document);
    };

    updateDocument = ({ userId, document }) => {
        const apiUrl = `${this.endPoints.users()}/${userId}/documents`;
        return this.put(apiUrl, document);
    };

    getUserCodes = ({ userId }) => {
        const apiUrl = `${this.endPoints.wallet()}/${userId}`;
        return this.get(apiUrl);
    };

    addSystemCredits = formattedData => {
        const apiUrl = `${this.endPoints.addSystemCredits()}`;
        return this.put(apiUrl, formattedData);
    };
}
