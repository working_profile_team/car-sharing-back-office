import axios from 'axios';
import state from 'helpers/state';
import EndPoints from 'clients/EndPoints';
import * as ErrorHandler from 'apis/VuLog/ErrorHandler';

export default class ApiClient {
    constructor(apiConf, stateUtil = state, baseUrl = 'baseUrl', AllEndPoints = EndPoints) {
        this.AllEndPoints = AllEndPoints;
        this.apiConf = apiConf;
        this.stateUtil = stateUtil;
        this.baseUrl = baseUrl;
        this.instance = axios.create({
            baseURL: apiConf.url[baseUrl],
            timeout: this.apiConf.timeout,
            headers: {
                Authorization: `Bearer ${state.getAccessToken()}`,
                'X-API-Key': this.apiConf['X-API-Key'],
                'Content-Type': 'application/json',
            },
            withCredentials: false,
        });

        if (apiConf.logAllNetwork) {
            axios.interceptors.request.use(
                config => {
                    console.log(`>>> ${config.method.toUpperCase()} ${config.url}`);
                    return config;
                },
                error => Promise.reject(error)
            );
            axios.interceptors.response.use(
                response => {
                    console.log(`<<< ${response.status} ${response.config.url}`);
                    return response;
                },
                error => Promise.reject(error)
            );
        }
    }

    getFleetId = () => localStorage.getItem('fleetId') || this.apiConf.defaultFleetId;

    get endPoints() {
        return this.AllEndPoints(this.getFleetId());
    }

    get(apiUrl, addHeader = false, mockAPIData) {
        const promise = () =>
            new Promise((resolve, reject) => {
                if (this.apiConf.useMockAPIUrls) {
                    console.log(`>>> MOCK GET: ${apiUrl}`);

                    this.callMockAPIRequest(mockAPIData, resolve, reject);
                } else {
                    const requestConfig = this.getRequestConfig('get', apiUrl, null, reject);
                    this.callAPIRequest(requestConfig, resolve, reject, addHeader);
                }
            });
        const response = this.stateUtil.guard().then(promise);
        return response;
    }

    post(apiUrl, payload, mockAPIData) {
        const promise = () =>
            new Promise((resolve, reject) => {
                if (this.apiConf.useMockAPIUrls) {
                    this.callMockAPIRequest(mockAPIData, resolve, reject);
                } else {
                    const requestConfig = this.getRequestConfig('post', apiUrl, payload, reject);
                    this.callAPIRequest(requestConfig, resolve, reject);
                }
            });
        return this.stateUtil.guard().then(promise);
    }

    put(apiUrl, payload, mockAPIData) {
        const promise = () =>
            new Promise((resolve, reject) => {
                if (this.apiConf.useMockAPIUrls) this.callMockAPIRequest(mockAPIData, resolve, reject);
                else {
                    const requestConfig = this.getRequestConfig('put', apiUrl, payload, reject);
                    this.callAPIRequest(requestConfig, resolve, reject);
                }
            });
        return this.stateUtil.guard().then(promise);
    }

    delete(apiUrl, mockAPIData) {
        const promise = () =>
            new Promise((resolve, reject) => {
                if (this.apiConf.useMockAPIUrls) this.callMockAPIRequest(mockAPIData, resolve, reject);
                else {
                    const requestConfig = this.getRequestConfig('delete', apiUrl, null, reject);
                    this.callAPIRequest(requestConfig, resolve, reject);
                }
            });
        return this.stateUtil.guard().then(promise);
    }

    callAPIRequest(requestConfig, resolve, reject, addHeader = false) {
        this.instance
            .request(requestConfig)
            .then(response => {
                if (addHeader) {
                    resolve({
                        data: response.data,
                        headers: response.headers,
                    });
                } else {
                    resolve(response.data);
                }
            })
            .catch(error => {
                if (this.apiConf.logAllNetwork) {
                    ErrorHandler.logError(error);
                }
                reject(error);
            });
    }

    getRequestConfig(requestMethod, apiUrl, payload, reject) {
        if (!this.apiConf.useMockAPIUrls && !apiUrl) {
            reject('API UNIMPLEMENTED: waiting for real endpoint, mainwhile you can use the mock data');
            return undefined;
        }

        const requestConfig = {
            url: apiUrl,
            method: requestMethod,
            timeout: this.apiConf.timeout,
            headers: {
                Authorization: `Bearer ${this.stateUtil.getAccessToken()}`,
                'X-API-Key': this.apiConf['X-API-Key'],
                'Content-Type': 'application/json',
            },
            withCredentials: false,
        };

        if (payload) {
            requestConfig.data = payload;
        }

        return requestConfig;
    }

    static callMockAPIRequest(mockAPIData, resolve, reject) {
        if (!mockAPIData) {
            reject('MOCK DATA UNIMPLEMENTED: you must have mock data for allowing tests');
            return;
        }
        resolve(mockAPIData);
    }
}
