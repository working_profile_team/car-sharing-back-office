import FunctionHelpers from "./FunctionHelpers";

describe('FunctionHelpers', () => {
    describe('removeDuplicatesInArray', () => {
        it('returns an array with unique entries only (numbers)', () => {
            expect(FunctionHelpers.removeDuplicatesInArray([1, 2, 3, 3, 3, 4, 4, 1])).toEqual([1, 2, 3, 4]);
        });

        it('returns an array with unique entries only (strings)', () => {
            expect(FunctionHelpers.removeDuplicatesInArray(['hello', 'hello', 'world', 'world', 'world'])).toEqual(['hello', 'world']);
        });

        it('returns an empty array when an empty array is passed', () => {
            expect(FunctionHelpers.removeDuplicatesInArray([])).toEqual([]);
        });

        it('returns an date not equal according to time zone', () => {
            const now = new Date();
            expect(FunctionHelpers.dateFromTimeZone(now, 'Europe/Madrid')).not.toEqual(now);
        });
    });
});
