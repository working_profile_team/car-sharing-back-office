import axios from 'axios';

export default async host => {
    const hostname = window.location.hostname;

    try {
        let config = await axios.get(`/clients/bo/${host || hostname}/config.json`);
        return {
            ...config,
            data: {
                ...config.data,
                currentHost: host || hostname,
            }
        };
    } catch (error) {
        throw new Error(`Configuration not found for : ${hostname}`);
    }
};
