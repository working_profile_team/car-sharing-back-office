import React from 'react';
import _ from 'lodash';
import moment from 'moment';

import FunctionHelpers from 'helpers/FunctionHelpers';
import DateDisplay from 'containers/DateDisplay';

/**
 * @param {Array} data
 * @param {Number} page
 * @param {String} order
 * @param {String} orderBy
 * @param {Number} pageSize
 * @param {Array} columnsData
 * @returns {Array}
 */
export function sortAndSliceData({ data, page, order, orderBy, pageSize = 10, columnsData }) {
    const sortedData = sortData({ data, order, orderBy, columnsData });
    return sliceData({ data: sortedData, page, pageSize });
}
/**
 * IMPORTANT
 *
 * Received `data` array will be mutated
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
 *
 * @param {Array} data
 * @param {String} order
 * @param {String} orderBy
 * @param {Array} columnsData
 * @returns {Array}
 */
export function sortData({ data, order, orderBy, columnsData }) {
    const orderByData = _.find(columnsData, columnData => columnData.fieldName === orderBy);
    if (orderByData === undefined) {
        return data;
    }
    data.sort((a, b) => {
        let orderedData;
        orderBy = orderBy.split('Parsed')[0];
        switch (orderByData.fieldType) {
            case 'Date':
            case 'DateTime':
                if (order === 'asc') {
                    orderedData = moment(a[orderBy]).isSameOrBefore(moment(b[orderBy])) ? -1 : 1;
                } else {
                    orderedData = moment(b[orderBy]).isSameOrBefore(moment(a[orderBy])) ? -1 : 1;
                }
                break;
            case 'Number':
            case 'IntegerWithZero':
                orderedData =
                    order === 'asc'
                        ? FunctionHelpers.undefined2Num(a[orderBy]) - FunctionHelpers.undefined2Num(b[orderBy])
                        : FunctionHelpers.undefined2Num(b[orderBy]) - FunctionHelpers.undefined2Num(a[orderBy]);
                break;
            case 'Distance':
            case 'Duration':
                if (order === 'asc') {
                    orderedData = a[orderBy] >= b[orderBy] ? 1 : -1;
                } else {
                    orderedData = b[orderBy] >= a[orderBy] ? 1 : -1;
                }
                break;
            default:
                orderedData =
                    order === 'asc' ? a[orderBy].localeCompare(b[orderBy]) : b[orderBy].localeCompare(a[orderBy]);
                break;
        }
        return orderedData;
    });
    return data;
}

/**
 * @param {Array} data
 * @param {Number} page
 * @param {Number} pageSize
 * @returns {Array}
 */
export function sliceData({ data, page, pageSize = 10 }) {
    const startPos = (page - 1) * pageSize;
    const endPos = page * pageSize;
    data = _.slice(data, startPos, endPos);
    return data;
}

/**
 * @param {Number} numMinutes
 * @returns {String}
 */
export function minutesToDuration(numMinutes) {
    const hours = numMinutes / 60;
    const rhours = Math.floor(hours);
    const minutes = (hours - rhours) * 60;
    const rminutes = Math.round(minutes);
    if (rhours === 0) {
        return `${rminutes} m`;
    }
    return `${rhours} h ${rminutes} m`;
}

/**
 * @param {Array} columnsData
 * @param {Object} itemData
 * @returns {Object}
 */
export function formatColumnsDataModel(columnsData, itemData, currency = ' €') {
    const formattedData = {};
    for (const columnData of columnsData) {
        switch (columnData.fieldType) {
            case 'Id':
                formattedData[columnData.fieldName] = FunctionHelpers.null2Num(itemData[columnData.fieldName]);
                formattedData.id = FunctionHelpers.null2Num(itemData[columnData.fieldName]);
                break;
            case 'Number':
                formattedData[columnData.fieldName] = FunctionHelpers.null2Num(itemData[columnData.fieldName]);
                break;
            case 'IntegerWithZero':
                formattedData[columnData.fieldName] = FunctionHelpers.null2IntegerZero(itemData[columnData.fieldName]);
                break;
            case 'String':
                formattedData[columnData.fieldName] = FunctionHelpers.null2String(itemData[columnData.fieldName]);
                break;
            case 'Date':
            case 'DateTime':
                formattedData[`${columnData.fieldName}`] = (
                    <DateDisplay value={itemData[columnData.fieldName.split('Parsed')[0]]} allwaysFull />
                );
                break;
            case 'Duration':
                formattedData[`${columnData.fieldName}`] = minutesToDuration(
                    FunctionHelpers.null2Num(itemData[columnData.fieldName.split('Parsed')[0]], 0)
                );
                break;
            case 'Distance':
                formattedData[`${columnData.fieldName}`] = `${Math.round(
                    FunctionHelpers.null2Num(itemData[columnData.fieldName.split('Parsed')[0]], 0) * 100
                ) / 100} km`;
                break;
            case 'Currency':
                formattedData[`${columnData.fieldName}`] =
                    Math.round(FunctionHelpers.null2Num(itemData[columnData.fieldName.split('Parsed')[0]], 0) * 100) /
                        100 +
                    currency;
                break;
            default:
                break;
        }
    }
    return formattedData;
}

/**
 * Filters received data array based on filters - check is performed based on full equality
 *
 * @param {Array} data
 * @param {Array} booleanFilters
 * @returns {Array}
 */
export function applyBooleanFilters({ data, booleanFilters }) {
    let dataFiltered = data;
    if (booleanFilters && booleanFilters.length > 0) {
        for (const filter of booleanFilters) {
            dataFiltered = _.filter(dataFiltered, o => o[filter.id] === filter.value);
        }
    }
    return dataFiltered;
}

/**
 * Returns a copy of the received object, minus the properties which are either empty strings, null or undefined
 *
 * @param {Object} obj
 * @returns {Object}
 */
export const filterOutEmptyProperties = obj =>
    Object.keys(obj)
        .filter(fieldName => obj[fieldName] !== '' && obj[fieldName] !== null && obj[fieldName] !== undefined)
        .reduce((acc, fieldName) => {
            acc[fieldName] = obj[fieldName];
            return acc;
        }, {});
