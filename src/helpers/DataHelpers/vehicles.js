import _ from 'lodash';
import FunctionHelpers from 'helpers/FunctionHelpers';
import { formatColumnsDataModel } from './common';
import { VEHICLE_STATUSES_CONVERSION } from 'constants/Vehicles';

export function convertStatusToText(fleetVehicle) {
    let vehicleStatus = '-1';
    if (fleetVehicle && fleetVehicle.id && fleetVehicle.vehicle_status !== undefined) {
        vehicleStatus = fleetVehicle.vehicle_status.toString();
    }

    for (const key in VEHICLE_STATUSES_CONVERSION) {
        if (Array.isArray(VEHICLE_STATUSES_CONVERSION[key])) {
            for (const newVal of VEHICLE_STATUSES_CONVERSION[key]) {
                if (newVal === vehicleStatus) {
                    return key;
                }
            }
        } else if (vehicleStatus === VEHICLE_STATUSES_CONVERSION[key]) {
            return key;
        }
    }
}

export function getVehicleStatus(vehicle, allFleets) {
    if (allFleets.length === 0) {
        return '';
    }
    const indexFleet = allFleets[0];
    const vehiculesFleet = indexFleet.vehicules;
    let vehicleStatus = 'NOT CONNECTED';
    const fleet = _.find(vehiculesFleet, elem => elem.fleetid === vehicle.fleetId);
    if (fleet) {
        const fleetVehicle = _.find(vehiculesFleet, elem => elem.id === vehicle.id);
        vehicleStatus = convertStatusToText(fleetVehicle);
    }
    return vehicleStatus;
}

/**
 * @param {Array} vehicles
 * @param {Array} columnsData
 * @returns {Array}
 */
export function formatApiVehiclesToTableData(vehicles, allFleets, columnsData) {
    return _.map(vehicles, vehicle => {
        const formattedData = formatColumnsDataModel(columnsData, vehicle);
        return {
            ...vehicle,
            ...formattedData,
            model: FunctionHelpers.null2String(vehicle.model.name),
            status: getVehicleStatus(vehicle, allFleets),
        };
    });
}
