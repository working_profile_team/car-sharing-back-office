import _ from "lodash";
import FunctionHelpers from "helpers/FunctionHelpers";
import {formatColumnsDataModel} from "./common"
import {getTripStatus} from './trips'
import { USER_ROLES } from "../../constants/Users";

/**
 * @param {Array} billingTrips
 * @param {Array} vehicles
 * @param {Array} columnsData
 * @returns {Array}
 */
export function formatApiBillingTripsToTableData(billingTrips, vehicles, columnsData, currency) {
    return _.map(billingTrips, (billingTrip) => {
        const flattenedData = {
            ...billingTrip,
            startDate: billingTrip.trip.tripStartDate,
            length: FunctionHelpers.null2Num(billingTrip.trip.distance),
            duration: FunctionHelpers.null2Num(billingTrip.trip.drivingDuration),
            paymentStatus: FunctionHelpers.null2String(billingTrip.invoice.billingStatus)
        };

        let formattedData = formatColumnsDataModel(columnsData, flattenedData, currency);
        return {
            id: FunctionHelpers.null2Num(billingTrip.trip.tripId),
            ...billingTrip,
            ...formattedData,
        };
    });
}

/**
 * @param {Array} billingOtherCharges
 * @param {Array} columnsData
 * @returns {Array}
 */
export function formatApiBillingOtherChargesToTableData(billingOtherCharges, columnsData, currency) {
    return _.map(billingOtherCharges, (billingOtherCharge) => {
        const flattenedData = {
            ...billingOtherCharge,
            billingDate: billingOtherCharge.invoice.billingDate,
            billingStatus: FunctionHelpers.null2String(billingOtherCharge.invoice.billingStatus)
        };

        let formattedData = formatColumnsDataModel(columnsData, flattenedData, currency);
        return {
            id: FunctionHelpers.null2Num(billingOtherCharge.invoice.id),
            ...billingOtherCharge,
            ...formattedData,
        };
    });
}

/**
 * @param {Array} users
 * @param {Array} columnsData
 * @returns {Array}
 */
export function formatApiUsersToTableData(users, columnsData) {
    return _.map(users, (user) => {
        let formattedData = formatColumnsDataModel(columnsData, user);
        return {
            ...user,
            ...formattedData,
        };
    });
}

/**
 * @param {Array} trips
 * @param {Array} columnsData
 * @returns {Array}
 */
export function formatApiUserTripsToTableData(trips, vehicles, columnsData) {
    return _.map(trips, (trip) => {
        let formattedData = formatColumnsDataModel(columnsData, trip);

        let vehicleName = '';
        let vehiclePlate = '';
        const foundVehicle = _.find(vehicles, (o) => o.id === trip.vehicleId);
        if (foundVehicle) {
            vehicleName = foundVehicle.name;
            vehiclePlate = foundVehicle.plate;
        }

        const tripStatus = getTripStatus(trip);

        return {
            ...trip,
            ...formattedData,
            name: `${vehiclePlate}-${vehicleName}`,
            tripStatus,
            fleetId: FunctionHelpers.null2String(trip.fleetId),
        };
    });
}



export function getLastRoleUser(roles) {

    let roleToDisplayReturn = '';
    USER_ROLES.some(role => {
        if(roles.includes(role.name)){
            roleToDisplayReturn = role.name;
            return true;
        }
        return false;
    });
    return roleToDisplayReturn;
}

export function formatApiUserSystemCreditsToTableData(userCredits, columnsData){

    return userCredits.map( (credit, creditIndex ) => {

        let customizedCredit = {
            ...credit,
            id: creditIndex
        }
        let formattedData = formatColumnsDataModel(columnsData, customizedCredit);
        return {
            ...formattedData,
        }
    })

}
