import {VehicleTable} from "redux/vehicles/models/VehicleTable.model";

import {
    formatApiVehiclesToTableData,
    convertStatusToText,
    getVehicleStatus,
} from "../vehicles";

import vehicles from '../../../__tests__/fixtures/vehicles.json';
import fleets from '../../../__tests__/fixtures/fleets.json';

describe('Vehicles data helpers', () => {

    describe('formatApiVehiclesToTableData', () => {
        it('returns data with expected additional fields', () => {

            const result = formatApiVehiclesToTableData(vehicles, fleets, VehicleTable.columnsData);
            expect(result).toHaveLength(27);
            expect(result[1].id).toEqual('296acf01-a63e-4f95-b500-a983795787b1');
            expect(result[1].name).toEqual('Ford Mustang testr');
            expect(result[1].vuboxId).toEqual('vubox_test03');
            expect(result[1].model).toEqual('Citroen C0');
            expect(result[1].status).toEqual('IN USE');
        });
    });

    describe('convertStatusToText', () => {
        it('returns NOT CONNECTED when vehicule_status is -1', () => {
            expect(convertStatusToText(undefined)).toBe('NOT CONNECTED');
        });

        it('returns AVAILABLE when vehicule_status is 0', () => {
            expect(convertStatusToText(fleets[0].vehicules[0])).toBe('AVAILABLE');
        });

        it('returns IN USE status when vehicule_status is 1', () => {
            expect(convertStatusToText(fleets[0].vehicules[1])).toBe('IN USE');
        });

        it('returns TO BE INFLEET status vehicule_status is 4', () => {
            expect(convertStatusToText(fleets[0].vehicules[6])).toBe('TO BE INFLEET');
        });
    });

    describe('getVehicleStatus', () => {
        it('returns NOT CONNECTED status when there is not exists the vehicle inside the fleet', () => {
            expect(getVehicleStatus(vehicles[0], fleets)).toBe('NOT CONNECTED');
        });


        it('returns IN USE status when there is a vehicle inside the fleet and his vehicle_status is IN USE', () => {
            expect(getVehicleStatus(vehicles[1], fleets)).toBe('IN USE');
        });

        it('returns AVAILABLE status when there is a vehicle inside the fleet and his vehicle_status is AVAILABLE', () => {
            expect(getVehicleStatus(vehicles[2], fleets)).toBe('AVAILABLE');
        });
    });
});
