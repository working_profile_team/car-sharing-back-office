import {TripTable} from "redux/trips/models/TripTable.model";
import {TripInProgressTable} from "redux/trips/models/TripInProgressTable.model";

import {
    formatApiTripsInProgressToTableData,
    formatApiTripsToTableData,
    getTripStatus
} from "../trips";

import trips from '../../../__tests__/fixtures/trips.json';
import tripsInProgress from '../../../__tests__/fixtures/tripsInProgress.json';
import vehicles from '../../../__tests__/fixtures/vehicles.json';

describe('Trips data helpers', () => {
    describe('getTripStatus', () => {
        let trip;

        beforeEach(() => {
            trip = {additionalInfo: {}}
        });

        it('handles cancelled status', () => {
            trip.additionalInfo.isCancel = true;
            expect(getTripStatus(trip)).toEqual('Cancelled');
        });

        it('handles suspicious status', () => {
            trip.additionalInfo.suspicious = true;
            expect(getTripStatus(trip)).toEqual('Suspicious');
        });

        it('handles billed status', () => {
            trip.additionalInfo.isBilled = true;
            expect(getTripStatus(trip)).toEqual('Billed');
        });

        it('handles pending status', () => {
            expect(getTripStatus({})).toEqual('Billing pending');
        });
    });

    describe('formatApiTripsToTableData', () => {
        it('returns data with expected additional fields', () => {

            const result = formatApiTripsToTableData(trips, vehicles, TripTable.columnsData);

            expect(result).toHaveLength(8);
            expect(result[0].id).toEqual('EF239FE92683C8E846DC21BE6A6F49C7');
            expect(result[0].tripId).toEqual('EF239FE92683C8E846DC21BE6A6F49C7');
            expect(result[0].name).toEqual('JKU-001-Scenic JKU');
            // expect(result[0].dateParsed.includes('ago')).toBe(true);
            expect(result[0].durationParsed).toEqual('45 m');
            expect(result[0].lengthParsed).toEqual('0 km');
            expect(result[0].tripStatus).toEqual('Billing pending');
        });
    });

    describe('formatApiTripsInProgressToTableData', () => {
        it('returns data with expected additional fields', () => {

            const normalisedTripsInProgress = tripsInProgress.map(t => {
                return {
                    ...t,
                    // todo: figure out what we can do about this, far from ideal
                    lastActiveDate: t.last_active_date
                };
            });

            const result = formatApiTripsInProgressToTableData(normalisedTripsInProgress, vehicles, TripInProgressTable.columnsData);
            expect(result).toHaveLength(1);
            expect(result[0].id).toEqual("627DF2E9A8F2F4C59DBB3E3C202BA806");
            expect(result[0].name).toEqual('STK-000344-Emulator 344');
            // expect(result[0].lastActiveDateParsed.includes('ago')).toBe(true);
            expect(result[0].vehicleStatus).toEqual('In Use');
            expect(result[0].actions).toEqual('VEHICLE DETAILS');
        });
    });
});
