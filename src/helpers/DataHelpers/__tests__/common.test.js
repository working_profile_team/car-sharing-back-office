import {VehicleTable} from "redux/vehicles/models/VehicleTable.model";

import vehicles from '../../../__tests__/fixtures/vehicles.json';
import {applyBooleanFilters, sortData} from "../common";


describe('Common helpers', () => {
    describe('sortData', () => {
        let data, parameters, columnsData;

        it('returns sorted data', () => {
            data = [
                {id: '1'}, {id: '2'}, {id: '3'}
            ];
            parameters = {
                page: 1, order: 'desc', orderBy: 'id'
            };
            columnsData = [{fieldName: 'id', fieldType: 'Id'}];
            const result = sortData({data, ...parameters, columnsData});

            expect(result).toHaveLength(3);
            expect(result[0].id).toEqual('3');
            expect(result[1].id).toEqual('2');
            expect(result[2].id).toEqual('1');
        });

        it('sorts dates correctly', () => {
            data = [
                {id: '1', date: '2018-01-01'},
                {id: '2', date: '2018-01-02'},
                {id: '3', date: '2018-01-03'}
            ];
            parameters = {
                page: 1, order: 'desc', orderBy: 'date'
            };
            columnsData = [{fieldName: 'date', fieldType: 'Date'}];
            const result = sortData({data, ...parameters, columnsData});

            expect(result).toHaveLength(3);
            expect(result[0].id).toEqual('3');
            expect(result[1].id).toEqual('2');
            expect(result[2].id).toEqual('1');
        });

        it('sorts datetimes correctly', () => {
            data = [
                {id: '1', date: '2018-01-01T01:00:00Z'},
                {id: '2', date: '2018-01-01T02:00:00Z'},
                {id: '3', date: '2018-01-01T03:00:00Z'}
            ];
            parameters = {
                page: 1, order: 'desc', orderBy: 'date'
            };
            columnsData = [{fieldName: 'date', fieldType: 'DateTime'}];
            const result = sortData({data, ...parameters, columnsData});

            expect(result).toHaveLength(3);
            expect(result[0].id).toEqual('3');
            expect(result[1].id).toEqual('2');
            expect(result[2].id).toEqual('1');
        });

        it('sorts numbers correctly', () => {
            data = [
                {id: '1', prop: 1},
                {id: '2', prop: 2},
                {id: '3', date: 0}
            ];
            parameters = {
                page: 1, order: 'desc', orderBy: 'prop'
            };
            columnsData = [{fieldName: 'prop', fieldType: 'Number'}];
            const result = sortData({data, ...parameters, columnsData});

            expect(result).toHaveLength(3);
            expect(result[0].id).toEqual('2');
            expect(result[1].id).toEqual('1');
            expect(result[2].id).toEqual('3');
        });

        it('sorts distances correctly', () => {
            data = [
                {id: '1', distance: 11.43},
                {id: '2', distance: 21.09},
                {id: '3', distance: 50.60}
            ];
            parameters = {
                page: 1, order: 'desc', orderBy: 'distance'
            };
            columnsData = [{fieldName: 'distance', fieldType: 'Distance'}];
            const result = sortData({data, ...parameters, columnsData});

            expect(result).toHaveLength(3);
            expect(result[0].id).toEqual('3');
            expect(result[1].id).toEqual('2');
            expect(result[2].id).toEqual('1');
        });

        it('sorts durations correctly', () => {
            data = [
                {id: '1', duration: 111},
                {id: '2', duration: 211},
                {id: '3', duration: 500}
            ];
            parameters = {
                page: 1, order: 'desc', orderBy: 'duration'
            };
            columnsData = [{fieldName: 'duration', fieldType: 'Duration'}];
            const result = sortData({data, ...parameters, columnsData});

            expect(result).toHaveLength(3);
            expect(result[0].id).toEqual('3');
            expect(result[1].id).toEqual('2');
            expect(result[2].id).toEqual('1');
        });
    });

    describe('applyBooleanFilters', () => {
        it('return filtered data, filtered by archived vehicles', () => {
            const booleanFilters = [{id: "archived", value: true}];
            expect(applyBooleanFilters({data: vehicles, booleanFilters})).toHaveLength(6);
        });

        it('return filtered data, filtered by not archived vehicles', () => {
            const booleanFilters = [{id: "archived", value: false}];
            expect(applyBooleanFilters({data: vehicles, booleanFilters})).toHaveLength(21);
        });
    });
});


