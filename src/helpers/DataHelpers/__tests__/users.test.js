import {BillingTripTable} from "redux/users/models/BillingTripTable.model";
import {BillingOtherChargeTable} from "redux/users/models/BillingOtherChargeTable.model";

import {
    formatApiBillingTripsToTableData,
    formatApiBillingOtherChargesToTableData
} from "../users";

import selectedUserBilling from '../../../__tests__/fixtures/selectedUserBilling.json';
import vehicles from '../../../__tests__/fixtures/vehicles.json';

describe('selectedUser Billing data helpers', () => {
    describe('formatApiBillingTripsToTableData', () => {
        it('returns selectedUser Billing Trips data with expected additional fields', () => {

            const result = formatApiBillingTripsToTableData(selectedUserBilling.allBillingTrips, vehicles, BillingTripTable.columnsData);

            expect(result).toHaveLength(4);
            expect(result[0].id).toEqual('F495175ACC595F69F31C1D7BA470411D');
            expect(result[0].durationParsed).toEqual('8 m');
            expect(result[0].lengthParsed).toEqual('120 km');
            expect(result[0].totalWithoutTax).toEqual('12 €');
            expect(result[0].paymentStatus).toEqual('PENDING');
            expect(result[0].totalTaxAmount).toEqual('2 €');
            expect(result[0].totalWithTax).toEqual('14 €');
        });
    });

    describe('formatApiBillingOtherChargesToTableData', () => {
        it('returns selectedUser Billing Other Charges data with expected additional fields', () => {

            const result = formatApiBillingOtherChargesToTableData(selectedUserBilling.allBillingOtherCharges, BillingOtherChargeTable.columnsData);

            expect(result).toHaveLength(2);
            expect(result[0].id).toEqual('5966a45e-c104-4dd7-9165-7841e50f8e44');
            // expect(result[0].startDateParsed.includes('ago')).toBe(true);
            expect(result[0].productName).toEqual('per_trip_waiver');
            expect(result[0].billingStatus).toEqual('PENDING');
            expect(result[0].totalWithoutTax).toEqual('4 €');
            expect(result[0].totalTaxAmount).toEqual('0 €');
            expect(result[0].totalWithTax).toEqual('4 €');
        });
    });
});
