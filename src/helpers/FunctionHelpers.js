import React from 'react';
// import moment from 'moment';
import moment from 'moment-timezone';

export default class FunctionHelpers {
    static dateDiff = (date1, date2) => {
        const diff = {};
        let tmp = date2 - date1;

        tmp = Math.floor(tmp / 1000);
        diff.sec = tmp % 60;

        tmp = Math.floor((tmp - diff.sec) / 60);
        diff.min = tmp % 60;

        tmp = Math.floor((tmp - diff.min) / 60);
        diff.hour = tmp % 24;

        tmp = Math.floor((tmp - diff.hour) / 24);
        diff.day = tmp;

        return diff;
    };

    static null2String = (value, defaultValue = '') => value || defaultValue;

    static null2Num = (value, defaultValue = undefined) => value || defaultValue;

    static null2IntegerZero = (value, defaultValue = undefined) =>
        value !== null && value !== undefined ? parseInt(value, 10) : parseInt(defaultValue, 10);

    static undefined2Num = (value, defaultValue = 0) => value || defaultValue;

    static null2Date = (value, defaultValue = undefined, formatIn = 'MM/DD/YYYY', formatOut = 'MM/DD/YYYY') => {
        let date = value || defaultValue;
        date = date ? moment(value, formatIn).format(formatOut) : undefined;
        return date;
    };

    static null2DateTime = (value, defaultValue = undefined) => {
        let date = value || defaultValue;
        date = date ? moment(value, 'MM/DD/YYYY hh:mm:ss A') : undefined;
        return date;
    };

    static null2Array = (value, defaultValue = []) => value || defaultValue;

    static stringToBoolean = value => {
        if (typeof value !== 'string') {
            return false;
        }
        value = value.toLowerCase();
        return value === 'true' || value === '1';
    };

    /**
     * This could be simplified using a Set, have to ensure we include the
     * babel polyfill (for IE10)
     *
     * @param {Array} arr
     * @returns {Array}
     */
    static removeDuplicatesInArray = arr => {
        const map = arr.reduce((acc, item) => {
            acc[item] = item;
            return acc;
        }, {});

        return Object.keys(map).map(key => map[key]);
    };

    static dateFromTimeZone = (date, timezone) => moment.tz(date, timezone).format();

    static formatSelectOptions = (options, format = 'obj') => {
        let emptyOption;
        switch (format) {
            case 'obj':
                emptyOption = { id: '', name: '' };
                break;
            case 'html':
                emptyOption = <option key="empty" value="" />;
                break;
            default:
                break;
        }
        return [emptyOption, ...options];
    };

    static formatMarkerPointCount = pointCount => {
        if (pointCount > 999) {
            const newPointCount = (pointCount / 1000).toFixed(1);
            return `${newPointCount}k`;
        }
        return pointCount;
    };
}
