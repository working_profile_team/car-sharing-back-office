import _ from 'lodash';
import moment from 'moment';

export default class ValidationHelpers {
    static testNonEmptyStringField = value => {
        if (typeof value !== 'string') {
            return false;
        }

        return value.trim() !== '';
    };

    /**
     * @todo: Add a few more special characters besides single quote and dash
     */
    static testStringField = value => {
        const frenchRegExp = /^[\w '\-\u00C0-\u00FF]{4,}$/;
        const chineseRegExp = /^[\u4E00-\u9FCC\u3400-\u4DB5\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29]|[\ud840-\ud868][\udc00-\udfff]|\ud869[\udc00-\uded6\udf00-\udfff]|[\ud86a-\ud86c][\udc00-\udfff]|\ud86d[\udc00-\udf34\udf40-\udfff]|\ud86e[\udc00-\udc1d]{1,}$/;
        return value !== undefined && (frenchRegExp.test(value) || chineseRegExp.test(value));
    };

    static testUserNameField = value => {
        const frenchRegExp = /^[A-Za-z\- '\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]{2,}$/;
        const chineseRegExp = /^[\u4E00-\u9FCC\u3400-\u4DB5\uFA0E\uFA0F\uFA11\uFA13\uFA14\uFA1F\uFA21\uFA23\uFA24\uFA27-\uFA29]|[\ud840-\ud868][\udc00-\udfff]|\ud869[\udc00-\uded6\udf00-\udfff]|[\ud86a-\ud86c][\udc00-\udfff]|\ud86d[\udc00-\udf34\udf40-\udfff]|\ud86e[\udc00-\udc1d]{1,}$/;
        return value !== undefined && (frenchRegExp.test(value) || chineseRegExp.test(value));
    };

    static testPersonsName = value => typeof value === 'string' && value.length >= 2;

    static testNumField = value => {
        const regExp = /^[0-9]{4,}$/;
        return regExp.test(value);
    };

    static testEmailField = value => {
        // TOO MESSY :(
        // eslint-disable-next-line
        const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regExp.test(value);
    };

    /**
     * Taken straight from UserWS (Java)
     *
     * @param {String} value
     * @returns {Boolean}
     */
    static testUsername = value => typeof value === 'string' && value.length <= 64 && value.length >= 6;

    static testStore(validation, context, limit = 3) {
        if (!validation.valid) {
            let errors = validation.errors;
            const totalErrors = errors.length;
            errors = errors.filter((elem, index) => index < limit);
            console.error(`${context}.totalErrors: `, totalErrors);
            for (const error of errors) {
                console.error(`${context}.message: `, error.message);
                const path = `${this.constructor.name}.${error.path.join(' . ')}`.replace(/\s/g, '');
                console.error(`${context}.path: `, path);
            }
        }
    }

    static testAttributesOfObjectsInArrayAreNotEmpty = array => {
        if (!Array.isArray(array)) {
            return false;
        }
        for (const obj of array) {
            for (const key of Object.keys(obj)) {
                if (array.length !== _.filter(array, o => o[key]).length) {
                    return false;
                }
            }
        }

        return true;
    };

    static testDateField = value => moment(value).isValid();
}
