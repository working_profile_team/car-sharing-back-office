import ValidationHelpers from '../ValidationHelpers';

describe('ValidationHelpers', () => {
    describe('testNonEmptyStringField', () => {
        it('returns true when non empty string is passed', () => {
            expect(ValidationHelpers.testNonEmptyStringField('hello world')).toBe(true);
        });

        it('returns false when empty string is passed', () => {
            expect(ValidationHelpers.testNonEmptyStringField('')).toBe(false);
            expect(ValidationHelpers.testNonEmptyStringField(' ')).toBe(false);
        });

        it('returns false when parameter passed is not of type string', () => {
            expect(ValidationHelpers.testNonEmptyStringField(100)).toBe(false);
        });
    });

    describe('testStringField', () => {
        it('returns true when standard characters are passed', () => {
            expect(ValidationHelpers.testStringField('hello')).toBe(true);
        });

        it('returns true when standard characters and spaces are passed', () => {
            expect(ValidationHelpers.testStringField('hello world')).toBe(true);
        });

        it('returns true when special characters are passed', () => {
            expect(ValidationHelpers.testStringField("hell's kitchen - I love it")).toBe(true);
        });

        it('returns false when a string less than 4 characters long is passed', () => {
            expect(ValidationHelpers.testStringField('hel')).toBe(false);
        });

        it('returns false when unrecognised special characters are passed', () => {
            expect(ValidationHelpers.testStringField('hell@')).toBe(false);
        });

        it('returns true when chinese characters are passed', () => {
            expect(ValidationHelpers.testStringField('會意字 會意字 會意字')).toBe(true);
        });
    });

    describe('testUserNameField', () => {
        it('returns true when standard characters are passed', () => {
            expect(ValidationHelpers.testUserNameField('hello')).toBe(true);
        });

        it('returns true when standard characters and spaces are passed', () => {
            expect(ValidationHelpers.testUserNameField('hello world')).toBe(true);
        });

        it('returns true when latin characters are passed', () => {
            expect(ValidationHelpers.testUserNameField('eñdasd éáíúópaodsipas fasdfuabdsçè')).toBe(true);
        });

        it('returns false when a string less than 2 characters long is passed', () => {
            expect(ValidationHelpers.testUserNameField('h')).toBe(false);
        });

        it('returns false when unrecognised special characters are passed', () => {
            expect(ValidationHelpers.testUserNameField('hell@')).toBe(false);
        });

        it('returns true when chinese characters are passed', () => {
            expect(ValidationHelpers.testUserNameField('會意字 會意字 會意字')).toBe(true);
        });
    });

    describe('testUsername', () => {
        it('returns true when length of string passed is within range', () => {
            expect(ValidationHelpers.testUsername('hello world')).toBe(true);
        });

        it('returns false when length of string passed is below lowest value', () => {
            expect(ValidationHelpers.testUsername('hello')).toBe(false);
        });

        it('returns true when length of string passed matches highest value', () => {
            expect(ValidationHelpers.testUsername('h'.repeat(64))).toBe(true);
        });

        it('returns false when length of string passed is above highest value', () => {
            expect(ValidationHelpers.testUsername('h'.repeat(65))).toBe(false);
        });
    });

    describe('testAttributesOfObjectsInArrayAreNotEmpty', () => {
        it('returns true when is an empty array', () => {
            expect(ValidationHelpers.testAttributesOfObjectsInArrayAreNotEmpty([])).toBe(true);
        });

        it('returns true when every object in array inside not empty', () => {
            expect(
                ValidationHelpers.testAttributesOfObjectsInArrayAreNotEmpty([
                    { firstAttr: "I'm not an empty attribute" },
                ])
            ).toBe(true);
        });

        it('returns false when exist an object inside array that is empty', () => {
            expect(
                ValidationHelpers.testAttributesOfObjectsInArrayAreNotEmpty([
                    { firstAttr: "I'm not an empty attribute", secondAttr: undefined },
                ])
            ).toBe(false);
        });

        it('returns false when is undefined', () => {
            expect(ValidationHelpers.testAttributesOfObjectsInArrayAreNotEmpty(undefined)).toBe(false);
        });

        it('returns false when is not an array', () => {
            expect(ValidationHelpers.testAttributesOfObjectsInArrayAreNotEmpty("I'm a string")).toBe(false);
        });
    });

    describe('testDateField', () => {
        it('returns true when is a valid date', () => {
            expect(ValidationHelpers.testDateField('2017-12-21T14:36:25Z')).toBe(true);
        });
    });
});
