import SearchHelpers from "../SearchHelpers";

describe('SearchHelpers', () => {
    describe('fuzzySearch', () => {
        it('returns a list with matches when the word is founded like subtring', () => {
            expect(SearchHelpers.fuzzySearch('ello', 'hello world')).toEqual([1,2,3,4])
        });
        it('returns a list with matches when the word is founded in a not continous way', () => {
            expect(SearchHelpers.fuzzySearch('heoo', 'hello world')).toEqual([0,1,4,7]);
        });
        it('returns a list with matches when the word is founded ignoring spaces', () => {
            expect(SearchHelpers.fuzzySearch('he oo', 'hello world')).toEqual([0,1,4,7]);
        });
        it('returns a list with matches when the word is founded considering case insensitive cases', () => {
            expect(SearchHelpers.fuzzySearch('he Oo', 'HelLO world')).toEqual([0,1,4,7]);
            expect(SearchHelpers.fuzzySearch('hE Oo', 'hello world')).toEqual([0,1,4,7]);
            expect(SearchHelpers.fuzzySearch('hE Oo', 'HelLO world')).toEqual([0,1,4,7]);
        });
        it('returns a list with matches when the word that we search is empty', () => {
            expect(SearchHelpers.fuzzySearch('', 'hello world')).toHaveLength(0);
        });
        it('returns a list with matches when words are the same length', () => {
            expect(SearchHelpers.fuzzySearch('hello world', 'hello world')).toEqual([0,1,2,3,4,6,7,8,9,10]);
        });

        it('returns false when empty string is passed', () => {
            expect(SearchHelpers.fuzzySearch('', 'hello world')).toHaveLength(0);
        });
        it('returns false when doesnt match', () => {
            expect(SearchHelpers.fuzzySearch('aaaaa', 'hello world')).toHaveLength(0);
        });
        it('returns false when doesnt match because of the quantity of letters', () => {
            expect(SearchHelpers.fuzzySearch('heooo', 'hello world')).toHaveLength(0);
        });
        it('returns false when doesnt match but words have the same length', () => {
            expect(SearchHelpers.fuzzySearch('hello worlda', 'hello world')).toHaveLength(0);
        });
    });
});
