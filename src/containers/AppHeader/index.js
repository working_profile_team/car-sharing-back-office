import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {withRouter} from 'react-router-dom'

import {withStyles} from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import SearchIcon from 'material-ui-icons/Search';
import CloseIcon from 'material-ui-icons/Close';

import Menu, {MenuItem} from 'material-ui/Menu';

import * as NavigationActions from "redux/navigation/actions";
import Localize from "../../components/Localize";

import OnDeconnexion from "redux/user/OnDeconnexion.workflow";
import SearchBar from "./SearchBar";
import {navigationBarTitleSelector} from "../../selectors/navigation";
import {getLoggedInUsersFullName} from "../../selectors/user";

const styles = {
    title: {
        flex: 1,
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap'
    },
    menu: {
        borderRadius: "5px"
    }
};

class AppHeader extends React.Component {

    state = {
        openMenu: false,
        anchorElementMenu: undefined,
    };

    constructor(props) {
        super(props);
        this.goToProfile = this._goToProfile.bind(this);
        this.handleMenuOnClick = this._handleMenuOnClick.bind(this);
        this.handleMenuOnClose = this._handleMenuOnClose.bind(this);
    }

    _handleMenuOnClick(event) {
        this.setState({openMenu: true, anchorElementMenu: event.currentTarget});
    }

    _handleMenuOnClose() {
        this.setState({openMenu: false});
    }

    _showSearchToggle() {
        this.props.onNavigationActions.showSearch(!this.props.showSearch);
    }

    _goToProfile() {
        this.handleMenuOnClose();
    }

    render() {
        const {classes} = this.props;
        return (
            <AppBar
                position="fixed"
                color={"primary"}
                style={{
                    display: "block",
                    zIndex: 199,
                    paddingLeft: 180,
                }}
            >
                <Toolbar style={{height: '64px'}}>
                    <Typography type="title" color="inherit" className={classes.title}>
                        {
                            this.props.title
                        }
                    </Typography>
                    <SearchBar show={this.props.showSearch}/>
                    <IconButton
                        onClick={this._showSearchToggle.bind(this)}
                        color="inherit" aria-label="Search">
                        {this.props.showSearch
                            ? <CloseIcon id="Selenium-Navigation-HideSearchBar-Icon"/>
                            : <SearchIcon id="Selenium-Navigation-ShowSearchBar-Icon"/>}
                    </IconButton>
                    <Button
                        id="Selenium-Navigation-ShowLoggedInDropdownMenu-Button"
                        onClick={this.handleMenuOnClick}
                        aria-owns="simple-menu"
                        aria-haspopup="true"
                        color="inherit"
                    >
                        {this.props.loggedInUsersFullName}
                    </Button>
                    <Menu
                        id="simple-menu"
                        anchorEl={this.state.anchorElementMenu}
                        open={this.state.openMenu}
                        onClose={this.handleMenuOnClose}
                        className={classes.menu}
                    >
                        <MenuItem
                            id="Selenium-Navigation-LoggedInDropdownMenu-GoToProfile-MenuItem"
                            onClick={this.goToProfile}
                        >
                            {this.props.loggedInUsersFullName}
                        </MenuItem>
                        <MenuItem
                            id="Selenium-Navigation-LoggedInDropdownMenu-Logout-MenuItem"
                            onClick={() => {
                                this.props.onDeconnexion(this);
                                this.handleMenuOnClose()
                            }}
                        >
                            {Localize.locale["AppHeader-Logout"]}
                        </MenuItem>
                        <MenuItem
                            id="Selenium-Navigation-LoggedInDropdownMenu-Preferences-MenuItem"
                            onClick={() => {
                                this.handleMenuOnClose();
                                this.props.history.push('/app/preferences', {referer: this.props.history.location});
                            }}
                        >
                            {Localize.locale["AppHeader-Preferences"]}
                        </MenuItem>
                        <MenuItem
                            disabled
                        >
                            { process.env.REACT_APP_VERSION || 'Dev env'}
                        </MenuItem>
                    </Menu>
                </Toolbar>
            </AppBar>
        );

    }

}


AppHeader.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
    loggedInUsersFullName: getLoggedInUsersFullName(state),
    showSearch: state.navigation.showSearch,
    title: navigationBarTitleSelector(state, ownProps.history.location),
});

const mapActionsToProps = (dispatch) => ({
    onDeconnexion: bindActionCreators(OnDeconnexion, dispatch),
    onNavigationActions: bindActionCreators(NavigationActions, dispatch),
});

export default withRouter(connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'AppHeader')(AppHeader)));
