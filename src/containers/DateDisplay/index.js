import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import Localize from 'components/Localize';

export class DateDisplay extends React.Component {
    componentWillMount() {
        // TODO change this when we handle locale from server
        const supportedLocales = ['en', 'fr'];
        let locale = this.props.locale;

        if (!supportedLocales.includes(locale)) {
            locale = 'en';
        }

        moment.locale(locale);
    }

    formatDate() {
        const { dateFormat, value, timeZone } = this.props;

        if (!dateFormat || !value) {
            return '';
        }

        const momentDate = moment.tz(value, timeZone);

        if (!momentDate.isValid()) {
            return Localize.locale.global.dates.invalidDate;
        }

        return momentDate.format(dateFormat);
    }

    render() {
        return <span>{this.formatDate()}</span>;
    }
}

DateDisplay.propTypes = {
    locale: PropTypes.string.isRequired,
    timeZone: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
    dateFormat: PropTypes.string,
};

DateDisplay.defaultProps = {
    dateFormat: 'LLL',
};

const mapStateToProps = state => ({
    locale: state.localization.current,
    timeZone: state.user.timeZone,
});

export default connect(mapStateToProps)(DateDisplay);
