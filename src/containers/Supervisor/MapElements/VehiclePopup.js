import React from 'react';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Popup } from 'react-mapbox-gl';
import Grid from 'material-ui/Grid';
import Table, { TableBody, TableCell, TableRow } from 'material-ui/Table';
import Divider from 'material-ui/Divider';
import Button from 'material-ui/Button';
import { BOX_STATUSES } from 'constants/Vehicles';
import OnSelectVehicle from 'redux/vehicles/OnSelectVehicle.workflow';
import OnToggleVehicle from 'redux/supervisor/OnToggleVehicle.workflow';
import Localize from 'components/Localize';
import state from 'helpers/state';
import * as aclKeys from 'constants/Acl';

const styles = {
    popupContainer: {
        maxWidth: '430px',
    },
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    button: {
        margin: '5px',
    },
    closeButton: {
        position: 'absolute',
        right: 0,
        top: 0,
        border: 'none',
        borderRadius: '0 3px 0 0',
        cursor: 'pointer',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    addressRow: {
        whiteSpace: 'normal',
    },
};

class VehiclePopup extends React.Component {
    constructor(props) {
        super(props);

        this.goToVehicleDetail = this.goToVehicleDetail.bind(this);
        this.toggleVehicleWorkflow = this.toggleVehicleWorkflow.bind(this);
    }

    goToVehicleDetail() {
        const vehicle = this.props.selectedVehicle;

        this.props.history.push(`/app/vehicles/${vehicle.id}`, {
            referer: this.props.location,
        });
        this.props.OnGoToVehicle({ id: vehicle.id });
    }

    toggleVehicleWorkflow(enable) {
        const vehicle = this.props.selectedFleetVehicle;

        this.props.OnToggleVehicle(this, {
            enable,
            fleetId: vehicle.fleetid.toLowerCase(),
            vehicleId: vehicle.id,
            subStatus: 'Needs to be fixed',
        });
    }

    actionButtons() {
        const classes = this.props.classes;
        const vehicle = this.props.selectedVehicle;

        let buttons = null;

        if (vehicle.id !== undefined) {
            buttons = (
                <Button
                    id="Selenium-Supervisor-VehiclePopup-Details-Button"
                    raised
                    color="primary"
                    className={classes.button}
                    onClick={this.goToVehicleDetail}
                >
                    {Localize.locale.vehicles.actions.vehicleDetails}
                </Button>
            );
        }

        return buttons;
    }

    render() {
        if (!this.props.open) {
            return null;
        }
        const classes = this.props.classes;
        const vehicle = this.props.selectedVehicle;
        const fleetVehicle = this.props.selectedFleetVehicle;
        const vehicleName = vehicle.name && vehicle.plate ? `${vehicle.name} - ${vehicle.plate}` : vehicle.id;
        const { boxStatus, currentLocation, autonomy, battery, odometer } = Localize.locale.vehicles.fieldLabels;
        const { actions, disableVehicle, enableVehicle } = Localize.locale.vehicles.actions;
        const { km, volts } = Localize.locale.vehicles.units;
        return (
            <Popup coordinates={[fleetVehicle.location.longitude, fleetVehicle.location.latitude]}>
                <div className={classes.popupContainer}>
                    <button
                        id="Selenium-Supervisor-VehiclePopup-Close-Button"
                        className={classes.closeButton}
                        onClick={this.props.onClosePopup}
                    >
                        X
                    </button>
                    <Grid container alignItems={'center'} direction={'row'} justify={'center'}>
                        <h2>{vehicleName}</h2>
                    </Grid>
                    <Table>
                        <TableBody>
                            <TableRow>
                                <TableCell>
                                    <strong>{boxStatus}</strong>
                                </TableCell>
                                <TableCell>{BOX_STATUSES[fleetVehicle.box_status]}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <strong>{currentLocation}</strong>
                                </TableCell>
                                <TableCell className={classes.addressRow}>{fleetVehicle.formattedAddr}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <strong>{autonomy}</strong>
                                </TableCell>
                                <TableCell>
                                    {fleetVehicle.autonomy}
                                    {km}
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <strong>{battery}</strong>
                                </TableCell>
                                <TableCell>
                                    {fleetVehicle.battery}
                                    {volts}
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <strong>{odometer}</strong>
                                </TableCell>
                                <TableCell>
                                    {fleetVehicle.km}
                                    {km}
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>

                    <Divider />

                    <Grid container alignItems={'center'} direction={'row'} justify={'center'}>
                        <p>
                            <strong>{actions}</strong>
                        </p>
                    </Grid>
                    {this.actionButtons()}
                    {state.isAllowed(aclKeys.enableVehicleAcl) &&
                        (fleetVehicle.vehicle_status === 0 ? (
                            <Button
                                id="Selenium-Supervisor-VehiclePopup-Disable-Button"
                                raised
                                color="primary"
                                className={classes.button}
                                onClick={() => this.toggleVehicleWorkflow(false)}
                            >
                                {disableVehicle}
                            </Button>
                        ) : (
                            <Button
                                id="Selenium-Supervisor-VehiclePopup-Enable-Button"
                                raised
                                color="primary"
                                className={classes.button}
                                onClick={() => this.toggleVehicleWorkflow(true)}
                            >
                                {enableVehicle}
                            </Button>
                        ))}
                </div>
            </Popup>
        );
    }
}

VehiclePopup.propTypes = {};

VehiclePopup.defaultProps = {};

const mapStateToProps = state => ({
    selectedFleetVehicle: state.fleet.selectedFleetVehicle,
    selectedVehicle: state.vehicles.selectedVehicle,
});

const mapActionsToProps = dispatch => ({
    OnGoToVehicle: bindActionCreators(OnSelectVehicle, dispatch),
    OnToggleVehicle: bindActionCreators(OnToggleVehicle, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'VehiclePopup')(withRouter(VehiclePopup)));
