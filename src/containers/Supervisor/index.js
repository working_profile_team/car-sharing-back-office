import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Dimensions from 'react-dimensions';
import SupervisorMap from 'components/SupervisorMap/SupervisorMap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as SupervisorSelectors from 'selectors/supervisor';
import OnComputeQuery from 'redux/supervisor/OnComputeQuery.workflow';
import OnSearchMarkerSelection from 'redux/supervisor/OnSearchMarkerSelection.workflow';
import OnRemoveSelection from 'redux/supervisor/OnRemoveSelection.workflow';

import SideBarFilters from './Menus/SideBarFilters';
import VehiclePopup from './MapElements/VehiclePopup';

import * as FleetActions from 'redux/fleet/actions';
import deepmerge from 'deepmerge';
import queryString from 'query-string';
import Localize from 'components/Localize';
import OnFetchModels from 'redux/supervisor/OnFetchModels.workflow';
import OnFilterChange from 'redux/supervisor/OnFilterChange.workflow';
import { zonesAssignedToService } from 'selectors/zones';

const styles = theme => ({
    root: {
        flex: 1,
        width: '100%',
        height: 'calc(100vh - 64px)',
        textAlign: 'center',
        position: 'relative',
    },
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    menuContainer: {
        backgroundColor: 'red',
        position: 'absolute',
        width: '23%',
        height: '25%',
        top: '100%',
        left: '100%',
        bottom: '100%',
    },
    drawer: {
        width: 'auto',
        flex: 'initial',
    },
    button: {
        width: '45px',
        margin: theme.spacing.unit,
    },
});

class Supervisor extends React.Component {
    state = {
        error: {
            active: false,
            message: '',
        },
        menus: {
            vehicule: {
                open: false,
                anchorEl: undefined,
            },
            booking: {
                open: false,
                anchorEl: undefined,
            },
        },
        firstLoading: true,
    };

    constructor(props) {
        super(props);
        this.onClickMarker = this._handleClickMarker.bind(this);
        this.onClosePopup = this._handleOnClosePopup.bind(this);
        this.handleOnParse = this._handleOnParse.bind(this);
        this.recenterMapOptions = this._recenterMapOptions.bind(this);
        this.getMapRef = this._getMapRef.bind(this);

        this.mapOptions = {
            center: this.props.mapCenter,
            zoom: [12],
        };

        this.currentZoom = [12];
        this.currentPos = this.props.mapCenter;
    }

    setStateMerge(obj) {
        this.setState(deepmerge(this.state, obj));
    }

    componentWillUpdate(nextProps) {
        const selectedFleetVehicle = this.props.selectedFleetVehicle;

        if (
            (nextProps.selectedFleetVehicle.fleetid &&
                nextProps.selectedFleetVehicle.id &&
                !selectedFleetVehicle.fleetid &&
                !selectedFleetVehicle.id) ||
            (nextProps.showFleetVehicleDetail && !this.props.showFleetVehicleDetail)
        ) {
            if (this.currentZoom[0] < 15) {
                this.mapOptions.center = [
                    nextProps.selectedFleetVehicle.location.longitude,
                    nextProps.selectedFleetVehicle.location.latitude - 0.00008 * 42,
                ];
                this.mapOptions.zoom = [15];
            } else {
                this.mapOptions.zoom = [this.currentZoom[0]];
                this.mapOptions.center = [this.currentPos[0], this.currentPos[1]];
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { location, redrawMapOnChange } = this.props;

        const vehicle = queryString.parse(location.search);
        if (location.search !== prevProps.location.search || this.state.firstLoading) {
            if (vehicle.fleetId && vehicle.vehicleId) {
                this.setState({ firstLoading: false });
                this.props.onSearchMarkerSelection(this, { fleetId: vehicle.fleetId, vehicleId: vehicle.vehicleId });
            }
        }

        // Redraw map on drawer toggle (after animation)
        const mapRef = this.mapRef;
        if (this.mapRef && redrawMapOnChange !== prevProps.redrawMapOnChange) {
            setTimeout(() => {
                mapRef.resize();
            });
        }
    }

    componentDidMount() {
        const vehicle = queryString.parse(this.props.location.search);
        if (!vehicle.fleetId || !vehicle.vehicleId) this.props.onFleetsActions.unselectFleetVehicle();
        this.props.onFetchModels(this);
    }

    _handleOnParse(data) {
        this.props.onComputeQuery(this, data);
    }

    _handleOnClosePopup() {
        this.props.history.push('/app/supervision/', {
            referer: this.props.location,
            name: Localize.locale.Supervisor,
        });
        this.props.onRemoveSelection(this);
        this.props.onFleetsActions.showFleetVehicleDetail(false);
    }

    _handleClickMarker(marker) {
        switch (marker.type) {
            case 'vehicule':
                this.props.history.push(
                    `/app/supervision?fleetId=${marker.payload.fleetid.toLowerCase()}&vehicleId=${marker.payload.id}`,
                    {
                        referer: this.props.location,
                        name: Localize.locale.Supervisor,
                    }
                );
                break;
            case 'zone':
                this.props.history.push(`/app/supervision?zone=${marker.payload.id}`, {
                    referer: this.props.location,
                    name: Localize.locale.Supervisor,
                });
                break;
            default:
                break;
        }
    }

    _getMapRef(map) {
        this.mapRef = map;
    }

    // TODO Should prevent translate after closing popup
    _recenterMapOptions(e) {
        const fleetVehicle = this.props.selectedFleetVehicle;

        this.currentZoom = [e.transform.tileZoom];
        this.currentPos = [e.transform._center.lng, e.transform._center.lat];
        if (!fleetVehicle.fleetid || !fleetVehicle.id || !this.props.showFleetVehicleDetail) {
            return;
        }
        this.mapOptions.zoom = this.currentZoom;
        this.mapOptions.center = this.currentPos;
    }

    render() {
        const classes = this.props.classes;
        const selectedFleetVehicle = this.props.selectedFleetVehicle;

        const mapOptions = this.mapOptions;

        return (
            <div className={classes.root} id="Selenium-Supervisor-Scene">
                <SideBarFilters />

                <SupervisorMap
                    ref="map"
                    {...mapOptions}
                    onStyleLoad={this.getMapRef}
                    fleets={this.props.fleets}
                    allFeatures={this.props.allFeatures}
                    onClickMarker={this.onClickMarker}
                    movingMethod="flyTo"
                    onMoveEnd={this.recenterMapOptions}
                    containerStyle={{
                        /*
                       Need to do this to prevent marignleft bug
                       */
                        height: '100%',
                        width: '100%',
                        // backgroundColor: "red",
                    }}
                >
                    <VehiclePopup
                        onClosePopup={this.onClosePopup}
                        open={this.props.showFleetVehicleDetail && selectedFleetVehicle.id}
                    />
                </SupervisorMap>
            </div>
        );
    }
}

Supervisor.propTypes = {
    classes: PropTypes.object.isRequired,
    fleets: PropTypes.array.isRequired,
};

Supervisor.defaultProps = {
    fleets: [],
};

const mapStateToProps = state => ({
    mapCenter: state.user.mapCenter,
    fleets: SupervisorSelectors.filterFleet(state),
    allZones: state.zone.allZones,
    allFeatures: zonesAssignedToService(state),
    selectedFleetVehicle: state.fleet.selectedFleetVehicle,
    redrawMapOnChange: state.navigation.openLeftDrawer,
    mergedVehicleData: state.fleet.mergedVehicleData,
    showFleetVehicleDetail: state.fleet.showFleetVehicleDetail,
});

const mapActionsToProps = dispatch => ({
    onFilterChange: bindActionCreators(OnFilterChange, dispatch),
    onComputeQuery: bindActionCreators(OnComputeQuery, dispatch),
    onSearchMarkerSelection: bindActionCreators(OnSearchMarkerSelection, dispatch),
    onRemoveSelection: bindActionCreators(OnRemoveSelection, dispatch),
    onFleetsActions: bindActionCreators(FleetActions, dispatch),
    onFetchModels: bindActionCreators(OnFetchModels, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(Dimensions()(withStyles(styles, 'Supervisor')(Supervisor)));
