import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import {FormControlLabel, FormGroup} from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';

import OnFilterChange from "redux/supervisor/OnFilterChange.workflow";
import {filterAvailableModels} from "selectors/supervisor";

import Localize from "components/Localize";


class ModelForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleCheckBox = this.handleCheckBox.bind(this);
    }

    handleCheckBox(key) {
        this.props.onFilterChange(this, {changeFilterKey: key});
    }

    render() {
        const sideFilters = this.props.sideFilters;
        if (this.props.availableModels.length === 0) {
            return null
        }

        return (
            <FormGroup>
                <p><strong>{Localize.locale["Forms-Models-Title"]}</strong></p>
                {
                    this.props.availableModels
                        .map((m) => {
                            return (
                                <FormControlLabel
                                    key={m.id}
                                    control={
                                        <Checkbox
                                            id={`Selenium-Supervisor-Filter-Model-${m.id}-Checkbox`}
                                            value={`model${m.id}`}
                                            checked={sideFilters[`model${m.id}`] || false}
                                            onChange={() => {
                                                this.handleCheckBox(`model${m.id}`)
                                            }}
                                        />
                                    }
                                    label={m.name}
                                />
                            );
                        })
                }
            </FormGroup>
        );
    }

}

const mapStateToProps = (state) => ({
    sideFilters: state.supervisor.sideFilters,
    allModels: state.vehicles.allModels,
    availableModels: filterAvailableModels(state)
});

const mapActionsToProps = (dispatch) => ({
    onFilterChange: bindActionCreators(OnFilterChange, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(ModelForm);
