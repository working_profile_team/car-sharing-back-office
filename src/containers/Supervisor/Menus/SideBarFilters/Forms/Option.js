import React from 'react';
import {withStyles} from 'material-ui/styles';

import {connect} from 'react-redux';
import { bindActionCreators } from "redux";

import {FormControlLabel, FormGroup} from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';

import OnFilterChange from "redux/supervisor/OnFilterChange.workflow";

import OnFetchOptions from "redux/supervisor/OnFetchOptions.workflow";
import Localize from "components/Localize";


const styles = {};

class OptionForm extends React.Component {

    constructor(props) {
        super(props);
        this.handleCheckBox = this.handleCheckBox.bind(this);
    }

    componentWillMount() {
        this.props.onFetchOptions(this);
    }

    handleCheckBox(key) {
        this.props.onFilterChange(this, { changeFilterKey: key });
    }

    render() {
        const sideFilters = this.props.sideFilters;

        return (
            <FormGroup>
                <p><strong>{Localize.locale["Forms-Options-Title"]}</strong></p>
                {
                    this.props.allOptions.map((o) => {
                        return (
                            <FormControlLabel
                                key={o.id}
                                control={
                                    <Checkbox
                                        id={`Selenium-Supervisor-Filter-${o.id}-Checkbox`}
                                        value={`option${o.id}`}
                                        checked={sideFilters[`option${o.id}`] || false}
                                        onChange={() => {
                                            this.handleCheckBox(`option${o.id}`)
                                        }}
                                    />
                                }
                                label={o.name}
                            />
                        );
                    })
                }
            </FormGroup>
        );
    }

}

OptionForm.propTypes = {};

OptionForm.defaultProps = {};

const mapStateToProps = (state) => ({
    sideFilters: state.supervisor.sideFilters,
    allOptions: state.vehicles.allOptions
});

const mapActionsToProps = (dispatch) => ({
    onFilterChange: bindActionCreators(OnFilterChange, dispatch),
    onFetchOptions: bindActionCreators(OnFetchOptions, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'OptionForm')(OptionForm));
