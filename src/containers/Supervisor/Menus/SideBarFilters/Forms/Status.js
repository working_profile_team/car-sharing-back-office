import React from 'react';
import {withStyles} from 'material-ui/styles';

import {connect} from 'react-redux';
import { bindActionCreators } from "redux";

import {FormControlLabel, FormGroup} from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';

import OnFilterChange from "redux/supervisor/OnFilterChange.workflow";
import Localize from "components/Localize";


const styles = {};

class StatusForm extends React.Component {

    constructor(props) {
        super(props);

        this.handleCheckBox = this.handleCheckBox.bind(this);
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
    }

    handleCheckBox(key) {
        this.props.onFilterChange(this, {changeFilterKey: key});
    }

    render() {
        const sideFilters = this.props.sideFilters;
        return (
            <FormGroup>
                <p><strong>{ Localize.locale["Forms-Status-Title"]}</strong></p>
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-Available-Checkbox"
                            value="vehiclestatusAvailable"
                            checked={sideFilters.vehiclestatusAvailable}
                            onChange={() => {
                                this.handleCheckBox('vehiclestatusAvailable')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Status-Available"]}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-InUse-Checkbox"
                            value="vehiclestatusInUse"
                            checked={sideFilters.vehiclestatusInUse}
                            onChange={() => {
                                this.handleCheckBox('vehiclestatusInUse')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Status-InUse"]}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-OutOfService-Checkbox"
                            value="vehiclestatusOutOfService"
                            checked={sideFilters.vehiclestatusOutOfService}
                            onChange={() => {
                                this.handleCheckBox('vehiclestatusOutOfService')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Status-OutOfService"]}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-Archived-Checkbox"
                            value="vehiclestatusArchived"
                            checked={sideFilters.vehiclestatusArchived}
                            onChange={() => {
                                this.handleCheckBox('vehiclestatusArchived')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Status-Archived"]}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-InFleet-Checkbox"
                            value="vehiclestatusToBeInfleet"
                            checked={sideFilters.vehiclestatusToBeInfleet}
                            onChange={() => {
                                this.handleCheckBox('vehiclestatusToBeInfleet')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Status-Infleet"]}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-Unsync-Checkbox"
                            value="vehiclestatusUnsync"
                            checked={sideFilters.vehiclestatusUnsync}
                            onChange={() => {
                                this.handleCheckBox('vehiclestatusUnsync')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Status-Unsync"]}
                />
            </FormGroup>
        );
    }

}

StatusForm.propTypes = {};

StatusForm.defaultProps = {};

const mapStateToProps = (state) => ({
    sideFilters: state.supervisor.sideFilters
});

const mapActionsToProps = (dispatch) => ({
    onFilterChange: bindActionCreators(OnFilterChange, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'StatusForm')(StatusForm));
