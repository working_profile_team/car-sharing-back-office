import React from 'react';
import {withStyles} from 'material-ui/styles';

import {connect} from 'react-redux';
import { bindActionCreators } from "redux";

import {FormControlLabel, FormGroup} from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';

import OnFilterChange from "redux/supervisor/OnFilterChange.workflow";
import Localize from "components/Localize";


const styles = {};

class EnergyForm extends React.Component {

    constructor(props) {
        super(props);

        this.handleCheckBox = this.handleCheckBox.bind(this);
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
    }

    handleCheckBox(key) {
        this.props.onFilterChange(this, { changeFilterKey: key });
    }

    render() {
        const sideFilters = this.props.sideFilters;
        return (
            <FormGroup>
                <p><strong>{Localize.locale["Forms-Energy-EnergyType"]}</strong></p>
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-ElectricEnergy-Checkbox"
                            value="energyElectric"
                            checked={sideFilters.energyElectric}
                            onChange={() => {
                                this.handleCheckBox('energyElectric')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Energy-Electric"]}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-HybridEnergy-Checkbox"
                            value="energyHybrid"
                            checked={sideFilters.energyHybrid}
                            onChange={() => {
                                this.handleCheckBox('energyHybrid')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Energy-Hybrid"]}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filter-FuelEnergy-Checkbox"
                            value="energyFuel"
                            checked={sideFilters.energyFuel}
                            onChange={() => {
                                this.handleCheckBox('energyFuel')
                            }}
                        />
                    }
                    label={Localize.locale["Forms-Energy-Fuel"]}
                />
            </FormGroup>
        );
    }

}

EnergyForm.propTypes = {};

EnergyForm.defaultProps = {};

const mapStateToProps = (state) => ({
    sideFilters: state.supervisor.sideFilters
});

const mapActionsToProps = (dispatch) => ({
    onFilterChange: bindActionCreators(OnFilterChange, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'EnergyForm')(EnergyForm));
