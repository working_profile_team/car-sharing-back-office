import React from 'react';
import {withStyles} from 'material-ui/styles';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import Grid from 'material-ui/Grid';

import Typography from 'material-ui/Typography';

import Divider from 'material-ui/Divider';

import {FormControlLabel} from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';

import OnFilterChange from "redux/supervisor/OnFilterChange.workflow";
import {showFilters} from "redux/supervisor/actions";

import StatusForm from './Forms/Status';
import EnergyForm from './Forms/Energy';
import ModelForm from './Forms/Model';
import Localize from "components/Localize";
import Drawer from 'material-ui/Drawer';

const styles = theme => ({
    separateRight: {
        borderRight: 'solid 1px rgba(0, 0, 0, 0.12)',
        marginRight: '10px',
    },
    backdropModal: {
        position: 'absolute',
    },
    paper: {
        position: 'absolute',
        backgroundColor: 'rgba(255, 255, 255, 1)',
        width: "350px",
        zIndex: 100,
        padding: '15px',
        textAlign: 'left',
        top: '64px',
        right: 0,
        height: "calc(100% - 96px)",
    }
});

class SideBarFilters extends React.Component {
    closeDrawer = () => {
        this.props.showFilters(false);
    };

    render() {
        const classes = this.props.classes;
        const {selectAllSideFilters} = this.props;

        return (
            <Drawer
                anchor="right"
                open={this.props.show}
                onClose={this.closeDrawer}
                type='temporary'
                classes={{paper: classes.paper}}
                elevation={1}
                ModalProps={{
                    BackdropProps: {classes: {root: classes.backdropModal}}
                }}
                id="Selenium-Supervisor-Sidebar-Filters"

            >
                <Typography type="headline" component="h4">
                    {Localize.locale["SideBar-Filters-FilteringOptions"]}
                </Typography>
                <br/>
                <FormControlLabel
                    control={
                        <Checkbox
                            id="Selenium-Supervisor-Filters-SelectAll-Checkbox"
                            value="selectAllSideFilters"
                            checked={selectAllSideFilters}
                            onChange={() => {
                                this.props.onFilterChange(this, {changeSelectAll: !selectAllSideFilters})
                            }}
                        />
                    }
                    label={Localize.locale["SideBar-Filters-Select-All"]}
                />
                <Divider/>

                <Grid container alignItems="flex-start" justify="flex-start" spacing={0}>

                    <Grid item xs={6}>
                        <StatusForm/>
                    </Grid>

                    <Grid item xs={6}>
                        <EnergyForm/>
                    </Grid>

                    <Grid item xs={6}>
                        <ModelForm className={classes.separateRight}/>
                    </Grid>

                </Grid>
            </Drawer>
        );
    }

}

SideBarFilters.propTypes = {};

SideBarFilters.defaultProps = {};

const mapStateToProps = (state) => ({
    selectAllSideFilters: state.supervisor.selectAllSideFilters,
    show: state.supervisor.filtersVisibility
});

const mapActionsToProps = (dispatch) => ({
    onFilterChange: bindActionCreators(OnFilterChange, dispatch),
    showFilters: bindActionCreators(showFilters, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'SideBarFilters')(SideBarFilters));
