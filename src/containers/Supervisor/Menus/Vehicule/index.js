import React from 'react';
import PropTypes from 'prop-types';

import {withStyles} from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import IconButton from "material-ui/IconButton";
import Typography from "material-ui/Typography";

import {Link, Route, Switch} from 'react-router-dom';

import {ListItem} from 'material-ui/List';

import VehiculeInfo from "./VehiculeInfo";

import LocationOnIcon from 'material-ui-icons/LocationOn';

const styles = {
    root: {
        flex: 1,
        width: "100%",
        height: "100%",
        // backgroundColor: "green",
    },
};



class VehiculeMenu extends React.Component {

    renderButton(text, icon) {

        return (
            <ListItem
                button
                style={{
                    width: "86%",
                    height: "100%",
                }}
            >
                <Grid
                    container
                    direction="column"
                    alignItems="stretch"
                    justify="center"
                >
                    <div
                        style={{
                            flex: 1,
                        }}
                    >
                        <LocationOnIcon
                            style={{
                                width: "calc(68px * 1.5)",
                                height: "calc(93px * 1.5)",
                            }}
                        />
                    </div>
                    <Typography
                        type="subheading"
                        style={{
                            textAlign: "center",
                        }}
                    >
                        {
                            text
                        }
                    </Typography>
                </Grid>
            </ListItem>
        )

        return (
            <Grid
                container
                gutter={0}
                direction="row"
                alignItems="center"
                justify="center"
            >
                <IconButton>
                    {
                        icon
                    }
                </IconButton>
                <Typography type="body2" style={{ textAlign: "center", paddingTop: "5px", paddingBottom: "2.5px" }}>
                    {
                        text
                    }
                </Typography>
            </Grid>
        )
    }


    renderRoot() {
        const styleSquare = {
            // backgroundColor: "red"
        }
        const {
            marker
        } = this.props;
        return (
            <div>
                <div className="row">
                    <Link to={"/app/supervision/vehicule/" + marker.id + "/info"} >
                        <div className="square" style={styleSquare}>
                            {
                                this.renderButton("Info", <LocationOnIcon />)
                            }
                        </div>
                    </Link>
                    <div className="square" style={styleSquare}>
                        {
                            this.renderButton("Status", <LocationOnIcon />)
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="square" style={styleSquare}>
                        {
                            this.renderButton("Users", <LocationOnIcon />)
                        }
                    </div>
                    <div className="square" style={styleSquare}>
                        {
                            this.renderButton("Booking", <LocationOnIcon />)
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="square" style={styleSquare}>
                        {
                            this.renderButton("Tickets", <LocationOnIcon />)
                        }
                    </div>
                    <div className="square" style={styleSquare}>
                        {
                            this.renderButton("Logs", <LocationOnIcon />)
                        }
                    </div>
                </div>
            </div>
        )
    }


    render() {
        const classes = this.props.classes;
        return (
            <div className="container" style={{ width: "100%", height: "100%"}}>
                <Switch>
                    <Route path="/app/supervision/vehicule/:idvehicule" render={this.renderRoot.bind(this)}/>
                    <Route path="/app/supervision/vehicule/:idvehicule/info" component={VehiculeInfo}/>
                </Switch>
            </div>
        );
    }


}


VehiculeMenu.propTypes = {
    classes: PropTypes.object.isRequired,

};


VehiculeMenu.defaultProps = {

};

export default withStyles(styles, 'VehiculeMenu')(VehiculeMenu);
