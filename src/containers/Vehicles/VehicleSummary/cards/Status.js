import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import { CardContent, CardHeader } from 'material-ui/Card';
import DateDisplay from 'containers/DateDisplay';
import Localize from 'components/Localize';
import { selectedFleetVehicleStatusNameSelector, selectedFleetVehicle } from 'selectors/fleets';
import Chip from 'material-ui/Chip';
import { BOX_STATUSES } from 'constants/Vehicles';

const styles = theme => ({
    root: {
        flex: 1,
        height: '100%',
    },
    content: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: '0px 60px 0px 16px',
    },
    chip: {
        display: 'flex',
        alignItems: 'center',
        flex: '1 1 auto',
        maxWidth: 200,
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flex: '1 1 auto',
        maxWidth: 200,
        minWidth: 100,
    },
    statusHeader: {
        padding: '16px 16px 0px 16px',
    },
    statusItemLabel: {
        color: 'rgba(0, 0, 0, 0.54)',
        fontSize: '12px',
    },
    statusItemHeader: {
        display: 'flex',
        alignItems: 'center',
        margin: '5px',
    },
    statusItemImg: {
        marginRight: '10px',
        height: 20,
    },
});

const StatusItem = ({ classes, iconName, value, label }) => (
    <div className={classes.item}>
        <div className={classes.statusItemHeader}>
            <img src={`/images/${iconName}.png`} alt={iconName} className={classes.statusItemImg} />
            {value}
        </div>
        <span className={classes.statusItemLabel}>{label}</span>
    </div>
);

const lastConnectedDateDisplay = date => {
    if (date === '' || date === undefined) {
        return null;
    }
    return (
        <span>
            {Localize.locale['VehicleDetail-Connected']} <DateDisplay value={date} />
        </span>
    );
};

const textStatusToColor = status => {
    if (status === '-1') {
        return '#FF0000';
    } else if (status === '0') {
        return '#079c00';
    }
    return '#FFDE00';
};

const Status = ({
    classes,
    selectedVehicleStatusName,
    speed,
    autonomy,
    isDoorClosed,
    isDoorLocked,
    km,
    battery,
    last_active_date,
    autonomyUnit,
    vuboxRawStatus,
}) => (
    <div className={classes.root}>
        <CardHeader
            title={Localize.locale.vehicles.subsectionTitles.status}
            subheader={lastConnectedDateDisplay(last_active_date)}
            className={classes.statusHeader}
        />
        <CardContent className={classes.content}>
            <div className={classes.chip}>
                <Chip
                    label={selectedVehicleStatusName.state}
                    style={{ backgroundColor: textStatusToColor(selectedVehicleStatusName.key), color: 'white' }}
                />
            </div>
            <StatusItem
                iconName="speed"
                classes={classes}
                value={speed ? `${speed} Km/h` : '-'}
                label={Localize.locale.vehicles.fieldLabels.speed}
            />
            <StatusItem
                iconName="autonomy"
                classes={classes}
                value={autonomy ? `${autonomy}${autonomyUnit}` : '-'}
                label={Localize.locale.vehicles.fieldLabels.autonomy}
            />
            <StatusItem
                iconName="doors"
                classes={classes}
                value={`${isDoorClosed ? Localize.locale.vehicles.fieldLabels.doorsClosed : '-'}`}
                label="Doors"
            />
            <StatusItem
                iconName="key"
                classes={classes}
                value={`${isDoorLocked ? Localize.locale.vehicles.fieldLabels.doorsLocked : '-'}`}
                label="Vehicle"
            />
            <StatusItem
                iconName="odometer"
                classes={classes}
                value={km ? `${km}Km` : '-'}
                label={Localize.locale.vehicles.fieldLabels.odometer}
            />
            <StatusItem
                iconName="battery"
                classes={classes}
                value={battery ? `${battery}V` : '-'}
                label={Localize.locale.vehicles.fieldLabels.auxBattery}
            />
            <StatusItem
                iconName="box"
                classes={classes}
                label={Localize.locale.vehicles.fieldLabels.boxStatus}
                value={vuboxStatusDisplay(vuboxRawStatus)}
            />
        </CardContent>
    </div>
);

Status.propTypes = {
    classes: PropTypes.object.isRequired,
    selectedVehicleStatusName: PropTypes.object.isRequired,
    speed: PropTypes.number,
    autonomy: PropTypes.number,
    isDoorClosed: PropTypes.bool,
    isDoorLocked: PropTypes.bool,
    km: PropTypes.number,
    battery: PropTypes.number,
    last_active_date: PropTypes.string,
    vuboxRawStatus: PropTypes.number,
};

Status.defaultProps = {
    speed: undefined,
    autonomy: undefined,
    isDoorClosed: undefined,
    isDoorLocked: undefined,
    km: undefined,
    battery: undefined,
    last_active_date: undefined,
};

const vuboxStatusDisplay = (status) => {
    return (!!status) ?
        `${BOX_STATUSES[status]} (${status})` :
        '-';
};

const mapStateToProps = state => {
    const { speed, autonomy, isDoorClosed, isDoorLocked, km, battery, last_active_date } = selectedFleetVehicle(state);
    const { box_status } = state.fleet.selectedFleetVehicle;
    return {
        speed,
        autonomy,
        isDoorClosed,
        isDoorLocked,
        km,
        battery,
        last_active_date,
        vuboxRawStatus: parseInt(box_status, 10),
        selectedVehicleStatusName: selectedFleetVehicleStatusNameSelector(state),
        autonomyUnit: state.application.autonomyUnit,
    };
};

export default connect(mapStateToProps)(withStyles(styles, 'Status')(withRouter(Status)));
