import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { compose, withHandlers, withProps, pure } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TabExpertMode from 'components/Tabs/TabExpertMode';
import TabTroubleshooting from 'components/Tabs/TabTroubleshooting';
import TabTrips from 'components/Tabs/TabTrips';
import { withStyles } from 'material-ui/styles';
import { Tabs, Tab, AppBar } from 'material-ui';
import PhonelinkRing from 'material-ui-icons/PhonelinkRing';
import ShowChart from 'material-ui-icons/ShowChart';
import Info from 'material-ui-icons/Info';
import Paper from 'material-ui/Paper';
import * as TripsActions from 'redux/trips/actions';
import * as VehiclesActions from 'redux/vehicles/actions';
import * as FleetActions from 'redux/fleet/actions';
import OnToggleVehicle from 'redux/supervisor/OnToggleVehicle.workflow';
import OnWakeVehicle from 'redux/supervisor/OnWakeVehicle.workflow';
import OnPingVehicle from 'redux/supervisor/OnPingVehicle.workflow';
import OnCancelBook from 'redux/vehicles/OnCancelBook.workflow';
import OnEndTrip from 'redux/vehicles/OnEndTrip.workflow';
import OnReleaseTrip from 'redux/vehicles/OnReleaseTrip.workflow';
import Localize from 'components/Localize';
import { BOX_STATUSES_CANCELABLE_TRIP } from 'constants/Vehicles';
import {
    tripsByVehicleTableDataSelector,
    tripsByVehicleTableCurrentPage,
    tripsByVehicleTableDataMaxPagesSelector,
} from 'selectors/trips';
import state from 'helpers/state';
import { getTabActionsSelected, canClickExpertCommandSelector } from 'selectors/vehicles';
import * as aclKeys from 'constants/Acl';

const styles = () => ({
    root: {
        flex: 1,
        height: '100%',
        margin: '10px',
    },
    paper: {
        paddingTop: 0,
        paddingBottom: 16,
        overflow: 'hidden',
        paddingLeft: 0,
        paddingRight: 0,
    },
    row: {
        marginTop: 0,
    },
    item: {
        paddingRight: '15px',
        minWidth: 'max-content',
    },
});

const mapStateToProps = state => ({
    listExpertCommands: state.fleet.listExpertCommands,
    selectedVehicle: state.vehicles.selectedVehicle,
    selectedFleetVehicle: state.fleet.selectedFleetVehicle,
    showExpertCommands: state.vehicles.showExpertCommands,
    tabActionsSelected: getTabActionsSelected(state),
    tripsData: tripsByVehicleTableDataSelector(state),
    tripsByVehicleTableCurrentPage: tripsByVehicleTableCurrentPage(state),
    tripsByVehicleTableDataMaxPages: tripsByVehicleTableDataMaxPagesSelector(state),
    showExpertCommandDialog: state.fleet.showExpertCommandResponse,
    expertCommandResponse: state.fleet.expertCommandResponse,
    canClickExpertCommand: canClickExpertCommandSelector(state),
});

const mapActionsToProps = dispatch => ({
    onTripsActions: bindActionCreators(TripsActions, dispatch),
    onVehiclesActions: bindActionCreators(VehiclesActions, dispatch),
    onWakeVehicle: bindActionCreators(OnWakeVehicle, dispatch),
    onPingVehicle: bindActionCreators(OnPingVehicle, dispatch),
    onToggleVehicle: bindActionCreators(OnToggleVehicle, dispatch),
    onCancelBook: bindActionCreators(OnCancelBook, dispatch),
    onReleaseTrip: bindActionCreators(OnReleaseTrip, dispatch),
    onEndTrip: bindActionCreators(OnEndTrip, dispatch),
    onFleetActions: bindActionCreators(FleetActions, dispatch),
});

const enhance = compose(
    withRouter,
    withStyles(styles, 'Commands'),
    connect(
        mapStateToProps,
        mapActionsToProps
    ),
    withHandlers({
        onOpenTripDetail: props => id => props.history.push(`/app/trips/${id}`, { referer: props.history.location }),
        onPageTrip: props => newPage => props.onTripsActions.tryChangeTripsByVehicleIdTablePageThunk({ newPage }),
        onRequestSortTrip: props => (orderBy, order) =>
            props.onTripsActions.tryChangeTripsByVehicleIdTablePageThunk({ orderBy, order }),
        onChangeTab: props => (event, selectedTabIndex) =>
            props.onVehiclesActions.setSelectedUsersTabIndex(selectedTabIndex),
        onClickWakeVehicle: ({ onWakeVehicle, selectedFleetVehicle: vehicle }) => () =>
            onWakeVehicle(this, {
                fleetId: (vehicle.fleetid || '-1').toLowerCase(),
                vehicleId: vehicle.id,
                showingVehicles: true,
            }),
        onClickPingVehicle: ({ onPingVehicle, selectedFleetVehicle: vehicle }) => () =>
            onPingVehicle({
                fleetId: vehicle.fleetid && vehicle.fleetid.toLowerCase(),
                vehicleId: vehicle.id,
                showingVehicles: true,
            }),
        onClickEnableVehicle: ({ onToggleVehicle, selectedFleetVehicle: vehicle }) => () =>
            onToggleVehicle(this, {
                enable: true,
                fleetId: vehicle.fleetid && vehicle.fleetid.toLowerCase(),
                vehicleId: vehicle.id,
                subStatus: 'Needs to be fixed',
                showingVehicles: true,
            }),
        onClickDisableVehicle: ({ onToggleVehicle, selectedFleetVehicle: vehicle }) => () =>
            onToggleVehicle(this, {
                enable: false,
                fleetId: vehicle.fleetid && vehicle.fleetid.toLowerCase(),
                vehicleId: vehicle.id,
                subStatus: 'Needs to be fixed',
                showingVehicles: true,
            }),
        onClickCancelBook: ({ onCancelBook, selectedFleetVehicle: vehicle }) => () =>
            onCancelBook({
                fleetId: vehicle.fleetid && vehicle.fleetid.toLowerCase(),
                vehicleId: vehicle.id,
                orderId: vehicle.orderId,
                userId: vehicle.userId,
            }),
        onClickReleaseTrip: ({ onReleaseTrip, selectedFleetVehicle: vehicle }) => () =>
            onReleaseTrip({
                fleetId: vehicle.fleetid && vehicle.fleetid.toLowerCase(),
                vehicleId: vehicle.id,
                orderId: vehicle.orderId,
            }),
        onClickEndTrip: ({ onEndTrip, selectedFleetVehicle: vehicle }) => () =>
            onEndTrip({
                fleetId: vehicle.fleetid && vehicle.fleetid.toLowerCase(),
                vehicleId: vehicle.id,
                orderId: vehicle.orderId,
                userId: vehicle.userId,
            }),
        onSendExpertCmd: ({ onFleetActions, selectedFleetVehicle: vehicle }) => value => () =>
            onFleetActions.sendExpertCmd({ vehicle_id: vehicle.id, cmd: value.name }),
        onClosePopup: ({ onFleetActions }) => () => onFleetActions.hideExpertCommandResponse(),
    }),
    withProps(
        ({
            selectedFleetVehicle,
            onClickCancelBook,
            onClickEndTrip,
            onClickDisableVehicle,
            onClickEnableVehicle,
            onClickReleaseTrip,
        }) => {
            const { userId, orderId } = selectedFleetVehicle;
            let disabled = false;
            let lastButton;

            if (BOX_STATUSES_CANCELABLE_TRIP.indexOf(selectedFleetVehicle.box_status) > -1) {
                lastButton = {
                    disabled,
                    onClickLastAction: onClickCancelBook,
                    keyLastAction: 'cancel',
                    idLastAction: 'Selenium-VehicleSummary-Cancel-Button',
                    labelLastButton: Localize.locale.vehicles.summary.commands.cancelBooking,
                };
            } else if (selectedFleetVehicle.box_status >= 4) {
                disabled = !(userId && orderId);
                lastButton = {
                    disabled,
                    onClickLastAction: onClickEndTrip,
                    keyLastAction: 'end',
                    idLastAction: 'Selenium-VehicleSummary-EndTrip-Button',
                    labelLastButton: Localize.locale.vehicles.summary.commands.end,
                };
            } else if (selectedFleetVehicle.vehicle_status === 0) {
                lastButton = {
                    disabled,
                    onClickLastAction: onClickDisableVehicle,
                    keyLastAction: 'disable',
                    idLastAction: 'Selenium-VehicleSummary-DisableVehicle-Button',
                    labelLastButton: Localize.locale.vehicles.summary.commands.disable,
                };
            } else if (selectedFleetVehicle.vehicle_status === 5) {
                lastButton = {
                    disabled,
                    onClickLastAction: onClickReleaseTrip,
                    keyLastAction: 'release',
                    idLastAction: 'Selenium-VehicleSummary-Release-Button',
                    labelLastButton: Localize.locale.vehicles.summary.commands.release,
                };
            } else {
                lastButton = {
                    disabled,
                    onClickLastAction: onClickEnableVehicle,
                    keyLastAction: 'enable',
                    idLastAction: 'Selenium-VehicleSummary-EnableVehicle-Button',
                    labelLastButton: Localize.locale.vehicles.summary.commands.enable,
                };
            }

            return {
                lastButton,
            };
        }
    ),
    pure
);

const Commands = ({
    classes,
    tabActionsSelected,
    onChangeTab,
    onClickWakeVehicle,
    onClickPingVehicle,
    onClickDiagnostics,
    lastButton,
    selectedFleetVehicle: vehicle,
    listExpertCommands,
    onSendExpertCmd,
    showExpertCommandDialog,
    expertCommandResponse,
    tripsData,
    tripsByVehicleTableDataMaxPages,
    onOpenTripDetail,
    onRequestSortTrip,
    onPageTrip,
    onClosePopup,
    tripsByVehicleTableCurrentPage: tripsByVehicleTableCurrentPageProps,
    canClickExpertCommand,
}) => (
        <div className={classes.root}>
            <Paper className={classes.paper} elevation={4}>
                <AppBar position="static" color="default">
                    <Tabs value={tabActionsSelected} indicatorColor="primary" textColor="primary" onChange={onChangeTab}>
                        {state.isAllowed(aclKeys.vehicleTroubleshootAcl) && (
                            <Tab
                                id="Selenium-Vehicle-Summary-Troubleshooting"
                                label={Localize.locale.vehicles.summary.commands.tabs.troubleshooting}
                                icon={<PhonelinkRing />}
                            />
                        )}
                        {state.isAllowed(aclKeys.vehicleTripAcl) && (
                            <Tab
                                id="Selenium-Vehicle-Summary-Trips"
                                label={Localize.locale.vehicles.summary.commands.tabs.trips}
                                icon={<ShowChart />}
                            />
                        )}
                        {state.isAllowed(aclKeys.vehicleExpertModeAcl) && (
                            <Tab
                                id="Selenium-Vehicle-Summary-ExpertMode"
                                label={Localize.locale.vehicles.summary.commands.tabs.expertMode}
                                icon={<Info />}
                            />
                        )}
                    </Tabs>
                </AppBar>
                {(() => index => tabContentList => tabContentList.filter(v => v)[index])()(tabActionsSelected)([
                    state.isAllowed(aclKeys.vehicleTroubleshootAcl) && (
                        <TabTroubleshooting
                            onClickWakeVehicle={onClickWakeVehicle}
                            onClickPingVehicle={onClickPingVehicle}
                            onClickDiagnostics={onClickDiagnostics}
                            onClickLastAction={lastButton.onClickLastAction}
                            keyLastAction={lastButton.keyLastAction}
                            idLastAction={lastButton.idLastAction}
                            labelLastButton={lastButton.labelLastButton}
                            disabled={lastButton.disabled}
                        />
                    ),
                    state.isAllowed(aclKeys.vehicleTripAcl) && (
                        <TabTrips
                            vehicleId={vehicle.id}
                            tripsData={tripsData}
                            tripsByVehicleTableCurrentPage={tripsByVehicleTableCurrentPageProps}
                            onOpenTripDetail={onOpenTripDetail}
                            tripsByVehicleTableDataMaxPages={tripsByVehicleTableDataMaxPages}
                            onPageTrip={onPageTrip}
                            onRequestSortTrip={onRequestSortTrip}
                        />
                    ),
                    state.isAllowed(aclKeys.vehicleExpertModeAcl) && (
                        <TabExpertMode
                            canClick={canClickExpertCommand}
                            listExpertCommands={listExpertCommands}
                            onSendExpertCmd={onSendExpertCmd}
                            onClosePopup={onClosePopup}
                            showExpertCommandDialog={showExpertCommandDialog}
                            expertCommandResponse={expertCommandResponse}
                        />
                    ),
                ])}
            </Paper>
        </div>
    );

Commands.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    onClickDiagnostics: PropTypes.func.isRequired,
    tabActionsSelected: PropTypes.number,
    onClickWakeVehicle: PropTypes.func,
    onClickPingVehicle: PropTypes.func,
    onChangeTab: PropTypes.func,
    lastButton: PropTypes.shape({}),
    selectedFleetVehicle: PropTypes.shape({}),
    listExpertCommands: PropTypes.arrayOf(PropTypes.string),
    onSendExpertCmd: PropTypes.func,
    onClosePopup: PropTypes.func,
    showExpertCommandDialog: PropTypes.bool,
    expertCommandResponse: PropTypes.string,
    tripsData: PropTypes.arrayOf(PropTypes.shape({})),
    tripsByVehicleTableDataMaxPages: PropTypes.number,
    onOpenTripDetail: PropTypes.func,
    onRequestSortTrip: PropTypes.func,
    onPageTrip: PropTypes.func,
    canClickExpertCommand: PropTypes.bool,
};

export default enhance(Commands);
