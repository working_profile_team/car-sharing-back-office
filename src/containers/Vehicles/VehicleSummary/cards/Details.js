import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import { CardContent, CardHeader } from 'material-ui/Card';
import Localize from 'components/Localize';
import moment from 'moment';
import { isSelectedVehicleAssignedToCurrentService } from 'selectors/vehicles';
import VehicleEdit from './VehicleEdit';

const styles = () => ({
    root: {
        flex: 1,
        height: '100%',
    },
    content: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    contentItems: {
        display: 'flex',
        flex: '1',
        flexWrap: 'wrap',
    },
    detailOptions: {
        flex: '0',
    },
    detailHeader: {
        padding: '16px 16px 0px 16px',
    },
    detailItem: {
        display: 'flex',
        flexDirection: 'column',
        minWidth: 200,
        margin: '15px 0px 15px 0px',
    },
    detailItemLabel: {
        color: 'rgba(0, 0, 0, 0.54)',
        fontSize: '12px',
    },
});

const commonPropTypes = {
    classes: PropTypes.shape().isRequired,
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.arrayOf(PropTypes.shape())]),
};

const DetailItem = ({ classes, label, value }) => (
    <div className={classes.detailItem}>
        <span className={classes.detailItemLabel}>{label}</span>
        <span>{value || '-'}</span>
    </div>
);

const DetailOptions = ({ classes, label, value }) => (
    <div className={classes.detailItem}>
        <span className={classes.detailItemLabel}>{label}</span>
        {value.length > 0 ? value.map((v, i) => <span key={i}>- {v.name}</span>) : '-'}
    </div>
);

DetailOptions.propTypes = commonPropTypes;

const lastConnectedDateDisplay = date => {
    if (date === '' || date === undefined) {
        return null;
    }
    return (
        <span>
            {Localize.locale['Components-VehicleInfo-LastUpdated']} {moment(date).fromNow()}
        </span>
    );
};

const Details = ({
    model,
    wakeUpProvider,
    vin,
    vuboxId,
    plate,
    updateDate,
    options,
    classes,
    service,
    externalId
}) => (
    <div className={classes.root}>
        <CardHeader
            title={Localize.locale.vehicles.subsectionTitles.details}
            className={classes.detailHeader}
            subheader={lastConnectedDateDisplay(updateDate)}
        />
        <CardContent className={classes.content}>
            <div className={classes.contentItems}>
                <DetailItem classes={classes} label={Localize.locale.vehicles.fieldLabels.plateNumber} value={plate} />
                <DetailItem
                    classes={classes}
                    label={Localize.locale.vehicles.fieldLabels.modelDescription}
                    value={model.description}
                />
                <DetailItem
                    classes={classes}
                    label={Localize.locale.vehicles.fieldLabels.wakeUpProvider}
                    value={wakeUpProvider}
                />
                <DetailItem classes={classes} label={Localize.locale.vehicles.fieldLabels.vin} value={vin} />
                <DetailItem classes={classes} label={Localize.locale.vehicles.fieldLabels.vuboxId} value={vuboxId} />
                <DetailItem
                    classes={classes}
                    label={Localize.locale.vehicles.fieldLabels.model}
                    value={model.description}
                />
                <DetailItem classes={classes} label={Localize.locale.vehicles.fieldLabels.name} value={model.name} />
                <DetailItem
                    classes={classes}
                    label={Localize.locale.vehicles.fieldLabels.energyType}
                    value={model.energyType}
                />
                <DetailItem
                    classes={classes}
                    label={Localize.locale.vehicles.fieldLabels.seats}
                    value={model.nbOfSeats}
                />
                <DetailItem classes={classes} label={Localize.locale.vehicles.fieldLabels.id} value={model.id} />
                <DetailItem
                    classes={classes}
                    label={Localize.locale.vehicles.fieldLabels.service}
                    value={service ? Localize.locale.services.assigned : Localize.locale.services.unassigned}
                />
                <DetailItem
                    classes={classes}
                    label={Localize.locale.vehicles.fieldLabels.externalId}
                    value={externalId}
                />
            </div>
            <div className={classes.detailOptions}>
                <DetailOptions classes={classes} label={Localize.locale.vehicles.fieldLabels.options} value={options} />
            </div>
        </CardContent>
        <VehicleEdit />
    </div>
);

Details.propTypes = {
    classes: PropTypes.shape().isRequired,
    options: PropTypes.arrayOf(PropTypes.shape()),
    model: PropTypes.shape(),
    wakeUpProvider: PropTypes.string,
    vin: PropTypes.string,
    vuboxId: PropTypes.string,
    plate: PropTypes.string,
    updateDate: PropTypes.string,
    service: PropTypes.bool.isRequired,
    externalId: PropTypes.string,
};

const mapStateToProps = state => {
    const { model, wakeUpProvider, vin, vuboxId, plate, updateDate, options, externalId } = state.vehicles.selectedVehicle;
    return {
        options,
        model,
        wakeUpProvider,
        vin,
        vuboxId,
        plate,
        updateDate,
        service: isSelectedVehicleAssignedToCurrentService(state),
        externalId,
    };
};

export default connect(mapStateToProps)(withStyles(styles, 'Details')(withRouter(Details)));
