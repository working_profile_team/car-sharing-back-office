import React from 'react';
import { withStyles } from 'material-ui/styles/index';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types'
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Localize from 'components/Localize';
import FunctionHelpers from 'helpers/FunctionHelpers';
import { CardContent, CardHeader, } from 'material-ui/Card';
import { fetchUserInformationByFleetThunk, } from 'redux/fleet/actions';
import { bindActionCreators, } from 'redux';

import { selectedFleetVehicle, selectedFleetUser, } from 'selectors/fleets';
import { isSBEnabledSelector, } from 'selectors/services';
import { timeZoneSelector, } from 'selectors/user';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    title: {
        color: theme.palette.secondary.main,
        paddingBottom: '5px',
        marginBottom: '20px',
    },
    hr: {
        marginBottom: '20px',
        marginTop: '20px',
    },
    formDataField: {
        marginRight: '1em',
        color: 'rgba(0, 0, 0, 0.54)',
        fontSize: '12px',
    },

    actionsButtons: {
        margin: '5px 15px',
        border: `2px solid ${theme.palette.primary[500]}`,
        borderRadius: '5px',
        display: 'block',
        color: theme.palette.primary[500],
    },
    hide: {
        display: 'none',
    },
    noDecoration: {
        textDecoration: 'none',
    },
});

class BookingInfo extends React.Component {

    static propTypes = {
        classes: PropTypes.shape({}),
        isSBEnabled: PropTypes.bool,
        timeZone: PropTypes.string,
        fetchUserInformationByFleetThunk: PropTypes.func,
        selectedFleetVehicle: PropTypes.shape({
            userId: PropTypes.string,
            orderId: PropTypes.string,
            booking_date: PropTypes.string,
            expiresOn: PropTypes.string,
        }),
        selectedFleetUser: PropTypes.shape({
            firstName: PropTypes.string,
            lastName: PropTypes.string,
        }),
    };

    componentDidMount() {
        this.props.fetchUserInformationByFleetThunk();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.selectedFleetVehicle.userId !== this.props.selectedFleetVehicle.userId) {
            this.props.fetchUserInformationByFleetThunk();
        }
    }

    handleDisplayUser = () =>
        this.props.selectedFleetUser ? (
            <Link
                id="Selenium-BookingInfo-Link-user"
                to={{
                    pathname: `/app/users/${this.props.selectedFleetVehicle.userId}`,
                    state: {
                        name: `User details for ${this.props.selectedFleetUser.firstName} ${
                            this.props.selectedFleetUser.lastName
                        }`,
                    },
                }}
            >
                {FunctionHelpers.null2String(
                    `${this.props.selectedFleetUser.firstName} ${this.props.selectedFleetUser.lastName}`
                )}
            </Link>
        ) : (
            Localize.locale.vehicles.messages.notAvailable
        );

    handleBookingDate = date => {
        const dateNow = FunctionHelpers.dateFromTimeZone(new Date(), this.props.timeZone);
        const dateBooking = FunctionHelpers.dateFromTimeZone(new Date(date), this.props.timeZone);
        const dateDiff = FunctionHelpers.dateDiff(new Date(dateBooking), new Date(dateNow));

        const day = dateDiff.day > 0 ? `${dateDiff.day} ${Localize.locale.vehicles.messages.day}, ` : '';
        const hour = dateDiff.hour > 0 ? `${dateDiff.hour} H ` : '';
        const min = dateDiff.min > 0 ? `${dateDiff.min} min ` : '';
        const sec = dateDiff.min <= 0 && dateDiff.hour <= 0 && dateDiff.day <= 0 ? `few secondes ` : '';

        return date !== null
            ? `${day} ${hour} ${min} ${sec} ${Localize.locale.vehicles.messages.ago}`
            : Localize.locale.vehicles.messages.notAvailable;
    };

    handleExpiresDate = expiresAt => {

        const dateExpires = FunctionHelpers.dateFromTimeZone(new Date(expiresAt), this.props.timeZone);
        const dateNow = FunctionHelpers.dateFromTimeZone(new Date(), this.props.timeZone);
        const dateDiff = FunctionHelpers.dateDiff(new Date(dateNow), new Date(dateExpires));

        const min = dateDiff.min > 0 ? `${dateDiff.min} min ` : '';
        const sec = dateDiff.min > 0 ? `${dateDiff.sec} seconde(s) ` : '';

        if (expiresAt !== null) {
            if (dateDiff.sec <= 0 && dateDiff.min <= 0) {
                return Localize.locale.vehicles.messages.expired;
            }
            return `${min} ${sec}`;
        }

        return Localize.locale.vehicles.messages.notAvailable;
    };

    render() {
        const classes = this.props.classes;
        const bookingDate = this.props.selectedFleetVehicle ? this.props.selectedFleetVehicle.booking_date : null;
        const expiresAt = this.props.selectedFleetVehicle ? this.props.selectedFleetVehicle.expiresOn : null;

        return (
            <div className={classes.root}>
                <CardHeader
                    title={Localize.locale.vehicles.subsectionTitles.bookingInformation}
                    className={classes.statusHeader}
                />
                <CardContent className={classes.content}>
                    <Grid>
                        <Grid container spacing={24}>
                            <Grid item xs={6} sm={3}>
                                <span className={classes.formDataField}>
                                    {Localize.locale.vehicles.summary.bookingInformation.booked}:
                                </span>
                                <br />
                                {this.handleBookingDate(bookingDate)}
                            </Grid>

                            {!this.props.isSBEnabled && (
                                <Grid item xs={6} sm={3}>
                                    <span className={classes.formDataField}>
                                        {Localize.locale.vehicles.summary.bookingInformation.expiresAt}:
                                    </span>
                                    <br />
                                    {this.handleExpiresDate(expiresAt)}
                                </Grid>
                            )}

                            <Grid item xs={6} sm={3}>
                                <span className={classes.formDataField}>
                                    {Localize.locale.vehicles.summary.bookingInformation.user}:
                                </span>
                                <br />
                                {this.handleDisplayUser()}
                            </Grid>

                            <Grid item xs={6} sm={3}>
                                <Link
                                    id="Selenium-BookingInfo-Link-trip"
                                    className={classes.noDecoration}
                                    to={`/app/trips/${this.props.selectedFleetVehicle.orderId}`}
                                >
                                    <Button id="Selenium-BookingInfo-Button-trip" className={classes.actionsButtons}>
                                        {Localize.locale.vehicles.summary.bookingInformation.tripDetails}
                                    </Button>
                                </Link>
                            </Grid>
                        </Grid>
                    </Grid>
                </CardContent>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    selectedFleetVehicle: selectedFleetVehicle(state),
    selectedFleetUser: selectedFleetUser(state),
    isSBEnabled: isSBEnabledSelector(state),
    timeZone: timeZoneSelector(state),
});

const mapActionsToProps = dispatch => ({
    fetchUserInformationByFleetThunk: bindActionCreators(fetchUserInformationByFleetThunk, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'BookingInfo')(withRouter(BookingInfo)));
