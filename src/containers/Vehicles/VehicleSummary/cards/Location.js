import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import { CardContent, CardHeader } from 'material-ui/Card';
import { Marker } from 'react-map-gl';
import { debounce } from 'lodash';
import StaticMap from 'components/Maps/StaticMap';
import Localize from 'components/Localize';
import MapboxClient from 'apis/Mapbox';
import { getMarkerPath } from 'components/Maps/Markers/VehiculeMarker';
import { compose, withHandlers } from 'recompose';
import { setMapCenter } from 'redux/user/actions';
import DateDisplay from 'containers/DateDisplay';

const mpClient = new MapboxClient();
const styles = () => ({
    root: {
        flex: 1,
    },
    content: {
        height: 400,
        padding: 16,
    },
    contentItems: {
        display: 'flex',
        flex: '1',
        flexWrap: 'wrap',
    },
    locationItem: {
        display: 'flex',
        flexDirection: 'column',
        minWidth: 200,
        margin: '15px 0px 15px 16px',
    },
    locationItemLabel: {
        color: 'rgba(0, 0, 0, 0.54)',
        fontSize: '12px',
        float: 'left',
    },
    map: {
        cursor: 'pointer',
        width: '100%',
    },
});

const widthImage = 118 / 5;
const heightImage = 164 / 5;
const LocationItem = ({ classes, label, value }) => (
    <div className={classes.locationItem}>
        <span className={classes.locationItemLabel}>{label}</span>
        <span id={`Selenium-Vehicle-LocationItem-${label}`}>{value || '-'}</span>
    </div>
);

class Location extends React.Component {
    static propTypes = {
        classes: PropTypes.shape({}).isRequired,
        vehicle: PropTypes.shape({
            id: PropTypes.string,
            boxid: PropTypes.string,
            location: PropTypes.shape({
                longitude: PropTypes.number,
                latitude: PropTypes.number,
                gpsDate: PropTypes.instanceOf(Date),
            }),
        }).isRequired,
        onMapClick: PropTypes.func.isRequired,
        mapbox: PropTypes.shape({}),
    };

    static defaultProps = {
        vehicle: {
            location: {
                longitude: undefined,
                latitude: undefined,
                gpsDate: undefined,
            },
            vehicle_status: 0,
            inZones: [],
        },
    };

    constructor() {
        super();
        this.mapRef = null;
        this.updateMapWidth = debounce(this.updateMapWidth, 150);
    }

    state = {
        mapWidth: 0,
        address: {
            formattedAddr: '',
        },
    };

    componentDidMount() {
        window.addEventListener('resize', this.updateMapWidth);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.mapWidth === 0 && this.mapRef !== null) {
            this.updateMapWidth();
        }
        if (prevProps.vehicle.location !== this.props.vehicle.location) {
            const self = this;
            const { location } = this.props.vehicle;
            mpClient.tryGeocodingOnVehicle({ location }).then(address => self.setState({ address }));
        }
    }

    setMapRef = el => (this.mapRef = el);

    updateMapWidth = () => {
        if (!this.mapRef) return;

        this.setState(() => ({ mapWidth: this.mapRef.offsetWidth }));
    };

    render() {
        const { vehicle, classes, mapbox } = this.props;
        const { address } = this.state;
        const { location } = vehicle;

        if (!location.latitude || !location.longitude) {
            return <div className={classes.root} />;
        }

        return (
            <div className={classes.root}>
                <CardHeader
                    title={Localize.locale.vehicles.subsectionTitles.location}
                    subheader={address.formattedAddr}
                />
                <div className={classes.contentItems}>
                    <LocationItem
                        classes={classes}
                        label={Localize.locale.vehicles.location.inZone}
                        value={
                            vehicle.inZones && vehicle.inZones.length > 0
                                ? vehicle.inZones.map((z, i) => <span key={i}>{z.name}</span>)
                                : '-'
                        }
                    />
                    <LocationItem
                        classes={classes}
                        label={Localize.locale.vehicles.location.lastUpdated}
                        value={
                            vehicle.location.gpsDate !== null ? (
                                <span id="Selenium-Vehicle-LocationItem-gpsDate">
                                    {' '}
                                    <DateDisplay value={vehicle.location.gpsDate} dateFormat="DD-MM-YYYY" />
                                </span>
                            ) : (
                                Localize.locale.vehicles.messages.notAvailable
                            )
                        }
                    />
                </div>

                <CardContent className={classes.content}>
                    <div onClick={this.props.onMapClick} ref={this.setMapRef} className={classes.map}>
                        <StaticMap
                            longitude={location.longitude}
                            latitude={location.latitude}
                            zoom={12}
                            width={this.state.mapWidth}
                            height={400}
                            bearing={10}
                            mapbox={mapbox}
                        >
                            <Marker longitude={location.longitude} latitude={location.latitude}>
                                <img
                                    src={getMarkerPath(vehicle.vehicle_status)}
                                    style={{
                                        marginLeft: widthImage / 4,
                                        width: widthImage,
                                        height: heightImage,
                                    }}
                                    alt="vehicle marker"
                                />
                            </Marker>
                        </StaticMap>
                    </div>
                </CardContent>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    vehicle: state.fleet.selectedFleetVehicle,
    mapbox: state.application.mapbox,
});

const mapDispatchToProps = dispatch => ({
    mapCenter: (lng, lat) => dispatch(setMapCenter([lng, lat])),
});

const enhance = compose(
    withRouter,
    withStyles(styles, 'Location'),
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withHandlers({
        onMapClick: ({ mapCenter, vehicle, history: { push } }) => () => {
            mapCenter(vehicle.location.longitude, vehicle.location.latitude);
            push({
                pathname: '/app/supervision',
                search: `?fleetId=${vehicle.fleetid}&vehicleId=${vehicle.id}`,
                state: { name: Localize.locale.Supervisor },
            });
        },
    })
);

export default enhance(Location);
