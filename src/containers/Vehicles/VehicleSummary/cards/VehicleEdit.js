import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import DateDisplay from 'containers/DateDisplay';
import Localize from 'components/Localize';
import * as VehiclesActions from 'redux/vehicles/actions';
import { openConfirmDialog, closeConfirmDialog } from 'redux/application/actions';
import OnToggleVehicleAssignmentToService from 'redux/vehicles/OnToggleVehicleAssignmentToService.workflow';
import OnArchiveVehicle from 'redux/vehicles/OnArchiveVehicle.workflow';
import { isSelectedVehicleAssignedToCurrentService } from 'selectors/vehicles';
import PopupConfirm from 'components/PopupConfirm';
import { OnOpenCreateTicket } from '../../../../redux/trips/actions';
import state from 'helpers/state';
import * as aclKeys from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        height: '100%',
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
        overflow: 'hidden',
    }),
    container: {
        margin: 0,
        width: '100%',
    },
    chipsContainer: {
        marginTop: 10,
        marginBottom: 12,
    },
    grey: {
        color: theme.palette.secondary.main,
    },
    formDataField: {
        fontWeight: 'bold',
        marginRight: '1em',
    },
    row: {
        margin: 0,
    },
    title: {
        color: theme.palette.secondary.main,
        paddingBottom: '5px',
    },
    item: {
        paddingRight: '15px',
        minWidth: 'max-content',
    },
});

class VehicleInfo extends React.Component {
    constructor(props) {
        super(props);
        this.onRequestEdit = this._handleRequestEdit.bind(this);
        this.onClickConfirmToggleServiceAssignment = this._handleConfirmToggleServiceAssignment.bind(this);
        this.onClickDialogTrigger = this._handleDialog.bind(this);
        this.onClickConfirmArchive = this._handleConfirmArchive.bind(this);
        this.openCreateTicket = this._handleOpenCreateTicket.bind(this);
    }

    _handleOpenCreateTicket() {
        this.props.history.push(`/app/tickets/create`, { referer: this.props.history.location });
        this.props.OnOpenCreateTicket({
            selectedVehicleId: this.props.selectedVehicle.id,
        });
    }

    _handleRequestEdit() {
        this.props.onVehiclesActions.changeFlowState('EDIT');
        const id = this.props.selectedVehicle.id;
        this.props.history.push(`/app/vehicles/${id}/edit`, { referer: this.props.history.location });
    }

    _handleConfirmToggleServiceAssignment() {
        this.props.toggleAssignmentToService(this.props.selectedVehicle.id);
        this.props.closeConfirmDialog();
    }

    _handleDialog(payload) {
        this.props.openConfirmDialog(payload);
    }

    _handleConfirmArchive() {
        this.props.archiveVehicle().then(() => {
            this.props.closeConfirmDialog();
        });
    }

    formatLastFetch() {
        return (
            <span>
                {this.props.lastFetch
                    ? `${Localize.locale['Components-VehicleInfo-LastUpdated']} `
                    : Localize.locale['Components-VehicleInfo-NeverUpdated']}
                <DateDisplay value={this.props.lastFetch} />
            </span>
        );
    }

    render() {
        const classes = this.props.classes;
        const selectedVehicle = this.props.selectedVehicle;
        const archiveTitle = Localize.locale.global.messages.permanentOperationTitle;
        const archiveMessage = `${Localize.locale.global.messages.permanentOperationDescription} ${
            Localize.locale.vehicles.archive.warningText
        }`;
        const assignmentTitle = this.props.isSelectedVehicleAssignedToCurrentService
            ? Localize.locale.vehicles.messages.confirmUnassignment
            : Localize.locale.vehicles.messages.confirmAssignment;
        const assignmentMessage = Localize.locale.global.messages.confirmYourChoice;

        return (
            <div className={classes.root}>
                <Grid container className={classes.row} spacing={16} direction={'row'}>
                    <Grid
                        item
                        style={{ paddingTop: '4px', paddingBottom: '4px', overflow: 'hidden', textAlign: 'left' }}
                        className={classes.item}
                    >
                        {state.isAllowed(aclKeys.editVehicleAcl) && (
                            <Button
                                raised
                                id="Selenium-VehicleInfo-EditVehicle-Button"
                                color={'primary'}
                                onClick={this.onRequestEdit}
                            >
                                {Localize.locale.vehicles.actions.edit}
                            </Button>
                        )}
                    </Grid>
                    <Grid
                        item
                        style={{ paddingTop: '4px', paddingBottom: '4px', overflow: 'hidden', textAlign: 'left' }}
                        className={classes.item}
                    >
                        {state.isAllowed(aclKeys.assignVehicleAcl) && (
                            <Button
                                raised
                                id="Selenium-VehicleInfo-ToggleServiceAssignment-Button"
                                color={'primary'}
                                onClick={() =>
                                    this.onClickDialogTrigger({
                                        title: assignmentTitle,
                                        message: assignmentMessage,
                                        confirmCallBack: this.onClickConfirmToggleServiceAssignment,
                                    })
                                }
                            >
                                {this.props.isSelectedVehicleAssignedToCurrentService
                                    ? Localize.locale.services.unassignFromService
                                    : Localize.locale.services.assignToService}
                            </Button>
                        )}
                    </Grid>
                    {selectedVehicle.id &&
                        !selectedVehicle.archived && (
                            <Grid
                                item
                                style={{
                                    paddingTop: '4px',
                                    paddingBottom: '4px',
                                    overflow: 'hidden',
                                    textAlign: 'left',
                                }}
                                className={classes.item}
                            >
                                {state.isAllowed(aclKeys.archiveVehicleAcl) && (
                                    <Button
                                        raised
                                        id="Selenium-VehicleInfo-Archive-Button"
                                        color={'primary'}
                                        onClick={() =>
                                            this.onClickDialogTrigger({
                                                title: archiveTitle,
                                                message: archiveMessage,
                                                confirmCallBack: this.onClickConfirmArchive,
                                            })
                                        }
                                    >
                                        {Localize.locale.vehicles.actions.archive}
                                    </Button>
                                )}
                            </Grid>
                        )}
                    <Grid
                        item
                        style={{ paddingTop: '4px', paddingBottom: '4px', overflow: 'hidden', textAlign: 'left' }}
                        className={classes.item}
                    >
                        {state.isAllowed(aclKeys.createVehicleTicketAcl) && (
                            <Button
                                id="Selenium-TicketCreate-Button"
                                color={'primary'}
                                onClick={this.openCreateTicket}
                                raised
                            >
                                {Localize.locale.ticketing.create}
                            </Button>
                        )}
                    </Grid>
                </Grid>
                <PopupConfirm
                    open={this.props.confirmDialog.active}
                    title={this.props.confirmDialog.title}
                    message={this.props.confirmDialog.message}
                    onClickCancel={() => {
                        this.props.closeConfirmDialog();
                    }}
                    onClickConfirm={() => {
                        this.props.confirmDialog.confirmCallBack();
                    }}
                />
            </div>
        );
    }
}

VehicleInfo.propTypes = {
    classes: PropTypes.object.isRequired,
};

VehicleInfo.defaultProps = {};

const mapStateToProps = state => ({
    selectedVehicle: state.vehicles.selectedVehicle,
    flowState: state.vehicles.flowState,
    lastFetch: state.fleet.lastFetch,
    confirmDialog: state.application.confirmDialog,
    isSelectedVehicleAssignedToCurrentService: isSelectedVehicleAssignedToCurrentService(state),
});

const mapActionsToProps = dispatch => ({
    onVehiclesActions: bindActionCreators(VehiclesActions, dispatch),
    OnOpenCreateTicket: bindActionCreators(OnOpenCreateTicket, dispatch),
    openConfirmAssignmentDialog() {
        dispatch(openConfirmDialog());
    },
    toggleAssignmentToService(vehicleId) {
        dispatch(OnToggleVehicleAssignmentToService(vehicleId));
    },
    archiveVehicle() {
        return dispatch(OnArchiveVehicle());
    },
    openConfirmDialog(payload) {
        dispatch(openConfirmDialog(payload));
    },
    closeConfirmDialog() {
        dispatch(closeConfirmDialog());
    },
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'VehicleInfo')(withRouter(VehicleInfo)));
