import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { withStyles } from 'material-ui/styles';

import * as VehiclesActions from 'redux/vehicles/actions';

import AutosuggestChipsLayout from 'components/AutosuggestChips/AutosuggestChipsLayout';
import Localize from 'components/Localize';

const styles = theme => ({
    root: {
        margin: 0,
    },
    item: {
        maxWidth: '350px',
        minWidth: '250px',
        width: '250px',
    },
    hide: {
        display: 'none',
    },
    emptyMessage: {
        fontStyle: 'italic',
    },
});

class AutoCompleteChipsOptions extends React.Component {
    constructor(props) {
        super(props);
        this.onRequestDelete = this._handleOnRequestDelete.bind(this);
        this.onRequestSelect = this._handleOnRequestSelect.bind(this);
        this.onChange = this._handleOnChange.bind(this);
    }

    componentDidMount() {
        this.props.onVehiclesActions.tryFetchAllOptions();
    }

    _handleOnRequestDelete = elem => () => {
        this.props.onVehiclesActions.removeOption(elem);
    };

    _handleOnRequestSelect = (event, data) => {
        // UGLY trick cannot stay like that
        setTimeout(() => {
            this.autoSuggestInput.input.blur();
        }, 100);
        this.props.onVehiclesActions.addOption(data.suggestion);
    };

    _handleOnChange(payload) {
        this.props.onVehiclesActions.changeOptionsAutocomplete(payload);
    }

    render() {
        const canModify = this.props.flowState === 'CREATE' || this.props.flowState === 'EDIT';

        return (
            <AutosuggestChipsLayout
                emptyMessage={Localize.locale['autosuggestChips-AutosuggestChipsOptions-EmptyMessage']}
                data={this.props.selectedVehicleOptions}
                onRequestDelete={this.onRequestDelete}
                canDelete={canModify}
                canAdd={canModify}
                onRequestSelect={this.onRequestSelect}
                autosuggestProps={{
                    label: Localize.locale['autosuggestChips-AutosuggestChipsOptions-Option'],
                    placeholder: Localize.locale['autosuggestChips-AutosuggestChipsOptions-Search'],
                    allSuggestions: this.props.allOptions,
                    currentSuggestions: this.props.autosuggest.suggestions,
                    onChangeAutosuggest: this.onChange,
                    onSuggestionSelected: this.onRequestSelect,
                    inputValue: this.props.autosuggest.value,
                    defaultValue: '',
                    currentChips: this.props.selectedVehicleOptions,
                    inputRef: ref => (this.autoSuggestInput = ref),
                }}
            />
        );
    }
}

AutoCompleteChipsOptions.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    allOptions: state.vehicles.allOptions,
    autosuggest: state.vehicles.autosuggestChipsOptions.autosuggest,
    selectedVehicle: state.vehicles.selectedVehicle,
    selectedVehicleOptions: state.vehicles.selectedVehicle.options,
    flowState: state.vehicles.flowState,
});

const mapActionsToProps = dispatch => ({
    onVehiclesActions: bindActionCreators(VehiclesActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'AutosuggestChipsOptions')(AutoCompleteChipsOptions));
