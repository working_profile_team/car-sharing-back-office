import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';

import {withStyles} from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Input, {InputLabel} from 'material-ui/Input';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogTitle,
} from 'material-ui/Dialog';
import { FormControl } from 'material-ui/Form';

import Localize from 'components/Localize'
import Button from "material-ui/Button";
import {
    sendPassthroughCmd,
    clearPassthroughCommandResponse
} from "redux/fleet/actions";

const styles = {
    root: {
        flex: 1,
        height: "100%",
        margin: '10px',
    },
    form: {
        width: '100%'
    },
    formControl: {
        width: '100%',
        marginBottom: 16
    }
};


class PassthroughPopup extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            command: '',
            uid: 0,
        };

        this.handleClose = this.handleClose.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.incrementUid = this.incrementUid.bind(this);
    }

    incrementUid() {
        const uid = this.state.uid === 99 ? 0 : (this.state.uid + 1);
        this.setState({uid});
    }

    handleClose() {
        this.props.onClose();
    }

    handleSubmit(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        this.props.sendPassthroughCmd(this.props.vehicleId, this.state.command);
        this.incrementUid();
    }

    render() {
        const {classes} = this.props;
        const diagStrings = Localize.locale.vehicles.diagnostics;

        return (
            <div className={classes.root}>
                <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    disableBackdropClick
                    disableEscapeKeyDown
                    aria-labelledby="form-dialog-title"
                >
                    <form className={classes.form} autoComplete="off" onSubmit={this.handleSubmit}>
                        <DialogTitle id="form-dialog-title">{diagStrings.passthrough.heading}</DialogTitle>
                        <DialogContent>
                            <Grid
                                container
                                spacing={8}
                            >
                                <Grid item xs={12} >
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="Selenium-FormVehiclePassthrough-Command" >
                                            {diagStrings.passthrough.enterCommand}
                                        </InputLabel>
                                        <Input
                                            id='Selenium-FormVehiclePassthrough-Command'
                                            value={this.state.command}
                                            onChange={event => { this.setState({ command: event.target.value }) }}
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12} >
                                    <h3>Latest command output</h3>
                                    <code>{this.props.passthroughCommandResponse}</code>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                { Localize.locale['Forms-Cancel'] }
                            </Button>
                            <Button color="primary" type='submit'>
                                { Localize.locale.actions.common.confirm }
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </div>
        )
    }

}


PassthroughPopup.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    passthroughCommandResponse: state.fleet.passthroughCommandResponse
});

const mapActionsToProps = (dispatch) => ({
    sendPassthroughCmd(vehicleId, cmd) {
        dispatch(clearPassthroughCommandResponse());
        dispatch(sendPassthroughCmd(vehicleId, cmd));
    }
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'PassthroughPopup')(PassthroughPopup));
