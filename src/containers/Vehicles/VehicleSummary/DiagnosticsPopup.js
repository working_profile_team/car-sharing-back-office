import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Input, { InputLabel } from 'material-ui/Input';
import Select from 'material-ui/Select';
import Dialog, { DialogActions, DialogContent, DialogTitle } from 'material-ui/Dialog';
import { FormControl } from 'material-ui/Form';

import Localize from 'components/Localize';
import Button from 'material-ui/Button';
import { sendPassthroughCmd, clearPassthroughCommandResponse } from 'redux/fleet/actions';
import FunctionHelpers from 'helpers/FunctionHelpers';

const styles = {
    root: {
        flex: 1,
        height: '100%',
        margin: '10px',
    },
    form: {
        width: '100%',
    },
    formControl: {
        width: '100%',
        marginBottom: 16,
    },
};

class DiagnosticsPopup extends React.Component {
    constructor(props) {
        super(props);

        this.commands = [
            ['lock', '>{UID},@CTRL,DOOR_CLOSE'],
            ['unlock', '>{UID},@CTRL,DOOR_OPEN'],
            ['engage_immobiliser', '>{UID},@CTRL,SADOFF'],
            ['disengage_immobiliser', '>{UID},@CTRL,SADON'],
            ['area_led', '>{UID},@CTRL,TEST_AREA_LED'],
            ['light_up_ckh_led', '>{UID},@CTRL,TEST_CKH_LED'],
            ['card_and_key', '>{UID},@CTRL,DIAG;VAPI_CDPROCESS'],
            ['rfid_last_card_id', '>{UID},@CTRL,SM_LAST_TAG'],
            ['rfid_red_led_on', '>{UID},@CTRL,LEDS;1--'],
            ['can_bus', '>{UID},@CTRL,DIAG;VAPI_REALTIME'],
            ['gps_calibration', '>{UID},@CTRL,GPS_RESET'],
        ];

        this.state = {
            uid: 0,
            command: this.commands[0][1],
        };

        this.handleClose = this.handleClose.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.incrementUid = this.incrementUid.bind(this);
    }

    incrementUid() {
        const uid = this.state.uid === 99 ? 0 : this.state.uid + 1;
        this.setState({ uid });
    }

    handleClose() {
        this.props.onClose();
    }

    handleSubmit(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        const command = this.state.command.replace('{UID}', this.state.uid);
        this.props.sendPassthroughCmd(this.props.vehicleId, command);
        this.incrementUid();
    }

    listCommands() {
        const diagStrings = Localize.locale.vehicles.diagnostics;

        return this.commands.map(([key, commandTemplate]) => {
            const command = commandTemplate.replace('{UID}', this.state.uid);

            return (
                <option id={`Selenium-UserBilling-Charge-MenuItem${key}`} value={commandTemplate} key={key}>
                    {diagStrings.commands[key]}: {command}
                </option>
            );
        });
    }

    render() {
        const { classes } = this.props;
        const diagStrings = Localize.locale.vehicles.diagnostics;

        return (
            <div className={classes.root}>
                <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    disableBackdropClick
                    disableEscapeKeyDown
                    aria-labelledby="form-dialog-title"
                >
                    <form className={classes.form} autoComplete="off" onSubmit={this.handleSubmit}>
                        <DialogTitle id="form-dialog-title">{diagStrings.simpleView.heading}</DialogTitle>
                        <DialogContent>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="Selenium-FormVehiclePassthrough-Command">
                                            {diagStrings.simpleView.selectCommand}
                                        </InputLabel>
                                        <Select
                                            native
                                            classes={{ select: 'Selenium-Select', selectMenu: 'Selenium-Select-Menu' }}
                                            value={this.state.command}
                                            onChange={event => {
                                                this.setState({ command: event.target.value });
                                            }}
                                            input={
                                                <Input
                                                    id="Selenium-FormVehiclePassthrough-Command"
                                                    style={{ width: '100%' }}
                                                />
                                            }
                                        >
                                            {FunctionHelpers.formatSelectOptions(this.listCommands(), 'html')}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <h3>{diagStrings.latestCommandOutput}</h3>
                                    <code>{this.props.passthroughCommandResponse}</code>
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                {Localize.locale['Forms-Cancel']}
                            </Button>
                            <Button color="primary" type="submit">
                                {Localize.locale.actions.common.confirm}
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </div>
        );
    }
}

DiagnosticsPopup.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    passthroughCommandResponse: state.fleet.passthroughCommandResponse,
});

const mapActionsToProps = dispatch => ({
    sendPassthroughCmd(vehicleId, cmd) {
        dispatch(clearPassthroughCommandResponse());
        dispatch(sendPassthroughCmd(vehicleId, cmd));
    },
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'DiagnosticsPopup')(DiagnosticsPopup));
