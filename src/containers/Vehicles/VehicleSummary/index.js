import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Card from 'material-ui/Card';
import Divider from 'material-ui/Divider';

import StatusCard from 'containers/Vehicles/VehicleSummary/cards/Status';
import BookingCard from 'containers/Vehicles/VehicleSummary/cards/BookingInfo';
import DetailsCard from 'containers/Vehicles/VehicleSummary/cards/Details';
import LocationCard from 'containers/Vehicles/VehicleSummary/cards/Location';
import CommandsCard from "containers/Vehicles/VehicleSummary/cards/Commands";
import PassthroughPopup from "containers/Vehicles/VehicleSummary/PassthroughPopup";
import DiagnosticsPopup from "containers/Vehicles/VehicleSummary/DiagnosticsPopup";

import * as TripsActions from "redux/trips/actions";
import * as VehiclesActions from "redux/vehicles/actions";
import { selectFleetVehicle } from 'redux/fleet/actions';
import OnSaveVehicle from 'redux/vehicles/OnSaveVehicle.workflow';
import OnSelectVehicle from 'redux/vehicles/OnSelectVehicle.workflow';

import * as ApplicationActions from 'redux/application/actions';
import queryString from 'query-string';

const styles = theme => ({
    root: {
        position: 'relative',
        fontSize: 14,
    },
    bottomActions: {
        padding: 0,
        margin: 0,
        position: 'absolute',
        bottom: -60,
        right: 0,
    },
    hideDisplay: {
        display: 'none',
    },
    button: {
        color: theme.palette.primary[500],
    },
    mainCard: {
        margin: 10,
    },
    divider: {
        height: 2,
        marginLeft: 16,
        marginRight: 16,
    },
});

class VehicleSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showPassthrough: false,
            showDiagnostics: false,
        };
    }

    componentDidMount() {
        const { location } = this.props;
        const qsParams = queryString.parse(location.search);
        // If querystring contains pass=through then show Vehicle Passthrough dialog
        const showPassthrough = Boolean(qsParams.pass && qsParams.pass === 'through');

        this.setState({ showPassthrough });

        this.props.onTripsActions.fetchTripsByVehicleIdThunk();
        this.props.OnSelectVehicle({ id: this.props.vehicleId });
        this.refreshFleetDataInterval = window.setInterval(this.refreshFleetData, 30000);
    }

    componentWillUnmount() {
        window.clearInterval(this.refreshFleetDataInterval);
    }

    componentDidUpdate(previousProps) {
        if (this.props.vehicleId !== previousProps.vehicleId) {
            this.props.OnSelectVehicle({ id: this.props.vehicleId });
        }
        if(this.props.id !== previousProps.id) {
            this.props.onTripsActions.fetchTripsByVehicleIdThunk();
        }
    }

    refreshFleetData = () => {
        this.props.loadFleetData(this.props.vehicleId);
    };

    handleOnClosePassthroughPopup = () => {
        this.setState({showPassthrough: false});
    };

    handleOnClickDiagnosticsButton = () => {
        this.setState({showDiagnostics: true});
    };

    handleOnCloseDiagnosticsPopup = () => {
        this.setState({showDiagnostics: false});
    };

    render() {
        const { classes, vehicle_status } = this.props;

        return (
            <div className={this.props.open ? '' : this.props.classes.hideDisplay}>
                <div className={classes.root}>
                    <Card className={classes.mainCard}>
                        <StatusCard />
                        {(vehicle_status === 1 || vehicle_status === 5) && (
                            <div>
                                <Divider className={classes.divider} />
                                <BookingCard />
                            </div>
                        )}
                        <Divider className={classes.divider} />
                        <DetailsCard />
                        <Divider className={classes.divider} />
                        <LocationCard />
                        <Divider className={classes.divider} />
                        <CommandsCard
                            onClickDiagnostics={this.handleOnClickDiagnosticsButton}
                        />
                        <DiagnosticsPopup
                            vehicleId={this.props.vehicleId}
                            open={this.state.showDiagnostics}
                            onClose={this.handleOnCloseDiagnosticsPopup}
                        />
                        <PassthroughPopup
                            vehicleId={this.props.vehicleId}
                            open={this.state.showPassthrough}
                            onClose={this.handleOnClosePassthroughPopup}
                        />
                    </Card>
                </div>
            </div>
        );
    }
}

VehicleSummary.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
};

VehicleSummary.defaultProps = {
    open: false,
};

const mapStateToProps = (state) => ({
    id: state.vehicles.selectedVehicle.id,
    selectedVehicle: state.vehicles.selectedVehicle,
    selectedVehicleModel: state.vehicles.selectedVehicle.model,
    flowState: state.vehicles.flowState,
    showExpertCommands: state.vehicles.showExpertCommands,
    vehicle_status: state.fleet.selectedFleetVehicle.vehicle_status,
});

const mapActionsToProps = (dispatch) => ({
    onTripsActions: bindActionCreators(TripsActions, dispatch),
    onVehiclesActions: bindActionCreators(VehiclesActions, dispatch),
    OnSaveVehicle: bindActionCreators(OnSaveVehicle, dispatch),
    OnSelectVehicle: bindActionCreators(OnSelectVehicle, dispatch),
    onApplicationActions: bindActionCreators(ApplicationActions, dispatch),
    loadFleetData: vehicleId => {
        dispatch(selectFleetVehicle({ vehicleId }));
    },
});

export default connect(
    mapStateToProps,
    mapActionsToProps,
)(withStyles(styles, 'VehicleSummary')(withRouter(VehicleSummary)));
