import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';

import VehicleList from 'containers/Vehicles/VehicleList';
import NotAssignedVehicleList from 'containers/Vehicles/NotAssignedVehicleList';
import VehicleDetail from 'containers/Vehicles/VehicleDetail';
import VehicleSummary from 'containers/Vehicles/VehicleSummary';
import VuboxToIntegrateSummary from 'containers/Vehicles/VuboxToIntegrateSummary';

import * as ApplicationActions from 'redux/application/actions';
import * as VehiclesActions from 'redux/vehicles/actions';
import * as FleetActions from 'redux/fleet/actions';

const styles = {
    root: {
        minWidth: '310px',
    },
};

class VehicleSection extends React.Component {
    componentDidMount() {
        const { onVehiclesActions, onFleetActions } = this.props;

        onFleetActions.tryFetchAllVuboxesToIntegrate();
        onVehiclesActions.tryFetchAllVehicles();
    }

    renderList() {
        const open = this.props.destinationRoute === 'vehicleList';

        if (open) {
            return <VehicleList open={open} />;
        }
        return null;
    }

    renderDetail() {
        const open = this.props.destinationRoute === 'editVehicle' || this.props.destinationRoute === 'createVehicle';

        if (open) {
            return (
                <VehicleDetail
                    open={open}
                    destinationRoute={this.props.destinationRoute}
                    vehicleId={this.props.match.params.vehicleId}
                />
            );
        }
        return null;
    }

    renderSummary() {
        const open = this.props.destinationRoute === 'vehicleSummary';

        if (open) {
            return <VehicleSummary open={open} vehicleId={this.props.match.params.vehicleId} />;
        }
        return null;
    }

    renderToIntegrateSummary() {
        const open =
            this.props.destinationRoute === 'vuboxToIntegrateSummary' ||
            this.props.destinationRoute === 'vuboxToIntegrateAssign';

        if (open) {
            return <VuboxToIntegrateSummary open={open} vuboxId={this.props.match.params.vuboxId} />;
        }
        return null;
    }

    renderNotAssignedList() {
        const open = this.props.destinationRoute === 'vuboxToIntegrateAssign';

        if (open) {
            return <NotAssignedVehicleList open={open} />;
        }
        return null;
    }

    render() {
        const classes = this.props.classes;
        return (
            <Grid className={classes.root} id="Selenium-Vehicle-Scene">
                {this.renderList()}
                {this.renderDetail()}
                {this.renderSummary()}
                {this.renderToIntegrateSummary()}
                {this.renderNotAssignedList()}
            </Grid>
        );
    }
}

VehicleSection.propTypes = {
    classes: PropTypes.object.isRequired,
};

VehicleSection.defaultProps = {};

const mapStateToProps = state => ({});

const mapActionsToProps = dispatch => ({
    onApplicationActions: bindActionCreators(ApplicationActions, dispatch),
    onVehiclesActions: bindActionCreators(VehiclesActions, dispatch),
    onFleetActions: bindActionCreators(FleetActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'VehicleSection')(VehicleSection));
