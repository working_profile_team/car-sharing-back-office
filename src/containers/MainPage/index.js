import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {Route, Switch} from 'react-router-dom';
import PrivateRoute from "../Root/PrivateRoute";

import Login from "containers/Login";
import Home from "containers/Home";

class MainPage extends React.Component {
    render() {
        return (
            <Switch>
                <Route
                    extact
                    path="/login"
                    component={Login}
                />
                <PrivateRoute
                    canUseApp={this.props.canUseApp}
                    isLoading={this.props.active}
                    extact
                    path="/"
                    component={Home}
                />
            </Switch>
        );

    }

}


MainPage.propTypes = {
    userLogged: PropTypes.bool.isRequired,
};


MainPage.defaultProps = {
    userLogged: false
};

const mapStateToProps = (state) => ({
    openLeft: state.navigation.openLeftDrawer,
    showSearch: state.navigation.showSearch
});


export default connect(mapStateToProps)(MainPage);
