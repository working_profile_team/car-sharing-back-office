import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { compose, withHandlers, pure, lifecycle } from 'recompose';
import { withRouter } from 'react-router-dom';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppBar from 'material-ui/AppBar';

import UserTable from 'containers/Users/UserList/UserTable';

import Localize from 'components/Localize';
import * as UsersActions from 'redux/users/actions';
import OnSelectUser from 'redux/users/OnSelectUser.workflow';
import {
    usersTableDataSelector,
    usersTableDataMaxPagesSelector,
    usersTableDataSizeSelector
} from 'selectors/users';
import Section from 'components/Section';

const styles = {
    root: {
        flex: 1,
        width: '100%',
        height: '100%',
        margin: '0',
        padding: '0',
    },
    hideDisplay: {
        display: 'none',
    },
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        padding: 0,
        margin: 0,
        justifyContent: 'space-around',
    },
};

const mapStateToProps = state => ({
    selectedTabIndex: state.users.selectedUsersTabIndex,
    usersTableData: usersTableDataSelector(state),
    usersTableDataMaxPages: usersTableDataMaxPagesSelector(state),
    usersTableDataSize: usersTableDataSizeSelector(state),
    usersTableCurrentPage: state.users.usersTableCurrentPage,
});

const mapActionsToProps = dispatch => ({
    onUsersActions: bindActionCreators(UsersActions, dispatch),
    OnSelectUser: bindActionCreators(OnSelectUser, dispatch),
});

const enhance = compose(
    withRouter,
    Section('Selenium-Users-Scene'),
    withStyles(styles, 'UserList'),
    connect(
        mapStateToProps,
        mapActionsToProps
    ),
    lifecycle({
        componentDidMount() {
            this.props.onUsersActions.tryFetchAllUsers();
        },
    }),
    withHandlers({
        onOpenUserDetail: props => id => {
            props.history.push(`/app/users/${id}`, {
                referer: props.history.location,
                name: `User details for ${id}`,
            });
            props.OnSelectUser({ id });
        },
        onClickAddUser: props => () => {
            props.onUsersActions.unselectUser();
            props.onUsersActions.changeFlowState('CREATE');
            props.history.push(`/app/users/create`, { referer: props.history.location });
        },
        onChangeTab: props => (event, selectedTabIndex) => {
            props.onUsersActions.setPageChangeInitialState();
            props.onUsersActions.setSelectedUsersTabIndex(selectedTabIndex);
        },
    }),
    pure
);

const UserList = ({
    classes,
    selectedTabIndex,
    usersTableData,
    usersTableDataMaxPages,
    usersTableDataSize,
    usersTableCurrentPage,
    onOpenUserDetail,
    onClickAddUser,
    onChangeTab,
    onUsersActions,
}) => (
    <Grid className={classes.root}>
        <AppBar position={'static'} color="default">
            <Tabs
                value={selectedTabIndex}
                scrollable
                scrollButtons="on"
                indicatorColor="primary"
                textColor="primary"
                classes={{ root: classes.root }}
                onChange={onChangeTab}
            >
                <Tab label={Localize.locale.users.types.pending} />
                <Tab label={Localize.locale.users.types.approved} />
                <Tab label={Localize.locale.users.types.rejected} />
                <Tab label={Localize.locale.users.types.suspended} />
                <Tab label={Localize.locale.users.types.all} />
            </Tabs>
        </AppBar>
        <UserTable
            tableData={usersTableData}
            tableDataSize={usersTableDataSize}
            tableDataMaxPages={usersTableDataMaxPages}
            tableCurrentPage={usersTableCurrentPage}
            tableActions={onUsersActions.tryChangeUsersTablePage}
            onOpenUserDetail={onOpenUserDetail}
            onClickAddUser={onClickAddUser}
        />
    </Grid>
);

UserList.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    selectedTabIndex: PropTypes.number.isRequired,
    usersTableData: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    usersTableDataMaxPages: PropTypes.number.isRequired,
    usersTableDataSize: PropTypes.number.isRequired,
    usersTableCurrentPage: PropTypes.shape({}).isRequired,
    onOpenUserDetail: PropTypes.func.isRequired,
    onClickAddUser: PropTypes.func.isRequired,
    onChangeTab: PropTypes.func.isRequired,
    onUsersActions: PropTypes.shape({}).isRequired,
};

export default enhance(UserList);
