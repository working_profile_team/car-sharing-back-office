import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { compose, withHandlers, pure, defaultProps } from 'recompose'

import { withStyles } from 'material-ui/styles';
import SortableFilterTable from "components/Tables/SortableFilterTable";

import * as ActionsUsers from "redux/users/actions";
import { UserTable as UserTableModel } from "redux/users/models/UserTable.model";

const styles = {
    root: {
        flex: 1,
        height: "100%",
        marginLeft: "20px",
        marginRight: "20px",
        paddingTop: 10
    },
};

const mapActionsToProps = (dispatch) => ({
    onActionUser: bindActionCreators(ActionsUsers, dispatch)
});

const enhance = compose(
    withStyles(styles, 'UserTable'),
    defaultProps({
        wrapperId: 'Selenium-UsersTable-Wrapper'
    }),
    connect(null, mapActionsToProps),
    withHandlers({
        onOpenItem: ({ onOpenUserDetail }) => onOpenUserDetail,
        onPage: ({ tableActions }) => page => tableActions({ newPage: page }),
        onRequestSort: ({ tableActions }) => (orderBy, order) => tableActions({ orderBy: orderBy, order: order }),
    }),
    pure
);

const UserTable = ({ classes, tableCurrentPage, wrapperId, title, tableData, tableDataSize, onOpenItem, onPage, onRequestSort, tableDataMaxPages, onClickAddUser }) => (
    <div className={classes.root}>
        <SortableFilterTable
            wrapperId={wrapperId}
            title={title}
            columns={UserTableModel.columnsData}
            data={tableData}
            dataSize={tableDataSize}
            order={tableCurrentPage.order}
            orderBy={tableCurrentPage.orderBy}
            onOpenItem={onOpenItem}
            onPage={onPage}
            onRequestSort={onRequestSort}
            currentPage={tableCurrentPage.page}
            maxPages={tableDataMaxPages}
            onClickAddItem={onClickAddUser}
            selectionable={false}
        />
    </div>
);


UserTable.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    tableCurrentPage: PropTypes.shape({}).isRequired,
    tableData: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    wrapperId: PropTypes.string,
    title: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape({})),
    tableDataSize: PropTypes.number,
    onOpenItem: PropTypes.func,
    onPage: PropTypes.func,
    onRequestSort: PropTypes.func,
    tableDataMaxPages: PropTypes.number,
    onClickAddUser: PropTypes.func,
};

export default enhance(UserTable);
