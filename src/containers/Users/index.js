import React from 'react';
import { Route, Switch } from 'react-router-dom';
import UserList from 'containers/Users/UserList';
import UserDetail from 'containers/Users/UserDetail';
import UserSummary from 'containers/Users/UserSummary';
import UserProfileDetail from 'containers/Users/UserProfileDetail';

const CreateUser = UserDetail('createUser');
const EditUser = UserDetail('editUser');

export default () => (
    <Switch>
        <Route exact path="/app/users/create" component={CreateUser} />
        <Route exact path="/app/users/:id/edit/profile" component={UserProfileDetail} />
        <Route exact path="/app/users/:id/edit" component={EditUser} />
        <Route exact path="/app/users/:id" component={UserSummary} />
        <Route exact path="/app/users" component={UserList} />
    </Switch>
);
