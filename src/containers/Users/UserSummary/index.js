import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom';
import { compose, pure, withHandlers, lifecycle } from 'recompose';
import * as UsersActions from 'redux/users/actions';
import Section from 'components/Section';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';
import State from 'helpers/state';
import UserSummaryTabs from './UserSummaryTabs';
import FormUserIdentity from './FormUserIdentity';
import FormUserProfile from './FormUserProfile';
import UserBilling from './UserBilling';
import UserTickets from './UserTickets';
import FormDocuments from './FormDocuments';
import UserPaymentDetails from './UserPaymentDetails';
import FormTroubleshoot from './FormTroubleshoot';
import FormUserTrips from './FormUserTrips';
import FormUserSystemCredits from './FormUserSystemCredits';

const styles = () => ({
    root: {
        position: 'relative',
        fontSize: 14,
    },
    hideDisplay: {
        display: 'none',
    },
    mainPaper: {
        margin: 10,
        padding: 1,
    },
    divider: {
        height: 2,
        marginLeft: 16,
        marginRight: 16,
    },
});

const mapStateToProps = state => ({
    userSummaryCurrentTab: state.users.userSummaryCurrentTab,
});

const mapActionsToProps = dispatch => ({
    onUsersActions: bindActionCreators(UsersActions, dispatch),
});

const enhance = compose(
    withRouter,
    Section('Selenium-Users-Scene'),
    withStyles(styles, 'UserSummarySection'),
    connect(
        mapStateToProps,
        mapActionsToProps
    ),
    lifecycle({
        componentDidMount() {
            this.props.onUsersActions.selectUserRequest(this.props.match.params.id);
        },
    }),
    withHandlers({
        onOpenTicketDetail: props => id =>
            props.history.push(`/app/tickets/${id}/edit`, { referer: props.history.location }),
    }),
    pure
);

const UserSummarySection = ({ classes, userSummaryCurrentTab: currentTabIndex, onOpenTicketDetail }) => (
    <div className={classes.root}>
        <Paper elevation={4} className={classes.mainPaper}>
            <FormUserIdentity />
            <Divider className={classes.divider} />
            <FormUserProfile />
        </Paper>

        <UserSummaryTabs />
        {(() => tabIndex => tabContentList => tabContentList.filter(v => v)[tabIndex])()(currentTabIndex)([
            State.isAllowed(FormTroubleshoot.aclKey) && <FormTroubleshoot />,
            State.isAllowed(UserBilling.aclKey) && <UserBilling />,
            State.isAllowed(UserTickets.aclKey) && <UserTickets onOpenTicketDetail={onOpenTicketDetail} />,
            State.isAllowed(FormDocuments.aclKey) && <FormDocuments />,
            State.isAllowed(FormUserTrips.aclKey) && <FormUserTrips />,
            State.isAllowed(UserPaymentDetails.aclKey) && <UserPaymentDetails />,
            State.isAllowed(FormUserSystemCredits.aclKey) && <FormUserSystemCredits />,
        ])}
    </div>
);

UserSummarySection.propTypes = {
    classes: PropTypes.shape().isRequired,
    userSummaryCurrentTab: PropTypes.number.isRequired,
    onOpenTicketDetail: PropTypes.func.isRequired,
};

export default enhance(UserSummarySection);
