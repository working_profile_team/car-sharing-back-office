import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import {withStyles} from 'material-ui/styles';
import SortableFilterTable from "components/Tables/SortableFilterTable";

import * as ActionsUsers from "redux/users/actions";
import Paper from 'material-ui/Paper';

const styles = {
    root: {
        flex: 1,
        height: "100%",
        margin: '10px',
    },
};

const columnData = [
    {id: 'name', button: false, disablePadding: true, label: 'Name'},
    {id: 'date', button: false, disablePadding: true, label: 'Date'},
    {id: 'type', button: false, disablePadding: false, label: 'Type'},
    {id: 'status', button: false, disablePadding: false, label: 'Status'},
];

class UserDocumentTable extends React.Component {
    constructor(props) {
        super(props);
        this.onOpenItem = this._handleOnOpenDetail.bind(this);
        this.onPage = this._handleOnPage.bind(this);
        this.onRequestSort = this._handleOnRequestSort.bind(this);
    }

    _handleOnOpenDetail(id) {
        this.props.onOpenUserDetail(id);
    }

    _handleOnPage(page) {
        this.props.onActionUser.tryChangeUserDocumentTablePage({ newPage: page });
    }

    _handleOnRequestSort(orderBy, order) {
        this.props.onActionUser.tryChangeUserDocumentTablePage({orderBy: orderBy, order: order});
    }


    render() {
        const classes = this.props.classes;
        const currentPage = this.props.userDocumentTableCurrentPage.page;

        return (
          <div className={classes.root} >
              <Paper className={classes.paper} elevation={4}>
                  <SortableFilterTable
                      title='Documents'
                      columns={columnData}
                      data={this.props.userDocumentTableCurrentPage.data}
                      totalData={this.props.userDocumentTableAllData}
                      order={this.props.userDocumentTableCurrentPage.order}
                      orderBy={this.props.userDocumentTableCurrentPage.orderBy}
                      onPage={this.onPage}
                      onRequestSort={this.onRequestSort}
                      currentPage={currentPage}
                      maxPages={this.props.userDocumentTableCurrentPage.maxPages}
                      selectionable={false}
                  />
              </Paper>
          </div>
        );

    }

}


UserDocumentTable.propTypes = {
    classes: PropTypes.object.isRequired,

};


UserDocumentTable.defaultProps = {

};


const mapStateToProps = (state) => ({
    userDocumentTableAllData: state.users.userDocumentTableAllData,
    userDocumentTableCurrentPage: state.users.userDocumentTableCurrentPage,
});

const mapActionsToProps = (dispatch) => ({
  onActionUser: bindActionCreators(ActionsUsers, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)( withStyles(styles, 'UserDocumentTable')(UserDocumentTable) );
