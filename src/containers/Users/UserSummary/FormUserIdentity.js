import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom';
import * as UsersActions from 'redux/users/actions';
import Button from 'material-ui/Button';
import Localize from 'components/Localize';
import { loggedInUserHasAdminRightsSelector } from 'selectors/user';
import { isSelectedUserAdminSelector } from 'selectors/users';
import {
    openConfirmDialog,
    closeConfirmDialog,
    updateSnackBar,
    openSnackBar,
    applicationError,
} from 'redux/application/actions';
import moment from 'moment';
import { CardHeader } from 'material-ui/Card';
import Chip from 'material-ui/Chip';
import { compose, withHandlers } from 'recompose';
import { getLastRoleUser } from 'helpers/DataHelpers/users';
import { OnOpenCreateTicket } from 'redux/trips/actions';
import { USER_GENDER_MALE, USER_GENDER_FEMALE } from 'constants/Users';
import state from 'helpers/state';
import * as aclKeys from 'constants/Acl';

const styles = () => ({
    root: {
        flex: 1,
        height: '100%',
        margin: 16,
    },
    actionsButtons: {
        width: '155px',
        margin: '5px 10px 0 0',
    },
    content: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        display: 'flex',
        alignItems: 'center',
        flex: '1 1 auto',
        maxWidth: 200,
    },
    identityHeader: {
        paddingLeft: 0,
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        flex: '1 1 auto',
        maxWidth: 200,
        minWidth: 100,
        marginBottom: '10px',
    },
    identityItemLabel: {
        color: 'rgba(0, 0, 0, 0.54)',
        fontSize: '12px',
    },
    identityItemHeader: {
        display: 'block',
        alignItems: 'center',
        marginTop: '5px',
        width: '100%',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
    },
});

const IdentityItem = ({ classes, value, label }) => (
    <div className={classes.item}>
        <span className={classes.identityItemLabel}>{label}</span>
        <div className={classes.identityItemHeader}>{value || '-'}</div>
    </div>
);

IdentityItem.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.shape()]),
    label: PropTypes.string.isRequired,
    classes: PropTypes.shape().isRequired,
};

const statusColor = {
    PENDING: '#55acee',
    APPROVED: '#09b83e',
    REJECTED: '#f1c40f',
    SUSPENDED: '#bd081c',
};

const FormUserIdentity = ({
    classes,
    accountStatus,
    firstName,
    userName,
    fleetId,
    lastName,
    birthDate,
    address,
    gender,
    locale,
    registrationDate,
    createTicket,
    resetPassword,
    onRequestEdit,
    roles,
}) => {
    const addressStr = ['streetName', 'city', 'postalCode', 'region', 'country']
        .map(v => address[v])
        .filter(v => v)
        .join(', ');

    const subheader = `${Localize.locale.users.summary.identity.createdThe}: ${moment(registrationDate).format('LLL')}`;

    const roleToDisplay = getLastRoleUser(roles);

    const genderType = () => {
        if (gender === USER_GENDER_MALE) {
            return `${Localize.locale.users.summary.identity.gender.male}`;
        } else if (gender === USER_GENDER_FEMALE) {
            return `${Localize.locale.users.summary.identity.gender.female}`;
        }
        return '';
    };

    return (
        <div className={classes.root}>
            <CardHeader
                title={Localize.locale['Forms-FormUsersIdentity']}
                subheader={subheader}
                className={classes.identityHeader}
            />
            <div className={classes.content}>
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-Status-Title']}
                    value={
                        <Chip
                            label={accountStatus}
                            style={{ backgroundColor: statusColor[accountStatus], color: 'white' }}
                        />
                    }
                />
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUsersIdentity-UserName']}
                    value={userName}
                />
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUsersIdentity-FirstName']}
                    value={firstName}
                />
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUsersIdentity-LastName']}
                    value={lastName}
                />
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUsersIdentity-Birthday']}
                    value={moment(birthDate).format('l')}
                />
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUsersIdentity-Fleet']}
                    value={fleetId}
                />
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUsersIdentity-Address']}
                    value={addressStr}
                />
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUsersIdentity-Gender']}
                    value={genderType()}
                />
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUsersIdentity-Language']}
                    value={locale}
                />
            </div>
            <div>
                <IdentityItem
                    classes={classes}
                    label={Localize.locale['form-FormUserRoles-UserRoles']}
                    value={roleToDisplay}
                />
            </div>
            {state.isAllowed(aclKeys.editUserAcl) && (
                <Button
                    raised
                    id="Selenium-FormUserIdentity-EditDetails-Button"
                    color={'primary'}
                    className={classes.actionsButtons}
                    onClick={onRequestEdit}
                >
                    {Localize.locale.actions.common.edit}
                </Button>
            )}
            {state.isAllowed(aclKeys.resetPasswordAcl) && (
                <Button
                    raised
                    id="Selenium-FormUserIdentity-ResetPassword-Button"
                    color={'primary'}
                    className={classes.actionsButtons}
                    onClick={resetPassword}
                >
                    {Localize.locale.users.resetPassword.buttonLabel}
                </Button>
            )}
            {state.isAllowed(aclKeys.createUserTicketAcl) && (
                <Button
                    raised
                    id="Selenium-TicketCreate-Button"
                    color={'primary'}
                    onClick={createTicket}
                    className={classes.actionsButtons}
                >
                    {Localize.locale.ticketing.create}
                </Button>
            )}
        </div>
    );
};

FormUserIdentity.propTypes = {
    classes: PropTypes.shape().isRequired,
    accountStatus: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    userName: PropTypes.string.isRequired,
    fleetId: PropTypes.string,
    lastName: PropTypes.string.isRequired,
    birthDate: PropTypes.string.isRequired,
    gender: PropTypes.string,
    locale: PropTypes.string,
    address: PropTypes.shape({
        streetName: PropTypes.string,
        city: PropTypes.string,
        postalCode: PropTypes.string,
        region: PropTypes.string,
        country: PropTypes.string,
    }).isRequired,
    registrationDate: PropTypes.string.isRequired,
    resetPassword: PropTypes.func.isRequired,
    onRequestEdit: PropTypes.func.isRequired,
    createTicket: PropTypes.func.isRequired,
    roles: PropTypes.arrayOf(PropTypes.string).isRequired,
};

FormUserIdentity.defaultProps = {
    address: {
        streetName: undefined,
        city: undefined,
        postalCode: undefined,
        region: undefined,
        country: undefined,
    },
};

const mapStateToProps = state => {
    const {
        id,
        accountStatus,
        firstName,
        userName,
        fleetId,
        lastName,
        birthDate,
        address,
        registrationDate,
        email,
        roles,
        gender,
        locale,
    } = state.users.selectedUser;
    return {
        id,
        accountStatus,
        firstName,
        userName,
        fleetId,
        lastName,
        birthDate,
        address,
        registrationDate,
        email,
        roles,
        gender,
        locale,
        showPromoteToAdminButton: loggedInUserHasAdminRightsSelector(state) && !isSelectedUserAdminSelector(state),
        isConfirmDialogOpen: state.application.confirmDialog.active,
    };
};

const enhanced = compose(
    withRouter,
    connect(
        mapStateToProps,
        dispatch => ({ dispatch })
    ),
    withHandlers({
        onRequestEdit: ({ id, history: { push, location } }) => () => {
            push(`/app/users/${id}/edit`, { referer: location });
        },
        createTicket: ({ id, history: { push, location }, dispatch }) => () => {
            dispatch(OnOpenCreateTicket({ selectedUserId: id }));
            push(`/app/tickets/create`, { referer: location });
        },
        openDialog: ({ dispatch }) => () => dispatch(openConfirmDialog()),
        closeDialog: ({ dispatch }) => () => dispatch(closeConfirmDialog()),
        resetPassword: ({ dispatch, email }) => async () => {
            try {
                await dispatch(UsersActions.resetPassword({ email }));
                dispatch(updateSnackBar({ message: Localize.locale.users.resetPassword.resetPasswordSuccess }));
                dispatch(openSnackBar());
            } catch (error) {
                dispatch(applicationError(Localize.locale.users.resetPassword.resetPasswordError));
            }
        },
    }),
    withStyles(styles, 'FormUserIdentity')
);

export default enhanced(FormUserIdentity);
