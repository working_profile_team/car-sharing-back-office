import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router-dom'
import SortableFilterTable from "components/Tables/SortableFilterTable";

import {UserTripTable} from "redux/users/models/UserTripTable.model"
import * as ActionsUsers from "redux/users/actions";
import {
    userTripsTableDataSelector,
    userTripsTableDataMaxPagesSelector
} from "selectors/users";

import Localize from "components/Localize";
import OnSelectTrip from "redux/trips/OnSelectTrip.workflow";

const styles = {
    root: {
        flex: 1,
        height: "100%",
        padding: "10px 0"

    },
    hideDisplay: {
        display: 'none',
    }
};


class UserTripsTable extends React.Component {

    onPage = (page) => {
        this.props.onActionUsers.tryChangeUserTripsTablePage({newPage: page});
    };

    onRequestSort = (orderBy, order) => {
        this.props.onActionUsers.tryChangeUserTripsTablePage({orderBy: orderBy, order: order});
    };

    onOpenItem = (id) => {
        this.props.history.push(`/app/trips/${id}`, {referer: this.props.history.location});
        this.props.onSelectTrip(this, {id: id});
    };

    render() {
        const classes = this.props.classes;
        const currentPage = this.props.UserTripsTableCurrentPage.page;
        return (
            <div className={classes.root}>
                <div className={this.props.open ? '' : this.props.classes.hideDisplay}>
                    <SortableFilterTable
                        title={Localize.locale.users.summary.billing.trips}
                        columns={UserTripTable.columnsData}
                        wrapperId={this.props.wrapperId}
                        data={this.props.userTripsTableData}
                        order={this.props.UserTripsTableCurrentPage.order}
                        orderBy={this.props.UserTripsTableCurrentPage.orderBy}
                        onOpenItem={this.onOpenItem}
                        onPage={this.onPage}
                        onRequestSort={this.onRequestSort}
                        currentPage={currentPage}
                        maxPages={this.props.userTripsTableDataMaxPages}
                        selectionable={false}
                    />
                </div>
            </div>
        );

    }

}


UserTripsTable.propTypes = {
    classes: PropTypes.object.isRequired,
};


UserTripsTable.defaultProps = {
    wrapperId: 'Selenium-UserTripsTable-Wrapper'
};


const mapStateToProps = (state) => ({
    userTripsTableData: userTripsTableDataSelector(state),
    userTripsTableDataMaxPages: userTripsTableDataMaxPagesSelector(state),
    UserTripsTableCurrentPage: state.users.selectedUser.userTripTableCurrentPage
});

const mapActionsToProps = (dispatch) => ({
    onActionUsers: bindActionCreators(ActionsUsers, dispatch),
    onSelectTrip: bindActionCreators(OnSelectTrip, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'UserTripsTable')(withRouter(UserTripsTable)));
