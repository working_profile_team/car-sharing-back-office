import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import OnCancelBook from 'redux/vehicles/OnCancelBook.workflow';
import OnEndTrip from 'redux/vehicles/OnEndTrip.workflow';
import Localize from 'components/Localize';
import { BOX_STATUSES_CANCELABLE_TRIP } from 'constants/Vehicles';
import Divider from 'material-ui/Divider';
import { startTripRequest, endTripRequest } from 'redux/fleet/actions';
import { lockVehicleRequest } from 'redux/vehicles/actions';
import { selectedFleetVehicleStatusNameSelector } from 'selectors/fleets';
import { lastTripStatusSelector } from 'selectors/users';
import { troubleshootAcl } from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
        overflow: 'hidden',
    }),
    row: {
        marginTop: 0,
    },
    item: {
        paddingRight: '15px',
        minWidth: 'max-content',
    },
    commands: {
        paddingRight: '15px',
        minWidth: 'max-content',
        display: 'inline-block',
    },
});

class FormTroubleshoot extends Component {
    cancelBookWorkflow = () => {
        const vehicle = this.props.selectedFleetVehicle;
        this.props.onCancelBook({
            fleetId: vehicle.fleetid.toLowerCase(),
            vehicleId: vehicle.id,
            orderId: vehicle.orderId,
            userId: vehicle.userId,
        });
    };

    endTrip = () => {
        const vehicle = this.props.selectedFleetVehicle;
        this.props.endTripRequest({
            fleetId: vehicle.fleetid.toLowerCase(),
            vehicleId: vehicle.id,
            orderId: vehicle.orderId,
            userId: vehicle.userId,
        });
    };

    startTrip = () => {
        const vehicle = this.props.selectedFleetVehicle;
        this.props.startTripRequest({
            fleetId: vehicle.fleetid.toLowerCase(),
            vehicleId: vehicle.id,
            orderId: vehicle.orderId,
            userId: vehicle.userId,
        });
    };

    lockVehicle = () => {
        const vehicle = this.props.selectedFleetVehicle;
        this.props.lockVehicleRequest({
            fleetId: vehicle.fleetid.toLowerCase(),
            vehicleId: vehicle.id,
            action: 1,
        });
    };

    unlockVehicle = () => {
        const vehicle = this.props.selectedFleetVehicle;
        this.props.lockVehicleRequest({
            fleetId: vehicle.fleetid.toLowerCase(),
            vehicleId: vehicle.id,
            action: 0,
        });
    };

    goToVehicleDetail = () => {
        const vehicle = this.props.selectedFleetVehicle;
        this.props.history.push(`/app/vehicles/${vehicle.id}`, {
            referer: this.props.location,
        });
    };

    goToTripDetail = () => {
        if (this.props.lastTripStatus === 'ON_TRIP') {
            const onGoingTrip = this.props.TripsInProgress.find(trip => trip.userId === this.props.selectedUser.id);
            this.props.history.push(`/app/trips/${onGoingTrip.orderId}`, {
                referer: this.props.location,
            });
            return;
        }
        const userLastTrip = this.props.selectedUser.userLastTrip;
        this.props.history.push(`/app/trips/${userLastTrip.tripId}`, {
            referer: this.props.location,
        });
    };

    tripButtonsRender = () => {
        const {
            selectedUser: { userLastTrip },
            selectedFleetVehicle: { userId, orderId, booking_status, box_status },
        } = this.props;

        let buttons = [];

        if (booking_status === 2) {
            if (BOX_STATUSES_CANCELABLE_TRIP.includes(box_status)) {
                buttons.push(
                    <Button
                        key="cancelBook"
                        raised
                        id="Selenium-VehicleSummary-Cancel-Button"
                        color={'primary'}
                        onClick={this.cancelBookWorkflow}
                    >
                        {Localize.locale.vehicles.summary.commands.cancelBooking}
                    </Button>
                );
            }
            if (box_status === 3) {
                const startTripButtonEnabled = userId && orderId;

                buttons.push(
                    <Button
                        key="start"
                        raised
                        id="Selenium-VehicleSummary-StartTrip-Button"
                        color={'primary'}
                        onClick={this.startTrip}
                        disabled={!startTripButtonEnabled}
                    >
                        {Localize.locale.vehicles.summary.commands.start}
                    </Button>
                );
            } else if (box_status >= 4) {
                const endTripButtonEnabled = userId && orderId;

                buttons.push(
                    <Button
                        key="end"
                        raised
                        id="Selenium-VehicleSummary-EndTrip-Button"
                        color={'primary'}
                        onClick={this.endTrip}
                        disabled={!endTripButtonEnabled}
                    >
                        {Localize.locale.vehicles.summary.commands.end}
                    </Button>
                );
            }
        }

        if (userLastTrip && userLastTrip.tripId) {
            buttons.push(
                <Button
                    key="tripDetails"
                    raised
                    id="Selenium-TripSummary-TripDetails-Button"
                    color={'primary'}
                    onClick={this.goToTripDetail}
                >
                    {Localize.locale.users.summary.commands.tripDetails}
                </Button>
            );
        }

        buttons = buttons.map(button => (
            <Grid key={button.key} item className={this.props.classes.commands}>
                {button}
            </Grid>
        ));
        return buttons;
    };

    vehicleButtonsRender = () => {
        let buttons = [];

        /*
        buttons.push(
            <Button
                key='lock'
                raised
                id="Selenium-VehicleSummary-Lock-Button"
                color={"primary"}
                onClick={this.lockVehicle}
            >
                {Localize.locale.vehicles.summary.commands.lock}
            </Button>
        );

        buttons.push(
            <Button
                key='unlock'
                raised
                id="Selenium-VehicleSummary-Unlock-Button"
                color={"primary"}
                onClick={this.unlockVehicle}
            >
                {Localize.locale.vehicles.summary.commands.unlock}
            </Button>
        ); */

        buttons.push(
            <Button
                key="vehicleDetails"
                raised
                id="Selenium-VehicleSummary-VehicleDetails-Button"
                color={'primary'}
                onClick={this.goToVehicleDetail}
            >
                {Localize.locale.vehicles.summary.commands.vehicleDetails}
            </Button>
        );

        buttons = buttons.map(button => (
            <Grid key={button.key} item className={this.props.classes.commands}>
                {button}
            </Grid>
        ));
        return buttons;
    };

    lastTripStatusLabel() {
        switch (this.props.lastTripStatus) {
            case 'BOOKED':
                return Localize.locale.vehicles.summary.troubleshootForm.booked;
            case 'ON_TRIP':
                return Localize.locale.vehicles.summary.troubleshootForm.onTrip;
            case 'COMPLETED':
                return Localize.locale.vehicles.summary.troubleshootForm.completed;
            default:
                return Localize.locale.vehicles.summary.troubleshootForm.notStartedYet;
        }
    }

    render() {
        const { classes, selectedFleetVehicle } = this.props;
        const textStatus = this.props.selectedVehicleStatusName;

        return (
            <div className={classes.root} id="Selenium-user-troubleShoot-panel">
                <Paper className={classes.paper} elevation={4}>
                    <div>
                        <Grid
                            container
                            spacing={16}
                            direction={'row'}
                            alignItems={'center'}
                            justify={'flex-start'}
                            className={classes.row}
                        >
                            <Grid item className={classes.item} xs={12}>
                                <Typography type="body1">
                                    <span style={{ color: 'rgba(0, 0, 0, 0.54)' }}>
                                        {Localize.locale.vehicles.summary.troubleshootForm.lastTripStatus}
                                    </span>
                                </Typography>

                                <span style={{ marginTop: '5px', display: 'block' }}>{this.lastTripStatusLabel()}</span>
                            </Grid>
                            <Grid item className={classes.item} xs={12}>
                                {selectedFleetVehicle.id && this.tripButtonsRender()}
                            </Grid>
                        </Grid>
                        {this.props.lastTripStatus !== 'NOT_STARTED' ? (
                            <Grid
                                container
                                spacing={16}
                                direction={'row'}
                                alignItems={'center'}
                                justify={'flex-start'}
                                className={classes.row}
                            >
                                <Grid item className={classes.item} xs={12}>
                                    <Divider />
                                </Grid>
                                <Grid item className={classes.item} xs={12}>
                                    <Typography type="body1">
                                        <span style={{ color: 'rgba(0, 0, 0, 0.54)' }}>
                                            {Localize.locale.vehicles.summary.troubleshootForm.vehicleStatus}
                                        </span>
                                    </Typography>
                                    <span style={{ marginTop: '5px', display: 'block' }}>{textStatus.state}</span>
                                </Grid>
                                <Grid item className={classes.item} xs={12}>
                                    {selectedFleetVehicle.id && this.vehicleButtonsRender()}
                                </Grid>
                            </Grid>
                        ) : (
                            ''
                        )}
                    </div>
                </Paper>
            </div>
        );
    }
}

export const aclKey = troubleshootAcl;

FormTroubleshoot.aclKey = aclKey;

FormTroubleshoot.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    TripsInProgress: PropTypes.arrayOf(PropTypes.shape({})),
    lastTripStatus: PropTypes.string.isRequired,
    selectedUser: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
    selectedFleetVehicle: state.fleet.selectedFleetVehicle,
    selectedVehicleStatusName: selectedFleetVehicleStatusNameSelector(state),
    lastTripStatus: lastTripStatusSelector(state),
    selectedUser: state.users.selectedUser,
    TripsInProgress: state.trips.allTripsInProgress,
});

const mapActionsToProps = dispatch => ({
    onCancelBook: bindActionCreators(OnCancelBook, dispatch),
    onEndTrip: bindActionCreators(OnEndTrip, dispatch),
    startTripRequest: bindActionCreators(startTripRequest, dispatch),
    endTripRequest: bindActionCreators(endTripRequest, dispatch),
    lockVehicleRequest: bindActionCreators(lockVehicleRequest, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles)(withRouter(FormTroubleshoot)));
