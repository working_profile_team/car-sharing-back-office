import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import UserSystemCredits from "./UserSystemCredits";
import Localize from 'components/Localize';
import FormUserAddSystemCredits from './FormUserAddSystemCredits';
import {
    requestAddSystemCredits,
    openFormSystemCredits,
    handleExpiryDate,
    handleValueChange,
    handleExpiryDateChange,
    } from 'redux/systemCredits/actions';
import bindActionCreators from 'redux/es/bindActionCreators';
import {
    isAddCreditFormOpenSelector,
    addCreditFormValuesSelector,
    expiryDateSwitchSelector,
    getExpiryDateSelector } from 'selectors/systemCredits';
import moment from 'moment';
import { systemCreditsAcl } from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        height: "100%",
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,

    }),
});

class FormUserSystemCredits extends React.Component {

    handleAddSystemCredits = () => {
        this.props.onOpenFormAddCredits(true)
    }

    handleDialogOpenClose = (status) => {
        this.props.onOpenFormAddCredits(status)
    }

    handleSaveSystemCredits = () => {

        const validityStartDate = moment().toISOString();
        const validityEndDate = this.props.addCreditFormValues.inputHasExpiryDate
            ? moment(this.props.addCreditFormValues.inputExpirationDate).toISOString()
            : moment().add(1, 'year').toISOString();

        this.props.requestAddSystemCredits({
            formData: {
                ...this.props.addCreditFormValues,
                validityStartDate,
                validityEndDate
            },
            userId: this.props.userId,
            successMessage: Localize.locale.users.summary.systemCredits.addCreditsForm.saveCreditsSuccessMessage,
            errorMessage: Localize.locale.users.summary.systemCredits.addCreditsForm.saveCreditsErrorMessage,
        });
    }

    handleExpiryDate = (event) => {
        this.props.handleExpiryDate(event.target.checked)
    }

    handleValueChange = (event) => {
        this.props.handleValueChange(event.target.value);
    }

    handleChangeExpiryDate = (value) => {
        this.props.handleChangeExpiryDate(value);
    }


    render() {
        const classes = this.props.classes;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={4}>
                    <Grid
                        container
                        spacing={0}
                    >
                        <Grid item xs={12}>
                            <UserSystemCredits
                                open={true}
                                classes={classes}
                            />
                        </Grid>
                        { this.props.addFormDialogOpen &&
                            <FormUserAddSystemCredits
                                handleSaveSystemCredits={this.handleSaveSystemCredits}
                                handleDialogOpenClose={this.handleDialogOpenClose}
                                isOpen={this.props.addFormDialogOpen}
                                handleExpiryDate={this.handleExpiryDate}
                                handleValueChange={this.handleValueChange}
                                expiryDateSwitch={this.props.expiryDateSwitch}
                                handleChangeExpiryDate={this.handleChangeExpiryDate}
                                currentExpiryDate={this.props.currentExpiryDate}
                                addCreditFormValues={this.props.addCreditFormValues}
                            />
                        }
                        <Grid item xs={12}>
                            <Button
                                raised
                                id="Selenium-FormUserIdentity-ResetPassword-Button"
                                color={"primary"}
                                className={classes.actionsButtons}
                                onClick={this.handleAddSystemCredits}
                            >
                                {Localize.locale.users.summary.systemCredits.buttonAddCreditsLabel}
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        )
    }

}

FormUserSystemCredits.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    addFormDialogOpen: isAddCreditFormOpenSelector(state),
    expiryDateSwitch: expiryDateSwitchSelector(state),
    currentExpiryDate: getExpiryDateSelector(state),
    addCreditFormValues: addCreditFormValuesSelector(state),
    userId: state.users.selectedUser.id,
});

const mapDispatchToProps = (dispatch) => ({
    onOpenFormAddCredits: bindActionCreators(openFormSystemCredits, dispatch),
    handleExpiryDate: bindActionCreators(handleExpiryDate, dispatch),
    handleValueChange: bindActionCreators(handleValueChange, dispatch),
    handleChangeExpiryDate: bindActionCreators(handleExpiryDateChange, dispatch),
    requestAddSystemCredits: bindActionCreators(requestAddSystemCredits, dispatch),

});

export const aclKey = systemCreditsAcl;

FormUserSystemCredits.aclKey = aclKey;

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, 'FormUserSystemCredits')(withRouter(FormUserSystemCredits)));
