import React from 'react';
import Paper from 'material-ui/Paper';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withStyles } from 'material-ui/styles';
import Tabs, { Tab } from 'material-ui/Tabs';
import Localize from 'components/Localize';
import PhonelinkRingIcon from 'material-ui-icons/PhonelinkRing';
import ReceiptIcon from 'material-ui-icons/Receipt';
import TicketIcon from 'material-ui-icons/ConfirmationNumber';
import CreditCardIcon from 'material-ui-icons/CreditCard';
import DocumentIcon from 'material-ui-icons/PictureInPicture';
import TrendingUpIcon from 'material-ui-icons/TrendingUp';
import RedeemIcon from 'material-ui-icons/Redeem';
import * as UsersActions from 'redux/users/actions';
import state from 'helpers/state';
import { troubleshootAcl, billingAcl, ticketsAcl, documentAcl, tripsAcl, payementAcl, systemCreditsAcl } from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        margin: '10px',
    },
});

class UserSummaryTabs extends React.Component {
    handleChange = (event, value) => {
        this.props.onUsersActions.setUserSummaryCurrentTab(value);
    };

    render() {
        const { classes, userSummaryCurrentTab } = this.props;
        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={4}>
                    <Tabs
                        value={userSummaryCurrentTab}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        scrollable
                    >
                        {state.isAllowed(troubleshootAcl) && (
                            <Tab
                                id="Selenium-Users-UserSummaryTabs-Tabs-Tab-troubleshoot"
                                icon={<PhonelinkRingIcon />}
                                label={Localize.locale.users.summary.tabs.troubleshoot}
                            />
                        )}
                        {state.isAllowed(billingAcl) && (
                            <Tab
                                id="Selenium-Users-UserSummaryTabs-Tabs-Tab-billing"
                                icon={<ReceiptIcon />}
                                label={Localize.locale.users.summary.tabs.billing}
                            />
                        )}
                        {state.isAllowed(ticketsAcl) && (
                            <Tab
                                id="Selenium-Users-UserSummaryTabs-Tabs-Tab-tickets"
                                icon={<TicketIcon />}
                                label={Localize.locale.users.summary.tabs.tickets}
                            />
                        )}
                        {state.isAllowed(documentAcl) && (
                            <Tab
                                id="Selenium-Users-UserSummaryTabs-Tabs-Tab-documents"
                                icon={<DocumentIcon />}
                                label={Localize.locale.users.summary.tabs.documents}
                            />
                        )}
                        {state.isAllowed(tripsAcl) && (
                            <Tab
                                id="Selenium-Users-UserSummaryTabs-Tabs-Tab-trips"
                                icon={<TrendingUpIcon />}
                                label={Localize.locale.users.summary.tabs.trips}
                            />
                        )}
                        {state.isAllowed(payementAcl) && (
                            <Tab
                                id="Selenium-Users-UserSummaryTabs-Tabs-Tab-paymentDetails"
                                icon={<CreditCardIcon />}
                                label={Localize.locale.users.summary.tabs.paymentDetails}
                            />
                        )}
                        {state.isAllowed(systemCreditsAcl) && (<Tab
                            id="Selenium-Users-UserSummaryTabs-Tabs-Tab-systemCredits"
                            icon={<RedeemIcon />}
                            label={Localize.locale.users.summary.tabs.credits}
                        />)}
                    </Tabs>
                </Paper>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userSummaryCurrentTab: state.users.userSummaryCurrentTab,
    selectedFleetVehicle: state.fleet.selectedFleetVehicle,
});

const mapActionsToProps = dispatch => ({
    onUsersActions: bindActionCreators(UsersActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles)(UserSummaryTabs));
