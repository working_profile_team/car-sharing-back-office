import React from 'react';
import PropTypes from 'prop-types';

import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router-dom'
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import UserTripsTable from "./UserTripsTable";
import { tripsAcl } from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,

    }),
});

class FormUserTrips extends React.Component {
    render() {
        const classes = this.props.classes;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={4}>
                    <Grid
                        container
                        spacing={0}
                    >
                        <Grid item xs={12}>
                            <UserTripsTable
                                open={true}
                            />
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        )
    }
}


FormUserTrips.propTypes = {
    classes: PropTypes.object.isRequired,
};

export const aclKey = tripsAcl;

FormUserTrips.aclKey = aclKey;

export default withStyles(styles, 'FormUserTrips')(withRouter(FormUserTrips));
