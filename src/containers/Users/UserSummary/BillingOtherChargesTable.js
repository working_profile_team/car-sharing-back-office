import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import {withStyles} from 'material-ui/styles';
import SortableFilterTable from "components/Tables/SortableFilterTable";

import {BillingOtherChargeTable} from "redux/users/models/BillingOtherChargeTable.model"
import * as ActionsUsers from "redux/users/actions";
import {
    billingOtherChargesTableDataSelector,
    billingOtherChargesTableDataMaxPagesSelector
} from "selectors/users";

import Localize from "components/Localize";
const styles = {
    root: {
        flex: 1,
        height: "100%",
        padding: "10px 0"

    },
    hideDisplay: {
        display: 'none',
    }
};


class BillingOtherChargesTable extends React.Component {
    constructor(props) {
        super(props);
        this.onPage = this._handleOnPage.bind(this);
        this.onRequestSort = this._handleOnRequestSort.bind(this);
    }

    _handleOnPage(page) {
        this.props.onActionUsers.tryChangeBillingOtherChargesTablePage({newPage: page});
    }

    _handleOnRequestSort(orderBy, order) {
        this.props.onActionUsers.tryChangeBillingOtherChargesTablePage({orderBy: orderBy, order: order});
    }

    render() {
        const classes = this.props.classes;
        const currentPage = this.props.billingOtherChargesTableCurrentPage.page;

        return (
            <div className={classes.root}>
                <div className={this.props.open ? '' : this.props.classes.hideDisplay}>
                    <SortableFilterTable
                        title={Localize.locale.users.summary.billing.otherCharges}
                        columns={BillingOtherChargeTable.columnsData}
                        wrapperId={this.props.wrapperId}
                        data={this.props.billingOtherChargesTableData}
                        order={this.props.billingOtherChargesTableCurrentPage.order}
                        orderBy={this.props.billingOtherChargesTableCurrentPage.orderBy}
                        onPage={this.onPage}
                        onRequestSort={this.onRequestSort}
                        currentPage={currentPage}
                        maxPages={this.props.billingOtherChargesTableDataMaxPages}
                        selectionable={false}
                        columnsDataActions={{invoiceButton: this.props.invoiceOnClick}}
                    />
                </div>
            </div>
        );

    }

}


BillingOtherChargesTable.propTypes = {
    classes: PropTypes.object.isRequired,
};


BillingOtherChargesTable.defaultProps = {
    wrapperId: 'Selenium-BillingOtherChargesTable-Wrapper'
};


const mapStateToProps = (state) => ({
    billingOtherChargesTableData: billingOtherChargesTableDataSelector(state),
    billingOtherChargesTableDataMaxPages: billingOtherChargesTableDataMaxPagesSelector(state),
    billingOtherChargesTableCurrentPage: state.users.selectedUser.billing.billingOtherChargesTableCurrentPage,
});

const mapActionsToProps = (dispatch) => ({
    onActionUsers: bindActionCreators(ActionsUsers, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'BillingOtherChargesTable')(BillingOtherChargesTable));
