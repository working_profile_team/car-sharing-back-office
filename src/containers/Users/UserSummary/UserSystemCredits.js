import React from 'react';
import PropTypes from 'prop-types';
import Localize from 'components/Localize';

import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom'
import bindActionCreators from "redux/es/bindActionCreators";

import { UserSystemCreditsTableModel } from "redux/users/models/UserSystemCredits.model";
import { SortableFilterTable } from "components/Tables/SortableFilterTable";
import { userSystemCreditsTableDataSelector } from "selectors/users";
import * as ActionsUsers from "redux/users/actions";

import { userTripsSystemCreditsDataMaxPagesSelector } from 'selectors/users';

const styles = theme => ({
    root: {
        flex: 1,
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
    }),
});


class UserSystemCredits extends React.Component {

    onPage = (page) => {
        this.props.onActionUsers.tryChangeUserSystemCreditsTablePage({newPage: page});
    };

    onRequestSort = (orderBy, order) => {
        this.props.onActionUsers.tryChangeUserSystemCreditsTablePage({orderBy: orderBy, order: order});
    };

    onOpenItem = (id) => {
    };

    render() {
        const classes = this.props.classes;

        const currentPage = this.props.userSystemCreditsTableCurrentPage.page;

        return (
            <div className={classes.root}>
                <div className={this.props.open ? 'asd' : this.props.classes.hideDisplay}>
                        <SortableFilterTable
                            classes={classes}
                            title={Localize.locale.users.summary.systemCredits.title}
                            columns={UserSystemCreditsTableModel.columnsData}
                            wrapperId={this.props.wrapperId}
                            data={this.props.userSystemCreditsTableData}
                            order={this.props.userSystemCreditsTableCurrentPage.order}
                            orderBy={this.props.userSystemCreditsTableCurrentPage.orderBy}
                            onOpenItem={this.onOpenItem}
                            onPage={this.onPage}
                            onRequestSort={this.onRequestSort}
                            currentPage={currentPage}
                            maxPages={this.props.userSystemCreditsMaxPages}
                            selectionable={false}
                        />
                </div>
            </div>
        );

    }

}


UserSystemCredits.propTypes = {
    classes: PropTypes.object.isRequired,
};


UserSystemCredits.defaultProps = {
    wrapperId: 'Selenium-UserSystemCredits-Wrapper'
};


const mapStateToProps = (state) => ({
    userSystemCreditsTableData: userSystemCreditsTableDataSelector(state),
    userSystemCreditsTableCurrentPage: state.users.selectedUser.userSystemCreditsTableCurrentPage,
    userSystemCreditsMaxPages: userTripsSystemCreditsDataMaxPagesSelector(state)
})

const mapDispatchToProps = (dispatch) => ({
    onActionUsers: bindActionCreators(ActionsUsers, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, 'UserSystemCredits')(withRouter(UserSystemCredits)));
