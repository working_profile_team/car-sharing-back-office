import React, { Component } from 'react';
import moment from 'moment';

import Dialog, { DialogTitle, DialogContent, DialogActions } from 'material-ui/Dialog';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Switch from 'material-ui/Switch';
import Grid from 'material-ui/Grid';
import FormControlLabel from 'material-ui/Form/FormControlLabel';
import { withStyles } from 'material-ui/styles';
import Localize from "components/Localize";

const styles = {
    root: {
      marginLeft: '5px',
    },
    label: {
        textTransform: 'capitalize',
        color: 'rgba(0, 0, 0, 0.54)',
        fontSize: '13px',
        paddingTop: '25px'
    },
};

class FormUserAddSystemCredits extends Component {

    constructor(props){
        super(props);

        this.state = {
            dateFieldError: ''
        }
    }

    handleClose = () => {
        this.props.handleDialogOpenClose(false);
    }
    handleSave = () => {
        this.props.handleSaveSystemCredits();
    }

    handleValueChange = (event) => {
        this.props.handleValueChange(event)
    }

    handleExpiryDate = (event) => {
        const newDate = event.target.value || null;
        if(newDate && moment(newDate).isSameOrBefore(moment().format('YYYY-MM-DD'))){
            this.setState({
                dateFieldError: 'Expiration date too old'
            })
        } else {
            if(this.state.dateFieldError !== '') {
                this.setState({
                    dateFieldError: ''
                });
            }
            this.props.handleChangeExpiryDate(newDate);
        }
    }
    checkCreditsInputValueValidity(value){
        const regex = /^([0-9]\d*)$/;
        const creditsValueCheck = regex.test(this.props.addCreditFormValues.inputCredits) && parseInt(this.props.addCreditFormValues.inputCredits, 10) !== 0;
        return creditsValueCheck && this.state.dateFieldError === '';
    }

    render(){
        const { classes } = this.props;
        return (
            <div>
                <Dialog
                    open={this.props.isOpen}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">{Localize.locale.users.summary.systemCredits.addCreditsForm.title}</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={8}>
                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={
                                        <TextField
                                            autoFocus
                                            margin="normal"
                                            id="credits"
                                            label={Localize.locale.users.summary.systemCredits.addCreditsForm.amountLabel}
                                            defaultValue={'0'}
                                            onChange={this.handleValueChange}
                                        />
                                    }
                                    classes={{
                                        root: classes.root,
                                        label: classes.label
                                    }}
                                    label={Localize.locale.users.summary.systemCredits.addCreditsForm.creditsLabel}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={
                                        <Switch
                                            checked={this.props.expiryDateSwitch}
                                            onChange={this.props.handleExpiryDate}
                                            value="checkedA"
                                        />
                                    }
                                    label={Localize.locale.users.summary.systemCredits.addCreditsForm.expirationDate}
                                />
                                <TextField
                                    id="date"
                                    label={this.state.dateFieldError !== '' ? this.state.dateFieldError : ''}
                                    type="date"
                                    defaultValue={this.props.currentExpiryDate}
                                    onChange={this.handleExpiryDate}
                                    disabled={!this.props.expiryDateSwitch}
                                    error={this.state.dateFieldError !== '' ? true : false}
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button
                            onClick={this.handleSave}
                            color="primary"
                            disabled={!this.checkCreditsInputValueValidity()}
                        >
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}
export default withStyles(styles)(FormUserAddSystemCredits);
