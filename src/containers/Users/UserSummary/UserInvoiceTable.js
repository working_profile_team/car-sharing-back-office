import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import {withStyles} from 'material-ui/styles';
import SortableFilterTable from "components/Tables/SortableFilterTable";

import * as ActionsUsers from "redux/users/actions";
import Paper from 'material-ui/Paper';

const styles = {
    root: {
        flex: 1,
        height: "100%",
        margin: '10px',
    },
};

const columnData = [
    {id: 'id', button: false, disablePadding: true, label: 'Id'},
    {id: 'date', button: false, disablePadding: true, label: 'Date'},
    {id: 'type', button: false, disablePadding: false, label: 'Type'},
    {id: 'amount', button: false, disablePadding: false, label: 'Amount'},
    {id: 'status', button: false, disablePadding: false, label: 'Status'},
];

class UserInvoiceTable extends React.Component {
    constructor(props) {
        super(props);
        this.onOpenItem = this._handleOnOpenDetail.bind(this);
        this.onPage = this._handleOnPage.bind(this);
        this.onRequestSort = this._handleOnRequestSort.bind(this);
    }

    _handleOnOpenDetail(id) {
        this.props.onOpenUserDetail(id);
    }

    _handleOnPage(page) {
        this.props.onActionUser.tryChangeUserInvoiceTablePage({ newPage: page });
    }

    _handleOnRequestSort(orderBy, order) {
        this.props.onActionUser.tryChangeUserInvoiceTablePage({orderBy: orderBy, order: order});
    }


    render() {
        const classes = this.props.classes;
        const currentPage = this.props.userInvoiceTableCurrentPage.page;

        return (
          <div className={classes.root} >
              <Paper className={classes.paper} elevation={4}>
                  <SortableFilterTable
                      title='Invoices'
                      columns={columnData}
                      data={this.props.userInvoiceTableCurrentPage.data}
                      totalData={this.props.userInvoiceTableAllData}
                      order={this.props.userInvoiceTableCurrentPage.order}
                      orderBy={this.props.userInvoiceTableCurrentPage.orderBy}
                      onPage={this.onPage}
                      onRequestSort={this.onRequestSort}
                      currentPage={currentPage}
                      maxPages={this.props.userInvoiceTableCurrentPage.maxPages}
                      selectionable={false}
                  />
              </Paper>
          </div>
        );

    }

}


UserInvoiceTable.propTypes = {
    classes: PropTypes.object.isRequired,

};


UserInvoiceTable.defaultProps = {

};


const mapStateToProps = (state) => ({
    userInvoiceTableAllData: state.users.userInvoiceTableAllData,
    userInvoiceTableCurrentPage: state.users.userInvoiceTableCurrentPage,
});

const mapActionsToProps = (dispatch) => ({
  onActionUser: bindActionCreators(ActionsUsers, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)( withStyles(styles, 'UserInvoiceTable')(UserInvoiceTable) );
