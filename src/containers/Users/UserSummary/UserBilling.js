import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import { FormControl } from 'material-ui/Form';
import Input, { InputLabel } from 'material-ui/Input';
import Select from 'material-ui/Select';
import Button from 'material-ui/Button';
import * as OnUsersActions from 'redux/users/actions';
import OnRefreshUserBilling from 'redux/users/OnRefreshUserBilling.workflow';
import FunctionHelpers from 'helpers/FunctionHelpers';
import Localize from 'components/Localize';
import Months from '../../../constants/Months';
import BillingTripsTable from './BillingTripsTable';
import BillingOtherChargesTable from './BillingOtherChargesTable';
import Years from '../../../constants/Years';
import ChargeProductPopup from './ChargeProductPopup';
import { getInvoiceThunk } from '../../../redux/users/actions';
import { billingAcl } from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        height: '100%',
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
    }),
    green: {
        color: 'green',
    },
    red: {
        color: 'red',
    },
    blueBordersContainer: {
        border: `solid 1px ${theme.palette.primary[500]}`,
        padding: '0 16px',
        color: theme.palette.primary[500],
    },
    bottomSpace: {
        marginBottom: '12px',
    },
    formColumn: {
        margin: '0',
        marginTop: '0.5em',
    },
});

class UserBilling extends React.Component {
    constructor(props) {
        super(props);
        this.listMonths = FunctionHelpers.formatSelectOptions(
            _.map(Months(Localize.locale.global.months), m => (
                <option id={`Selenium-UserBilling-MenuItem${m.value}`} value={m.value} key={m.value}>
                    {m.name}
                </option>
            )),
            'html'
        );
        this.listYears = FunctionHelpers.formatSelectOptions(
            _.map(Years, y => (
                <option id={`Selenium-UserBilling-MenuItem${y}`} value={y} key={y}>
                    {y}
                </option>
            )),
            'html'
        );
    }

    updateRange = payload => {
        this.props.onRefreshUserBilling(payload);
    };

    showChargeProductPopup(show) {
        const { openChargeProductPopup } = this.props.onUsersActions;

        openChargeProductPopup(show);
    }

    _invoiceOnClick = (event, rowId, row) => {
        this.props.getInvoice(row.invoice.id).then(data => {
            const newBlob = new Blob([data]);

            // IE doesn't allow using a blob object directly as link href
            // instead it is necessary to use msSaveOrOpenBlob
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
            }

            // For other browsers:
            // Create a link pointing to the ObjectURL containing the blob.
            const objectUrl = window.URL.createObjectURL(newBlob);
            const link = document.createElement('a');
            link.href = objectUrl;
            link.download = 'invoice.pdf';
            link.click();

            setTimeout(() => {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
            }, 100);
        });
    };

    render() {
        const { classes, currency } = this.props;
        const { openChargePopup, customerBalance, month, year } = this.props.selectedUserBilling;

        return (
            <div className={classes.root}>
                <ChargeProductPopup open={openChargePopup} />
                <Paper className={classes.paper} elevation={4}>
                    <Grid container spacing={0}>
                        <Grid item xs={12}>
                            <Typography type="headline" component="h3">
                                {Localize.locale.users.summary.billing.title}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <p>
                                {Localize.locale.users.summary.billing.customerBalance}{' '}
                                <span className={customerBalance >= 0 ? classes.green : classes.red}>
                                    {customerBalance}
                                    {currency}
                                </span>
                            </p>
                        </Grid>
                        <Grid item xs={12}>
                            <p>{Localize.locale.users.summary.billing.showActivity}</p>
                        </Grid>
                        <Grid item xs={12} className={classes.bottomSpace}>
                            <Grid container>
                                <Grid item>
                                    <FormControl>
                                        <Grid item>
                                            <div style={{ position: 'relative' }}>
                                                <InputLabel htmlFor="Selenium-UserSummary-Billing-Input-Month">
                                                    {Localize.locale.users.summary.billing.month}
                                                </InputLabel>
                                                <Select
                                                    native
                                                    classes={{
                                                        select: 'Selenium-Select',
                                                        selectMenu: 'Selenium-Select-Menu',
                                                    }}
                                                    value={month}
                                                    onChange={event => {
                                                        this.updateRange({ month: event.target.value });
                                                    }}
                                                    input={
                                                        <Input
                                                            id="Selenium-UserSummary-Billing-Input-Month"
                                                            style={{ width: '200px' }}
                                                        />
                                                    }
                                                >
                                                    {this.listMonths}
                                                </Select>
                                            </div>
                                        </Grid>
                                    </FormControl>
                                </Grid>
                                <Grid item>
                                    <FormControl>
                                        <Grid item>
                                            <div style={{ position: 'relative' }}>
                                                <InputLabel htmlFor="Selenium-UserSummary-Billing-Input-Year">
                                                    {Localize.locale.users.summary.billing.year}
                                                </InputLabel>
                                                <Select
                                                    native
                                                    classes={{
                                                        select: 'Selenium-Select',
                                                        selectMenu: 'Selenium-Select-Menu',
                                                    }}
                                                    value={year}
                                                    onChange={event => {
                                                        this.updateRange({ year: event.target.value });
                                                    }}
                                                    input={
                                                        <Input
                                                            id="Selenium-UserSummary-Billing-Input-Year"
                                                            style={{ width: '200px' }}
                                                        />
                                                    }
                                                >
                                                    {this.listYears}
                                                </Select>
                                            </div>
                                        </Grid>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <BillingTripsTable open invoiceOnClick={this._invoiceOnClick} />
                        </Grid>
                        <Grid item xs={12}>
                            <BillingOtherChargesTable open invoiceOnClick={this._invoiceOnClick} />
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container className={classes.formColumn}>
                                <Grid item style={{ paddingTop: '4px', paddingBottom: '4px' }}>
                                    <Button
                                        raised
                                        id="Selenium-UserSummary-Billing-ChargeProduct-Button"
                                        color={'primary'}
                                        onClick={() => this.showChargeProductPopup(true)}
                                    >
                                        {Localize.locale.users.summary.billing.chargeProduct}
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        );
    }
}

UserBilling.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    selectedUserBilling: state.users.selectedUser.billing,
    currency: state.application.codeCurrency,
});

const mapActionsToProps = dispatch => ({
    onUsersActions: bindActionCreators(OnUsersActions, dispatch),
    onRefreshUserBilling: bindActionCreators(OnRefreshUserBilling, dispatch),
    getInvoice(invoiceId) {
        return dispatch(getInvoiceThunk(invoiceId));
    },
});

export const aclKey = billingAcl;

UserBilling.aclKey = aclKey;

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'UserBilling')(withRouter(UserBilling)));
