import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import {withStyles} from 'material-ui/styles';
import SortableFilterTable from 'components/Tables/SortableFilterTable';
import {TicketTable} from 'redux/tickets/models/TicketTable.model'
import * as ActionsTickets from 'redux/tickets/actions';
import {OnOpenCreateTicket} from "redux/trips/actions";
import {
    ticketsByUserTableDataSelector,
    ticketsByUserTableDataMaxPagesSelector,
    ticketsTableFilterChipSelector
} from "selectors/tickets";
import Localize from 'components/Localize';
import {bindActionCreators} from "redux";
import { ticketsAcl } from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,

    }),
    green: {
        color: 'green'
    },
    red: {
        color: 'red'
    },
    blueBordersContainer: {
        border: `solid 1px ${theme.palette.primary[500]}`,
        padding: '0 16px',
        color: theme.palette.primary[500]
    },
    bottomSpace: {
        marginBottom: '12px'
    },
    formColumn: {
        margin: '0',
        marginTop: '0.5em'
    },
});

class UserTickets extends React.Component {

    componentDidMount(){
        this.props.onTicketsActions.tryFetchTicketsByUser();
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.userSelected !== this.props.userSelected) {
            this.props.onTicketsActions.tryFetchTicketsByUser();
        }
    }

    handleOnPage = (page) => {
        this.props.onTicketsActions.tryChangeTicketsTablePage({newPage: page});
    };

    handleOnRequestSort = (orderBy, order) => {
        this.props.onTicketsActions.tryChangeTicketsTablePage({orderBy: orderBy, order: order});
    };

    handleOnOpenTicketDetail = (id) => {
        this.props.onOpenTicketDetail(id);
    };

    render() {
        const classes = this.props.classes;
        const currentPage = this.props.ticketsTableCurrentPage.page;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={4}>
                    <Grid item style={{overflow: 'hidden', textAlign: 'left'}} className={classes.item}>
                        <SortableFilterTable
                            title={Localize.locale.users.summary.tickets.title}
                            columns={TicketTable.columnsData}
                            data={this.props.ticketsByUserTableData}
                            totalData={this.props.ticketsByUserTableData}
                            order={this.props.ticketsTableCurrentPage.order}
                            orderBy={this.props.ticketsTableCurrentPage.orderBy}
                            onOpenItem={this.handleOnOpenTicketDetail}
                            currentPage={currentPage}
                            maxPages={this.props.ticketsByUserTableDataMaxPages}
                            onPage={this.handleOnPage}
                            onRequestSort={this.handleOnRequestSort}
                            selectionable={false}
                            wrapperId="Selenium-TicketsTable-Wrapper"
                        />
                    </Grid>
                </Paper>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    userSelected: state.users.selectedUser.id,
    dataTickets:  state.tickets.ticketsByUser.data,
    ticketsTableAllData: state.tickets.ticketsTableAllData,
    ticketsByUserTableDataMaxPages: ticketsByUserTableDataMaxPagesSelector(state),
    ticketsTableCurrentPage: state.tickets.ticketsTableCurrentPage,
    advancedFilters: state.tickets.advancedFilters,
    advancedFiltersChips: ticketsTableFilterChipSelector(state),
    ticketsByUserTableData: ticketsByUserTableDataSelector(state),
});

const mapActionsToProps = (dispatch) => ({
    onOpenCreateTicket : bindActionCreators(OnOpenCreateTicket, dispatch),
    onTicketsActions: bindActionCreators(ActionsTickets, dispatch)
});

export const aclKey = ticketsAcl;

UserTickets.aclKey = aclKey;

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'UserTickets')(withRouter(UserTickets)));
