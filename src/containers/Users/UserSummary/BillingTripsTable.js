import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import SortableFilterTable from 'components/Tables/SortableFilterTable';
import { BillingTripTable } from 'redux/users/models/BillingTripTable.model';
import { tryChangeBillingTripsTablePage } from 'redux/users/actions';
import { billingTripsTableDataSelector, billingTripsTableDataMaxPagesSelector } from 'selectors/users';
import Localize from 'components/Localize';
import { updateInvoiceThunk } from '../../../redux/users/actions';

const styles = {
    root: {
        flex: 1,
        height: '100%',
        padding: '10px 0',
    },
    hideDisplay: {
        display: 'none',
    },
};

class BillingTripsTable extends PureComponent {
    render() {
        const {
            invoiceOnClick,
            classes,
            payOnClick,
            tripOnClick,
            onPage,
            onRequestSort,
            wrapperId,
            billingTripsTableData,
            billingTripsTableDataMaxPages,
            billingTripsTableCurrentPage,
        } = this.props;
        return (
            <div className={classes.root}>
                <SortableFilterTable
                    title={Localize.locale.users.summary.billing.trips}
                    columns={BillingTripTable.columnsData}
                    wrapperId={wrapperId}
                    data={billingTripsTableData}
                    order={billingTripsTableCurrentPage.order}
                    orderBy={billingTripsTableCurrentPage.orderBy}
                    onPage={onPage}
                    onRequestSort={onRequestSort}
                    currentPage={billingTripsTableCurrentPage.page}
                    maxPages={billingTripsTableDataMaxPages}
                    selectionable={false}
                    columnsDataActions={{
                        tripDetailsButton: tripOnClick,
                        invoiceButton: invoiceOnClick,
                        payButton: payOnClick,
                    }}
                    actionsDisplayCondition={{
                        payButton: ({ paymentStatus }) => paymentStatus === BillingTripTable.paymentStatus.REFUSED,
                        invoiceButton: ({ paymentStatus }) => paymentStatus !== BillingTripTable.paymentStatus.REFUSED,
                    }}
                />
            </div>
        );
    }
}

BillingTripsTable.propTypes = {
    classes: PropTypes.shape().isRequired,
    invoiceOnClick: PropTypes.func.isRequired,
    payOnClick: PropTypes.func.isRequired,
    onPage: PropTypes.func.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    tripOnClick: PropTypes.func.isRequired,
    billingTripsTableData: PropTypes.arrayOf(PropTypes.shape()).isRequired,
    billingTripsTableDataMaxPages: PropTypes.number.isRequired,
    billingTripsTableCurrentPage: PropTypes.shape().isRequired,
    wrapperId: PropTypes.string.isRequired,
};

BillingTripsTable.defaultProps = {
    wrapperId: 'Selenium-BillingTripsTable-Wrapper',
};

const mapStateToProps = state => ({
    billingTripsTableData: billingTripsTableDataSelector(state),
    billingTripsTableDataMaxPages: billingTripsTableDataMaxPagesSelector(state),
    billingTripsTableCurrentPage: state.users.selectedUser.billing.billingTripsTableCurrentPage,
});

const mapActionsToProps = (dispatch, ownProps) => ({
    onPage: page => dispatch(tryChangeBillingTripsTablePage({ newPage: page })),
    onRequestSort: (orderBy, order) => dispatch(tryChangeBillingTripsTablePage({ orderBy, order })),
    payOnClick: (event, rowId, row) =>
        dispatch(updateInvoiceThunk(row.invoice.id, BillingTripTable.paymentStatus.PENDING)),
    tripOnClick: (event, rowId, row) =>
        ownProps.history.push(`/app/trips/${row.id}`, { referer: ownProps.history.location }),
});

export default withRouter(
    connect(
        mapStateToProps,
        mapActionsToProps
    )(withStyles(styles, 'BillingTripsTable')(BillingTripsTable))
);
