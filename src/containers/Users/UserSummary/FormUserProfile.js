import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom';
import Button from 'material-ui/Button';
import { CardHeader } from 'material-ui/Card';
import { compose, withHandlers } from 'recompose';
import Localize from 'components/Localize';
import state from 'helpers/state';
import * as aclKeys from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        height: '100%',
        margin: 16,
    },
    actionsButtons: {
        width: '155px',
        margin: '5px 10px 0 0',
    },
    content: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        display: 'flex',
        alignItems: 'center',
        flex: '1 1 auto',
        maxWidth: 200,
    },
    identityHeader: {
        paddingLeft: 0,
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        flex: '1 1 auto',
        margin: '10px 10px 5px 0px',
    },
    profileItemLabel: {
        color: 'rgba(0, 0, 0, 0.54)',
        fontSize: '12px',
    },
    profileItemHeader: {
        display: 'flex',
        alignItems: 'center',
        marginTop: '5px',
    },
});

const ProfileItem = ({ classes, value, label }) => (
    <div className={classes.item}>
        <span className={classes.profileItemLabel}>{label}</span>
        <div className={classes.profileItemHeader}>{value || '-'}</div>
    </div>
);

const FormUserProfile = ({ onRequestEdit, email, address, profile, classes }) => {
    const {
        rfid,
        identificationNumber,
        phoneNumber,
        entity: { billingAddress },
    } = profile;

    const addressStr = ['streetName', 'city', 'postalCode', 'region', 'country']
        .map(v => (billingAddress ? billingAddress[v] : address[v]))
        .filter(v => v)
        .join(', ');

    return (
        <div className={classes.root}>
            <CardHeader title={Localize.locale['Forms-FormUserProfile']} className={classes.identityHeader} />
            <div className={classes.content}>
                <ProfileItem classes={classes} label={Localize.locale['Forms-FormUserProfile-Email']} value={email} />
                <ProfileItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUserProfile-PhoneNumber']}
                    value={phoneNumber}
                />
                <ProfileItem classes={classes} label={Localize.locale['Forms-FormUserProfile-RfID']} value={rfid} />
                <ProfileItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUserProfile-IdNumber']}
                    value={identificationNumber}
                />
                <ProfileItem
                    classes={classes}
                    label={Localize.locale['Forms-FormUserProfile-BillingAddress']}
                    value={addressStr}
                />
            </div>
            {state.isAllowed(aclKeys.editUserProfilAcl) && (
                <Button
                    raised
                    id="Selenium-FormUserProfile-EditProfileAndBillingInfo-Button"
                    color="primary"
                    className={classes.actionsButtons}
                    onClick={onRequestEdit}
                >
                    {Localize.locale.actions.common.edit}
                </Button>
            )}
        </div>
    );
};

FormUserProfile.propTypes = {
    classes: PropTypes.object.isRequired,
    onRequestEdit: PropTypes.func.isRequired,
};

FormUserProfile.defaultProps = {
    profile: {
        rfid: undefined,
        identificationNumber: undefined,
        entity: { billingAddress: undefined },
    },
};

const mapStateToProps = state => {
    const {
        email,
        address,
        profiles: [profile],
        id,
    } = state.users.selectedUser;
    return {
        id,
        email,
        address,
        profile,
    };
};

const enhanced = compose(
    withRouter,
    connect(mapStateToProps),
    withStyles(styles, 'FormUserProfile'),
    withHandlers({
        onRequestEdit: ({ id, history: { location, push } }) => () => {
            push(`/app/users/${id}/edit/profile`, { referer: location });
        },
    })
);

export default enhanced(FormUserProfile);
