import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import moment from 'moment';

import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router-dom'
import Grid from 'material-ui/Grid';
import Dialog, {DialogActions, DialogContent, DialogTitle,} from 'material-ui/Dialog';

import FunctionHelpers from "helpers/FunctionHelpers";
import * as OnUsersActions from "redux/users/actions";

import Localize from 'components/Localize'
import TextField from 'material-ui/TextField';
import Button from "material-ui/Button";

const styles = theme => ({
    root: {
        flex: 1,
        margin: '10px',
    },
    form: {
        width: '100%'
    },
    formControl: {
        width: '100%',
        marginBottom: 16
    }
});


class EditDocumentPopup extends React.Component {

    componentWillMount() {
        this.minStartDate = moment().utc().add(1, 'day').format('YYYY-MM-DD');
    }

    handleClose = () => {
        const { openEditDocumentPopup, getSelectedUserDocumentByDocumentType } = this.props.onUsersActions;
        const selectedUser = this.props.selectedUser;
        const documentType = selectedUser.editDocumentPopup.documentType;
        openEditDocumentPopup({open: false});
        getSelectedUserDocumentByDocumentType({documentType});
    };

    handleSubmit = () => {
        const selectedUser = this.props.selectedUser;
        const documentType = selectedUser.editDocumentPopup.documentType;
        if (documentType){
            this.props.onUsersActions.updateSelectedUserDocumentRequest({documentType});
        }
    }

    onChangeSelectedUserDocument = (documentData) => {
        const selectedUser = this.props.selectedUser;
        const documentType = selectedUser.editDocumentPopup.documentType;
        this.props.onUsersActions.changeSelectedUserDocument({documentType, documentData});
    };


    render() {
        const classes = this.props.classes;
        const selectedUser = this.props.selectedUser;
        let selectedUserDocument = selectedUser.documents[selectedUser.editDocumentPopup.documentType];
        if (!selectedUserDocument){
            selectedUserDocument = {};
        }

        return (
            <div className={classes.root}>
                <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    disableBackdropClick
                    disableEscapeKeyDown
                    aria-labelledby="form-dialog-title"
                >
                    <form className={classes.form} autoComplete="off">
                        <DialogTitle id="form-dialog-title">{ Localize.locale.users.summary.userDocumentForm.editDocument }</DialogTitle>
                        <DialogContent>
                            <Grid
                                container
                                spacing={8}
                            >
                                <Grid item xs={12} >
                                    <TextField
                                        id={"Selenium-FormUserIdentity-TextField-BirthDate"}
                                        label="Document Number"
                                        disabled={this.props.flowState === 'READ_ONLY'}
                                        defaultValue={FunctionHelpers.null2String(selectedUserDocument.documentNumber)}
                                        className={classnames(classes.input, 'Selenium-TextField')}
                                        fullWidth={true}
                                        onChange={event => {this.onChangeSelectedUserDocument({documentNumber: FunctionHelpers.null2String(event.target.value)})}}
                                        InputLabelProps={{
                                            shrink: true
                                        }}
                                    />
                                </Grid>
                                    <Grid item xs={12} >
                                        <TextField
                                            id={"Selenium-UserSummary-EditDocumentPopup-TextField-ExpirationDate"}
                                            type="date"
                                            label="ExpirationDate"
                                            InputLabelProps={{
                                                shrink: true
                                            }}
                                            inputProps={{
                                                min: this.minStartDate
                                            }}
                                            defaultValue={FunctionHelpers.null2Date(selectedUserDocument.expirationDate, undefined, 'YYYY-MM-DD', 'YYYY-MM-DD')}
                                            className={classnames(classes.input, 'Selenium-TextField')}
                                            fullWidth={true}
                                            required
                                            onChange={event => {this.onChangeSelectedUserDocument({expirationDate: FunctionHelpers.null2Date(event.target.value, undefined, 'YYYY-MM-DD', 'YYYY-MM-DDTHH:mm:ss.sss[Z]')})}}

                                        />
                                    </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                { Localize.locale['Forms-Cancel'] }
                            </Button>
                            <Button
                                onClick={this.handleSubmit}
                                color="primary"
                                disabled={!selectedUserDocument.documentNumber || !moment(selectedUserDocument.expirationDate).isValid()}
                            >
                                { Localize.locale.actions.common.confirm }
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </div>
        )
    }

}


EditDocumentPopup.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    selectedUser: state.users.selectedUser
});

const mapActionsToProps = (dispatch) => ({
    onUsersActions: bindActionCreators(OnUsersActions, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'EditDocumentPopup')(withRouter(EditDocumentPopup)));
