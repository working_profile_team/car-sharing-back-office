import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router-dom'
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import * as UsersActions from "redux/users/actions";
import EditDocumentPopup from "./EditDocumentPopup";
import FormDocument from "./FormDocument";
import * as _ from "lodash";
import { documentAcl } from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        height: "100%",
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,
    }),
    formItem: {
        minWidth: '220px'
    }
});

class FormDocuments extends React.Component {
    render() {
        const classes = this.props.classes;
        const selectedUser = this.props.selectedUser;


        return (
            <div className={classes.root}>
                <EditDocumentPopup open={selectedUser.editDocumentPopup.open}/>
                <Paper className={classes.paper} elevation={4}>
                    <Grid
                        container
                        spacing={0}
                    >
                        {
                            _.map(selectedUser.availableDocumentTypes, (documentType, documentTypeIndex) => (
                                <Grid item xs={6} className={classes.formItem} key={documentTypeIndex}>
                                    <FormDocument type={documentType.type}/>
                                </Grid>
                            ))

                        }
                    </Grid>
                </Paper>
            </div>
        )
    }

}

FormDocuments.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    selectedUser: state.users.selectedUser
});

const mapActionsToProps = (dispatch) => ({
    onUsersActions: bindActionCreators(UsersActions, dispatch),
});

export const aclKey = documentAcl;

FormDocuments.aclKey = aclKey;

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'FormDocuments')(withRouter(FormDocuments)));
