import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import _ from 'lodash';

import { withStyles } from 'material-ui/styles';
import { withRouter } from 'react-router-dom';
import Grid from 'material-ui/Grid';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import Select from 'material-ui/Select';
import Dialog, { DialogActions, DialogContent, DialogTitle } from 'material-ui/Dialog';
import { FormControl } from 'material-ui/Form';

import { netAmountDisplaySelector, taxAmountDisplaySelector } from 'selectors/users';
import { currency } from 'selectors/application';

import * as OnUsersActions from 'redux/users/actions';
import OnSendProductCharge from 'redux/users/OnSendProductCharge.workflow';

import Localize from 'components/Localize';
import Button from 'material-ui/Button';
import FunctionHelpers from 'helpers/FunctionHelpers';

const styles = theme => ({
    root: {
        flex: 1,
        margin: '10px',
    },
    form: {
        width: '100%',
    },
    formControl: {
        width: '100%',
        marginBottom: 16,
    },
});

class ChargeProductPopup extends React.Component {
    constructor(props) {
        super(props);

        this.listChargeAmountTypes = FunctionHelpers.formatSelectOptions(
            _.map(
                [
                    { name: Localize.locale.users.summary.billing.chargePopup.totalAmount, value: 'total' },
                    { name: Localize.locale.users.summary.billing.chargePopup.netAmount, value: 'net' },
                ],
                c => (
                    <option id={`Selenium-UserBilling-Charge-MenuItem${c.value}`} value={c.value} key={c.value}>
                        {c.name}
                    </option>
                )
            ),
            'html'
        );

        this.onProductChange = this.onProductChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onProductChange(productId) {
        const { availableChargeProducts } = this.props;
        const { updateUserChargeProduct } = this.props.onUsersActions;
        const product = _.find(availableChargeProducts, p => p.id === productId);

        updateUserChargeProduct({ product: productId, amount: product.price, tax: product.taxRate * 100 });
    }

    listProducts() {
        let availableProductList = this.props.availableChargeProducts;
        availableProductList = _.map(availableProductList, c => (
            <option id={`Selenium-UserBilling-Charge-MenuItem${c.id}`} value={c.id} key={c.id}>
                {c.name}
            </option>
        ));
        return availableProductList;
    }

    handleClose() {
        const { openChargeProductPopup } = this.props.onUsersActions;

        openChargeProductPopup(false);
    }

    handleSubmit() {
        const { onSendProductCharge } = this.props;

        onSendProductCharge();
    }

    render() {
        const classes = this.props.classes;
        const { productChargeFormData } = this.props;
        const { updateUserChargeProduct } = this.props.onUsersActions;
        const canShowAmounts = productChargeFormData.amount && productChargeFormData.tax;
        const { currency } = this.props;

        return (
            <div className={classes.root}>
                <Dialog
                    open={this.props.open}
                    onClose={this.handleClose}
                    disableBackdropClick
                    disableEscapeKeyDown
                    aria-labelledby="form-dialog-title"
                >
                    <form className={classes.form} autoComplete="off">
                        <DialogTitle id="form-dialog-title">
                            {Localize.locale.users.summary.billing.chargeProduct}
                        </DialogTitle>
                        <DialogContent>
                            <Grid container spacing={8}>
                                <Grid item xs={12}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="Selenium-FormUserIdentity-Input-Product">
                                            {Localize.locale.users.summary.billing.chargePopup.selectToBeCharged}
                                        </InputLabel>
                                        <Select
                                            native
                                            classes={{ select: 'Selenium-Select', selectMenu: 'Selenium-Select-Menu' }}
                                            value={FunctionHelpers.null2String(productChargeFormData.product)}
                                            onChange={event => {
                                                this.onProductChange(event.target.value);
                                            }}
                                            input={
                                                <Input
                                                    id="Selenium-FormUserIdentity-Input-Product"
                                                    style={{ width: '100%' }}
                                                />
                                            }
                                        >
                                            {this.listProducts()}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={5}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="Selenium-FormUserIdentity-Input-Type">
                                            {Localize.locale.users.summary.billing.chargePopup.type}
                                        </InputLabel>
                                        <Select
                                            native
                                            classes={{ select: 'Selenium-Select', selectMenu: 'Selenium-Select-Menu' }}
                                            disabled
                                            value={productChargeFormData.amountType}
                                            onChange={event => {
                                                updateUserChargeProduct({ amountType: event.target.value });
                                            }}
                                            input={
                                                <Input
                                                    id="Selenium-FormUserIdentity-Input-Type"
                                                    style={{ width: '100%' }}
                                                />
                                            }
                                        >
                                            {this.listChargeAmountTypes}
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={1} /> {/* Simple offset */}
                                <Grid item xs={3}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="Selenium-FormUserIdentity-Charge-Amount">
                                            {Localize.locale.users.summary.billing.chargePopup.amount}
                                        </InputLabel>
                                        <Input
                                            id="Selenium-FormUserIdentity-Charge-Amount"
                                            type="number"
                                            fullWidth
                                            inputProps={{
                                                min: 0,
                                            }}
                                            value={FunctionHelpers.null2String(productChargeFormData.amount)}
                                            onChange={event => {
                                                updateUserChargeProduct({ amount: event.target.value });
                                            }}
                                            startAdornment={
                                                <InputAdornment position="start">{currency}</InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </Grid>
                                <Grid item xs={1} /> {/* Simple offset */}
                                <Grid item xs={2}>
                                    <FormControl className={classes.formControl}>
                                        <InputLabel htmlFor="Selenium-FormUserIdentity-Charge-Amount">
                                            {Localize.locale.users.summary.billing.chargePopup.tax}
                                        </InputLabel>
                                        <Input
                                            id="Selenium-FormUserIdentity-Charge-Amount"
                                            type="number"
                                            disabled
                                            fullWidth
                                            inputProps={{
                                                min: 0,
                                                max: 20,
                                            }}
                                            value={productChargeFormData.tax}
                                            onChange={event => {
                                                updateUserChargeProduct({ tax: event.target.value });
                                            }}
                                            endAdornment={<InputAdornment position="end">%</InputAdornment>}
                                        />
                                    </FormControl>
                                </Grid>
                                {canShowAmounts ? (
                                    <Grid item xs={12}>
                                        <p>
                                            {Localize.locale.users.summary.billing.chargePopup.net}:{' '}
                                            {this.props.netAmountDisplay}
                                            {currency}
                                        </p>
                                        <p>
                                            {Localize.locale.users.summary.billing.chargePopup.tax}:{' '}
                                            {this.props.taxAmountDisplay}
                                            {currency}
                                        </p>
                                    </Grid>
                                ) : null}
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                {Localize.locale['Forms-Cancel']}
                            </Button>
                            <Button onClick={this.handleSubmit} color="primary">
                                {Localize.locale.actions.common.confirm}
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </div>
        );
    }
}

ChargeProductPopup.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    selectedUserBilling: state.users.selectedUser.billing,
    availableChargeProducts: FunctionHelpers.formatSelectOptions(
        state.users.selectedUser.billing.availableChargeProducts
    ),
    productChargeFormData: state.users.selectedUser.billing.productChargeFormData,
    netAmountDisplay: netAmountDisplaySelector(state),
    taxAmountDisplay: taxAmountDisplaySelector(state),
    currency: currency(state),
});

const mapActionsToProps = dispatch => ({
    onUsersActions: bindActionCreators(OnUsersActions, dispatch),
    onSendProductCharge: bindActionCreators(OnSendProductCharge, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'ChargeProductPopup')(withRouter(ChargeProductPopup)));
