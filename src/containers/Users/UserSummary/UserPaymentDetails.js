import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from "react-router-dom";

import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

import DateDisplay from 'containers/DateDisplay';
import Localize from 'components/Localize'

import {withStyles} from 'material-ui/styles';
import {bindActionCreators} from "redux";
import * as ActionsUsers from '../../../redux/users/actions';
import FunctionHelpers from 'helpers/FunctionHelpers';
import { payementAcl } from 'constants/Acl';

const styles = theme => ({
    root: {
        flex: 1,
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,

    }),
    item: {
        paddingTop: '4px',
        paddingBottom: '4px',
        overflow: 'hidden'
    },
    formItemLabel: {
        fontWeight: 'bold',
        marginRight: '1em',
    }
});

class UserPaymentDetails extends React.Component {

    componentDidMount(){
        this.props.onActionsUsers.fetchPaymentDetailsByUser();
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.selectedUserId !== this.props.selectedUserId) {
            this.props.onActionsUsers.fetchPaymentDetailsByUser();
        }
    }

    handleCardSummary = (cardSummary, notAvailable) => {
        return cardSummary ? `*****${cardSummary}` : notAvailable;
    };

    render() {
        const classes = this.props.classes;
        const userPaymentDetails = this.props.userPaymentDetails !== null || this.props.userPaymentDetails !== undefined ? this.props.userPaymentDetails : '';
        const notAvailable = Localize.locale.users.summary.paymentDetails.event.notAvailable;

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={4}>
                    <Grid container spacing={0}>

                        <Grid item xs={12}  className={classes.item}>
                            <Typography type="body1">
                                <span className={classes.formItemLabel}>
                                    {Localize.locale.users.summary.paymentDetails.cardSummary}:
                                </span>
                                { this.handleCardSummary(userPaymentDetails.cardSummary, notAvailable) }
                            </Typography>
                        </Grid>

                        <Grid item xs={12}  className={classes.item}>
                            <Typography type="body1">
                                <span className={classes.formItemLabel}>
                                    {Localize.locale.users.summary.paymentDetails.expiryDate}:
                                </span>
                                { userPaymentDetails.expiryDate ?  <DateDisplay value={userPaymentDetails.expiryDate} dateFormat="MM-YYYY" /> : notAvailable }
                            </Typography>
                        </Grid>

                        <Grid item xs={12}  className={classes.item}>
                            <Typography type="body1">
                                <span className={classes.formItemLabel}>
                                    {Localize.locale.users.summary.paymentDetails.holderName}:
                                </span>
                                { FunctionHelpers.null2String( userPaymentDetails.holderName, notAvailable ) }
                            </Typography>
                        </Grid>

                    </Grid>
                </Paper>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    selectedUserId: state.users.selectedUser.id,
    userPaymentDetails: state.users.selectedUser.paymentDetails.data
});

const mapActionsToProps = (dispatch) => ({
    onActionsUsers: bindActionCreators(ActionsUsers, dispatch)
});

export const aclKey = payementAcl;

UserPaymentDetails.aclKey = aclKey;

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'UserPaymentDetails')(withRouter(UserPaymentDetails)));
