import React from 'react';
import PropTypes from 'prop-types';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";

import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router-dom'
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import DateDisplay from 'containers/DateDisplay';
import Button from 'material-ui/Button';

import FunctionHelpers from 'helpers/FunctionHelpers';
import Localize from 'components/Localize'
import * as UsersActions from "redux/users/actions";

const styles = theme => ({
    root: {
        flex: 1,
        height: "100%",
        margin: '10px',
    },
    paper: theme.mixins.gutters({
        paddingTop: 16,
        paddingBottom: 16,

    }),
    formItemLabel: {
        fontWeight: 'bold',
        marginRight: '1em',
    },
    itemButtons: {
        paddingRight: '15px',
        minWidth: 'max-content',
        marginTop: '1em',
        marginBottom: '0.5em'
    },
    formItem: {
        paddingTop: '4px',
        paddingBottom: '4px',
        overflow: 'hidden'
    }
});


class FormDocument extends React.Component {
    showEditDocumentPopup = (show) => {
        const {openEditDocumentPopup} = this.props.onUsersActions;

        openEditDocumentPopup({documentType: this.props.type, open: show});
    };

    parseDocumentTypeName = (documentType) => {
        return documentType.replace(/([a-z](?=[A-Z]))/g, '$1 ')
    };

    render() {
        const classes = this.props.classes;
        const selectedUser = this.props.selectedUser;
        const selectedUserDocument = selectedUser.documents[this.props.type];


        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={4}>
                    <Grid
                        container
                        spacing={0}
                    >
                        {
                            (!selectedUserDocument || !selectedUserDocument.id) ?
                                <p>{Localize.locale.users.summary.userDocumentForm.thereIsNoAssociatedDocument}</p>
                                :
                                <div>
                                    <Grid item xs={12} className={classes.formItem}>
                                        <Typography type="body1">
                                <span className={classes.formItemLabel}>
                                    {Localize.locale.users.summary.userDocumentForm.documentNumber}:
                                </span>
                                            {FunctionHelpers.null2String(selectedUserDocument.documentNumber)}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} className={classes.formItem}>
                                        <Typography type="body1">
                                            <span className={classes.formItemLabel}>
                                                {Localize.locale.users.summary.userDocumentForm.expirationDate}:
                                            </span>
                                            <DateDisplay
                                                value={selectedUserDocument.expirationDate}
                                                 dateFormat="DD-MM-YYYY"
                                            />
                                        </Typography>
                                    </Grid>
                                </div>
                        }
                        <Grid
                            item
                            className={classes.itemButtons}
                            xs={12}
                        >
                            <Button
                                raised
                                id={`Selenium-UserSummary-Document-${this.props.type}-EditButton`}
                                color={"primary"}
                                onClick={() => this.showEditDocumentPopup(true)}
                            >
                                {`${Localize.locale.users.summary.userDocumentForm.edit} ${Localize.locale.users.summary.userDocumentForm.documentTypes[this.props.type]}`}
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
            </div>
        )
    }

}


FormDocument.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
    selectedUser: state.users.selectedUser
});

const mapActionsToProps = (dispatch) => ({
    onUsersActions: bindActionCreators(UsersActions, dispatch),
});

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'FormDocument')(withRouter(FormDocument)));
