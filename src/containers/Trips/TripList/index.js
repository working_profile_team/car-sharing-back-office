import { compose, pure, withHandlers, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as TripsAction from 'redux/trips/actions';
import {
    tripsTableDataForTabSelectedSelector,
    tripsTableDataMaxPagesForTabSelectedSelector,
    tripsTableDataSizeForTabSelectedSelector,
    tripsTableCurrentPageForTabSelectedSelector,
    advancedFiltersIsVisibleSelector,
    advancedFiltersSelector,
    selectedTabIndexSelector,
    columnsSelector,
    advancedFiltersChipSelector,
} from 'selectors/trips';

const mapStateToProps = state => ({
    advancedFiltersIsVisible: advancedFiltersIsVisibleSelector(state),
    selectedTabIndex: selectedTabIndexSelector(state),
    columns: columnsSelector(state),
    rows: tripsTableDataForTabSelectedSelector(state),
    currentPage: tripsTableCurrentPageForTabSelectedSelector(state).page,
    maxPage: tripsTableDataMaxPagesForTabSelectedSelector(state),
    dataSize: tripsTableDataSizeForTabSelectedSelector(state),
    order: tripsTableCurrentPageForTabSelectedSelector(state).order,
    orderBy: tripsTableCurrentPageForTabSelectedSelector(state).orderBy,
    advancedFilters: advancedFiltersSelector(state),
    advancedFiltersChips: advancedFiltersChipSelector(state),
});

const mapActionsToProps = TripsAction;

const enhance = compose(
    withRouter,
    connect(
        mapStateToProps,
        mapActionsToProps
    ),
    withHandlers({
        onOpenTripDetail: ({ history }) => id => history.push(`/app/trips/${id}`, { referer: history.location }),
        onChangeTab: ({ setSelectedTabIndex }) => (event, selectedTabIndex) => setSelectedTabIndex(selectedTabIndex),
        onPage: ({
            currentPageChange,
            fetchTripsForPage,
            tryChangeTripsInProgressTablePage,
            selectedTabIndex,
        }) => page => {
            if (selectedTabIndex === 0) {
                tryChangeTripsInProgressTablePage({ newPage: page });
            } else {
                currentPageChange({ page });
                fetchTripsForPage();
            }
        },
        onRequestSort: ({
            currentPageChange,
            fetchTripsForPage,
            tryChangeTripsInProgressTablePage,
            selectedTabIndex,
        }) => (orderBy, order) => {
            if (selectedTabIndex === 0) {
                tryChangeTripsInProgressTablePage({ orderBy, order });
            } else {
                currentPageChange({ orderBy, order });
                fetchTripsForPage();
            }
        },
        onClickAdvancedFilters: ({ setAdvancedFiltersVisible }) => () => setAdvancedFiltersVisible(true),
        onCloseFilters: ({ setAdvancedFiltersVisible }) => () => setAdvancedFiltersVisible(false),
        onClearFilters: ({ clearAdvancedFilters, fetchTripsForPage }) => () => {
            clearAdvancedFilters();
            fetchTripsForPage();
        },
        onChangeAdvancedFilters: ({ changeAdvancedFilters, fetchTripsForPage }) => field => event => {
            changeAdvancedFilters({
                type: field,
                value: event.target.value === '' ? null : event.target.value,
            });
            fetchTripsForPage();
        },
        onRemoveToolbarChip: ({ clearAdvancedFilter, fetchTripsForPage }) => ({ category }) => () => {
            clearAdvancedFilter(category);
            fetchTripsForPage();
        },
    }),
    lifecycle({
        componentDidMount() {
            this.props.fetchTripsForPage();
        },
    }),
    pure
);

export default enhance;
