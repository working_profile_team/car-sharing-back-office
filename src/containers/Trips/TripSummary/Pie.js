import React from 'react';
import PropTypes from 'prop-types';

import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Localize from 'components/Localize';

import { withStyles } from 'material-ui/styles';

import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

import * as TripsActions from 'redux/trips/actions';
import { getSelectedTripSelector } from 'selectors/trips';
import FunctionHelpers from 'helpers/FunctionHelpers';

import Chart from '../../../../node_modules/chart.js/src/chart';

const styles = theme => ({
    root: {
        width: '100%',
    },
    paper: {
        position: 'relative',
        boxShadow: 'none',
    },
    canvas: {
        maxHeight: 350,
    },
    h3: {
        color: 'rgba(0, 0, 0, .87)',
        fontSize: '1.6em',
        fontWeight: 400,
        marginBottom: 30,
    },
    grid: {
        fontSize: '16px',
    },
    legend: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    label: {
        color: '#999999',
        letterSpacing: '-0.1px',
    },
    chartBlue: {
        color: 'rgba(54, 162, 235, 0.8)',
    },
    chartYellow: {
        color: 'rgba(255, 206, 86, 0.8)',
    },
    chartPink: {
        color: 'rgba(255, 99, 132, 0.8)',
    },
});

class Pie extends React.Component {
    componentDidMount() {
        this.drawChart();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.selectedTrip.duration !== this.props.selectedTrip.duration && this.props.selectedTrip.duration) {
            this.drawChart();
        } else if (
            prevProps.selectedTrip.duration !== this.props.selectedTrip.duration &&
            !this.props.selectedTrip.duration
        ) {
            this.clearChart();
        }
    }

    clearChart() {
        if (this.myChart) {
            this.myChart.destroy();
        }
    }

    drawChart() {
        const selectedTrip = this.props.selectedTrip;
        const ctx = this.canvas.getContext('2d');

        const pauseDuration = parseFloat(selectedTrip.pauseDuration || 0).toFixed(0);
        const bookingDuration = parseFloat(selectedTrip.bookingDuration || 0).toFixed(0);
        const drivingDuration = parseFloat(selectedTrip.drivingDuration || 0).toFixed(0);

        this.myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: [
                    {
                        data: [drivingDuration, bookingDuration, pauseDuration],
                        backgroundColor: [
                            'rgba(54, 162, 235, 0.8)',
                            'rgba(255, 206, 86, 0.8)',
                            'rgba(255, 99, 132, 0.8)',
                        ],
                    },
                ],
                // These labels appear in the legend and in the tooltips when hovering different arcs
                labels: [
                    Localize.locale.trips.summary.driving,
                    Localize.locale.trips.summary.booking,
                    Localize.locale.trips.summary.pause,
                ],
            },
            options: {
                rotation: 1 * Math.PI,
                circumference: 1 * Math.PI,
                responsive: true,
                maintainAspectRatio: false,
            },
        });
    }

    render() {
        const classes = this.props.classes;
        const selectedTrip = this.props.selectedTrip;
        const { drivingDuration, pauseDuration, bookingDuration } = Localize.locale.trips.summary;
        const { min } = Localize.locale.global.units;
        return (
            <div className={classes.root} id="Selenium-TripSummary-Charts">
                <Typography type="headline" component="h3" className={classes.h3}>
                    {Localize.locale.trips.sectionTitles.durationBreakdown}
                </Typography>
                <Grid container justify="flex-start" direction="row" className={classes.grid}>
                    <Grid item className={classes.paper}>
                        <canvas ref={ref => (this.canvas = ref)} width="400" className={classes.canvas} />
                    </Grid>
                    <Grid item className={classes.legend}>
                        <div>
                            <div className={classes.label}>{drivingDuration}</div>
                            <div className={classes.chartBlue}>
                                {`${parseFloat(FunctionHelpers.null2Num(selectedTrip.drivingDuration, 0)).toFixed(0)} 
                                ${min}`}
                            </div>
                        </div>
                        <div>
                            <div className={classes.label}>{pauseDuration}</div>
                            <div className={classes.chartPink}>
                                {`${parseFloat(FunctionHelpers.null2Num(selectedTrip.pauseDuration, 0)).toFixed(0)} 
                                ${min}`}
                            </div>
                        </div>
                        <div>
                            <div className={classes.label}>{bookingDuration}</div>
                            <div className={classes.chartYellow}>
                                {`${parseFloat(FunctionHelpers.null2Num(selectedTrip.bookingDuration, 0)).toFixed(0)} 
                                ${min}`}
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

Pie.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    selectedTrip: getSelectedTripSelector(state),
});

const mapActionsToProps = dispatch => ({
    onTripsActions: bindActionCreators(TripsActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'Pie')(withRouter(Pie)));
