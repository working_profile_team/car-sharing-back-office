import React from 'react';
import PropTypes from 'prop-types';

import { withRouter, Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';

import Typography from 'material-ui/Typography';
import moment from 'moment';

import * as TripsActions from 'redux/trips/actions';
import { OnOpenCreateTicket } from 'redux/trips/actions';
import Localize from 'components/Localize';
import FunctionHelpers from 'helpers/FunctionHelpers';
import Button from 'material-ui/Button';
import { getSelectedTripSelector } from 'selectors/trips';
import Chip from 'material-ui/Chip';

import QueryBuilder from 'material-ui-icons/QueryBuilder';
import Timeline from 'material-ui-icons/Timeline';
import DirectionsCar from 'material-ui-icons/DirectionsCar';
import Person from 'material-ui-icons/Person';

import classnames from 'classnames';

const styles = theme => ({
    root: {
        width: '100%',
    },
    h3: {
        color: 'rgba(0, 0, 0, .87)',
        fontSize: '1.6em',
        fontWeight: 400,
        marginBottom: 30,
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        flex: '1 1 auto',
        minWidth: 100,
    },
    statusItemIcon: {
        color: '#9b9b9b',
        marginRight: 10,
    },
    statusItemLabel: {
        color: 'rgba(0, 0, 0, 0.54)',
        fontSize: '12px',
    },
    statusItemHeader: {
        display: 'flex',
        alignItems: 'center',
        margin: '5px',
    },
    bgBlue: {
        backgroundColor: '#2196f3',
    },
    bgGreen: {
        backgroundColor: '#5dab49',
    },
    link: {
        color: 'rgb(0, 0, 238)',
    },
    chip: {
        fontWeight: 300,
    },
    live: {
        color: '#2196f3',
        fontWeight: '700',
        fontSize: '18px',
    },
    updating: {
        color: '#adadad',
        marginLeft: '10px',
    },
    dot: {
        width: 12,
        height: 12,
        display: 'inline-block',
        borderRadius: '100%',
        backgroundColor: '#2196f3',
        marginRight: 12,
        verticalAlign: 'baseline',
    },
    btnGrid: {
        marginLeft: 'auto',
    },
});

const StatusItem = ({ classes, icon, value, label }) => (
    <Grid item className={classes.item}>
        <div className={classes.statusItemHeader}>
            {icon}
            {value}
        </div>
        <span className={classes.statusItemLabel}>{label}</span>
    </Grid>
);

const { inProgress, history } = Localize.locale.trips.types;

const tripStatus = isOngoing => (isOngoing ? inProgress : history);

class Details extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nextRefresh: '60',
        };
        this.openCreateTicket = this._handleOpenCreateTicket.bind(this);
    }

    componentWillMount() {
        this.nextRefreshInterval = setInterval(() => {
            this.setState({
                nextRefresh: moment
                    .duration(
                        moment(this.props.lastRefreshTime)
                            .add(60, 'seconds')
                            .diff(moment().clone())
                    )
                    .seconds(),
            });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.nextRefreshInterval);
    }

    _handleOpenCreateTicket() {
        this.props.history.push(`/app/tickets/create`, { referer: this.props.history.location });
        this.props.OnOpenCreateTicket({
            selectedTripId: this.props.selectedTrip.tripId,
            selectedVehicleId: this.props.selectedVehicle.id,
            selectedUserId: this.props.selectedUser.id,
        });
    }

    render() {
        const classes = this.props.classes;
        const selectedTrip = this.props.selectedTrip;
        const selectedVehicle = this.props.selectedVehicle;
        const selectedUser = this.props.selectedUser;
        const autonomyUnit = this.props.autonomyUnit;

        const { totalDuration, mileage, vehicle, user } = Localize.locale.trips.summary;

        return (
            <div className={classes.root} id="Selenium-TripSummary-Details">
                <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="flex-start"
                    classes={{ typeContainer: classes.container }}
                >
                    {selectedTrip.isOngoing && (
                        <Grid item className={classes.live}>
                            <span className={classes.dot} />
                            {Localize.locale.trips.live}
                            <span className={classes.updating}>
                                {Localize.locale.trips.updatingIn} {this.state.nextRefresh}{' '}
                                {Localize.locale.global.units.seconds}...
                            </span>
                        </Grid>
                    )}
                    {selectedTrip.isOngoing && (
                        <Grid item className={classes.btnGrid}>
                            <Button
                                id="Selenium-TicketCreate-Button"
                                color={'primary'}
                                onClick={this.openCreateTicket}
                                raised
                            >
                                {Localize.locale.ticketing.create}
                            </Button>
                        </Grid>
                    )}
                </Grid>
                <Grid container>
                    <Grid item>
                        <Typography type="headline" component="h3" className={classes.h3}>
                            {Localize.locale.trips.sectionTitles.details}
                        </Typography>
                    </Grid>

                    {!selectedTrip.isOngoing && (
                        <Grid item className={classes.btnGrid}>
                            <Button
                                id="Selenium-TicketCreate-Button"
                                color={'primary'}
                                onClick={this.openCreateTicket}
                                raised
                            >
                                {Localize.locale.ticketing.create}
                            </Button>
                        </Grid>
                    )}
                </Grid>
                <Grid container direction="row" className={classes.container}>
                    <Grid item>
                        <Chip
                            label={tripStatus(selectedTrip.isOngoing)}
                            className={classnames(
                                classes.chip,
                                selectedTrip.isOngoing ? classes.bgBlue : classes.bgGreen
                            )}
                            style={{ color: 'white' }}
                        />
                    </Grid>
                    <StatusItem
                        icon={<QueryBuilder className={classes.statusItemIcon} />}
                        classes={classes}
                        value={`${parseFloat(FunctionHelpers.null2Num(selectedTrip.duration, 0)).toFixed(0)} 
                        ${Localize.locale.global.units.min}`}
                        label={totalDuration}
                    />
                    <StatusItem
                        icon={<Timeline className={classes.statusItemIcon} />}
                        classes={classes}
                        value={`${parseFloat(FunctionHelpers.null2Num(selectedTrip.length, 0)).toFixed(2)} 
                        ${autonomyUnit}`}
                        label={mileage}
                    />
                    <StatusItem
                        icon={<DirectionsCar className={classes.statusItemIcon} />}
                        classes={classes}
                        value={
                            <Link
                                to={{
                                    pathname: `/app/vehicles/${selectedVehicle.id}`,
                                    state: { name: `Vehicle details for ${selectedVehicle.id}` },
                                }}
                                className={classes.link}
                            >
                                {`${selectedVehicle.name} - ${selectedVehicle.plate}`}
                            </Link>
                        }
                        label={vehicle}
                    />
                    <StatusItem
                        icon={<Person className={classes.statusItemIcon} />}
                        classes={classes}
                        value={
                            <Link
                                to={{
                                    pathname: `/app/users/${selectedUser.id}`,
                                    state: { name: `User details for ${selectedUser.id}` },
                                }}
                                className={classes.link}
                            >
                                {FunctionHelpers.null2String(selectedUser.userName)}
                            </Link>
                        }
                        label={user}
                    />
                </Grid>
            </div>
        );
    }
}

Details.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    selectedTrip: PropTypes.shape({ tripId: PropTypes.string }),
    selectedVehicle: PropTypes.shape({ id: PropTypes.string }),
    selectedUser: PropTypes.shape({ id: PropTypes.string }),
};

Details.defaultProps = {};

const mapStateToProps = state => ({
    selectedTrip: getSelectedTripSelector(state),
    selectedVehicle: state.vehicles.selectedVehicle,
    selectedUser: state.users.selectedUser,
    autonomyUnit: state.application.autonomyUnit,
    lastRefreshTime: state.application.lastRefresh,
});

const mapActionsToProps = dispatch => ({
    onTripsActions: bindActionCreators(TripsActions, dispatch),
    OnOpenCreateTicket: bindActionCreators(OnOpenCreateTicket, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'Details')(withRouter(Details)));
