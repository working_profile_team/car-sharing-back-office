import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Divider from 'material-ui/Divider';

import * as TripsActions from 'redux/trips/actions';
import OnSelectTrip from 'redux/trips/OnSelectTrip.workflow';
import OnUnselectTrip from 'redux/trips/OnUnselectTrip.workflow';
import { getSelectedTripSelector } from 'selectors/trips';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import Section from 'components/Section';

import DetailsCard from './Details';
import TripMap from './TripMap';
import PieCard from './Pie';
import Timelines from './Timelines';

const styles = {
    root: {
        position: 'relative',
        fontSize: 14,
        padding: '30px',
    },
    divider: {
        margin: '30px 0',
    },
};

const mapStateToProps = state => ({
    selectedTrip: getSelectedTripSelector(state),
});

const mapActionsToProps = dispatch => ({
    onTripsActions: bindActionCreators(TripsActions, dispatch),
    onSelectTrip: bindActionCreators(OnSelectTrip, dispatch),
    onUnselectTrip: bindActionCreators(OnUnselectTrip, dispatch),
});

const enhance = compose(
    withRouter,
    Section('Selenium-Trips-Scene'),
    withStyles(styles, 'TripSummary'),
    connect(
        mapStateToProps,
        mapActionsToProps
    )
);

class TripSummary extends Component {
    constructor(props) {
        super(props);
        this.onRequestClose = this._handleRequestClose.bind(this);

        this.hasRequested = false;
        this.refreshInterval = null;
    }

    componentWillMount() {
        const { selectedTrip } = this.props;
        const isOngoing =
            !selectedTrip.duration &&
            !selectedTrip.length &&
            !selectedTrip.bookingDuration &&
            !selectedTrip.pauseDuration; // We don't have this data for an ongoing trip

        if (isOngoing) {
            const self = this;
            this.refreshInterval = setInterval(() => {
                self.props.onSelectTrip(self, { id: self.props.match.params.id });
            }, 60000);
        }
    }

    componentDidMount() {
        const tripId = this.props.match.params.id;
        if (tripId && !this.hasRequested) {
            this.props.onSelectTrip(this, { id: tripId });
            this.hasRequested = true;
        }
    }

    componentDidUpdate() {
        const tripId = this.props.match.params.id;
        if (tripId && !this.hasRequested) {
            this.props.onSelectTrip(this, { id: tripId });
            this.hasRequested = true;
        }
    }

    componentWillUnmount() {
        clearInterval(this.refreshInterval);
        this.props.onUnselectTrip();
    }

    _handleRequestClose(redirect) {
        if (!redirect) {
            this.props.history.push('/app/trips', {
                referer: this.props.history.location,
            });
        }
    }

    render() {
        const classes = this.props.classes;
        const isOngoing = this.props.selectedTrip.isOngoing;

        return (
            <div className={classes.root}>
                <DetailsCard />
                <Divider className={classes.divider} />
                <TripMap />
                <Divider className={classes.divider} />
                <Timelines />
                {!isOngoing && (
                    <div>
                        <Divider className={classes.divider} />
                        <PieCard />
                    </div>
                )}
            </div>
        );
    }
}

TripSummary.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    selectedTrip: PropTypes.shape({ isOngoing: PropTypes.bool.isRequired }),
};

TripSummary.defaultProps = {
    open: false,
};

export default enhance(TripSummary);
