import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Localize from 'components/Localize';

import { withStyles } from 'material-ui/styles';

import Typography from 'material-ui/Typography';

import Chart from 'chart.js/src/chart';
import moment from 'moment';

import * as TripsActions from 'redux/trips/actions';

const styles = theme => ({
    root: {
        width: '100%',
    },
    paper: {
        position: 'relative',
        marginBottom: 30,
    },
    h3: {
        color: 'rgba(0, 0, 0, .87)',
        fontSize: '1.6em',
        fontWeight: 400,
        marginBottom: 30,
    },
    canvas: {
        maxHeight: 350,
    },
});

class Timelines extends Component {
    static propTypes = {
        classes: PropTypes.shape({}).isRequired,
        timeSeries: PropTypes.array.isRequired,
    };

    static defaultProps = {
        timeSeries: [],
    };

    componentDidUpdate(prevProps) {
        const isValidArray = Array.isArray(this.props.timeSeries);
        if (isValidArray && prevProps.timeSeries !== this.props.timeSeries) {
            this.drawChart();
        } else if (!isValidArray && prevProps.timeSeries !== this.props.timeSeries) {
            this.clearChart();
        }
    }

    componentWillUnmount() {
        this.clearChart();
    }

    colors = ['#22c3a5', '#8d4b35', '#a87b25', '#edb80d', '#fa3456', '#6f44ef'];

    clearChart = () => {
        if (this.myChart) {
            this.myChart.destroy();
        }
    };

    arrangeTimelabels = timeSeries => {
        const timeLabels = [];
        if (timeSeries && timeSeries.length > 0) {
            for (const d of timeSeries[0].data) {
                timeLabels.push(moment(d.date).format('HH:mm.ss'));
            }
        }
        return timeLabels;
    };

    arrangeDataSets = timeSeries => {
        const dataSets = [];

        if (!timeSeries) {
            return dataSets;
        }

        let i = 0;
        for (const serie of timeSeries) {
            const color = this.colors[i];
            const dataSet = {
                label: serie.name,
                yAxisID: serie.name,
                backgroundColor: color,
                borderColor: color,
                fill: false,
                pointRadius: 0,
                data: [],
            };
            for (const d of serie.data) {
                dataSet.data.push(d.value);
            }
            dataSets.push(dataSet);
            i++;
        }
        return dataSets;
    };

    arrangeYAxes = () => [
        {
            scaleLabel: { display: true, labelString: 'Autonomy' },
            position: 'left',
            id: 'autonomy',
            type: 'linear',
            ticks: { min: 0, beginAtZero: true },
        },
        {
            scaleLabel: { display: true, labelString: 'Odometer' },
            position: 'left',
            id: 'odo',
            type: 'linear',
        },
        {
            scaleLabel: { display: true, labelString: 'Speed' },
            position: 'left',
            id: 'speed',
            type: 'linear',
        },
        {
            scaleLabel: { display: true, labelString: 'Aux Battery/NB Sat/State' },
            position: 'right',
            id: 'auxBattery',
            type: 'linear',
            ticks: { min: 0, beginAtZero: true, max: 16 },
        },
        {
            display: false,
            scaleLabel: { display: false, labelString: 'NB Sat' },
            position: 'right',
            id: 'nbSat',
            type: 'linear',
            ticks: { min: 0, beginAtZero: true, max: 16 },
        },
        {
            display: false,
            scaleLabel: { display: false, labelString: 'State' },
            position: 'right',
            id: 'state',
            type: 'linear',
            ticks: { min: 0, beginAtZero: true, max: 16 },
        },
    ];

    drawChart = () => {
        const ctx = this.canvas.getContext('2d');

        const { timeSeries } = this.props;
        const labels = this.arrangeTimelabels(timeSeries);
        const data = this.arrangeDataSets(timeSeries);
        const yAxes = this.arrangeYAxes(data);

        this.myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels,
                datasets: data,
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'TimeSeries of the trip',
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true,
                },
                scales: {
                    xAxes: [
                        {
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Time (HH:mm:ss)',
                            },
                        },
                    ],
                    yAxes,
                },
            },
        });
    };

    render() {
        const classes = this.props.classes;

        return (
            <div className={classes.root} id="Selenium-TripSummary-Timelines">
                <Typography type="headline" component="h3" className={classes.h3}>
                    {Localize.locale.trips.sectionTitles.timeseries}
                </Typography>
                <div className={classes.paper}>
                    <canvas ref={ref => (this.canvas = ref)} width="400" height="400" className={classes.canvas} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    timeSeries: state.trips.selectedTripDetails.timeSeries,
});

const mapActionsToProps = dispatch => ({
    onTripsActions: bindActionCreators(TripsActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'Timelines')(withRouter(Timelines)));
