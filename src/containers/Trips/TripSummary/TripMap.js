import React from 'react';
import PropTypes from 'prop-types';

import { withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Localize from 'components/Localize';

import { withStyles } from 'material-ui/styles';

import Typography from 'material-ui/Typography';
import { LngLatBounds } from 'mapbox-gl';
import { GeoJSONLayer, Marker } from 'react-mapbox-gl';

import MapBox from 'components/Maps/Mapbox';
import * as TripsActions from 'redux/trips/actions';
import { getSelectedTripSelector } from 'selectors/trips';

import DirectionsCar from 'material-ui-icons/DirectionsCar';

import classnames from 'classnames';

const styles = theme => ({
    root: {
        width: '100%',
    },
    paper: {
        position: 'relative',
    },
    h3: {
        color: 'rgba(0, 0, 0, .87)',
        fontSize: '1.6em',
        fontWeight: 400,
        marginBottom: 30,
    },
    marker: {
        margin: 'auto',
        textAlign: 'center',
        height: '60px',
        width: '60px',
        lineHeight: '60px',
        borderRadius: '100%',
        backgroundColor: 'rgba(51, 122, 183, 0.8)',
        color: '#fff',
        fontWeight: 500,
        cursor: 'default',
    },
    greenMarker: {
        backgroundColor: 'rgba(93, 171, 73, 0.9)',
        boxShadow: '0 0 0 rgba(93, 171, 73, 0.4)',
        animation: 'pulse 2s infinite',
    },
    '@keyframes pulse': {
        '0%': {
            boxShadow: '0 0 0 0 rgba(93, 171, 73, 0.4)',
        },
        '70%': {
            boxShadow: '0 0 0 10px rgba(93, 171, 73, 0)',
        },
        '100%': {
            boxShadow: '0 0 0 0 rgba(93, 171, 73, 0)',
        },
    },
    iconCar: {
        width: 30,
        height: 30,
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
    },
});

class Pie extends React.Component {
    constructor(props) {
        super(props);

        this.getMapRef = this._getMapRef.bind(this);
    }

    _getMapRef(map) {
        const selectedTripDetails = this.props.selectedTripDetails;
        const coordinates = selectedTripDetails.itinerary.geometry.coordinates;

        this.mapRef = map;
        const bounds = coordinates.reduce(
            (bounds, coord) => bounds.extend(coord),
            new LngLatBounds(coordinates[0], coordinates[0])
        );

        this.mapRef.fitBounds(bounds, {
            padding: 40,
        });
    }

    render() {
        const classes = this.props.classes;
        const selectedTripDetails = this.props.selectedTripDetails;
        const selectedTrip = this.props.selectedTrip;
        const { start, end } = Localize.locale.trips;

        if (!selectedTripDetails || !selectedTripDetails.itinerary) {
            return null;
        }

        return (
            <div className={classes.root} id="Selenium-TripSummary-Map">
                <div className={classes.paper}>
                    <Typography type="headline" component="h3" className={classes.h3}>
                        {Localize.locale.trips.sectionTitles.location}
                    </Typography>
                    <MapBox
                        ref="map"
                        containerStyle={{
                            /*
                             Need to do this to prevent marignleft bug
                             */
                            height: '350px',
                            width: '100%',
                            // backgroundColor: "red",
                        }}
                        onMoveEnd={this.recenterMapOptions}
                        onStyleLoad={this.getMapRef}
                        center={
                            selectedTripDetails.itinerary.geometry.coordinates[
                                selectedTripDetails.itinerary.geometry.coordinates.length - 1
                            ]
                        }
                        mapbox={this.props.mapbox}
                    >
                        <Marker coordinates={selectedTripDetails.itinerary.geometry.coordinates[0]} anchor="center">
                            <div className={classes.marker}>{start}</div>
                        </Marker>

                        <Marker
                            coordinates={
                                selectedTripDetails.itinerary.geometry.coordinates[
                                    selectedTripDetails.itinerary.geometry.coordinates.length - 1
                                ]
                            }
                            anchor="center"
                        >
                            <div className={classnames([classes.marker, classes.greenMarker])}>
                                {selectedTrip.isOngoing ? <DirectionsCar className={classes.iconCar} /> : end}
                            </div>
                        </Marker>

                        <GeoJSONLayer
                            data={selectedTripDetails.itinerary}
                            linePaint={{
                                'line-color': '#3887BE',
                                'line-opacity': 0.5,
                                'line-width': 5,
                            }}
                        />
                    </MapBox>
                </div>
            </div>
        );
    }
}

Pie.propTypes = {
    classes: PropTypes.object.isRequired,
    mapCenter: PropTypes.arrayOf(PropTypes.number),
};

const mapStateToProps = state => ({
    selectedTripDetails: state.trips.selectedTripDetails,
    selectedTrip: getSelectedTripSelector(state),
    mapbox: state.application.mapbox,
    mapCenter: state.user.mapCenter,
});

const mapActionsToProps = dispatch => ({
    onTripsActions: bindActionCreators(TripsActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'Pie')(withRouter(Pie)));
