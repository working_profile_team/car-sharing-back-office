import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import TripListHOC from 'containers/Trips/TripList';
import TripListComponent from 'components/Trip/TripList';
import TripSummary from 'containers/Trips/TripSummary';
import { clearCurrentPage } from 'redux/trips/actions';

const TripList = TripListHOC(TripListComponent);

const enhance = compose(
    withRouter,
    connect(
        null,
        { clearCurrentPage }
    ),
    lifecycle({
        componentDidMount() {
            this.props.clearCurrentPage();
        },
        componentWillUnmount() {
            this.props.clearCurrentPage();
        },
    })
);

export default enhance(() => (
    <Switch>
        <Route exact path="/app/trips" component={TripList} />
        <Route exact path="/app/trips/:id" component={TripSummary} />
    </Switch>
));
