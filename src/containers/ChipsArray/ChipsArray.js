import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { withStyles } from 'material-ui/styles';
import Chip from 'material-ui/Chip';

const styles = () => ({
    chip: {
        marginRight: '8px',
        marginBottom: '8px',
    },
    row: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
});

const enhance = withStyles(styles, 'ChipsArray');

const ChipsArray = ({ classes, style, canDelete, onRequestDelete, canClick, onClick, data }) => (
    <div className={classes.row} style={style}>
        {data.map(chip => (
            <Chip
                key={chip.id}
                label={chip.name}
                id={`Selenium-Chip-${chip.id}`}
                onDelete={canDelete ? onRequestDelete(chip) : undefined}
                onClick={canClick ? onClick(chip) : undefined}
                className={classnames(classes.chip, 'Selenium-Chip')}
            />
        ))}
    </div>
);

ChipsArray.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    classes: PropTypes.shape({}).isRequired,
    style: PropTypes.shape({}),
    canDelete: PropTypes.bool,
    onRequestDelete: PropTypes.func,
    canClick: PropTypes.bool,
    onDelete: PropTypes.func,
    onClick: PropTypes.func,
};

ChipsArray.defaultProps = {
    data: [],
};

export default enhance(ChipsArray);
