import React from 'react';

import {
    Route,
    Redirect
} from 'react-router-dom';

const PrivateRoute = ({ component: Component, canUseApp, isLoading, ...rest }) => {
    return (
      <Route {...rest} render={props => (
        isLoading ?
          null :
          canUseApp ? (
           <Component {...props}/>
         ) : (
           <Redirect to={{
             pathname: '/login',
             state: { from: props.location }
           }}/>
         )
      )}/>
    )
};

export default PrivateRoute;

