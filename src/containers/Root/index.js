import React from "react";

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import OnStart from "redux/user/OnStart.workflow";

import {withRouter} from 'react-router-dom';

import {withStyles} from 'material-ui/styles';
import * as FiltersUser from "selectors/user";
import {closeModal, closeSnackBar} from "redux/application/actions";

import "./index.css"
import Realtime from "./Realtime";
import PopupLoading from "components/PopupLoading";
import FeedbackSnackBar from "components/FeedbackSnackBar";
import MainPage from 'containers/MainPage'
import {showLoadingPopupSelector} from "selectors/application";

const styles = {
    progressBar: {
        height: "10px",
    },
};

class Root extends React.Component {
    constructor(props) {
        super(props);
        this.handleCloseSnackBar = this.handleCloseSnackBar.bind(this);
        this.handleCloseLoading = this.handleCloseLoading.bind(this);
    }

    componentDidMount() {
        this.props.onStart(this);
    }

    handleCloseSnackBar() {
        this.props.onCloseSnackBar();
    }

    handleCloseLoading() {
        this.props.onCloseLoading();
    }

    render() {
        const showLoadingPopup = this.props.showLoadingPopup;
        const showSnackBar = this.props.snackBar.active;
        const isLoading = this.props.loading.active;

        return (
            <div>
                <Realtime
                    canUseApp={this.props.canUseApp}
                >
                    { showSnackBar &&
                        <FeedbackSnackBar
                            open={this.props.snackBar.active}
                            message={this.props.snackBar.message}
                            error={this.props.snackBar.error}
                            actionData={this.props.snackBar.actionData}
                            onClose={this.handleCloseSnackBar}
                        />}
                    <MainPage
                        canUseApp={this.props.canUseApp}
                        active={isLoading}
                        location={this.props.location}
                        locale={this.props.locale}
                    />
                </Realtime>

                {showLoadingPopup &&
                    <PopupLoading
                        open={true}
                        message={this.props.loading.message}
                        error={this.props.loading.error}
                        onClose={this.handleCloseLoading}
                    />}
            </div>
        )
    }

}


const mapStateToProps = (state) => ({
    showLoadingPopup: showLoadingPopupSelector(state),
    progress: state.application.progress,
    loading: state.application.loading,
    snackBar: state.application.snackBar,
    user: state.user,
    locale: state.localization.current,
    canUseApp: FiltersUser.canUseApp(state)
});


const mapActionsToProps = (dispatch) => ({
    onStart: bindActionCreators(OnStart, dispatch),
    onCloseLoading: bindActionCreators(closeModal, dispatch),
    onCloseSnackBar: bindActionCreators(closeSnackBar, dispatch)
});


export default withRouter(connect(mapStateToProps, mapActionsToProps)(withStyles(styles, 'Root')(Root)));
