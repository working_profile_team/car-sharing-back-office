import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setLastRefresh } from 'redux/application/actions';
import OnComponentDidMount from 'redux/supervisor/OnComponentDidMount.workflow';
import OnRefreshFleet from 'redux/supervisor/OnRefreshFleet.workflow';
import OnRefreshTrips from 'redux/trips/OnRefreshTrips.workflow';
import OnRefreshBookingRequests from 'redux/bookingRequests/OnRefreshBookingRequests.workflow';
import { shouldRenderAppSelector } from 'selectors/application';
import { isSBEnabledSelector } from 'selectors/services';

const styles = {};

/*
    @description: business settings for supervisor map
*/
class Realtime extends Component {
    constructor(props) {
        super(props);

        this.refreshFleetInterval = undefined;
    }

    componentDidUpdate(prevProps) {
        if (!this.props.canUseApp) {
            if (this.refreshFleetInterval !== undefined) {
                clearInterval(this.refreshFleetInterval);
            }
            return;
        }
        if (this.props.canUseApp && !prevProps.canUseApp) {
            if (this.refreshFleetInterval === undefined) {
                this.props.onComponentDidMount({});
                this.refreshInterval = setInterval(() => {
                    this.props.setLastRefresh();
                    if (!this.props.isRequestingFleet) {
                        this.props.onRefreshFleet(this, {});
                    }
                    if (!this.props.isRequestingTrips) {
                        this.props.onRefreshTrips();
                    }
                    if (this.props.isSBEnabled && !this.props.isRequestingBookingRequests) {
                        this.props.onRefreshBookingRequests();
                    }
                }, 60000);
            }
        }
    }

    componentWillUnmount() {
        if (this.refreshFleetInterval !== undefined) {
            clearInterval(this.refreshFleetInterval);
        }
    }

    render() {
        return <div>{this.props.shouldRenderApp && this.props.children}</div>;
    }
}

Realtime.propTypes = {
    classes: PropTypes.object.isRequired,
};

Realtime.defaultProps = {};

const mapStateToProps = state => ({
    shouldRenderApp: shouldRenderAppSelector(state),
    isRequestingFleet: state.fleet.isRequestingFleet,
    isRequestingTrips: state.trips.isRequestingTrips,
    isRequestingBookingRequests: state.trips.isRequestingBookingRequests,
    isSBEnabled: isSBEnabledSelector(state),
});

const mapActionsToProps = dispatch => ({
    setLastRefresh: bindActionCreators(setLastRefresh, dispatch),
    onComponentDidMount: bindActionCreators(OnComponentDidMount, dispatch),
    onRefreshFleet: bindActionCreators(OnRefreshFleet, dispatch),
    onRefreshTrips: bindActionCreators(OnRefreshTrips, dispatch),
    onRefreshBookingRequests: bindActionCreators(OnRefreshBookingRequests, dispatch),
});

export default connect(
    mapStateToProps,
    mapActionsToProps
)(withStyles(styles, 'Realtime')(Realtime));
