import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import NotFound from 'components/NotFound';
import Supervisor from 'containers/Supervisor';
import Users from 'containers/Users';
import VehicleSection from 'containers/Vehicles';
import ZoneSection from 'containers/Zones/Tabs';
import StationsSection from 'containers/Stations';

import Preferences from 'containers/Preferences';
import PromosSection from 'containers/Promos';
import PromoCreate from 'containers/Promos/PromoCreate';
import BookingRequestsSection from 'containers/BookingRequests';
import AppHeader from 'containers/AppHeader';
import AppNavigationMenu from 'components/AppNavigationMenu/index';
import { isSBEnabledSelector } from 'selectors/services';
import state from 'helpers/state';
import BookingRequestsList from 'containers/BookingRequests/BookingRequestsList';

import Trips from 'containers/Trips';
import TicketsRoutes from 'containers/Tickets';
import * as aclKeys from 'constants/Acl';
import BookingCreate from 'containers/BookingRequests/BookingCreate';

const styles = {
    root: {
        flex: 1,
        width: 'calc(100% - 180px)',
        marginLeft: '180px',
        height: '100vh',
    },
    content: {
        paddingTop: 64,
        height: '100vh',
        boxSizing: 'border-box',
    },
};

const commonPropsType = {
    match: PropTypes.shape().isRequired,
};

const VehiclesRoutes = ({ match }) => (
    <Switch>
        <Route exact path={match.url} render={props => <VehicleSection {...props} destinationRoute="vehicleList" />} />
        <Route
            exact
            path={`${match.url}/create`}
            render={props => <VehicleSection {...props} destinationRoute="createVehicle" />}
        />

        <Route
            exact
            path={`${match.url}/:vehicleId/edit`}
            render={props => <VehicleSection {...props} destinationRoute="editVehicle" />}
        />

        <Route
            exact
            path={`${match.url}/:vehicleId`}
            render={props => <VehicleSection {...props} destinationRoute="vehicleSummary" />}
        />
    </Switch>
);

VehiclesRoutes.propTypes = commonPropsType;

const PromosRoutes = ({ match }) => (
    <Switch>
        <Route exact path={match.url} render={props => <PromosSection {...props} destinationRoute="list" />} />
        <Route exact path={`${match.url}/create`} render={props => <PromoCreate {...props} />} />
        <Route exact path={`${match.url}/:promoId/edit`} render={props => <PromoCreate {...props} />} />
    </Switch>
);

PromosRoutes.propTypes = commonPropsType;

const ZonesRoutes = ({ match }) => (
    <Switch>
        <Route exact path={match.url} component={ZoneSection} />
        <Route exact path={`${match.url}/:selectedZoneId`} component={ZoneSection} />
    </Switch>
);

ZonesRoutes.propTypes = commonPropsType;

const BookingRequestsRoutes = ({ match }) => (
    <Switch>
        <Route exact path="/app/booking-requests/create" component={BookingCreate} />
        <Route exact path={match.url} component={BookingRequestsList} />
        <Route path={`${match.url}/:id`} component={BookingRequestsSection} />
    </Switch>
);

BookingRequestsRoutes.propTypes = commonPropsType;

const StationsRoutes = ({ match }) => (
    <Switch>
        <Route exact path={`${match.url}/list`} component={StationsSection} />
        <Route path={`${match.url}/list/:id`} component={StationsSection} />
    </Switch>
);

StationsRoutes.propTypes = commonPropsType;

class Home extends React.Component {
    componentDidUpdate(prevProps) {
        const { location } = this.props;
        if (location.pathname !== prevProps.location.pathname) {
            window.scrollTo(0, 0);
        }
    }

    render() {
        const { classes, isSBEnabled } = this.props;

        return (
            <div className={classes.root}>
                <AppHeader />
                <AppNavigationMenu isSBEnabled={isSBEnabled} />
                <div className={classes.content}>
                    <Switch>
                        <Route exact path="/" render={() => <Redirect to="/app/supervision" />} />
                        <Route exact path="/" component={Supervisor} />
                        <Route
                            exact
                            path="/app/vuboxes-to-integrate/:vuboxId/assign-to-vehicle"
                            render={props => <VehicleSection {...props} destinationRoute="vuboxToIntegrateAssign" />}
                        />
                        <Route
                            exact
                            path="/app/vuboxes-to-integrate/:vuboxId"
                            render={props => <VehicleSection {...props} destinationRoute="vuboxToIntegrateSummary" />}
                        />
                        <Route exact path="/app/preferences" component={Preferences} />
                        {state.isAllowed(aclKeys.pageSupervisionAcl) && (
                            <Route path="/app/supervision" component={Supervisor} />
                        )}
                        {state.isAllowed(aclKeys.pageVehicleAcl) && (
                            <Route path="/app/vehicles" component={VehiclesRoutes} />
                        )}
                        {state.isAllowed(aclKeys.pageUsersAcl) && <Route path="/app/users" component={Users} />}
                        {state.isAllowed(aclKeys.pageTripsAcl) && <Route path="/app/trips" component={Trips} />}
                        {state.isAllowed(aclKeys.pagePromosAcl) && (
                            <Route path="/app/promos" component={PromosRoutes} />
                        )}
                        {state.isAllowed(aclKeys.pageTicketsAcl) && (
                            <Route path="/app/tickets" component={TicketsRoutes} />
                        )}
                        {!isSBEnabled &&
                            state.isAllowed(aclKeys.pageZonesAcl) && (
                                <Route path="/app/zones" component={ZonesRoutes} />
                            )}
                        {isSBEnabled &&
                            state.isAllowed(aclKeys.pageBookingAcl) && (
                                <Route path="/app/booking-requests" component={BookingRequestsRoutes} />
                            )}
                        {isSBEnabled &&
                            state.isAllowed(aclKeys.pageStationsAcl) && (
                                <Route path="/app/stations" component={StationsRoutes} />
                            )}
                        <Route component={NotFound} />
                    </Switch>
                </div>
            </div>
        );
    }
}

Home.propTypes = {
    classes: PropTypes.shape({}).isRequired,
    isSBEnabled: PropTypes.bool.isRequired,
    location: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
    isSBEnabled: isSBEnabledSelector(state),
});

export default connect(mapStateToProps)(withStyles(styles, 'Home')(Home));

