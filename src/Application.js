import React, { PureComponent } from 'react';
import Root from 'containers/Root';
import { BrowserRouter } from 'react-router-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import { Provider } from 'react-redux';
import { MaterialHues } from 'constants/Colors';
import getClientConfig from 'helpers/client';
import state from 'helpers/state';
import configureStore from './redux';

const rights = require('./resources/rights.json');

state.setRights(rights);

const theme = createMuiTheme({
    palette: {
        primary: MaterialHues.VulogBlue,
        secondary: {
            main: '#B2B2B2',
        },
        danger: {
            main: '#F44336',
        },
    },
});

class Application extends PureComponent {
    state = {
        isLoading: true,
    };

    componentDidMount() {
        if (process.env.NODE_ENV !== 'development') {
            this.loadConf();
        } else if (process.env.NODE_ENV === 'development' && sessionStorage.getItem('selectedHost')) {
            this.loadConf(sessionStorage.getItem('selectedHost'));
        }
    }

    getClients = () => {
        if (process.env.NODE_ENV === 'development' && !sessionStorage.getItem('selectedHost')) {
            return require('./host').clients;
        }

        return [];
    };

    async loadConf(hostName) {
        try {
            const conf = await getClientConfig(hostName);
            this.store = configureStore(conf.data, {});
            window.document.title = `${conf.data.title} - BackOffice`;
            this.setState({ isLoading: false });
        } catch (e) {
            console.error(e);
        }
    }

    selectHost = (event) => {
        const host = event.target.value;
        sessionStorage.setItem('selectedHost', host);
        this.loadConf(host);
    };

    store = null;

    render() {
        let select =  <div>
            <label>Environment:</label>
            <select onChange={this.selectHost}>
            {this.getClients().map(client => (
                <option
                    key={client}
                    value={client}
                    style={{
                        padding: 10,
                    }}
                >{client}</option>
            ))}
        </select></div>;

        return this.state.isLoading ? (
            <div>
                {process.env.NODE_ENV === 'development' && select}
            </div>
        ) : (
            <div
                style={{
                    width: '100vw',
                    height: '100vh',
                    maxWidth: '100%',
                    backgroundColor: 'transparent',
                }}
            >
                <Provider store={this.store}>
                    <MuiThemeProvider theme={theme}>
                        <BrowserRouter>
                            <Root />
                        </BrowserRouter>
                    </MuiThemeProvider>
                </Provider>
            </div>
        );
    }
}

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

export default Application;
